/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef SGDCHUNKEDEDGE_H_
#define SGDCHUNKEDEDGE_H_
#include <random>
#define SGD_RANDOMIZE_EDGE 1
#define OPTIMIZE_COPYING_CHUNK 1
/////////////////////////////////////////////////////////
/************************************************************************
 * Chunked executor.
 ************************************************************************/
using namespace Galois::OpenCL;
struct SGDChunkedEdgeFunctor {
   typedef SGD_LC_LinearArray_Undirected_Graph<Galois::CPUArray, unsigned int, unsigned int> GraphTy;
   typedef Array<cl_int> ArrayType;
   typedef GPUArray<cl_int> GPUArrayType;
   typedef cl_float FeatureType;
   typedef Array<FeatureType> FeatureArrayType;
   typedef GPUArray<FeatureType> GPUFeatureArrayType;
   typedef CL_Kernel Kernel;
   //////////////////////////////
   struct ChunkInfo {
      int chunk_size;
      cl_int * edge_info;
      FeatureType * rating;
      cl_int * edge_id;
      void allocate(size_t size) {
         chunk_size = size;
         edge_info = new cl_int[2 * size];
         rating = new FeatureType[size];
         edge_id = new cl_int[size];
      }
      void deallocate() {
         delete[] edge_info;
         delete[] rating;
         delete[] edge_id;
      }
   };
   //////////////////////////////
   const int NUM_CHUNKS;
   const int NUM_BUFFERS;
   GraphTy graph;
   std::vector<int> movies;
   ArrayType * metadata;
   ArrayType * locks;
   FeatureArrayType * features;
#if OPTIMIZE_COPYING_CHUNK
   GPUArrayType * edge_wl;
   GPUArrayType * edge_info;
   GPUFeatureArrayType * ratings;
#else
   ArrayType * edge_wl;
   ArrayType * edge_info;
   FeatureArrayType * ratings;
#endif

   std::vector<int> **all_edges;
   ChunkInfo ** chunk_info;
   Kernel sgd_kernel;
   int num_diagonals;
   float accumulated_error;
   int round;
   unsigned int max_rating;
   unsigned int gpu_wl_size;
   /************************************************************************
    *
    ************************************************************************/
   SGDChunkedEdgeFunctor(const char * filename) :
         //NUM_CHUNKS(ceil(4918636/1024)) {
      NUM_CHUNKS(16),NUM_BUFFERS(8) {
      round = 0;
      fprintf(stderr, "Creating SGDChunkedEdgeFunctor -  features =[%d], Chunks=[%d] .\n", SGD_FEATURE_SIZE, NUM_CHUNKS);
      std::cout << "Checking sizes: int: " << sizeof(int) << ", cl_int: " << sizeof(cl_int) << ", float: " << sizeof(float) << " ,cl_float: " << sizeof(cl_float) << "\n";
      graph.read(filename);
      long dev_memory = Galois::OpenCL::OpenCL_Setup::get_device_memory();
      long min_chunk_size = ((float)(dev_memory-graph.num_nodes()*65*4)/(float)(16*NUM_BUFFERS));
      std::cout<<"min chunk_size " <<  min_chunk_size << ", num_chunks :: " << ceil(graph.num_edges()/(float)min_chunk_size) << "\n";
      allocate();
      initialize_features_random();
      std::cout << "Number of movies found :: " << movies.size() << "\n";
      sgd_kernel.init("sgd_edge_operator.cl", "sgd_edge_operator");
      sgd_kernel.set_arg_list(edge_info, locks, edge_wl, metadata, features, ratings);
      if (false) {
         std::ofstream header("graph_header.h");
         header << Galois::OpenCL::str_SGD_LC_LinearArray_Undirected_Graph << "\n";
         header.close();
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   void allocate() {
      features = new FeatureArrayType(graph.num_nodes() * SGD_FEATURE_SIZE);
      fprintf(stderr, "Size of edges :: %6.6g MB\n", (float) (graph.num_edges() * sizeof(unsigned int) / (1024 * 1024)));
      metadata = new ArrayType(16);
      locks = new ArrayType(graph.num_nodes());
      all_edges = new std::vector<int>*[NUM_CHUNKS];
      gpu_wl_size = ceil(graph.num_edges() / NUM_CHUNKS);
      for (int i = 0; i < NUM_CHUNKS; ++i){
         all_edges[i] = new std::vector<int>();
      }
#if OPTIMIZE_COPYING_CHUNK
      edge_wl = new GPUArrayType(gpu_wl_size);
      edge_info = new GPUArrayType(2 * gpu_wl_size);
      ratings = new GPUFeatureArrayType(gpu_wl_size);
#else
      edge_wl = new ArrayType(gpu_wl_size);
      edge_info = new ArrayType(2 * gpu_wl_size);
      ratings = new FeatureArrayType(gpu_wl_size);
#endif
      chunk_info = new ChunkInfo *[NUM_CHUNKS];
   }
   /************************************************************************
    *
    ************************************************************************/
   void deallocate() {
      for (int i = 0; i < NUM_CHUNKS; ++i) {
         chunk_info[i]->deallocate();
         delete chunk_info[i];
         delete all_edges[i];
      }
      delete[] all_edges;
      delete features;
      delete edge_info;
      delete edge_wl;
      delete metadata;
      delete locks;
      delete[] chunk_info;
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_device() {
      features->copy_to_device();
      locks->copy_to_device();
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_host() {
      features->copy_to_host();
      locks->copy_to_host();
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_chunk_to_host() {
      metadata->copy_to_host();
#if OPTIMIZE_COPYING_CHUNK
#else
      edge_info->copy_to_host();
      edge_wl->copy_to_host();
      ratings->copy_to_host();
#endif
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_chunk_to_device() {
      metadata->copy_to_device();
#if OPTIMIZE_COPYING_CHUNK
#else
      edge_info->copy_to_device();
      edge_wl->copy_to_device();
      ratings->copy_to_device();
#endif
   }
   /************************************************************************
    *
    ************************************************************************/
   void populate_worklist(int chunk_id) {
      {
         metadata->host_ptr()[0] = chunk_info[chunk_id]->chunk_size;
         metadata->host_ptr()[1] = round;
         metadata->host_ptr()[2] = graph.num_edges();
         float t = SGDCommon::SGD_STEP_SIZE(round);
         metadata->host_ptr()[3] = (t);
         metadata->host_ptr()[4] = chunk_info[chunk_id]->chunk_size;
         sgd_kernel.set_arg(6, sizeof(cl_float), &t);
         sgd_kernel.set_work_size(chunk_info[chunk_id]->chunk_size);
      }
#if OPTIMIZE_COPYING_CHUNK
  //    fprintf(stderr, "%d chunk :: size :: %d , ptr %d, host_0 :%d, \n", chunk_id, chunk_info[chunk_id]->chunk_size, chunk_info[chunk_id]->edge_info, metadata->host_ptr()[0]);
      edge_info->copy_to_device(chunk_info[chunk_id]->edge_info);
      ratings->copy_to_device(chunk_info[chunk_id]->rating);
      edge_wl->copy_to_device(chunk_info[chunk_id]->edge_id);
      /*Galois::OpenCL::CHECK_CL_ERROR(clEnqueueWriteBuffer(Galois::OpenCL::OpenCL_Setup::queue, edge_info->device_data, CL_TRUE, 0, sizeof(int)*chunk_info[chunk_id]->chunk_size*2, (void *)chunk_info[chunk_id]->edge_info,0 ,0,0),"Edges copy failed");
      Galois::OpenCL::CHECK_CL_ERROR(clEnqueueWriteBuffer(Galois::OpenCL::OpenCL_Setup::queue, ratings->device_data, CL_TRUE, 0, sizeof(FeatureType)*chunk_info[chunk_id]->chunk_size,(void *) chunk_info[chunk_id]->rating,0 ,0,0),"Ratings copy failed");
      Galois::OpenCL::CHECK_CL_ERROR(clEnqueueWriteBuffer(Galois::OpenCL::OpenCL_Setup::queue, edge_wl->device_data, CL_TRUE, 0, sizeof(int)*chunk_info[chunk_id]->chunk_size, (void *)chunk_info[chunk_id]->edge_id,0 ,0,0),"Edge-id copy failed");*/
/*      int * tmp = new int [chunk_info[chunk_id]->chunk_size];
      Galois::OpenCL::CHECK_CL_ERROR(clEnqueueReadBuffer(Galois::OpenCL::OpenCL_Setup::queue, edge_wl->device_data, CL_TRUE, 0, sizeof(int)*chunk_info[chunk_id]->chunk_size, (void *)tmp,0 ,0,0),"Edges copy failed");
      for(size_t i = 0; i < chunk_info[chunk_id]->chunk_size; ++i){
         fprintf(stderr, "[%d,%d,%d,%3.3g]", chunk_info[chunk_id]->edge_id[i],chunk_info[chunk_id]->edge_info[2*i],chunk_info[chunk_id]->edge_info[2*i+1],chunk_info[chunk_id]->rating[i]);
         assert(tmp[i]>=0);
         assert(chunk_info[chunk_id]->edge_id[i]>=0);
         assert(chunk_info[chunk_id]->edge_info[i]>=0);
         assert(chunk_info[chunk_id]->edge_info[2*i]>=0);
         assert(chunk_info[chunk_id]->rating[2*i]>=0);
      }
      delete tmp;*/
      metadata->copy_to_device();
#else
      memcpy(edge_info->host_ptr(), chunk_info[chunk_id]->edge_info, sizeof(int) * chunk_info[chunk_id]->chunk_size * 2);
      memcpy(ratings->host_ptr(), chunk_info[chunk_id]->rating, sizeof(FeatureType) * chunk_info[chunk_id]->chunk_size);
      memcpy(edge_wl->host_ptr(), chunk_info[chunk_id]->edge_id, sizeof(int) * chunk_info[chunk_id]->chunk_size);
      copy_chunk_to_device();
#endif
   }
   /************************************************************************
    *
    ************************************************************************/
   void initialize_features_random() {
      Galois::OpenCL::Timer timer;
      timer.start();
      SGDCommon::initialize_features_random(graph, features, locks, movies);
      max_rating = 0;
      for (unsigned int i = 0; i < graph.num_edges(); ++i) {
         max_rating = std::max(max_rating, graph.out_edge_data()[i]);
      }
      const int items_per_chunk = ceil(graph.num_edges() / NUM_CHUNKS);
      std::cout <<" , max_Rating: " << max_rating << ", Chunk-size: " << items_per_chunk
            << ", movies: " << movies.size() << "\n";
      if (false) {
         std::ofstream header("/workspace/rashid/netflix_deg.csv");
         std::vector<int> edge_count;
         for (unsigned int it = 0; it < movies.size(); ++it) {
            unsigned int i = movies[it];
            edge_count.push_back(graph.outgoing_index()[i + 1] - graph.outgoing_index()[i]);
         }
         std::sort(edge_count.data(), edge_count.data() + movies.size());
         for (unsigned int i = 0; i < edge_count.size(); ++i)
            header << edge_count[i] << "\n";
         header.close();
      }
      timer.stop();
      fprintf(stderr, "Initialization time %6.6f s \n", timer.get_time_seconds());
      timer.clear();
      timer.start();
      distribute_chunks();
      timer.stop();
      fprintf(stderr, "Distribution time %6.6f s \n", timer.get_time_seconds());
      timer.clear();
      timer.start();
      cache_chunks();
      timer.stop();
      fprintf(stderr, "Caching time %6.6f s \n", timer.get_time_seconds());
   }
   /************************************************************************
    *
    ************************************************************************/
   void cache_chunks() {
      for (int c = 0; c < NUM_CHUNKS; ++c) {
         chunk_info[c] = new ChunkInfo();
         chunk_info[c]->allocate(all_edges[c]->size());
         for (unsigned int i = 0; i < all_edges[c]->size(); ++i) {
            const int id = (*all_edges[c])[i];
            const int src = graph.get_edge_src(id);
            const int dst = graph.out_neighbors()[id];
            const float rating = graph.out_edge_data()[id] / (float) max_rating;
            chunk_info[c]->edge_info[2 * i] = src;
            chunk_info[c]->edge_info[2 * i + 1] = dst;
            chunk_info[c]->rating[i] = rating;
            chunk_info[c]->edge_id[i] = i;
         }
      }
      fprintf(stderr, "Done caching chunks\n");
   }
   /************************************************************************
    *
    ************************************************************************/
   void distribute_chunks() {
#ifndef SGD_RANDOMIZE_EDGE
      {
         std::vector<int>***diagonals; // diagonals[i][segment][item#];
         const int num_diagonals = movies.size();
         diagonals = new std::vector<int>**[num_diagonals];
         const int num_segments = ceil((graph.num_nodes() - movies.size()) / (float) num_diagonals) + 1;
         fprintf(stderr, "Diagonals[%d], Segments[%d]\n", num_diagonals, num_segments);
         for (int i = 0; i < num_diagonals; ++i) {
            diagonals[i] = new std::vector<int>*[num_segments];
            for (int j = 0; j < num_segments; ++j) {
               diagonals[i][j] = new std::vector<int>();
               diagonals[i][j]->reserve(graph.num_edges() / num_diagonals);
            }
         }
         int max_diag = 0, max_seg = 0;
         for (unsigned int i = 0; i < graph.num_edges(); ++i) {
            const unsigned int src = graph.get_edge_src()[i];
            const unsigned int dst = graph.out_neighbors()[i];
            const int diag = abs(src - dst) % num_diagonals;
            const int seg = floor(abs(src - dst) / (float) num_diagonals);
            max_diag = std::max(max_diag, diag);
            max_seg = std::max(max_seg, seg);
            diagonals[diag][seg]->push_back(i);
         }
         fprintf(stderr, "Max [%d, %d]\n", max_diag, max_seg);
         int sum = 0;
         int max_size = 0;
         int min_size = graph.num_edges();
         for (int i = 0; i < num_diagonals; ++i) {
            //fprintf(stderr, "%d :: ",i);
            for (int j = 0; j < num_segments; ++j) {
               //fprintf(stderr, "[%d]",diagonals[i][j]->size());
               sum += diagonals[i][j]->size();
               max_size = std::max(max_size, (int) diagonals[i][j]->size());
               min_size = std::min(min_size, (int) diagonals[i][j]->size());
            }
            //fprintf(stderr, "\n");
         }
         assert(sum == (int)graph.num_edges());
         fprintf(stderr, "Total:: %d, max:: %d , min:: %d\n", sum, max_size, min_size);
         const int diags_per_chunk = ceil(num_diagonals / (float) NUM_CHUNKS);
         int chunk_sum = 0;
         int max_chunk_size = 0;
         fprintf(stderr, "Chunks:: ");
         for (int c = 0; c < NUM_CHUNKS; ++c) {
            for (int j = 0; j < diags_per_chunk; ++j) {
               for (int s = 0; s < num_segments && c * diags_per_chunk + j < num_diagonals; ++s) {
                  std::vector<int> & vec = *diagonals[c * diags_per_chunk + j][s];
                  for (auto val : vec)
                  all_edges[c]->push_back(val);
               }
            }
            fprintf(stderr, "[%d]", all_edges[c]->size());
            chunk_sum += all_edges[c]->size();
            max_chunk_size = std::max(max_chunk_size, (int) all_edges[c]->size());
         }
         fprintf(stderr, "\t Chunk sizes:: [%d],", chunk_sum);
         delete edge_wl;
         delete edge_info;
         delete ratings;
         edge_wl = new ArrayType(max_chunk_size);
         edge_info = new ArrayType(2 * max_chunk_size);
         ratings = new FeatureArrayType(max_chunk_size);

         assert(chunk_sum == graph.num_edges());
         //cleanup
         for (int i = 0; i < num_diagonals; ++i) {
            for (int j = 0; j < num_segments; ++j) {
               delete diagonals[i][j];
            }
            delete[] diagonals[i];
         }
         delete[] diagonals;

      }
#else
      //const int items_per_chunk = gpu_wl_size;//ceil(graph.num_edges() / NUM_CHUNKS);
      int * edge_wl = new int[graph.num_edges()];
      for (size_t i = 0; i < graph.num_edges(); ++i) {
         edge_wl[i] = i;
      }
      std::random_shuffle(edge_wl, &edge_wl[graph.num_edges()]);
      for (int i = 0; i < NUM_CHUNKS; ++i) {
         int * start = edge_wl + i * gpu_wl_size;
         int num_items = std::min((int) (graph.num_edges() - (i * gpu_wl_size)), (int) gpu_wl_size);
         all_edges[i]->resize(num_items);
         memcpy(all_edges[i]->data(), start, num_items * sizeof(int));
      }
      delete edge_wl;
#endif
   }
   /************************************************************************
    *
    ************************************************************************/
   void operator()(int num_steps) {
      copy_to_device();
      SGDCommon::compute_err(graph, features,max_rating);
      for (round = 0; round < num_steps; ++round) {
         this->gpu_operator();
      }
      copy_to_host();
      SGDCommon::compute_err(graph, features,max_rating);
   }
   /************************************************************************
    *
    ************************************************************************/
   void gpu_operator() {
      int i = 0;
      std::vector<int> steps_per_round;
      Galois::OpenCL::Timer timer;
      float kernel_time = 0.0f;
      timer.start();
      for (int c = 0; c < NUM_CHUNKS; ++c) {
         i = 0;
         //fprintf(stderr, "Processing chunk :: %d, size :: %d \n", c, all_edges[c]->size());
         populate_worklist(c);
         //fprintf(stderr, "\t[Done :%d]", c);
         Galois::OpenCL::Timer kernel_timer;
         kernel_timer.start();
         do {
            ++i;
            sgd_kernel();
            metadata->copy_to_host();
            //float ratio_remaining = metadata->host_ptr()[0]/ (float)all_edges[c]->size();
           // fprintf(stderr, "Step[%d], [%d], %6.6g \n", i, metadata->host_ptr()[0], ratio_remaining);
#ifdef EARLY_APPROXIMATE_TERMINATION //If 2% of iterations can be ignored for better performance.
            if(ratio_remaining < 0.02f) break;
#endif
            assert(metadata->host_ptr()[0]>=0&& "Items processed overflows!" );
         } while (metadata->host_ptr()[0] != 0);
         kernel_timer.stop();
         kernel_time += kernel_timer.get_time_seconds();
         steps_per_round.push_back(i);
      }
      timer.stop();
      fprintf(stderr, "Round: [%d], Total time: %6.6gs ,GPU_Work: %6.6g , :: [", round, timer.get_time_seconds(), kernel_time);
      for (auto it : steps_per_round) {
         fprintf(stderr, "%d, ", it);
      }
      fprintf(stderr, "]\n");

   }
   /************************************************************************
    *
    ************************************************************************/
   ~SGDChunkedEdgeFunctor() {
      deallocate();
      fprintf(stderr, "Destroying SGDChunkedEdgeFunctor object.\n");
   }
};
//###################################################################//
#endif /* SGDCHUNKEDEDGE_H_ */
