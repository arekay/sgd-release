/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef SGD_COMMON_CL_
#define SGD_COMMON_CL_
/*************************************************************************
 *
 *************************************************************************/

#define  SGD_FEATURE_SIZE 16
#define _SGD_USE_SHARED_MEM_ 1
typedef float16 FeatureTypeSIMD;
typedef float FeatureType;
#define FLOAT_SIMD_SIZE (sizeof(FeatureTypeSIMD)/sizeof(float))//number of floats per simd-type. 16*4/4=16
typedef float EdgeData;
/*************************************************************************
 * Overload dot product if not available.
 *************************************************************************/
float ACTIVE_OVERLOAD_ATTRIBUTE dot(float16 a, float16 b) {
   return dot(a.lo.lo,b.lo.lo) + dot(a.lo.hi,b.lo.hi) +dot(a.hi.lo,b.hi.lo)+dot(a.hi.hi,b.hi.hi);
}

/*************************************************************************
 * Dot product implementation for float16 in the local-shared memory.
 *************************************************************************/
FeatureType dot_product_l(__local FeatureType * sd, __local FeatureType *dd, FeatureType init) {
   for (int i = 0; i < SGD_FEATURE_SIZE; ++i) {
      init += (sd[i] * dd[i]);
   }
   return init;
}
/*************************************************************************
 * Dot product implementation for float16 in the global memory.
 *************************************************************************/

FeatureType dot_product(__global FeatureType * sd, __global FeatureType *dd, FeatureType init) {
   for (int i = 0; i < SGD_FEATURE_SIZE; ++i) {
      init += (sd[i] * dd[i]);
   }
   return init;
}
/*************************************************************************
 * Unqualified update (on private memory)
 *************************************************************************/
void do_update_l(FeatureType * s, FeatureType * d, FeatureType err, FeatureType step_size) {
   const FeatureType SGD_LAMBDA = 0.05f;
   for (int i = 0; i < SGD_FEATURE_SIZE; ++i) {
      FeatureType p_s = s[i];
      FeatureType p_d = d[i];
      s[i] -= step_size * (err * p_d + SGD_LAMBDA * p_s);
      d[i] -= step_size * (err * p_s + SGD_LAMBDA * p_d);
   }
}
/*************************************************************************
 * Update on global memory.
 *************************************************************************/
void do_update(volatile __global FeatureType * s, volatile __global FeatureType * d, FeatureType err, FeatureType step_size) {
   const FeatureType SGD_LAMBDA=0.05f;
   for(int i = 0; i < SGD_FEATURE_SIZE; ++i) {
      FeatureType p_s = s[i];
      FeatureType p_d = d[i];
      s[i] -= step_size * (err * p_d + SGD_LAMBDA * p_s);
      d[i] -= step_size * (err * p_s + SGD_LAMBDA * p_d);
   }

}
/*************************************************************************
 * Update without vector/simd extensions.
 *************************************************************************/
void sgd_kernel_global_nosimd(__global FeatureTypeSIMD * _s_ptr, __global FeatureTypeSIMD * _d_ptr, FeatureType rating, FeatureType step_size) {
   __global FeatureType * s_ptr = (__global FeatureType * )_s_ptr;
   __global FeatureType * d_ptr = (__global FeatureType * )_d_ptr;
      FeatureType err = -rating;
      for(int i=0; i<SGD_FEATURE_SIZE; ++i)
         err+=(s_ptr[i]* d_ptr[i]);
      {
         const FeatureType SGD_LAMBDA=0.05f;
         for(int i = 0; i < SGD_FEATURE_SIZE; ++i) {
            FeatureType p_s = s_ptr[i];
            FeatureType p_d = d_ptr[i];
            s_ptr[i] -= step_size * (err * p_d + SGD_LAMBDA * p_s);
            d_ptr[i] -= step_size * (err * p_s + SGD_LAMBDA * p_d);
         }
      }
}
/*************************************************************************
 *
 *************************************************************************/

void sgd_kernel_global_simd(__global FeatureTypeSIMD * s_ptr, __global FeatureTypeSIMD * d_ptr, FeatureType rating, FeatureType step_size) {
      FeatureType err = -rating;
      for(int i=0; i<SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE; ++i){
         err+=dot(s_ptr[i], d_ptr[i]);
      }
      const FeatureTypeSIMD SGD_LAMBDA=0.05f;
      for(int i = 0; i < SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE; ++i){
         FeatureTypeSIMD p_s = s_ptr[i];
         FeatureTypeSIMD p_d = d_ptr[i];
         s_ptr[i] -= step_size * (err * p_d + SGD_LAMBDA * p_s);
         d_ptr[i] -= step_size * (err * p_s + SGD_LAMBDA * p_d);
      }
}
/*************************************************************************
 *
 *************************************************************************/
void sgd_kernel_private_simd(__global FeatureTypeSIMD * s_ptr, __global FeatureTypeSIMD * d_ptr, FeatureType rating, FeatureType step_size) {
   FeatureTypeSIMD src_l[SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
   FeatureTypeSIMD dst_l[SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
   for(int i=0; i<SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE;++i) {
      src_l[i] = s_ptr[i];
      dst_l[i] = d_ptr[i];
   }
   //FeatureType err = dot_product_l(src_l, dst_l, -rating);
   FeatureType err = -rating;
   for(int i=0; i<SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE; ++i)
      err+=dot(src_l[i], dst_l[i]);
   //do_update_l(src_l, dst_l, err, step_size);
   {
      const FeatureTypeSIMD SGD_LAMBDA=0.05f;
      for(int i = 0; i < SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE; ++i) {
         FeatureTypeSIMD p_s = src_l[i];
         FeatureTypeSIMD p_d = dst_l[i];
         src_l[i] -= step_size * (err * p_d + SGD_LAMBDA * p_s);
         dst_l[i] -= step_size * (err * p_s + SGD_LAMBDA * p_d);
      }
   }
   for(int i=0; i<SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE;++i) {
      s_ptr[i] = src_l[i];
      d_ptr[i] = dst_l[i];
   }
}
/*************************************************************************
 *
 *************************************************************************/
void sgd_kernel_private_nosimd(__global FeatureTypeSIMD * _s_ptr, __global FeatureTypeSIMD * _d_ptr, FeatureType rating, FeatureType step_size) {
   __global FeatureType *s_ptr = (__global FeatureType *)(_s_ptr);
   __global FeatureType *d_ptr = (__global FeatureType *)(_d_ptr);
   FeatureType src_l[SGD_FEATURE_SIZE];
   FeatureType dst_l[SGD_FEATURE_SIZE];
   for(int i=0; i<SGD_FEATURE_SIZE;++i) {
      src_l[i] = s_ptr[i];
      dst_l[i] = d_ptr[i];
   }
   FeatureType err = -rating;
   for(int i=0; i<SGD_FEATURE_SIZE; ++i)
      err+=dot(src_l[i], dst_l[i]);

   {
      const FeatureType SGD_LAMBDA=0.05f;
      for(int i = 0; i < SGD_FEATURE_SIZE; ++i) {
         FeatureType p_s = src_l[i];
         FeatureType p_d = dst_l[i];
         src_l[i] -= step_size * (err * p_d + SGD_LAMBDA * p_s);
         dst_l[i] -= step_size * (err * p_s + SGD_LAMBDA * p_d);
      }
   }
   for(int i=0; i<SGD_FEATURE_SIZE;++i) {
      s_ptr[i] = src_l[i];
      d_ptr[i] = dst_l[i];
   }
}
/*************************************************************************
 *
 *************************************************************************/

void sgd_kernel_shared_movie_simd(__local FeatureTypeSIMD * src_l, int dst,FeatureType rating,FeatureType step_size, __global FeatureTypeSIMD * features) {
   __global FeatureTypeSIMD * d_ptr = &features[dst*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
   FeatureType err = -rating;
   for(int i=0; i<SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE; ++i)
   err+=dot(src_l[i], d_ptr[i]);
   {
      const FeatureTypeSIMD SGD_LAMBDA=0.05f;
      for(int i = 0; i < SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE; ++i) {
         FeatureTypeSIMD p_s = src_l[i];
         FeatureTypeSIMD p_d = d_ptr[i];
         src_l[i] -= step_size * (err * p_d + SGD_LAMBDA * p_s);
         d_ptr[i] -= step_size * (err * p_s + SGD_LAMBDA * p_d);
      }
   }
}


/*************************************************************************
 *
 *************************************************************************/
bool lock(__global int * locks, int s, int d) {
#ifdef _SGD_NO_LOCKS_
   return true;
#else
   volatile __global int * src = &locks[s]; //&(node_data(graph, s)->lock);
   volatile __global int * dst = &locks[d];//&(node_data(graph, d)->lock);
   int my_id = get_global_id(0);
   if(atomic_cmpxchg(src, -1, my_id)==-1) {
      if(atomic_cmpxchg(dst, -1, my_id)==-1) {
         return true;
      }
      atomic_cmpxchg(src, my_id, -1);
      return false;
   }
   return false;
#endif
}
/*************************************************************************
 *
 *************************************************************************/
void unlock( __global int * locks, int s, int d) {
#ifdef _SGD_NO_LOCKS_
#else
   volatile __global int * src = &locks[s]; //&(node_data(graph, s)->lock);
   volatile __global int * dst = &locks[d];//&(node_data(graph, d)->lock);
   int my_id = get_global_id(0);
   atomic_cmpxchg(dst, my_id, -1);
   atomic_cmpxchg(src, my_id, -1);
#endif
}
/*************************************************************************
 *
 *************************************************************************/
bool lock_1(__global int * locks, int s) {
#ifdef _SGD_NO_LOCKS_
   return true;
#else
   volatile __global int * src = &locks[s]; //&(node_data(graph, s)->lock);
   int my_id = get_global_id(0);
   if(atomic_cmpxchg(src, -1, my_id)==-1) {
      return true;
   }
   return false;
#endif
}
/*************************************************************************
 *
 *************************************************************************/
void unlock_1( __global int * locks, int s) {
#ifdef _SGD_NO_LOCKS_
#else
   volatile __global int * src = &locks[s]; //&(node_data(graph, s)->lock);
   int my_id = get_global_id(0);
   atomic_cmpxchg(src, my_id, -1);
#endif
}
/*************************************************************************
 *
 *************************************************************************/
#endif /* SGD_COMMON_CL_ */
