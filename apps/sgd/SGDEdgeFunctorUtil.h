/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef SGDEDGEFUNCTORUTIL_H_
#define SGDEDGEFUNCTORUTIL_H_

#include <random>
#include <thread>
#include <atomic>
/////////////////////////////////////////////////////////
/************************************************************************
 * Chunked executor.
 ************************************************************************/
using namespace Galois::OpenCL;
struct SGDEdgeUtil {
   typedef Array<cl_int> ArrayType;
   typedef GPUArray<cl_int> GPUArrayType;
   typedef Array<FeatureType> FeatureArrayType;
   typedef GPUArray<FeatureType> GPUFeatureArrayType;
   typedef CL_Kernel Kernel;
   ////////////////////////////////////////////////////////////
   /************************************************
    *
    *************************************************/
   int round;
   ArrayType * edge_info;
   ArrayType * metadata;
   Kernel sgd_edge_kernel;
   ArrayType * locks;
   FeatureArrayType * features;
   std::string filename;
   /************************************************************************
    *
    ************************************************************************/
   SGDEdgeUtil() :
         round(0), edge_info(0), metadata(0), locks(0), features(0) {
      sgd_edge_kernel.init("./apps/sgd/sgd_edge_operator.cl", "sgd_edge_operator_small");
   }
   void init(std::string _filename, ArrayType * _locks, FeatureArrayType * _features, ArrayType * _edge_metadata, ArrayType * _next_edge_wl) {
      filename = _filename;
      locks = _locks;
      features = _features;
      metadata = _edge_metadata;
      edge_info = _next_edge_wl;
      sgd_edge_kernel.set_arg_list(locks, features, metadata, /*buffers[i]->edge_wl,*/edge_info);
      //Initialize meta-data
   }
   /************************************************************************
    *
    ************************************************************************/
   std::pair<int, float> operator()(int num_items, int round, float node_time = 0.0f) {
      //Copy metadata to num_items
      Galois::OpenCL::Timer timer, kernel_timer;
      kernel_timer.clear();
      std::vector<std::pair<int, float> > step_stats;
      step_stats.reserve(1000);
      timer.start();
      {
         metadata->host_ptr()[0] = num_items;
         metadata->host_ptr()[1] = round;
         metadata->host_ptr()[2] = num_items;
         float t = SGDCommon::SGD_STEP_SIZE(round);
         metadata->host_ptr()[3] = (t);
         metadata->host_ptr()[4] = num_items;
         sgd_edge_kernel.set_arg(4, sizeof(cl_float), &t);
         sgd_edge_kernel.set_work_size(num_items);
      }
      int curr_step = 0;
      float kernel_time = 0;
      float operator_time = 0;
      metadata->copy_to_device();
//      fprintf(stderr, "XXXXXXXXXXXXXXX %6,6g\n", node_time);
      step_stats.push_back(std::pair<int, float>(num_items, node_time));
      kernel_timer.start();
      do {
         ++curr_step;
         Galois::OpenCL::OpenCL_Setup::finish();
         sgd_edge_kernel();
         Galois::OpenCL::OpenCL_Setup::finish();
         float curr_kernel_time = sgd_edge_kernel.last_exec_time();
         kernel_time += curr_kernel_time;
         metadata->copy_to_host();
         step_stats.push_back(std::pair<int, float>(metadata->host_ptr()[0], curr_kernel_time));
//         fprintf(stderr, "Step[%d], [%d]\n", curr_step, metadata->host_ptr()[0]);
#ifdef EARLY_APPROXIMATE_TERMINATION //If 2% of iterations can be ignored for better performance.
         float ratio_remaining = metadata->host_ptr()[0]/ (float)num_items;
         fprintf(stderr, "Step[%d], [%d], %6.6g \n", curr_step, metadata->host_ptr()[0], ratio_remaining);
//     if(ratio_remaining < 0.02f) break;
#endif
         assert(metadata->host_ptr()[0] >= 0 && "Items processed overflows!");
      } while (metadata->host_ptr()[0] != 0);
      kernel_timer.stop();
      timer.stop();
      static bool isfirst = true;
      if (isfirst) {
         fprintf(stderr, "Printing stats....\n");
         isfirst = false;
         std::string log_file_name((filename));
         log_file_name.append(".hybrid_locked_stats.csv");
         std::ofstream out_file(log_file_name);
         for (auto i : step_stats) {
            out_file << i.first << "," << i.second << "\n";
         }
         out_file.close();
      }
      kernel_time = kernel_timer.get_time_seconds();
      operator_time = timer.get_time_seconds();
      (void)operator_time;
      return std::pair<int, float>(curr_step, kernel_time);
   }
   /************************************************************************
    *
    ************************************************************************/
   ~SGDEdgeUtil() {
      fprintf(stderr, "Destroying SGDAsynEdgeFunctor object.\n");
   }
};
//###################################################################//
#endif /* SGDEDGEFUNCTORUTIL_H_ */
