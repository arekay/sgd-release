/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef GALOISGPU_APPS_SGD_SGDALLGRAPHMATCHINGEDGECOALESCED_H_
#define GALOISGPU_APPS_SGD_SGDALLGRAPHMATCHINGEDGECOALESCED_H_

#include <random>
#include <thread>
#include <atomic>
/////////////////////////////////////////////////////////
/************************************************************************
 * SGD Static schedule, flat version.
 1) Determine matchings for all the edges.
 2) Process items per-matching w/o locks
 ************************************************************************/
using namespace Galois::OpenCL;
struct SGDAllGraphMatchingEdgeCoalesced {
//   static const bool PrintMatchingStats = false;
   typedef SGD_LC_LinearArray_Undirected_Graph<Galois::CPUArray, unsigned int, unsigned int> GraphTy;
   typedef Array<int> ArrayType;
   typedef GPUArray<int> GPUArrayType;
   typedef Array<FeatureType> FeatureArrayType;
   typedef GPUArray<FeatureType> GPUFeatureArrayType;
   typedef CL_Kernel Kernel;
   /************************************************
    *************************************************/
   GraphTy graph;
   std::vector<int> movies;
   std::vector<int> user_indices;
   float accumulated_error;
   int round;
   unsigned int max_rating;
   char filename[512];
   int * edge_info;

   GPUArrayType * edge_kernel_edge_info;
   ArrayType *lock_free_wl_size;
   FeatureArrayType * features;
   ArrayType *edge_step;
   FeatureArrayType * ratings;
   ArrayType *metadata;
   ArrayType *movies_ordered;
   Kernel lock_free_edge_kernel;
   int filtered_edges;
   std::map<int, int> edge_reorder_map;
   Galois::StatsManager stats;
   /************************************************************************
    *
    ************************************************************************/
   SGDAllGraphMatchingEdgeCoalesced(int num_movies, int num_users) :
         round(0) {
      strcpy(filename, "generated-input");
      fprintf(stderr, "Creating SGDAllGraphMatchingEdgeCoalesced -  features =[%d], UseSharedMem=", SGD_FEATURE_SIZE);
#ifdef _SGD_USE_SHARED_MEM_
      fprintf(stderr, "[YES]");
#else
      fprintf(stderr, "[NO]");
#endif

      SGDCommon::complete_bipartitie(graph, num_movies, num_users);
      allocate();
      initialize_features_random();
      fprintf(stderr, "NumMovies[%ld]\n", movies.size());
   }
   /************************************************************************
    *
    ************************************************************************/
   SGDAllGraphMatchingEdgeCoalesced(int num_m) :
         round(0) {
      strcpy(filename, "gen-diagonal-input");
      fprintf(stderr, "Creating SGDAllGraphMatchingEdgeCoalesced -  features =[%d], UseSharedMem=", SGD_FEATURE_SIZE);
      SGDCommon::diagonal_graph(graph, num_m);
      allocate();
      initialize_features_random();
      fprintf(stderr, "Number of movies found :: %ld\n", movies.size());
   }
   /************************************************************************
    *
    *metadata (16)
    *edge_info, worklist, ratings (2+1+1)*NE
    *locks, features*64, (1+64)NN
    ************************************************************************/
   SGDAllGraphMatchingEdgeCoalesced(bool road, const char * p_filename) :
         round(0) {
      strcpy(filename, p_filename);
      fprintf(stderr, "Creating SGDAllGraphMatchingEdgeCoalesced -  features =[%d], UseSharedMem=", SGD_FEATURE_SIZE);
#ifdef _SGD_USE_SHARED_MEM_
      fprintf(stderr, "[YES]");
#else
      fprintf(stderr, "[NO]");
#endif

      if (road) {
         SGDCommon::read_as_bipartite(graph, filename);
      } else {
         graph.read(p_filename);
      }
      allocate();
      initialize_features_random();
      fprintf(stderr, "NumMovies[%ld]\n", movies.size());
   }
   /************************************************************************
    *
    ************************************************************************/
   void verify_schedule() {
      std::ofstream out_file("edge_schedule_dyn.csv");
      out_file << "src,dst,step\n";
      for (size_t i = 0; i < edge_reorder_map.size(); ++i) {
         int edge_id = edge_reorder_map[i];
         int src = graph.get_edge_src(edge_id);
         int dst = graph.out_neighbors()[edge_id];
         int step = edge_step->host_ptr()[i];
         out_file << src << "," << dst << "," << step << "\n";
      }
      out_file.close();
   }
   /************************************************************************
    *
    ************************************************************************/
   void build_schedule(int num_steps) {
      std::vector<int> ** edge_id_per_step;
      edge_id_per_step = new std::vector<int>*[num_steps];
      for (int i = 0; i < num_steps; ++i) {
         edge_id_per_step[i] = new std::vector<int>();
      }
      int max_size = 0;
      for (size_t i = 0; i < edge_reorder_map.size(); ++i) {
         int edge_id = edge_reorder_map[i];
         int step = edge_step->host_ptr()[i];
         assert(step != -1 && "Unprocessed edge - invalid step!");
         edge_id_per_step[step]->push_back(edge_id);
         max_size = std::max(max_size, (int) edge_id_per_step[step]->size());
      }
      fprintf(stderr, "Max_size, %d, Reorder_size, %lu\n", max_size, edge_reorder_map.size());
      edge_kernel_edge_info = new GPUArrayType(2 * graph.num_edges());
      lock_free_wl_size = new ArrayType(num_steps + 1);
      lock_free_edge_kernel.set_arg_list(features, metadata, edge_kernel_edge_info, ratings, lock_free_wl_size);
      lock_free_wl_size->host_ptr()[0] = 0;
      fprintf(stderr, "Sorting matches...!\n");
      for (int i = 0; i < num_steps; ++i) {
         lock_free_wl_size->host_ptr()[i + 1] = lock_free_wl_size->host_ptr()[i] + edge_id_per_step[i]->size();
//            std::sort(edge_id_per_step[i]->begin(), edge_id_per_step[i]->end(), [&](const int & lhs, const int & rhs) {return graph.out_neighbors()[lhs]<graph.out_neighbors()[rhs];});
         std::sort(edge_id_per_step[i]->begin(), edge_id_per_step[i]->end(), [&](const int & lhs, const int & rhs) {return graph.get_edge_src()[lhs]<graph.get_edge_src()[rhs];});

         for (size_t j = 0; j < edge_id_per_step[i]->size(); ++j) {
            int new_index = lock_free_wl_size->host_ptr()[i] + j;
            int edge_id = (*edge_id_per_step[i])[j];
            edge_step->host_ptr()[edge_id] = i;
            int src = graph.get_edge_src(edge_id);
            int dst = graph.out_neighbors()[edge_id];
            float rating = graph.out_edge_data()[edge_id] / (float) max_rating;
            edge_info[2 * new_index] = src;
            edge_info[2 * new_index + 1] = dst;
            ratings->host_ptr()[new_index] = rating;
         }
      }
      edge_kernel_edge_info->copy_to_device(edge_info);
      for (int i = 0; i < num_steps; ++i)
         delete edge_id_per_step[i];
      delete[] edge_id_per_step;
   }
   /************************************************************************
    *
    ************************************************************************/
   void allocate() {
      features = new FeatureArrayType(graph.num_nodes() * SGD_FEATURE_SIZE);
      metadata = new ArrayType(16);
      ratings = new FeatureArrayType(graph.num_edges());
      edge_step = new ArrayType(graph.num_edges());

      edge_info = new int[2 * graph.num_edges()];
      lock_free_edge_kernel.init("./apps/sgd/sgd_static_operator.cl", "sgd_lock_free_edge_coalesced_operator");
   }
   /************************************************************************
    *
    ************************************************************************/
   void deallocate() {
      delete features;
      delete metadata;
      delete ratings;
      delete edge_step;

      delete[] edge_info;
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_device() {
      features->copy_to_device();
      ratings->copy_to_device();
      metadata->copy_to_device();
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_host() {
      features->copy_to_host();
      ratings->copy_to_host();
      metadata->copy_to_host();
   }
   /************************************************************************
    *
    ************************************************************************/
   void populate_worklist() {
      metadata->host_ptr()[0] = 0; //graph.num_edges();
      metadata->host_ptr()[1] = round;
      metadata->host_ptr()[2] = graph.num_edges();
      float t = SGDCommon::SGD_STEP_SIZE(round);
      metadata->host_ptr()[3] = (t);
      metadata->host_ptr()[4] = graph.num_edges();
      metadata->host_ptr()[5] = 0; // Step#
      metadata->host_ptr()[6] = graph.num_nodes();
      copy_to_device();
   }
   /************************************************************************
    *
    ************************************************************************/
   void initialize_features_random() {
      Galois::OpenCL::Timer timer;
      timer.start();
      SGDCommon::initialize_features_random(graph, features, movies);
      movies.clear();
      unsigned int max_degree = 0, max_degree_id = 0;
      for (unsigned int i = 0; i < graph.num_nodes(); ++i) {
         if (graph.num_neighbors(i) > max_degree) {
            max_degree = graph.num_neighbors(i);
            max_degree_id = i;
         }
         if (graph.num_neighbors(i) > 0) {
            movies.push_back(i);
         } else {
            user_indices.push_back(i);
         }
      }
      max_rating = 0;
      for (unsigned int i = 0; i < graph.num_edges(); ++i) {
         max_rating = std::max(max_rating, graph.out_edge_data()[i]);
      }
      fprintf(stderr, "] , max_Rating: %d, movies: %ld, Max degree:: %d for node: %d\n", max_rating, movies.size(), max_degree, max_degree_id);
      timer.stop();
      SGDCommon::print_distributions(graph);
      fprintf(stderr, "InitTime [%6.6f s], ", timer.get_time_seconds());
      timer.clear();
      timer.start();
      distribute_chunks();
      timer.stop();
      fprintf(stderr, "DistTime [%6.6f s], ", timer.get_time_seconds());
      timer.clear();
   }
   /************************************************************************
    *
    ************************************************************************/
   void distribute_chunks() {
      std::vector<int> in_edge_wl(graph.num_edges());
      std::vector<int> edge_wl;
      for (size_t i = 0; i < graph.num_edges(); ++i) {
         in_edge_wl[i] = i;
      }
      SGDCommon::edge_filter(SGDCommon::EdgeFilterType::RandomShuffle, graph, in_edge_wl, edge_wl);
      /*edge_wl.resize(in_edge_wl.size());
       std::copy(in_edge_wl.begin(), in_edge_wl.end(), edge_wl.begin());*/

      filtered_edges = edge_wl.size();
      edge_reorder_map.clear();
      for (int j = 0; j < filtered_edges; ++j) {
         int edge_index = edge_wl[j];
         edge_reorder_map[j] = edge_index;
         int src = graph.get_edge_src(edge_index);
         int dst = graph.out_neighbors()[edge_index];
         float wt = graph.out_edge_data()[edge_index] / (float) max_rating;
         edge_info[2 * j] = src;
         edge_info[2 * j + 1] = dst;
         ratings->host_ptr()[j] = wt;
         edge_step->host_ptr()[j] = -1;
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   void operator()(int num_steps) {
      round = 0;
      std::string log_file_name(basename(filename));
      log_file_name.append("AllGraphEdge");
      int steps_per_round = 0;
      { //Check if file exists...
         std::string matching_filename(log_file_name);
         matching_filename.append(".matching");
         ifstream matching_file(matching_filename, ios::binary);
         if (matching_file.good()) {
            fprintf(stderr, "Matching info found... loading: %s \n", matching_filename.c_str());
            steps_per_round = SGDCommon::BuildMatchingCompact::from_file(matching_file, graph.num_edges(), edge_info, edge_step->host_ptr());
            edge_step->copy_to_device(); //To ensure device has a copy as well.
         } else {
            fprintf(stderr, "Matching info NOT found... building: %s \n", matching_filename.c_str());
            SGDCommon::BuildMatchingCompact matching(graph.num_nodes(), graph.num_edges(), edge_info, edge_step, log_file_name);
            steps_per_round = matching.operator()<false>(0, graph.num_edges());
            matching.to_file(matching_filename);
         }
      }
      fprintf(stderr, "Allocated post-matching %6.6g, MaxMatching %d\n", (Galois::OpenCL::OpenCL_Setup::stats.allocated/ (float) (1024 * 1024)), steps_per_round);
      build_schedule(steps_per_round);
      copy_to_device();
      //if (EdgeSchedule == true)
      {
         fprintf(stderr, "BuidlEdgeSchedule :: Allocated %6.6g\n", (Galois::OpenCL::OpenCL_Setup::stats.allocated/ (float) (1024 * 1024)));
         SGDCommon::compute_err(graph, features, max_rating);
         for (; round < num_steps; ++round) {
            this->lock_free_edge_operator(steps_per_round);
            SGDCommon::compute_err(graph, features, max_rating);
         }
      }
   }
   /************************************************************************
    *
    ************************************************************************/

   void lock_free_edge_operator(int num_steps) {
      Galois::OpenCL::Timer timer;
      timer.start();
      copy_to_device();
      lock_free_wl_size->copy_to_device();
      float f = SGDCommon::SGD_STEP_SIZE(round);
      lock_free_edge_kernel.set_arg(5, sizeof(float), &f);
      metadata->host_ptr()[0] = graph.num_edges();
      timer.stop();
      float copy_time = timer.get_time_seconds();
      timer.clear();
      timer.start();
      float kernel_time = 0;
//      lock_free_edge_kernel.set_work_size(26869);
#if 0 //Host-side barrier
      for (int i = 0; i < num_steps; ++i) {
         metadata->host_ptr()[5] = i;
         metadata->copy_to_device();
         int num_items = lock_free_wl_size->host_ptr()[i + 1] - lock_free_wl_size->host_ptr()[i];
         lock_free_edge_kernel.set_work_size(num_items);
         lock_free_edge_kernel();
         kernel_time+=lock_free_edge_kernel.last_exec_time();
//         metadata->copy_to_host();
      }
#else
      //Device side barrier
      metadata->host_ptr()[0] = 0;
      metadata->host_ptr()[2] = graph.num_edges();
      metadata->host_ptr()[6] = num_steps;
      metadata->host_ptr()[5] = 0;
      metadata->host_ptr()[10] = 0;
      metadata->host_ptr()[11] = 0;
      metadata->copy_to_device();

      lock_free_edge_kernel.set_work_size(1);
      const size_t l_m_size = Galois::OpenCL::OpenCL_Setup::local_memory_size(Galois::OpenCL::OpenCL_Setup::get_default_device());
      (void)l_m_size; // suppress warning

      size_t local_dim[2], global_dim[2];
      local_dim[0] = SGD_FEATURE_SIZE;
      local_dim[1] = 16;//l_m_size/(SGD_FEATURE_SIZE*3*sizeof(float)*SGD_FEATURE_SIZE);//lock_free_edge_kernel.local/SGD_FEATURE_SIZE;
      global_dim[0] = SGD_FEATURE_SIZE;
      global_dim[1] = local_dim[1] * Galois::OpenCL::OpenCL_Setup::get_device_eu();

      lock_free_edge_kernel.set_arg(6,sizeof(float)*SGD_FEATURE_SIZE*local_dim[1],NULL);
      lock_free_edge_kernel.set_arg(7,sizeof(float)*SGD_FEATURE_SIZE*local_dim[1],NULL);
      lock_free_edge_kernel.set_arg(8,sizeof(float)*SGD_FEATURE_SIZE*local_dim[1],NULL);


      fprintf(stderr, "Total, %d, Global, [%d,%d] , Local, [%d,%d] \n", (int)(global_dim[0] *global_dim[1]), (int)global_dim[0], (int)global_dim[1], (int)local_dim[0], (int)local_dim[1]);
//      fprintf(stderr, "Threads:%lud, ", lock_free_edge_kernel.local * Galois::OpenCL::OpenCL_Setup::get_device_eu());
      lock_free_edge_kernel.set_work_size(lock_free_edge_kernel.local * Galois::OpenCL::OpenCL_Setup::get_device_eu());

      Galois::OpenCL::Timer k_timer;
      k_timer.start();
      Galois::OpenCL::CHECK_CL_ERROR(clEnqueueNDRangeKernel(lock_free_edge_kernel.device->command_queue(), lock_free_edge_kernel.kernel, 2, nullptr,
            &global_dim[0], &local_dim[0], 0, nullptr, &lock_free_edge_kernel.event), "Kernel");
      kernel_time += lock_free_edge_kernel.last_exec_time();
      k_timer.stop();

//      lock_free_edge_kernel();
      fprintf(stderr, "KTime: %6.6g, E :%d, ", k_timer.get_time_seconds(), metadata->host_ptr()[0]);
      kernel_time += lock_free_edge_kernel.last_exec_time();
#endif
      timer.stop();
      metadata->copy_to_host();
      float exec_time = timer.get_time_seconds();
      stats.add_stats("Round", round);
      stats.add_stats("Steps", num_steps);
      stats.add_stats("ETotalTime", exec_time + copy_time);
      stats.add_stats("GPUWork", exec_time);
      stats.add_stats("Kernel", kernel_time);
      stats.end_round();
//      fprintf(stderr, "Round, %d, Steps, %d , Total-time(s), %6.6g ,GPU_Work(s), %6.6g, Kernel(s), %6.6g \t", round, num_steps, exec_time + copy_time, exec_time, kernel_time);
      copy_to_host();
   }
   /************************************************************************
    *
    ************************************************************************/
   ~SGDAllGraphMatchingEdgeCoalesced() {
      deallocate();
      stats.print();
      fprintf(stderr, "Destroying SGDAllGraphMatchingEdgeCoalesced -  features =[%d].\n", SGD_FEATURE_SIZE);
      Galois::OpenCL::OpenCL_Setup::cleanup();
   }
};
//###################################################################//
#endif /* GALOISGPU_APPS_SGD_SGDALLGRAPHMATCHINGEDGECOALESCED_H_ */
