/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics :enable
#pragma OPENCL EXTENSION cl_khr_global_int32_extended_atomics : enable
#pragma OPENCL EXTENSION cl_khr_local_int32_base_atomics :enable
#pragma OPENCL EXTENSION cl_khr_local_int32_extended_atomics : enable
#pragma OPENCL EXTENSION cl_nv_compile_options:enable

/////////////////////////////////////////////////////
#ifdef cl_khr_int64_base_atomics
#pragma OPENCL EXTENSION cl_khr_int64_base_atomics : enable
#endif
#ifdef cl_khr_int64_extended_atomics
#pragma OPENCL EXTENSION cl_khr_int64_extended_atomics : enable
#endif
#ifdef cl_khr_fp64
#pragma OPENCL EXTENSION cl_khr_fp64: enable
#endif

#ifdef cl_khr_fp16
#pragma OPENCL EXTENSION cl_khr_fp16: enable
#endif

#ifdef cl_amd_fp64
#pragma OPENCL EXTENSION cl_amd_fp64: enable
#endif
#ifdef cl_amd_vec3
#pragma OPENCL EXTENSION cl_amd_vec3: enable
#endif

//////////////////////////////////////////////////////////////
#include "./apps/sgd/SGDCommon.h"
//#######################################################################################################################################////
float mydot(float16 a, float16 b) {
   return dot(a.lo.lo, b.lo.lo) + dot(a.lo.hi, b.lo.hi) + dot(a.hi.lo, b.hi.lo) + dot(a.hi.hi, b.hi.hi);
}

FeatureType dot_product(__global FeatureType * sd, __global FeatureType *dd, FeatureType init) {
   for(int i=0; i < SGD_FEATURE_SIZE; ++i) {
      init+=(sd[i] * dd[i]);
   }
   return init;
}
FeatureType dot_product_l(FeatureType * sd, FeatureType *dd, FeatureType init) {
   for (int i = 0; i < SGD_FEATURE_SIZE; ++i) {
      init += (sd[i] * dd[i]);
   }
   return init;
}
//#######################################################################################################################################////
bool lock(__global int * locks, int s, int d) {
#ifdef _SGD_NO_LOCKS_
   return true;
#else
   volatile __global int * src = &locks[s]; //&(node_data(graph, s)->lock);
   volatile __global int * dst = &locks[d];//&(node_data(graph, d)->lock);
   int my_id = get_global_id(0);
   if(atomic_cmpxchg(src, -1, my_id)==-1) {
      if(atomic_cmpxchg(dst, -1, my_id)==-1) {
         return true;
      }
      atomic_cmpxchg(src, my_id, -1);
      return false;
   }
   return false;
#endif
}
///
void unlock( __global int * locks, int s, int d) {
#ifdef _SGD_NO_LOCKS_
#else
   volatile __global int * src = &locks[s]; //&(node_data(graph, s)->lock);
   volatile __global int * dst = &locks[d];//&(node_data(graph, d)->lock);
   int my_id = get_global_id(0);
   atomic_cmpxchg(dst, my_id, -1);
   atomic_cmpxchg(src, my_id, -1);
#endif
}

////#######################GLOBAL_BARIIER#######################
void start_global_barrier(int fold, volatile __global volatile int* count) {
   barrier(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
   if(get_local_id(0) == 0) {
      atomic_add(count, 1);
      int count_val = atomic_or(count,0);
      while(count_val < fold* get_num_groups(0)) {
         count_val = atomic_or(count,0);
      }
   }
   barrier(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void do_sgd_update_shared_movie(__local FeatureTypeSIMD * src_l, int dst,FeatureType rating,FeatureType step_size, __global FeatureTypeSIMD * features) {
   __global FeatureTypeSIMD * d_ptr = &features[dst*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
   FeatureType err = -rating;
   for(int i=0; i<SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE; ++i)
   err+=mydot(src_l[i], d_ptr[i]);
   {
      const FeatureTypeSIMD SGD_LAMBDA=0.05f;
      for(int i = 0; i < SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE; ++i) {
         FeatureTypeSIMD p_s = src_l[i];
         FeatureTypeSIMD p_d = d_ptr[i];
         src_l[i] -= step_size * (err * p_d + SGD_LAMBDA * p_s);
         d_ptr[i] -= step_size * (err * p_s + SGD_LAMBDA * p_d);
      }
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void sgd_lock_free_node_operator(
      __global int * movies,
      __global FeatureTypeSIMD * features,
      __global int * metadata, __global int * edge_indices,__global int * edge_destinations,
      __global FeatureType * ratings, __global int * edge_step, FeatureType step_size, __local FeatureTypeSIMD * cache) {
   const uint my_id = get_global_id(0);
   const int max_steps = metadata[6];
   int private_work_counter = 0;
   __global int * work_counter = &metadata[8];
   __global int * barrier_start = & metadata[10];
   __global int * barrier_end = & metadata[11];
   const int movie_start = metadata[12];
   const int movie_end = metadata[13];
   bool work_done = false;
   __local FeatureTypeSIMD * src_l;
   __global FeatureTypeSIMD * s_ptr;

   if(my_id < movie_end)
   {
      const int movie_id = movies[my_id];
      src_l = &cache[get_local_id(0)*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
      s_ptr = &features[movie_id*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
      for(int i=0; i<SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE;++i) {
         src_l[i] = s_ptr[i];
      }
      /////////////////////////////////////////
      int edge_index=edge_indices[my_id];
      for(int curr_step = 0; curr_step < max_steps; ++curr_step) {
         start_global_barrier(curr_step+1, barrier_start);
//         atomic_add(work_counter, 1);
         if(edge_index < edge_indices[my_id+1] && edge_step[edge_index]==curr_step) {
            const int dst = edge_destinations[edge_index ];
            const FeatureType rating = ratings[edge_index ];
            work_done=true;
            do_sgd_update_shared_movie(src_l, dst, rating, step_size, features);
            //atomic_add(work_counter, 1);
            private_work_counter+=1;
            ++edge_index;
         }
         start_global_barrier(curr_step+1, barrier_end);
      }   //End for

      if(work_done) {
         atomic_add(work_counter, private_work_counter);
         for(int i=0; i<SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE;++i) {
            s_ptr[i] = src_l[i];
         }   //End for
      }   //End if
   }   // End if
}   //End fun.
