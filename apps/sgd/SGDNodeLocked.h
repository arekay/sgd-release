/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef SGDNODE_LOCKD_H_
#define SGDNODE_LOCKD_H_
#include <random>
#include <thread>
#include <atomic>
#include "../../apps/sgd/SGDEdgeFunctorUtil.h"
using namespace Galois::OpenCL;
struct SGDNodeLocked {
   typedef SGD_LC_LinearArray_Undirected_Graph<Galois::CPUArray, unsigned int, unsigned int> GraphTy;
   typedef Array<cl_int> ArrayType;
   typedef GPUArray<cl_int> GPUArrayType;
   typedef Array<FeatureType> FeatureArrayType;
   typedef GPUArray<FeatureType> GPUFeatureArrayType;
   typedef CL_Kernel Kernel;
   ////////////////////////////////////////////////////////////
   /************************************************
    *
    *************************************************/
   struct AsyncConfig {
      AsyncConfig(int buff, int ch, int wl_size) :
            NUM_BUFFERS(buff), NUM_CHUNKS(ch), GPU_WL_SIZE(wl_size) {
      }
      int NUM_BUFFERS;
      int NUM_CHUNKS;
      int GPU_WL_SIZE;
      template<typename Graph>
      void compute_requirements(Graph & g) {
         using namespace Galois::OpenCL;
         const size_t num_nodes = g.num_nodes();
         const size_t num_edges = g.num_edges();
         const long device_memory = OpenCL_Setup::get_device_memory();
         //Global data is locks + features
         const long global_data = sizeof(int) * num_nodes + num_nodes * (SGD_FEATURE_SIZE + 1) * sizeof(FeatureType);
         const long total_chunk_mem = device_memory - global_data;
         const long edge_data_size = 3 * sizeof(int) * num_edges + 16 * sizeof(int);
         // chunk_data is (src,dst,rating) + metadata
         if (edge_data_size > total_chunk_mem) {
            NUM_BUFFERS = 2; // Use 2-copy engines on the GPU.
            NUM_CHUNKS = ceil((3 * sizeof(int) * num_edges + 16 * sizeof(int)) / (float) (total_chunk_mem / NUM_BUFFERS));
         }
         GPU_WL_SIZE = num_edges / NUM_CHUNKS;
         fprintf(stderr, "Mem. %ld, GlobalData, %ld, ChunkMem, %ld, chunks_required, %d, buffered_chunks, %d \n", device_memory, global_data, total_chunk_mem, NUM_BUFFERS,
               NUM_CHUNKS);

      }
      template<typename Graph>
      void init(Graph & g) {
         using namespace Galois::OpenCL;
         const size_t num_edges = g.num_edges();
         GPU_WL_SIZE = num_edges / NUM_CHUNKS;
      }
   };
   AsyncConfig config;
   GraphTy graph;
   std::vector<int> movies;
   ArrayType * locks;
   FeatureArrayType * features;
   ArrayType * metadata;
   ArrayType * edge_metadata;
   ArrayType * edge_indices;
   ArrayType * edge_destinations;
   FeatureArrayType * ratings;
   ArrayType * movie_index;
   float accumulated_error;
   int round;
   unsigned int max_rating;
   char filename[512];
   Kernel sgd_node_kernel;
   Galois::StatsManager stats;
   /************************************************************************
    *
    *metadata (16)
    *edge_info, worklist, ratings (2+1+1)*NE
    *locks, features*64, (1+64)NN
    ************************************************************************/
   SGDNodeLocked(bool road, const char * p_filename) :
         config(1, 1, 0), round(0) {
      strcpy(filename, p_filename);
      fprintf(stderr, "Creating SGDNodeLocked -  features =[%d], Chunks=[%d], Buffers=[%d],SharedMem=", SGD_FEATURE_SIZE, config.NUM_CHUNKS, config.NUM_BUFFERS);
#ifdef _SGD_USE_SHARED_MEM_
      fprintf(stderr, "[YES]");
#else
      fprintf(stderr, "[NO]");
#endif
      if (road) {
         SGDCommon::read_as_bipartite(graph, filename);
      } else {
         graph.read(p_filename);
      }
      config.init(graph);
      allocate();
      initialize_features_random();
      prepare_metadata();
      fprintf(stderr, "NumMovies[%ld]\n", movies.size());
      if (false) {
         std::ofstream header("graph_header.h");
         header << Galois::OpenCL::str_SGD_LC_LinearArray_Undirected_Graph << "\n";
         header.close();
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   SGDNodeLocked(int num_m) :
         config(1, 1, 0), round(0) {
      strcpy(filename, "gen-diagonal-input");
      fprintf(stderr, "Creating SGDNodeLocked -  features =[%d], Chunks=[%d], Buffers=[%d] .\n", SGD_FEATURE_SIZE, config.NUM_CHUNKS, config.NUM_BUFFERS);
      SGDCommon::diagonal_graph(graph, num_m);
      config.init(graph);
      allocate();
      initialize_features_random();
      fprintf(stderr, "Number of movies found :: %ld\n", movies.size());
   }
   /************************************************************************
    *
    ************************************************************************/
   SGDNodeLocked(int num_m, int num_u) :
         config(1, 1, 0), round(0) {
      strcpy(filename, "generated-input");
      fprintf(stderr, "Creating SGDNodeLocked -  features =[%d], Chunks=[%d], Buffers=[%d] .\n", SGD_FEATURE_SIZE, config.NUM_CHUNKS, config.NUM_BUFFERS);
#ifdef _SGD_USE_SHARED_MEM_
      fprintf(stderr, "[YES]");
#else
      fprintf(stderr, "[NO]");
#endif
      SGDCommon::complete_bipartitie(graph, num_m, num_u);
      config.init(graph);
      allocate();
      initialize_features_random();
      prepare_metadata();
      fprintf(stderr, "NumMovies[%ld]\n", movies.size());
   }
   /************************************************************************
    *
    ************************************************************************/
   void allocate() {
      features = new FeatureArrayType(graph.num_nodes() * SGD_FEATURE_SIZE);
      locks = new ArrayType(graph.num_nodes());
      metadata = new ArrayType(16);
      edge_metadata = new ArrayType(16);
      edge_indices = new ArrayType(graph.num_nodes() + 1);
      edge_destinations = new ArrayType(graph.num_edges());
      ratings = new FeatureArrayType(graph.num_edges());
      sgd_node_kernel.init("./apps/sgd/sgd_node_operator.cl", "sgd_node_operator");
      sgd_node_kernel.set_arg_list(locks, features, metadata, edge_indices, edge_destinations, ratings);
#ifdef _SGD_USE_SHARED_MEM_
      unsigned long l_m_size = Galois::OpenCL::OpenCL_Setup::local_memory_size();
      size_t local_size = std::min((size_t)sgd_node_kernel.get_default_workgroup_size(), (size_t)(l_m_size/(sizeof(float)*SGD_FEATURE_SIZE)));
      local_size=(512+768)/2;
      sgd_node_kernel.set_arg(8, sizeof(float) * SGD_FEATURE_SIZE * local_size, NULL);
#else
      size_t local_size = sgd_node_kernel.get_default_workgroup_size();
      //sgd_node_kernel.set_arg(8, sizeof(float) * 0, NULL);
#endif
      fprintf(stderr, "Local size used :: %d\n", (int) local_size);
      sgd_node_kernel.set_work_size(graph.num_nodes(), local_size);
   }
   /************************************************************************
    *
    ************************************************************************/
   void deallocate() {
      delete features;
      delete locks;
      delete metadata;
      delete edge_metadata;
      delete edge_indices;
      delete edge_destinations;
      delete ratings;
   }
   /************************************************************************
    *
    ************************************************************************/
	void check_dst()
      {
	edge_destinations->copy_to_host();
       int counter = 0;
       for(size_t i=0; i<edge_destinations->size(); ++i){
       if(edge_destinations->host_ptr()[i]==-1)
       counter++;
       }
       fprintf(stderr, "Processed, %d, totoal, %d\t", counter,(int)edge_destinations->size());
       }

   void copy_to_device() {
      //features->copy_to_device();
      locks->copy_to_device();
      edge_destinations->copy_to_device(graph.out_neighbors());
      //edge_destinations->copy_to_device();
#if 0      
{
       int counter = 0;
       for(size_t i=0; i<locks->size(); ++i){
       if(locks->host_ptr()[i]!=-1)
       counter++;
       }
       fprintf(stderr, "Locked, %d, totoal, %d\n", counter, locks->size());
       }
       {
       int counter = 0;
       for(size_t i=0; i<edge_destinations->size(); ++i){
       if(edge_destinations->host_ptr()[i]==-1)
       counter++;
       }
       fprintf(stderr, "EDest-Locked, %d, totoal, %d\n", counter, edge_destinations->size());
       }
     #endif   
      metadata->copy_to_device();
      edge_indices->copy_to_device();
      //edge_destinations->copy_to_device();
      ratings->copy_to_device();
      movie_index->copy_to_device();
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_host() {
      features->copy_to_host();
//      locks->copy_to_host();
      metadata->copy_to_host();
   }
   /************************************************************************
    *
    ************************************************************************/
   void prepare_metadata() {
      metadata->host_ptr()[0] = graph.num_nodes();
      //metadata->host_ptr()[1] = round;
      metadata->host_ptr()[2] = graph.num_edges();
      //metadata->host_ptr()[3] step_size;
      metadata->host_ptr()[4] = movies.size();
      metadata->host_ptr()[6] = 1; //Start phase
      metadata->host_ptr()[7] = graph.num_edges(); //wl_0 size!
      metadata->host_ptr()[8] = 0; //work_counter!
      metadata->host_ptr()[9] = 0; //next_wl_counter
      sgd_node_kernel.set_arg(6, movie_index);
      if (false) {
         unsigned int counter = 0;
         for (int i = 0; i < metadata->host_ptr()[4]; ++i) {
            unsigned int movie = movies[i];
            for (unsigned int idx = graph.outgoing_index()[movie]; idx < graph.outgoing_index()[movie + 1]; ++idx) {
               //int dst = graph.out_neighbors()[idx];
               counter++;
            }
         }
//         std::cout << "Should touch :: " << counter << " edges\n";
      }
      sgd_node_kernel.set_work_size(movies.size(), sgd_node_kernel.local);
   }
   /************************************************************************
    *
    ************************************************************************/
   void initialize_features_random() {
      Galois::OpenCL::Timer timer;
      timer.start();
      SGDCommon::initialize_features_random(graph, features, locks, movies);

      movies.clear();
      //For each node, initialize features to random, and lock to be unlocked.
      memcpy(edge_indices->host_ptr(), graph.outgoing_index(), sizeof(unsigned int) * (graph.num_nodes() + 1));
      /*edge_indices->copy_to_device(graph.outgoing_index());
       edge_indices->copy_to_host();
       edge_destinations->copy_to_device(graph.out_neighbors());
       edge_destinations->copy_to_host();*/
      memcpy(edge_destinations->host_ptr(), graph.out_neighbors(), sizeof(unsigned int) * (graph.num_edges()));
      unsigned int max_degree = 0, max_degree_id = 0;
      for (unsigned int i = 0; i < graph.num_nodes(); ++i) {
         if (graph.num_neighbors(i) > max_degree) {
            max_degree = graph.num_neighbors(i);
            max_degree_id = i;
         }
         if (graph.num_neighbors(i) > 0) {
            movies.push_back(i);
         } else {

         }
      }
      max_rating = 0;
      for (unsigned int i = 0; i < graph.num_edges(); ++i) {
         max_rating = std::max(max_rating, graph.out_edge_data()[i]);
      }
      for (unsigned int i = 0; i < graph.num_edges(); ++i) {
         ratings->host_ptr()[i] = graph.out_edge_data()[i] / (float) max_rating;
      }
      //TODO: RK - shuffle if necessary. Usually a bad idea (for bgg) since locality is lost in features.
      //std::random_shuffle(movies.begin(), movies.end());
      const int items_per_chunk = ceil(graph.num_edges() / config.NUM_CHUNKS);
      fprintf(stderr, "] , max_Rating: %d, Chunk-size: %d, movies: %ld, Max degree:: %d for node: %d\n", max_rating, items_per_chunk, movies.size(), max_degree, max_degree_id);

      movie_index = new ArrayType(movies.size());
      memcpy(movie_index->host_ptr(), movies.data(), sizeof(int) * movies.size());
//      std::sort(movie_index->host_ptr(), movie_index->host_ptr()+movies.size(), [&](int m1, int m2){return graph.num_neighbors(m1)>graph.num_neighbors(m2);});
      timer.stop();
      copy_to_device();
      fprintf(stderr, "InitTime, %6.6f (s), ", timer.get_time_seconds());
      timer.clear();
      timer.start();
      timer.stop();
      fprintf(stderr, "DistTime, %6.6f (s) \n", timer.get_time_seconds());
   }
   /************************************************************************
    *
    ************************************************************************/
   void operator()(int num_steps) {
      SGDCommon::compute_err(graph, features, max_rating);
      for (round = 0; round < num_steps; ++round) {
         copy_to_device();
         float t = SGDCommon::SGD_STEP_SIZE(round);
         metadata->host_ptr()[1] = round;
         metadata->host_ptr()[3] = (t);
         metadata->host_ptr()[8] = 0; //work_counter!
         metadata->host_ptr()[9] = 0; //work_counter!
         metadata->copy_to_device();
         sgd_node_kernel.set_arg(7, sizeof(cl_float), &t);
         this->gpu_operator();
         copy_to_host();
         SGDCommon::compute_err(graph, features, max_rating);
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   void gpu_operator() {
      float kernel_time = 0;
      int steps = 0;
      Galois::OpenCL::Timer timer;
      std::vector<std::pair<int, float> > step_stats;
      step_stats.reserve(5000);
      Galois::OpenCL::OpenCL_Setup::finish();
      timer.start();
//      int prev = graph.num_edges();
	// check_dst();
//         fprintf(stderr, "Step,%d, EdgesRemaining, %d, time, %6.6g\n", steps, (int) (graph.num_edges() - metadata->host_ptr()[8]), 0);
//
      while (metadata->host_ptr()[8] != (int) graph.num_edges()) {
         steps++;
         sgd_node_kernel();
         sgd_node_kernel.wait();
         float curr_kernel_time = sgd_node_kernel.last_exec_time();
         kernel_time += curr_kernel_time;
         metadata->copy_to_host();
         step_stats.push_back(std::pair<int, float>(graph.num_edges() - metadata->host_ptr()[8], curr_kernel_time));
	// check_dst();
        // fprintf(stderr, "Step,%d, EdgesRemaining, %d, time, %6.6g\n", steps, (int) (graph.num_edges() - metadata->host_ptr()[8]), curr_kernel_time);
//         assert(prev != metadata->host_ptr()[8] && "Livelock?");
//         prev = metadata->host_ptr()[8];
      }
//      fprintf(stderr, "\n");
      timer.stop();
      static bool isfirst = true;
      if (isfirst) {
         fprintf(stderr, "Printing stats....\n");
         isfirst = false;
         std::string log_file_name((filename));
         log_file_name.append(".node_locked_stats.csv");
         std::ofstream out_file(log_file_name);
         for (auto i : step_stats) {
            out_file << i.first << "," << i.second << "\n";
         }
         out_file.close();
      }
      stats.add_stats("Round", round);
      stats.add_stats("Steps", steps);
      stats.add_stats("NodeTime", timer.get_time_seconds());
      stats.add_stats("KernelTime", kernel_time);
      stats.end_round();

//      fprintf(stderr, "Round, %d, Steps %d , Node-time(s), %6.6g, Kernel-time(s), %6.6g ", round, steps, timer.get_time_seconds(), kernel_time);
   }
   /************************************************************************
    *
    ************************************************************************/
   ~SGDNodeLocked() {
      deallocate();
      stats.print();
      fprintf(stderr, "Destroying SGDNodeLocked object.\n");
   }
};
#endif /* SGDNODE_LOCKD_H_ */
