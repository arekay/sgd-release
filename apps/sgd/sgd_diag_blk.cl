/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#define BLOCKSIZE 1

//sgd_agm_operator2<<<num_blocks, block_size>>>(features->device_ptr(), metadata->device_ptr(),new_ratings->device_ptr(), new_col->device_ptr(),new_index->device_ptr(), step_size, iter);
sgd_agm_operator2(features,metadata,new_ratings, new_col,new_index, step_size, iter);


__global__ void sgd_agm_operator2(float * features, int * metadata,
        float *ratings, int *col, int *index, float step_size, int iter) {
    const uint my_id = threadIdx.y + blockIdx.x * BLOCKSIZE;
    int movie_size = metadata[2];
//   int user_size = metadata[0];
    int start = index[iter];
//   int ty = threadIdx.y;
    int tx = threadIdx.x;
    int i, j;
    __local float _P1[BLOCKSIZE * (SGD_FEATURE_SIZE)];
    __local float _P2[BLOCKSIZE];
    float row_regs[R];
    float col_regs[C];
    //int block_i_offset, block_j_offset;
    if (my_id <= index[iter + 1] - start - 1) {
        // const int block_i_offset = R*col[start+my_id];//src_shared[ty];    //src_shared[SGD_FEATURE_SIZE*ty+ i];
        const int block_j_offset = C
                * (iter - movie_size / R + 1 + col[start + my_id]) + movie_size; //col[start+my_id];//src_shared[ty];    //src_shared[SGD_FEATURE_SIZE*ty+ i];
        #pragma unroll C
        for (j = 0; j < C; j++) {
            const int dst = block_j_offset + j;
            float * d_ptr = &features[dst * SGD_FEATURE_SIZE];
            col_regs[j] = d_ptr[tx];

        }

        #pragma unroll R
        for (i = 0; i < R; i++) {
            const int src = R * col[start + my_id] + i; //src_shared[ty];    //src_shared[SGD_FEATURE_SIZE*ty+ i];
            float * s_ptr = &features[src * SGD_FEATURE_SIZE];
            row_regs[i] = s_ptr[tx];
            for (j = 0; j < C; j++) {
                float rating = ratings[(start + my_id) * R * C + i * C + j];
                if (rating != -1) {
                  //if(tx == 0)
                  //       atomicAdd(&metadata[4], 1);
                    sgd_kernel_global_simd(&row_regs[i], &col_regs[j], rating,
                            step_size, _P1, _P2);

                }
                __syncthreads();
            }
            s_ptr[tx] = row_regs[i];
        }
        #pragma unroll C
        for (j = 0; j < C; j++) {
            const int dst = block_j_offset + j;
            float * d_ptr = &features[dst * SGD_FEATURE_SIZE];
            d_ptr[tx] = col_regs[j];

        }

    }
}
