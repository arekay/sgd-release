/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#include "cl_common.cl"
#include "apps/sgd/sgd_common.cl"
/*************************************************************************
 *
 *************************************************************************/
__kernel void matching_operator( __global int * locks, __global int * metadata, __global int * edge_info, __global int * edge_step) {
   const uint my_id = get_global_id(0);
   const int edge_start = metadata[1];
   const int edge_end = metadata[2];
   const int chunk_size = metadata[4];
   const int curr_step = metadata[5];
   __global int * work_counter = &metadata[0];
   if(my_id < chunk_size) {
      const int edge_id = edge_start + my_id;
      const int src = edge_info[2*edge_id];
      const int dst = edge_info[2*edge_id+1];
      if(src != -1)
      {
         if(lock(locks, src, dst)==true)
         {
            edge_step[edge_id]=curr_step;
            atomic_sub(work_counter,1);
            //atomic_xchg(&edge_info[2*edge_id],-1);
            edge_info[2*edge_id]=-1;
         }
      }
   }
}
/*************************************************************************
 *
 *************************************************************************/
__kernel void matching_operator2( __global int * locks, __global int * metadata, __global int * edge_info) {
   const uint my_id = get_global_id(0);
   const int edge_start = metadata[1];
   const int edge_end = metadata[2];
   const int chunk_size = metadata[4];
   const int curr_step = metadata[5];
   __global int * work_counter = &metadata[0];
   if(my_id < chunk_size) {
      const int edge_id = edge_start + my_id;
      const int src = edge_info[3*edge_id];
      const int dst = edge_info[3*edge_id+1];
      if(src != -1)
      {
         if(lock(locks, src, dst)==true)
         {
            edge_info[3*edge_id+2]=curr_step;
            atomic_sub(work_counter,1);
            edge_info[3*edge_id]=-1;
         }
      }
   }
}
/*************************************************************************
 *
 *************************************************************************/
__kernel void matching_operator_2wl( __global int * locks,
      __global int * metadata,
      __global int * edge_info,
      __global int * edge_step,
      __global int * wl_0,
      __global int * wl_1) {
   const uint my_id = get_global_id(0);
   __global int * next_work_counter= &metadata[0];
   __global int * edge_processed= &metadata[7];
   const int edge_start = metadata[1];
   const int edge_end = metadata[2];
   //const int num_edges = metadata[3];
   const int chunk_size = metadata[4];
   const int curr_step = metadata[5];
   //__global int * work_counter = &metadata[0];
   __global int * curr_wl;
   __global int * next_wl;

   if(curr_step%2==0) {
      curr_wl = wl_0;
      next_wl = wl_1;
   } else {
      curr_wl = wl_1;
      next_wl = wl_0;
   }

   if(my_id < chunk_size) {
      int edge_id = curr_wl[my_id];
      const int src = edge_info[2*edge_id];
      const int dst = edge_info[2*edge_id+1];
      {
         if(lock(locks, src, dst)==true)
         {
            edge_step[edge_id]=curr_step;
            atomic_add(edge_processed,1);
         } //end if lock succeeded
         else {
            int index = atomic_add(next_work_counter, 1);
            next_wl[index] = edge_id;
         } //end if-else
      }
   }
}
/*************************************************************************
 *
 *************************************************************************/
__kernel void sgd_dyn_operator( __global int * locks,
      __global FeatureTypeSIMD * features,
      __global int * metadata,
      __global int * edge_info,
      __global FeatureType * ratings,
      __global int * edge_step,
      __global int * wl_0,
      __global int * wl_1,
      FeatureType step_size) {
   const uint my_id = get_global_id(0);
   const int num_edges = metadata[2];
   const int chunk_size = metadata[4];
   const int curr_step = metadata[5];
   //__global int * work_counter = &metadata[0];
   __global int * next_work_counter;
   __global int * curr_wl;
   __global int * next_wl;

   next_work_counter= &metadata[0];
   if(curr_step%2==0) {
      curr_wl = wl_0;
      next_wl = wl_1;
   } else {
      curr_wl = wl_1;
      next_wl = wl_0;
   }

   if(my_id < chunk_size) {
      int edge_id = curr_wl[my_id];
      const int src = edge_info[2*edge_id];
      const int dst = edge_info[2*edge_id+1];
      //if(src != -1)
      {
         const FeatureType rating = ratings[edge_id];
         if(lock(locks, src, dst)==true)
         {
            __global FeatureTypeSIMD * s_ptr = &features[src*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
            __global FeatureTypeSIMD * d_ptr = &features[dst*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
#ifdef USE_LOCAL_CACHE //Use local caches by default. Slightly faster.
            sgd_kernel_local_simd(s_ptr,d_ptr,rating, step_size);
//            sgd_kernel_local_nosimd(s_ptr,d_ptr,rating, step_size);
#else
            sgd_kernel_global_simd(s_ptr,d_ptr,rating, step_size);
//            sgd_kernel_global_nosimd(s_ptr,d_ptr,rating, step_size);
#endif
            edge_step[edge_id]=curr_step;
            //unlock(locks, src, dst); //can release lock here as subsequent writes by thread are to non-shared data or via atomics
            //    atomic_sub(work_counter,1);
            //  atomic_xchg(&edge_info[2*edge_id],-1);
         }          //end if lock succeeded
         else {
            int index = atomic_add(next_work_counter, 1);
            next_wl[index] = edge_id;
         }          //end if-else
      }
   }
}

__kernel void unlock_kernel( __global int * locks, __global int * metadata) {
   const uint my_id = get_global_id(0);
   const int num_nodes= metadata[6];
   const int num_edges = metadata[2];
   const int chunk_size = metadata[4];
   const int curr_step = metadata[6];
   if(my_id < num_nodes) {
      //atomic_xchg(&locks[my_id], -1);
      locks[my_id]= -1;
   }
}

/*************************************************************************
 *
 *************************************************************************/
__kernel void lock_edge_operator( __global int * locks, __global FeatureTypeSIMD * features, __global int * metadata, __global int * edge_info, __global FeatureType * ratings, FeatureType step_size) {
   const uint my_id = get_global_id(0);
   const int num_edges = metadata[2];
   const int chunk_size = metadata[4];
   const int edge_start = metadata[5];
   const int edge_end = metadata[6];
   __global int * work_counter = &metadata[0];
   if(my_id < edge_end)
   {
      const int src = edge_info[2*my_id];
      const int dst = edge_info[2*my_id+1];
      if(src != -1)
      {
         const FeatureType rating = ratings[my_id];
         if(lock(locks, src, dst)==true)
         {
            __global FeatureTypeSIMD * s_ptr = &features[src*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
            __global FeatureTypeSIMD * d_ptr = &features[dst*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
#ifdef USE_LOCAL_CACHE //Use local caches by default. Slightly faster.
            sgd_kernel_local_simd(s_ptr,d_ptr,rating, step_size);
//            sgd_kernel_local_nosimd(s_ptr,d_ptr,rating, step_size);
#else
            sgd_kernel_global_simd(s_ptr,d_ptr,rating, step_size);
//            sgd_kernel_global_nosimd(s_ptr,d_ptr,rating, step_size);
#endif
            unlock(locks, src, dst); //can release lock here as subsequent writes by thread are to non-shared data or via atomics
            atomic_sub(work_counter,1);
//            atomic_xchg(&edge_info[2*my_id],-1);
            edge_info[2*my_id]=-1;
         }
      }
   }
}
/*************************************************************************
 *#######################GLOBAL_BARIIER#######################
 *************************************************************************/
void start_global_barrier(int fold, volatile __global volatile int* count) {
   barrier(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
   if(get_local_id(0) == 0) {
      atomic_add(count, 1);
   }
   barrier(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
   if(get_local_id(0) == 0) {
      int count_val = atomic_or(count,0);
         while(atomic_or(count,0)!= fold* get_num_groups(0)) {
         count_val = atomic_or(count,0);
      }
   }
   barrier(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
}
/*************************************************************************
 *#######################LOCK_FREE_EDGE_OP#######################
 *************************************************************************/
__kernel void sgd_lock_free_edge_operator( __global FeatureTypeSIMD * features,
      __global int * metadata,
      __global int * edge_info,
      __global FeatureType * ratings,
      __global int * edge_wl_indices,
      FeatureType step_size
#if PROFILE_KERNEL_TIMES
      , __global uint *kernel_times,
      __global uint * op_times,
      __global ulong * time_stamp
#endif
) {
   const uint my_id = get_global_id(0);
   const int num_edges = metadata[2];
   const int max_steps = metadata[6];
   //const int curr_step = metadata[5];
   __global int * work_counter = &metadata[0];
   __global int * barrier_start= & metadata[10];
   __global int * barrier_end= & metadata[11];

#if PROFILE_KERNEL_TIMES
   ulong op_start_time = get_time();
#endif
   //__global int * curr_step = metadata[5];

   for(int local_curr_step =0; local_curr_step<max_steps; ++local_curr_step) {
      start_global_barrier(local_curr_step+1, barrier_start);
      { //////////////////////START WORK
         const int start = edge_wl_indices[local_curr_step];
         const int end = edge_wl_indices[local_curr_step+1];
         const int num_items = end-start;
         const int items_per_thread = ceil(num_items / (float)get_global_size(0));

         for(int my_work_item = 0; my_work_item < items_per_thread; ++my_work_item)
         {
            int my_work_index = my_id + start + my_work_item*get_global_size(0);
//            int my_edge_id = start+my_work_item*
            if(my_work_index< end) {
//               atomic_add(work_counter, 1);

               const int src = edge_info[2*my_work_index];
               const int dst = edge_info[2*my_work_index+1];
               const FeatureType rating = ratings[my_work_index];
               __global FeatureTypeSIMD * s_ptr = &features[src*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
               __global FeatureTypeSIMD * d_ptr = &features[dst*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
#if PROFILE_KERNEL_TIMES
               ulong start_time = get_time();
#endif
#ifdef USE_LOCAL_CACHE //Use local caches by default. Slightly faster.
               sgd_kernel_local_simd(s_ptr,d_ptr,rating, step_size);
               //            sgd_kernel_local_nosimd(s_ptr,d_ptr,rating, step_size);
#else
               sgd_kernel_global_simd(s_ptr,d_ptr,rating, step_size);
               //          sgd_kernel_global_nosimd(s_ptr,d_ptr,rating, step_size);
#endif

#if PROFILE_KERNEL_TIMES
               if(my_id==0)
               {
                  time_stamp[my_work_index] = op_start_time;
                  kernel_times[my_work_index] = get_diff_time(start_time);
                  op_times[my_work_index] = get_diff_time(op_start_time);
                  op_start_time = get_time();
               }
#endif

            }//end if (my_work_index < end)
         }
      } //////////////////////END WORK
#if PROFILE_KERNEL_TIMES
//      op_start_time = get_time();
#endif
      start_global_barrier(local_curr_step+1, barrier_end);
   } //End for-step
}
/*************************************************************************
 *#######################LOCK_FREE_EDGE_OP#######################
 *************************************************************************/
__kernel void host_sgd_lock_free_edge_operator( __global FeatureTypeSIMD * features,
      __global int * metadata,
      __global int * edge_info,
      __global FeatureType * ratings,
      __global int * edge_wl_indices,
      FeatureType step_size) {
   const uint my_id = get_global_id(0);
   const int num_edges = metadata[2];
   const int curr_step = metadata[5];
   __global int * work_counter = &metadata[0];
   //__global int * curr_step = metadata[5];
   { //////////////////////START WORK
      const int start = edge_wl_indices[curr_step];
      const int end = edge_wl_indices[curr_step+1];
      const int num_items = end-start;
      if(my_id < num_items) {
         int my_work_index = my_id + start;
         const int src = edge_info[2*my_work_index];
         const int dst = edge_info[2*my_work_index+1];
         const FeatureType rating = ratings[my_work_index];
         __global FeatureTypeSIMD * s_ptr = &features[src*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
         __global FeatureTypeSIMD * d_ptr = &features[dst*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
#ifdef USE_LOCAL_CACHE //Use local caches by default. Slightly faster.
         sgd_kernel_local_simd(s_ptr,d_ptr,rating, step_size);
         //            sgd_kernel_local_nosimd(s_ptr,d_ptr,rating, step_size);
#else
         sgd_kernel_global_simd(s_ptr,d_ptr,rating, step_size);
         //          sgd_kernel_global_nosimd(s_ptr,d_ptr,rating, step_size);
#endif
      }

   } //End for-step
}
/*************************************************************************
 * Entry point for a AGM-N
*************************************************************************/
#if 0 // OLD CODE
__kernel void sgd_lock_free_node_operator(
      __global int * movies,
      __global FeatureTypeSIMD * features,
      __global int * metadata, __global int * edge_indices,__global int * edge_destinations,
      __global FeatureType * ratings, __global int * edge_step, FeatureType step_size
#ifdef _SGD_USE_SHARED_MEM_
      , __local FeatureTypeSIMD * cache
#endif
      ) {
   const uint my_id = get_global_id(0);
   const int max_steps = metadata[7];
   int private_work_counter = 0;
   __global int * work_counter = &metadata[8];
   __global int * barrier_start = & metadata[10];
   __global int * barrier_end = & metadata[11];
   const int movie_start = metadata[12];
   const int movie_end = metadata[13];
   bool work_done = false;
   __local FeatureTypeSIMD * src_l;
   __global FeatureTypeSIMD * s_ptr;

   if(movie_start + my_id < movie_end)
   {
      const int movie_id = movies[movie_start+ my_id];
#ifdef _SGD_USE_SHARED_MEM_
      src_l = &cache[get_local_id(0)*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
      s_ptr = &features[movie_id*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
      for(int i=0; i<SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE;++i) {
         src_l[i] = s_ptr[i];
      }
#endif
      /////////////////////////////////////////
      int edge_index=edge_indices[movie_id];
      for(int curr_step = 0; curr_step < max_steps; ++curr_step) {
         start_global_barrier(curr_step+1, barrier_start);
         if(edge_index < edge_indices[movie_id+1] && edge_step[edge_index]==curr_step) {
            const int dst = edge_destinations[edge_index];
            const FeatureType rating = ratings[edge_index];
#ifdef _SGD_USE_SHARED_MEM_
            work_done=true;
            sgd_kernel_shared_movie_simd(src_l, dst, rating, step_size, features);
#else
            __global FeatureTypeSIMD * s_ptr = &features[movie_id*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
            __global FeatureTypeSIMD * d_ptr = &features[dst*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
            sgd_kernel_global_simd(s_ptr, d_ptr, rating, step_size);
#endif
            atomic_add(work_counter,1);
            ++edge_index;
         }
         start_global_barrier(curr_step+1, barrier_end);
      }   //End for
#ifdef _SGD_USE_SHARED_MEM_
      if(work_done) {
         for(int i=0; i<SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE;++i) {
            s_ptr[i] = src_l[i];
         }   //End for
      }   //End if
#endif
   }   // End if
}   //End fun.
//END OLD CODE
#else //NEW CODE
/*************************************************************************
 * Entry point for a AGM-N
 *************************************************************************/
__kernel void sgd_lock_free_node_operator(
      __global int * movies,
      __global FeatureTypeSIMD * features,
      __global int * metadata,
      __global int * edge_indices,
      __global int * edge_destinations,
      __global FeatureType * ratings,
      __global int * edge_step,
      FeatureType step_size,
      __local FeatureTypeSIMD * cache
      ) {
   const uint my_id = get_global_id(0);
   int private_work_counter = 0;
   /*NOTE: The metadata is shared by two clients - SGM and AGMN!!!*/
   const int max_steps = metadata[6];
   __global int * work_counter = &metadata[8];
   __global int * barrier_start = & metadata[10];
   __global int * barrier_end = & metadata[11];
   const int movie_start = metadata[12];
   const int movie_end = metadata[13];
   bool work_done = false;
   __local FeatureTypeSIMD * src_l=0;
   __global FeatureTypeSIMD * s_ptr=0;
   int edge_index = 0;
#ifdef _SGD_USE_SHARED_MEM_
   //Load the movie feature to local memory
   if(movie_start + my_id < movie_end){
      const int movie_id = movies[movie_start+ my_id];
      src_l = &cache[get_local_id(0)*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
      s_ptr = &features[movie_id*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
      for(int i=0; i<SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE;++i) {
         src_l[i] = s_ptr[i];
      }//End for SGD_FEATURE_SIZE
   }//End load from cache
#endif
   if(movie_start + my_id < movie_end) {
      const int movie_id = movies[movie_start+ my_id];
      edge_index = edge_indices[movie_id];
   }

   ///#############################################################//
   for(int curr_step = 0; curr_step < max_steps; ++curr_step) {
      start_global_barrier(curr_step+1, barrier_start);
      if(movie_start + my_id < movie_end) {
         const int movie_id = movies[movie_start+ my_id];
         if(edge_index < edge_indices[movie_id+1] && edge_step[edge_index]==curr_step) {
            const int dst = edge_destinations[edge_index];
            const FeatureType rating = ratings[edge_index];
#ifdef _SGD_USE_SHARED_MEM_
            work_done=true;
            sgd_kernel_shared_movie_simd(src_l, dst, rating, step_size, features);
#else
            __global FeatureTypeSIMD * s_ptr = &features[movie_id*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
            __global FeatureTypeSIMD * d_ptr = &features[dst*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
            sgd_kernel_global_simd(s_ptr, d_ptr, rating, step_size);
#endif
            atomic_add(work_counter , 1);
            ++edge_index;
         }
      }   //End my_id check within movie bounds
      start_global_barrier(curr_step+1, barrier_end);
   }   //End for
   ///#############################################################//
#ifdef _SGD_USE_SHARED_MEM_
   //Store the movie features from local to global memory
   if(movie_start + my_id < movie_end){
      if(work_done) {
         for(int i=0; i<SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE;++i) {
            s_ptr[i] = src_l[i];
         }   //End for SGD_FEATURE_SIZE
      }//End if
   }   // End if
#endif
   ///#############################################################//
}   //End fun.
#endif //END NEW CODE
/*************************************************************************
 *#######################LOCK_FREE_EDGE_OP_COALESCED#######################
 *************************************************************************/


/*************************************************************************
 *#######################GLOBAL_BARIIER#######################
 *************************************************************************/
void start_global_barrier_y(int fold, volatile __global volatile int* count) {
   barrier(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
   if(get_local_id(1) == 0) {
      atomic_add(count, 1);
      int count_val = atomic_or(count,0);
      while(count_val < fold* get_num_groups(0)) {
         count_val = atomic_or(count,0);
      }
   }
   barrier(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
}
/*************************************************************************
 *
 *************************************************************************/
void sgd_kernel_global_simd_coal(__local FeatureType* s_ptr,
                                 __local FeatureType* d_ptr,
                                 __local FeatureType *r_ptr,
                                 FeatureType step_size) {
      const uint my_id = get_local_id(0);

      const FeatureType p_s = s_ptr[my_id];
      const FeatureType p_d = d_ptr[my_id];
      float err = r_ptr[my_id];
      r_ptr[my_id]= p_s * p_d;
      {
         float temp = 0;
         if(my_id  >= 1){
            temp = r_ptr[my_id -1];
            r_ptr[my_id]+=temp;
            temp=0;
         }
         barrier(CLK_LOCAL_MEM_FENCE);
         if(my_id  >= 2){
            temp = r_ptr[my_id -2];
            r_ptr[my_id]+=temp;
            temp=0;
                  }
         barrier(CLK_LOCAL_MEM_FENCE);
         if(my_id  >= 4){
            temp = r_ptr[my_id -4];
            r_ptr[my_id]+=temp;
            temp=0;
                  }
         barrier(CLK_LOCAL_MEM_FENCE);
         if(my_id  >= 8){
            temp = r_ptr[my_id -8];
            r_ptr[my_id]+=temp;
            temp=0;
         }
         barrier(CLK_LOCAL_MEM_FENCE);
         r_ptr[my_id] = r_ptr[get_local_size(0)-1]-err;
         barrier(CLK_LOCAL_MEM_FENCE);
      }
      const FeatureType SGD_LAMBDA=0.05f;
      {
         s_ptr[my_id] -= step_size * (r_ptr[my_id]* p_d + SGD_LAMBDA * p_s);
         d_ptr[my_id] -= step_size * (r_ptr[my_id]* p_s + SGD_LAMBDA * p_d);
      }
}
/*************************************************************************
 *
 *************************************************************************/
__kernel void sgd_lock_free_edge_coalesced_operator(
      __global FeatureType * features, /*0*/
      __global int * metadata,/*1*/
      __global int * edge_info,/*2*/
      __global FeatureType * ratings, /*3*/
      __global int * edge_wl_indices, /*4*/
      FeatureType step_size, /*5*/
      __local FeatureType * src_cache, /*6*/
      __local FeatureType * dst_cache, /*7*/
      __local FeatureType * ratings_cache /*8*/
) {
   const uint my_id = get_global_id(1);
   const uint work_idx = get_local_id(0) + get_local_id(1)*get_local_size(0);
   const int num_edges = metadata[2];
   const int max_steps = metadata[6];
   //const int curr_step = metadata[5];
   __global int * work_counter = &metadata[0];
   __global int * barrier_start= & metadata[10];
   __global int * barrier_end= & metadata[11];
   //__global int * curr_step = metadata[5];

    __local FeatureType * s_ptr = &src_cache[get_local_id(1)*get_local_size(0)];
    __local FeatureType * d_ptr = &dst_cache[get_local_id(1)*get_local_size(0)];
    __local FeatureType * r_ptr = &ratings_cache[get_local_id(1)*get_local_size(0)];

   for(int local_curr_step =0; local_curr_step<max_steps; ++local_curr_step) {
      start_global_barrier_y(local_curr_step, barrier_start);
      { //////////////////////START WORK
         const int start = edge_wl_indices[local_curr_step];
         const int end = edge_wl_indices[local_curr_step+1];
         const int num_items = end-start;
         const int items_per_thread = ceil(num_items / (float)get_global_size(1));
         for(int my_work_item = 0; my_work_item < items_per_thread; ++my_work_item)
         {
            int my_work_index = my_id + start + my_work_item*get_global_size(1);
//            int my_edge_id = start+my_work_item*
            if(my_work_index< end) {
//               atomic_add(work_counter, 1);
               const int src = edge_info[2*my_work_index];
               const int dst = edge_info[2*my_work_index+1];
               //const FeatureType rating = ratings[my_work_index];
               s_ptr[get_local_id(0)] = features[src*SGD_FEATURE_SIZE+get_local_id(0)];
               d_ptr[get_local_id(0)] = features[dst*SGD_FEATURE_SIZE+get_local_id(0)];
               r_ptr[get_local_id(0)] = ratings[my_work_index];

#ifdef USE_LOCAL_CACHE //Use local caches by default. Slightly faster.
               sgd_kernel_local_simd(s_ptr,d_ptr,rating, step_size);
               //            sgd_kernel_local_nosimd(s_ptr,d_ptr,rating, step_size);
#else
       sgd_kernel_global_simd_coal(s_ptr,d_ptr,r_ptr, step_size);
#endif
               features[src*SGD_FEATURE_SIZE+get_local_id(0)] = s_ptr[get_local_id(0)];
               features[dst*SGD_FEATURE_SIZE+get_local_id(0)] = d_ptr[get_local_id(0)];
            }
         }
      } //////////////////////END WORK
      start_global_barrier_y(local_curr_step, barrier_end);
   } //End for-step
}
/*************************************************************************
 *
 *************************************************************************/
