/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef _SGD_DYNAMIC_SCHEDULE_H_
#define _SGD_DYNAMIC_SCHEDULE_H_

#include <random>
#include <thread>
#include <atomic>
//#define OPTIMIZE_COPYING_CHUNK 1
/////////////////////////////////////////////////////////
/************************************************************************
 * SGD Static schedule, flat version.
 1) Determine matchings for all the edges.
 2) Process items per-matching w/o locks
 ************************************************************************/
using namespace Galois::OpenCL;
template<bool EdgeSchedule, bool PrintMatchingStats = false>
struct SGDAllGraphMatchingSchedule {
   typedef SGD_LC_LinearArray_Undirected_Graph<Galois::CPUArray, unsigned int, unsigned int> GraphTy;
   typedef Array<int> ArrayType;
   typedef GPUArray<int> GPUArrayType;
   typedef Array<FeatureType> FeatureArrayType;
   typedef GPUArray<FeatureType> GPUFeatureArrayType;
   typedef CL_Kernel Kernel;
   struct BlockInfo {
      int start;
      int end;
      int max_degree;
      BlockInfo(int s, int e, int m) :
            start(s), end(e), max_degree(m) {
      }
   };
   ////////////////////////////////////////////////////////////
   /************************************************
    *************************************************/
   GraphTy graph;
   std::vector<int> movies;
   std::vector<int> user_indices;
   float accumulated_error;
   int round;
   unsigned int max_rating;
   char filename[512];
   int * edge_info;

   GPUArrayType * edge_kernel_edge_info;
   ArrayType *lock_free_wl_size;

   FeatureArrayType * features;
   ArrayType *edge_step;
   FeatureArrayType * ratings;
   ArrayType *metadata;

   ArrayType *edge_indices;
   ArrayType *edge_destinations;

   ArrayType *movies_ordered;

   std::vector<BlockInfo*> movie_ranges;
#if PROFILE_KERNEL_TIMES
   Array<cl_uint> * kernel_times;
   Array<cl_uint> * op_times;
   Array<cl_ulong> * time_stamps;
#endif
   Kernel lock_free_edge_kernel;
   Kernel lock_free_node_kernel;

   int filtered_edges;
   std::map<int, int> edge_reorder_map;
   Galois::StatsManager stats;

   std::string matching_header_str; // should be either CREATE or LOAD
   float matching_time; // the time taken to either create or load the matching.
   /************************************************************************
    *
    ************************************************************************/
   SGDAllGraphMatchingSchedule(int num_movies, int num_users) :
         round(0) {
      matching_header_str = "";
      matching_time = 0;
      sprintf(filename, "gen-input_%d_%d", num_movies, num_users);
      fprintf(stderr, "Creating SGDAllGraphMatchingSchedule[%s] -  features =[%d], UseSharedMem=", EdgeSchedule ? "Edge" : "Node", SGD_FEATURE_SIZE);
#ifdef _SGD_USE_SHARED_MEM_
      fprintf(stderr, "[YES]");
#else
      fprintf(stderr, "[NO]");
#endif

      SGDCommon::complete_bipartitie(graph, num_movies, num_users);
      allocate();
      initialize_features_random();
      fprintf(stderr, "NumMovies[%ld]\n", movies.size());
   }
   /************************************************************************
     *
     ************************************************************************/
   SGDAllGraphMatchingSchedule(int num_m) :
          round(0) {
      matching_header_str = "";
      matching_time = 0;
      sprintf(filename, "gen-diagonal-input_%d", num_m);
       fprintf(stderr, "Creating SGDAllGraphMatchingSchedule[%s] -  features =[%d], UseSharedMem=", EdgeSchedule ? "Edge" : "Node", SGD_FEATURE_SIZE);
       SGDCommon::diagonal_graph(graph, num_m);
       allocate();
       initialize_features_random();
       fprintf(stderr, "Number of movies found :: %ld\n", movies.size());
    }
   /************************************************************************
    *
    *metadata (16)
    *edge_info, worklist, ratings (2+1+1)*NE
    *locks, features*64, (1+64)NN
    ************************************************************************/
   SGDAllGraphMatchingSchedule(bool road, const char * p_filename) :
         round(0) {
      matching_header_str = "";
            matching_time = 0;
      strcpy(filename, p_filename);
      fprintf(stderr, "Creating SGDAllGraphMatchingSchedule[%s] -  features =[%d], UseSharedMem=", EdgeSchedule ? "Edge" : "Node", SGD_FEATURE_SIZE);
#ifdef _SGD_USE_SHARED_MEM_
      fprintf(stderr, "[YES]");
#else
      fprintf(stderr, "[NO]");
#endif

      if (road) {
         SGDCommon::read_as_bipartite(graph, filename);
      } else {
         graph.read(p_filename);
      }
      allocate();
      initialize_features_random();
      fprintf(stderr, "NumMovies[%ld]\n", movies.size());
   }
   /************************************************************************
    *
    ************************************************************************/
   void verify_schedule() {
      std::ofstream out_file("edge_schedule_dyn.csv");
      out_file << "src,dst,step\n";
      for (size_t i = 0; i < edge_reorder_map.size(); ++i) {
         int edge_id = edge_reorder_map[i];
         int src = graph.get_edge_src(edge_id);
         int dst = graph.out_neighbors()[edge_id];
         int step = edge_step[i];
         out_file << src << "," << dst << "," << step << "\n";
      }
      out_file.close();
   }
   /************************************************************************
    *
    ************************************************************************/
#define _SGD_AGME_SORT_MATCHINGS_ 1
   void build_schedule(int num_steps) {
      std::vector<int> ** edge_id_per_step;
      edge_id_per_step = new std::vector<int>*[num_steps];
      for (int i = 0; i < num_steps; ++i) {
         edge_id_per_step[i] = new std::vector<int>();
      }
      int max_size = 0;
      for (size_t i = 0; i < edge_reorder_map.size(); ++i) {
         int edge_id = edge_reorder_map[i];
         int step = edge_step->host_ptr()[i];
         assert(step != -1 && "Unprocessed edge - invalid step!");
         edge_id_per_step[step]->push_back(edge_id);
         max_size = std::max(max_size, (int) edge_id_per_step[step]->size());
      }
      fprintf(stderr, "Max_size, %d, Reorder_size, %lu\n", max_size, edge_reorder_map.size());
//      std::cout << "Max size:: " << max_size << "Reorder size: " << edge_reorder_map.size() << "\n";
      edge_kernel_edge_info = new GPUArrayType(2 * graph.num_edges());
      lock_free_wl_size = new ArrayType(num_steps + 1);
      lock_free_edge_kernel.set_arg_list(features, metadata, edge_kernel_edge_info, ratings, lock_free_wl_size);
#if PROFILE_KERNEL_TIMES
      lock_free_edge_kernel.set_arg(6, kernel_times);
      lock_free_edge_kernel.set_arg(7, op_times);
      lock_free_edge_kernel.set_arg(8, time_stamps);

#endif
      lock_free_wl_size->host_ptr()[0] = 0;
#if _SGD_AGME_SORT_MATCHINGS_
      fprintf(stderr, "Sorting matches...!\n");
#endif
      for (int i = 0; i < num_steps; ++i) {
         lock_free_wl_size->host_ptr()[i + 1] = lock_free_wl_size->host_ptr()[i] + edge_id_per_step[i]->size();
#if _SGD_AGME_SORT_MATCHINGS_
//            std::sort(edge_id_per_step[i]->begin(), edge_id_per_step[i]->end(), [&](const int & lhs, const int & rhs) {return graph.out_neighbors()[lhs]<graph.out_neighbors()[rhs];});
            std::sort(edge_id_per_step[i]->begin(), edge_id_per_step[i]->end(), [&](const int & lhs, const int & rhs) {return graph.get_edge_src()[lhs]<graph.get_edge_src()[rhs];});
#endif
         for (size_t j = 0; j < edge_id_per_step[i]->size(); ++j) {
            int new_index = lock_free_wl_size->host_ptr()[i] + j;
            int edge_id = (*edge_id_per_step[i])[j];
            edge_step->host_ptr()[edge_id] = i;
            int src = graph.get_edge_src(edge_id);
            int dst = graph.out_neighbors()[edge_id];
            float rating = graph.out_edge_data()[edge_id] / (float) max_rating;
            edge_info[2 * new_index] = src;
            edge_info[2 * new_index + 1] = dst;
            ratings->host_ptr()[new_index] = rating;
         }
      }
      edge_kernel_edge_info->copy_to_device(edge_info);
      for (int i = 0; i < num_steps; ++i)
         delete edge_id_per_step[i];
      delete[] edge_id_per_step;
   }
   /************************************************************************
    *
    ************************************************************************/
   void build_schedule_node(int num_steps) {
      delete edge_kernel_edge_info;
      delete lock_free_wl_size;
      (void) num_steps;
      struct EdgeTuple {
         int dst;
         float wt;
         int step;
         EdgeTuple(int d, float w, int s) :
               dst(d), wt(w), step(s) {
         }
      };
      Galois::OpenCL::Timer reorder_timer;
      reorder_timer.clear();
      reorder_timer.start();
      { //1) Allocate structure,
         edge_indices = new ArrayType(graph.num_nodes() + 1);
         edge_destinations = new ArrayType(graph.num_edges());
         movies_ordered = new ArrayType(movies.size());
         std::sort(movies.begin(), movies.end(), [&](const int & lhs, const int & rhs) {return graph.num_neighbors(lhs)>graph.num_neighbors(rhs);});
         std::copy(movies.begin(), movies.end(), movies_ordered->host_ptr());
      }
      { //1.1) Sort data according to step.
         for (size_t i = 0; i < movies.size(); ++i) {
            int m = movies[i];
            std::vector<EdgeTuple> tmp_store;
            for (unsigned int e_i = graph.outgoing_index()[m]; e_i < graph.outgoing_index()[m + 1]; ++e_i) {
               int dst = graph.out_neighbors()[e_i];
               float rating = graph.out_edge_data()[e_i] / (float) (max_rating);
               int step = edge_step->host_ptr()[e_i];
               tmp_store.push_back(EdgeTuple(dst, rating, step));
            }
            std::sort(tmp_store.begin(), tmp_store.end(), [](const EdgeTuple & lhs, const EdgeTuple & rhs) {return lhs.step<rhs.step;});
            //Now copy back!
            for (unsigned int e_i = graph.outgoing_index()[m], j = 0; e_i < graph.outgoing_index()[m + 1]; ++e_i, ++j) {
               graph.out_neighbors()[e_i] = tmp_store[j].dst;
               ratings->host_ptr()[e_i] = tmp_store[j].wt; /// (float) (max_rating);
               edge_step->host_ptr()[e_i] = tmp_store[j].step;
               //tmp_store.push_back(EdgeTuple(dst, rating, step));
            }
         }
      }
      { //2) Copy data

         memcpy(edge_indices->host_ptr(), graph.outgoing_index(), sizeof(unsigned int) * graph.num_nodes());
         memcpy(edge_destinations->host_ptr(), graph.out_neighbors(), sizeof(unsigned int) * graph.num_edges());
         ratings->copy_to_device();
         edge_step->copy_to_device();
         features->copy_to_device();
         movies_ordered->copy_to_device();
      }
      unsigned long l_m_size = Galois::OpenCL::OpenCL_Setup::local_memory_size();
      size_t local_size = std::min((size_t) lock_free_node_kernel.get_default_workgroup_size(), (size_t) (l_m_size / (sizeof(float) * SGD_FEATURE_SIZE)));
      //TODO RK : Fix hack for invalid kernel sizes.
      local_size=(512+768)/2;
      //END HACK
      lock_free_node_kernel.set_work_size(local_size * Galois::OpenCL::OpenCL_Setup::get_device_eu(), local_size);
      lock_free_node_kernel.set_arg_list(movies_ordered, features, metadata, edge_indices, edge_destinations, ratings, edge_step);
      const size_t num_threads = local_size * Galois::OpenCL::OpenCL_Setup::get_device_eu();
      for (size_t i = 0; i < movies.size(); i += num_threads) {
         BlockInfo * curr_block = new BlockInfo(i, std::min(i + num_threads, movies.size()), graph.num_neighbors(movies[i]));
         movie_ranges.push_back(curr_block);
      }
      reorder_timer.stop();
      fprintf(stderr, "NodeReorderTime,%6.6g, ", reorder_timer.get_time_seconds());
//      fprintf(stderr, "Memory allocated :: %6.6g \n", (Galois::OpenCL::OpenCL_Setup::allocated_bytes)/(float)(1024*1024));
      OpenCL_Setup::stats.print();
   }
   /************************************************************************
    *
    ************************************************************************/
   void allocate() {
      features = new FeatureArrayType(graph.num_nodes() * SGD_FEATURE_SIZE);
      metadata = new ArrayType(16);
      ratings = new FeatureArrayType(graph.num_edges());
      edge_step = new ArrayType(graph.num_edges());
#if PROFILE_KERNEL_TIMES
      kernel_times = new Array<cl_uint>(graph.num_edges());
      op_times = new Array<cl_uint>(graph.num_edges());
      time_stamps= new Array<cl_ulong>(graph.num_edges());
      for(size_t i=0; i<graph.num_edges(); ++i){
         kernel_times->host_ptr()[i] = 0;
         op_times->host_ptr()[i] = 0;
         time_stamps->host_ptr()[i] = 0;
      }
//      fprintf(stderr, "Allocated timer-memory :: %u \n", graph.num_edges());
      kernel_times->copy_to_device();
      op_times->copy_to_device();
      time_stamps->copy_to_device();
      Galois::OpenCL::OpenCL_Setup::append_build_args(" -DPROFILE_KERNEL_TIMES=1 ");
#endif
      edge_info = new int[2 * graph.num_edges()];
      lock_free_edge_kernel.init("./apps/sgd/sgd_static_operator.cl", "sgd_lock_free_edge_operator");
      lock_free_node_kernel.init("./apps/sgd/sgd_static_operator.cl", "sgd_lock_free_node_operator");
   }
   /************************************************************************
    *
    ************************************************************************/
   void deallocate() {
      if (EdgeSchedule == false) {
         delete edge_indices;
         delete edge_destinations;
         delete movies_ordered;
      }
      delete features;
      delete metadata;
      delete ratings;
      delete edge_step;
#if PROFILE_KERNEL_TIMES
      delete kernel_times;
      delete op_times;
      delete time_stamps;
#endif
      delete[] edge_info;
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_device() {
      features->copy_to_device();
      ratings->copy_to_device();
      metadata->copy_to_device();
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_host() {
      features->copy_to_host();
      ratings->copy_to_host();
      metadata->copy_to_host();
   }
   /************************************************************************
    *
    ************************************************************************/
   void populate_worklist() {
      metadata->host_ptr()[0] = 0; //graph.num_edges();
      metadata->host_ptr()[1] = round;
      metadata->host_ptr()[2] = graph.num_edges();
      float t = SGDCommon::SGD_STEP_SIZE(round);
      metadata->host_ptr()[3] = (t);
      metadata->host_ptr()[4] = graph.num_edges();
      metadata->host_ptr()[5] = 0; // Step#
      metadata->host_ptr()[6] = graph.num_nodes();
      copy_to_device();
   }
   /************************************************************************
    *
    ************************************************************************/
   void initialize_features_random() {
      Galois::OpenCL::Timer timer;
      timer.start();
      SGDCommon::initialize_features_random(graph, features, movies);
      movies.clear();
      unsigned int max_degree = 0, max_degree_id = 0;
      for (unsigned int i = 0; i < graph.num_nodes(); ++i) {
         if (graph.num_neighbors(i) > max_degree) {
            max_degree = graph.num_neighbors(i);
            max_degree_id = i;
         }
         if (graph.num_neighbors(i) > 0) {
            movies.push_back(i);
         } else {
            user_indices.push_back(i);
         }
      }
      graph._max_degree_node = max_degree_id;
      graph._max_degree = max_degree;
      max_rating = 0;
      std::vector<int> user_degrees(graph.num_nodes());
      int max_user_degree =0;
      int max_user_id = 0;
      for (unsigned int i = 0; i < graph.num_edges(); ++i) {
         max_rating = std::max(max_rating, graph.out_edge_data()[i]);
         user_degrees[graph.out_neighbors()[i]]++;
         if(max_user_degree < user_degrees[graph.out_neighbors()[i]]){
            max_user_degree  = user_degrees[graph.out_neighbors()[i]];
            max_user_id= graph.out_neighbors()[i];
         }
      }
      if(max_user_degree > (int)max_degree){
         graph._max_degree = max_user_degree;
         graph._max_degree_node = max_user_id;
      }

      max_rating = 0;
      for (unsigned int i = 0; i < graph.num_edges(); ++i) {
         max_rating = std::max(max_rating, graph.out_edge_data()[i]);
      }
      timer.stop();
      fprintf(stderr, "] , max_Rating: %d, nodes:%ld, edges: %ld, movies: %ld, users: %ld, max_user_degree:: %d, Max degree:: %d for node: %d\n",
                        max_rating, graph.num_nodes() , graph.num_edges(),movies.size(), user_indices.size() ,max_user_degree, max_degree, max_degree_id);

//      fprintf(stderr, "] , max_Rating: %d, movies: %ld, Max degree:: %d for node: %d\n", max_rating, movies.size(), max_degree, max_degree_id);
      SGDCommon::print_distributions(graph);
      fprintf(stderr, "InitTime [%6.6f s], ", timer.get_time_seconds());
      timer.clear();
      timer.start();
      distribute_chunks();
      timer.stop();
      fprintf(stderr, "DistTime [%6.6f s], ", timer.get_time_seconds());
      timer.clear();
   }
   /************************************************************************
    *
    ************************************************************************/
   void distribute_chunks() {
      std::vector<int> in_edge_wl(graph.num_edges());
      std::vector<int> edge_wl;
      for (size_t i = 0; i < graph.num_edges(); ++i) {
         in_edge_wl[i] = i;
      }
      SGDCommon::edge_filter(SGDCommon::EdgeFilterType::IdentityFilter, graph, in_edge_wl, edge_wl);
//      SGDCommon::edge_filter(SGDCommon::EdgeFilterType::AverageRatingsHighDegreeMovie, graph, in_edge_wl, edge_wl);
//      SGDCommon::edge_filter(SGDCommon::EdgeFilterType::HighRatingsHighDegreeMovie, graph, in_edge_wl, edge_wl);
//      SGDCommon::edge_filter(SGDCommon::EdgeFilterType::LowRatings, graph, in_edge_wl, edge_wl);
//      SGDCommon::edge_filter(SGDCommon::EdgeFilterType::LowDegreeMovie, graph, in_edge_wl, edge_wl);

      /*edge_wl.resize(in_edge_wl.size());
       std::copy(in_edge_wl.begin(), in_edge_wl.end(), edge_wl.begin());*/

      filtered_edges = edge_wl.size();
      edge_reorder_map.clear();
      for (int j = 0; j < filtered_edges; ++j) {
         int edge_index = edge_wl[j];
         edge_reorder_map[j] = edge_index;
         int src = graph.get_edge_src(edge_index);
         int dst = graph.out_neighbors()[edge_index];
         float wt = graph.out_edge_data()[edge_index] / (float) max_rating;
         edge_info[2 * j] = src;
         edge_info[2 * j + 1] = dst;
         ratings->host_ptr()[j] = wt;
         edge_step->host_ptr()[j] = -1;
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   void operator()(int num_steps) {
      round = 0;
      std::string log_file_name(basename(filename));
      log_file_name.append(EdgeSchedule ? "AllGraphEdge" : "AllGraphNode");
      int steps_per_round = 0;
      Galois::OpenCL::Timer matching_timer;
      matching_timer.clear();
      { //Check if file exists...
         std::string matching_filename(log_file_name);
         matching_filename.append(".matching");
         ifstream matching_file(matching_filename, ios::binary);
         if (matching_file.good()) {
            fprintf(stderr, "Matching info found... loading: %s \n", matching_filename.c_str());
            matching_timer.start();
            steps_per_round = SGDCommon::BuildMatchingCompact::from_file(matching_file, graph.num_edges(), edge_info, edge_step->host_ptr());
            matching_timer.stop();
            matching_time = matching_timer.get_time_seconds();
            matching_header_str="MatchLoad";
            edge_step->copy_to_device(); //To ensure device has a copy as well.
         } else {
            fprintf(stderr, "Matching info NOT found... building: %s \n", matching_filename.c_str());
            SGDCommon::BuildMatchingCompact matching(graph.num_nodes(), graph.num_edges(), edge_info, edge_step, log_file_name);
            matching_timer.start();
            steps_per_round = matching.operator()<PrintMatchingStats>(0, graph.num_edges());
            matching_timer.stop();
            matching_time = matching_timer.get_time_seconds();
            matching_header_str="MatchCreate";
            matching.to_file(matching_filename);
         }
      }
      fprintf(stderr, "Allocated post-matching %6.6g, MaxMatching %d\n",  (Galois::OpenCL::OpenCL_Setup::stats.allocated/(float)(1024*1024)), steps_per_round);
      build_schedule(steps_per_round);
      copy_to_device();
      if (EdgeSchedule == true) {
         fprintf(stderr, "BuidlEdgeSchedule :: Allocated %6.6g\n",  (Galois::OpenCL::OpenCL_Setup::stats.allocated/(float)(1024*1024)));
         SGDCommon::compute_err(graph, features, max_rating);
         for (; round < num_steps; ++round) {
            this->lock_free_edge_operator(steps_per_round);
            SGDCommon::compute_err(graph, features, max_rating);
         }
      } else {
         //This reorders the edges per-node to obey the iteration order
         //as determined by the matchings.
         fprintf(stderr, "BuidlNodeSchedule :: Allocated %6.6g\n",  (Galois::OpenCL::OpenCL_Setup::stats.allocated/(float)(1024*1024)));
         build_schedule_node(steps_per_round);
         fprintf(stderr, "Number of blocks :: %d\n", (int)movie_ranges.size());
         SGDCommon::compute_err(graph, features, max_rating);
         for (; round < num_steps; ++round) {
            this->lock_free_node_operator(steps_per_round);
            SGDCommon::compute_err(graph, features, max_rating);
         }
      }
   }
   /************************************************************************
    *
    ************************************************************************/

   void lock_free_node_operator(int num_steps) {
      Galois::OpenCL::Timer local_timer;
      float copy_time = 0;
      float setup_time = 0;
      float kernel_time = 0;
      local_timer.clear();
      local_timer.start();
      //3) Execute kernel
      unsigned long l_m_size = Galois::OpenCL::OpenCL_Setup::local_memory_size();
      size_t local_size = std::min((size_t) lock_free_node_kernel.get_default_workgroup_size(), (size_t) (l_m_size / (sizeof(float) * SGD_FEATURE_SIZE)));
      //TODO RK : Fix hack for invalid kernel sizes.
            local_size=(512+768)/2;
            //END HACK
//      fprintf(stderr, "Local workgroup size :: %d \n",(int) lock_free_node_kernel.get_default_workgroup_size());
      float f = SGDCommon::SGD_STEP_SIZE(round);
      //movies,features,metadata, edge_indices,edge_destinations,ratings, edge_step, step_size, cache
      lock_free_node_kernel.set_arg(7, sizeof(float), &f);
#ifdef _SGD_USE_SHARED_MEM_
      lock_free_node_kernel.set_arg(8, sizeof(float) * SGD_FEATURE_SIZE * local_size, NULL);
#endif
      metadata->host_ptr()[4] = movies.size();
      metadata->host_ptr()[0] = graph.num_nodes();
      metadata->host_ptr()[2] = graph.num_edges();
      metadata->host_ptr()[8] = 0; //work_counter
      metadata->host_ptr()[6] = num_steps; //num_steps
      local_timer.stop();
      setup_time = local_timer.get_time_seconds();
      local_timer.clear();
      local_timer.start();
      std::vector<std::pair<int, float>> range_stats;
      //int edges_processed = 0;
      for (auto range : movie_ranges) {
         metadata->host_ptr()[10] = 0; //barrier_start
         metadata->host_ptr()[11] = 0; //barrier_end
         metadata->host_ptr()[12] = range->start; //movies_start
         metadata->host_ptr()[13] = range->end; //movies_end
         metadata->host_ptr()[6] = num_steps; //range.max_degree;//
         metadata->copy_to_device();
         lock_free_node_kernel.set_work_size(std::min(local_size * Galois::OpenCL::OpenCL_Setup::get_device_eu(), size_t(range->end - range->start)), local_size);
         lock_free_node_kernel();
         Galois::OpenCL::OpenCL_Setup::finish();
         float f = lock_free_node_kernel.last_exec_time();
//         edges_processed+=range->end-range->start;
         //fprintf(stderr, "[Steps:%d, EdgesProcessed: %d, %6.6g,%6.6g]\n", num_steps, edges_processed, f, kernel_time);
         metadata->copy_to_host();
         //edges_processed=  metadata->host_ptr()[8];
//         fprintf(stderr, "[Steps:%d, EdgesProcessed: %d, %6.6g,%6.6g]\n", num_steps, edges_processed, f, kernel_time);
         kernel_time += f;
         range_stats.push_back(std::pair<int, float>(metadata->host_ptr()[8], f));
      }

      local_timer.stop();
      float exec_time = local_timer.get_time_seconds();
      //metadata->copy_to_host();
      stats.add_stats("Round",round);
      stats.add_stats("Edges",metadata->host_ptr()[8]);
      stats.add_stats("Setup(s)",setup_time);
      stats.add_stats("TotalTime",exec_time+copy_time);
      stats.add_stats("GPUWork",exec_time);
      stats.add_stats("Kernel",kernel_time);
      stats.add_stats(matching_header_str, matching_time);
      stats.end_round();
//      stats.add_stats("",);
//      fprintf(stderr, "E:%d, ", metadata->host_ptr()[8]);
//      fprintf(stderr, "Round, %d, Setup(s), %6.6g, Total-time(s), %6.6g ,GPU_Work(s), %6.6g, Kernel(s), %6.6g \t",
//            round, setup_time, exec_time + copy_time, exec_time, kernel_time);
      { //4) Copy results
         features->copy_to_host();
      }

   }
   /************************************************************************
    *
    ************************************************************************/
   void lock_free_edge_operator(int num_steps) {
      Galois::OpenCL::Timer timer;
      timer.start();
      copy_to_device();
      lock_free_wl_size->copy_to_device();
      float f = SGDCommon::SGD_STEP_SIZE(round);
      lock_free_edge_kernel.set_arg(5, sizeof(float), &f);
      metadata->host_ptr()[0] = graph.num_edges();
      timer.stop();
      float copy_time = timer.get_time_seconds();
      timer.clear();
      timer.start();
      float kernel_time = 0;
//      lock_free_edge_kernel.set_work_size(26869);
#if 0 //Host-side barrier
      for (int i = 0; i < num_steps; ++i) {
         metadata->host_ptr()[5] = i;
         metadata->copy_to_device();
         int num_items = lock_free_wl_size->host_ptr()[i + 1] - lock_free_wl_size->host_ptr()[i];
         lock_free_edge_kernel.set_work_size(num_items);
         lock_free_edge_kernel();
         kernel_time+=lock_free_edge_kernel.last_exec_time();
//         metadata->copy_to_host();
      }
#else
      //Device side barrier
      metadata->host_ptr()[0] = 0;
      metadata->host_ptr()[2] = graph.num_edges();
      metadata->host_ptr()[6] = num_steps;
      metadata->host_ptr()[5] = 0;
      metadata->host_ptr()[10] = 0;
      metadata->host_ptr()[11] = 0;
      metadata->copy_to_device();
      lock_free_edge_kernel.set_work_size(1);
      fprintf(stderr, "Threads: Local= %lu, EU = %u, ", lock_free_edge_kernel.local , Galois::OpenCL::OpenCL_Setup::get_device_eu());
      lock_free_edge_kernel.set_work_size(lock_free_edge_kernel.local * Galois::OpenCL::OpenCL_Setup::get_device_eu());
      lock_free_edge_kernel();
      Galois::OpenCL::OpenCL_Setup::finish();
      metadata->copy_to_host();
      fprintf(stderr, "E :%d, ", metadata->host_ptr()[0]);
      kernel_time += lock_free_edge_kernel.last_exec_time();
#endif
      timer.stop();
#if PROFILE_KERNEL_TIMES
      kernel_times->copy_to_host();
      op_times->copy_to_host();
      time_stamps->copy_to_host();
      int * matching_number = new int [graph.num_edges()];
      for(auto m_number = 0; m_number < num_steps; ++m_number){
         for(auto m_step = lock_free_wl_size->host_ptr()[m_number];m_step <lock_free_wl_size->host_ptr()[m_number+1]; ++m_step ){
            matching_number[m_step]=  m_number;
         }
      }
      std::ofstream out_file ("agme_profile_edges.log");
//      out_file << " LOG FILE " << filename << " created \n";
//      out_file << " Src,Dst,KTime, OTime, Step,TimeStamp\n";
      for(size_t i=0; i<graph.num_edges(); ++i){
         int src = this->edge_info[2*i];
         int dst = this->edge_info[2*i+1];
//         if(kernel_times->host_ptr()[i]!=0)
//         if(src==(int)(graph._max_degree_node) || dst==(int)(graph._max_degree_node))
            if(time_stamps->host_ptr()[i]!=0){
            out_file << src << " , " << dst << " , " << kernel_times->host_ptr()[i] << ", " << op_times->host_ptr()[i] << ", " << matching_number[i] << ", "<<time_stamps->host_ptr()[i]<<"\n";
         }
      }
      delete [] matching_number;
#endif

      float exec_time = timer.get_time_seconds();
      stats.add_stats("Round",round);
      stats.add_stats("Steps",num_steps);
      stats.add_stats("ETotalTime",exec_time+copy_time);
      stats.add_stats("GPUWork",exec_time);
      stats.add_stats("Kernel",kernel_time);
      stats.add_stats(matching_header_str, matching_time);
      stats.end_round();
//      fprintf(stderr, "Round, %d, Steps, %d , Total-time(s), %6.6g ,GPU_Work(s), %6.6g, Kernel(s), %6.6g \t", round, num_steps, exec_time + copy_time, exec_time, kernel_time);
      copy_to_host();
   }
   /************************************************************************
    *
    ************************************************************************/
   ~SGDAllGraphMatchingSchedule() {
      deallocate();
      stats.print();
      fprintf(stderr, "Destroying SGDAllGraphMatchingSchedule[%s] -  features =[%d].\n", EdgeSchedule ? "Edge" : "Node", SGD_FEATURE_SIZE);
   }
};
//###################################################################//
#endif /* _SGD_DYNAMIC_SCHEDULE_H_ */
