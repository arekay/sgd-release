/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef SGDCHUNKED2WLEDGE_H_
#define SGDCHUNKED2WLEDGE_H_
#include <random>
#define SGD2WL_RANDOMIZE_EDGE 1
/////////////////////////////////////////////////////////
/************************************************************************
 *
 ************************************************************************/
using namespace Galois::OpenCL;
struct SGDChunked2WLEdgeFunctor {
   typedef SGD_LC_LinearArray_Undirected_Graph<Galois::CPUArray, unsigned int, unsigned int> GraphTy;
   typedef Array<int> ArrayType;
   typedef cl_float FeatureType;
   typedef Array<FeatureType> FeatureArrayType;
   typedef CL_Kernel Kernel;
   const int NUM_CHUNKS;
   GraphTy graph;
   std::vector<int> movies;
   ArrayType * edge_wl;
   ArrayType * edge_wl_next;
   ArrayType * metadata;
   ArrayType * locks;
   ArrayType * edge_info;
   FeatureArrayType * features;
   FeatureArrayType * ratings;
   std::vector<int> **all_edges;
   Kernel sgd_kernel;
   int num_diagonals;
   float accumulated_error;
   int round;
   unsigned int max_rating;
   /************************************************************************
    *
    ************************************************************************/
   SGDChunked2WLEdgeFunctor(const char * filename) :
         NUM_CHUNKS(1) {
      round = 0;
      fprintf(stderr, "Creating SGDChunked2WLEdgeFunctor - [%d] features, [%d] Chunks.\n", SGD_FEATURE_SIZE, NUM_CHUNKS);
      std::cout << "Checking sizes: int: " << sizeof(int) << ", cl_int: " << sizeof(cl_int) << ", float: " << sizeof(float) << " ,cl_float: " << sizeof(cl_float) << "\n";
      graph.read(filename);
      allocate();
      initialize_features_random();
      std::cout << "Number of movies found :: " << movies.size() << "\n";
      sgd_kernel.init("sgd_edge_operator.cl", "sgd_edge_operator_2wl");
      //volatile __global int * edge_info, volatile __global int * locks, volatile __global int * wl_0, volatile __global int * wl_1, volatile __global int * metadata, volatile __global FeatureType * features, volatile __global FeatureType * ratings, FeatureType step_size
      sgd_kernel.set_arg_list(edge_info, locks, edge_wl, edge_wl_next, metadata, features, ratings);
      if (false) {
         std::ofstream header("graph_header.h");
         header << Galois::OpenCL::str_SGD_LC_LinearArray_Undirected_Graph << "\n";
         header.close();
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   void allocate() {
      features = new FeatureArrayType(graph.num_nodes() * SGD_FEATURE_SIZE);
      fprintf(stderr, "Size of edges :: %6.6g MB\n", (float) (graph.num_edges() * sizeof(unsigned int) / (1024 * 1024)));
      metadata = new ArrayType(16);
      locks = new ArrayType(graph.num_nodes());
      all_edges = new std::vector<int>*[NUM_CHUNKS];
      unsigned int gpu_wl_size = ceil(graph.num_edges() / (float) NUM_CHUNKS);
      for (int i = 0; i < NUM_CHUNKS; ++i)
         all_edges[i] = new std::vector<int>();
      edge_wl = new ArrayType(gpu_wl_size);
      edge_wl_next = new ArrayType(gpu_wl_size);
      edge_info = new ArrayType(2 * gpu_wl_size);
      ratings = new FeatureArrayType(gpu_wl_size);
   }
   /************************************************************************
    *
    ************************************************************************/
   void deallocate() {
      for (int i = 0; i < NUM_CHUNKS; ++i)
         delete all_edges[i];
      delete[] all_edges;
      delete features;
      delete edge_info;
      delete edge_wl;
      delete edge_wl_next;
      delete metadata;
      delete locks;
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_device() {
      features->copy_to_device();
      locks->copy_to_device();
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_host() {
      features->copy_to_host();
      locks->copy_to_host();
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_chunk_to_host() {
      metadata->copy_to_host();
      edge_info->copy_to_host();
      edge_wl->copy_to_host();
      ratings->copy_to_host();
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_chunk_to_device() {
      metadata->copy_to_device();
      edge_info->copy_to_device();
      edge_wl->copy_to_device();
      ratings->copy_to_device();
   }
   /************************************************************************
    *
    ************************************************************************/
   void populate_worklist(int chunk_id) {
      //fprintf(stderr, "Populating %d with %d elements\n", chunk_id, all_edges[chunk_id]->size());
      for (unsigned int i = 0; i < all_edges[chunk_id]->size(); ++i) {
         const int id = (*all_edges[chunk_id])[i];
         const int src = graph.get_edge_src(id);
         const int dst = graph.out_neighbors()[id];
         const float rating = graph.out_edge_data()[id] / (float) max_rating;
         assert(src >= 0 && src < (int) graph.num_nodes());
         assert(dst >= 0 && dst < (int) graph.num_nodes());
         ///Write to GPU-array
         edge_info->host_ptr()[2 * i] = src;
         edge_info->host_ptr()[2 * i + 1] = dst;
         ratings->host_ptr()[i] = rating;
         edge_wl->host_ptr()[i] = i;
         //fprintf(stderr, "[%d, %d, %6.6g] ", src, dst, rating);
      }
      {
         metadata->host_ptr()[0] = all_edges[chunk_id]->size();
         metadata->host_ptr()[1] = round;
         metadata->host_ptr()[2] = graph.num_edges();
         float t = SGDCommon::SGD_STEP_SIZE(round);
         metadata->host_ptr()[3] = (t);
         metadata->host_ptr()[4] = 0; //current phase (initially 0)
         //WL sizes for alternating phases.
         metadata->host_ptr()[5] = all_edges[chunk_id]->size();
         metadata->host_ptr()[6] = 0;
         //       fprintf(stderr, "Step-size:: %6.6g, Size: %d\t", t, metadata->host_ptr()[0]);
         sgd_kernel.set_arg(7, sizeof(cl_float), &t);
         sgd_kernel.set_work_size(all_edges[chunk_id]->size());
      }

   }
   /************************************************************************
    *
    ************************************************************************/

   void initialize_features_random() {
      Galois::OpenCL::Timer timer;
      timer.start();
      SGDCommon::initialize_features_random(graph, features, locks, movies);

      max_rating = 0;
      for (unsigned int i = 0; i < graph.num_edges(); ++i) {
         max_rating = std::max(max_rating, graph.out_edge_data()[i]);
      }
      const int items_per_chunk = ceil(graph.num_edges() / NUM_CHUNKS);
      std::cout << "] , max_Rating: " << max_rating << ", Chunk-size: " << items_per_chunk << ", movies: " << movies.size() << "\n";
      if (false) {
         std::ofstream header("/workspace/rashid/netflix_deg.csv");
         std::vector<int> edge_count;
         for (unsigned int it = 0; it < movies.size(); ++it) {
            unsigned int i = movies[it];
            edge_count.push_back(graph.outgoing_index()[i + 1] - graph.outgoing_index()[i]);
         }
         std::sort(edge_count.data(), edge_count.data() + movies.size());
         for (unsigned int i = 0; i < edge_count.size(); ++i)
            header << edge_count[i] << "\n";
         header.close();
      }
      distribute_chunks();
   }
   /************************************************************************
    *
    ************************************************************************/
   void distribute_chunks() {
#ifdef SGD2WL_RANDOMIZE_EDGE
      {
         std::vector<int>***diagonals; // diagonals[i][segment][item#];
         const int num_diagonals = movies.size();
         diagonals = new std::vector<int>**[num_diagonals];
         const int num_segments = ceil((graph.num_nodes() - movies.size()) / (float) num_diagonals) + 1;
         fprintf(stderr, "Diagonals[%d], Segments[%d] ", num_diagonals, num_segments);
         for (int i = 0; i < num_diagonals; ++i) {
            diagonals[i] = new std::vector<int>*[num_segments];
            for (int j = 0; j < num_segments; ++j) {
               diagonals[i][j] = new std::vector<int>();
               diagonals[i][j]->reserve(graph.num_edges() / num_diagonals);
            }
         }
         int max_diag = 0, max_seg = 0;
         for (unsigned int i = 0; i < graph.num_edges(); ++i) {
            const unsigned int src = graph.get_edge_src()[i];
            const unsigned int dst = graph.out_neighbors()[i];
            const int diag = abs((int)(src - dst)) % num_diagonals;
            const int seg = floor(abs((int)(src - dst)) / (float) num_diagonals);
            max_diag = std::max(max_diag, diag);
            max_seg = std::max(max_seg, seg);
            diagonals[diag][seg]->push_back(i);
         }
         fprintf(stderr, "Max-seg [%d, %d] ", max_diag, max_seg);
         int sum = 0;
         int max_size = 0;
         int min_size = graph.num_edges();
         for (int i = 0; i < num_diagonals; ++i) {
            //fprintf(stderr, "%d :: ",i);
            for (int j = 0; j < num_segments; ++j) {
               //fprintf(stderr, "[%d]",diagonals[i][j]->size());
               sum += diagonals[i][j]->size();
               max_size = std::max(max_size, (int) diagonals[i][j]->size());
               min_size = std::min(min_size, (int) diagonals[i][j]->size());
            }
            //fprintf(stderr, "\n");
         }
         assert(sum == (int) graph.num_edges());
         fprintf(stderr, "Total:: %d, max:: %d , min:: %d\n", sum, max_size, min_size);
         const int diags_per_chunk = ceil(num_diagonals / (float) NUM_CHUNKS);
         unsigned int chunk_sum = 0;
         int max_chunk_size = 0;
         fprintf(stderr, "Chunks:: ");
         for (int c = 0; c < NUM_CHUNKS; ++c) {
            for (int j = 0; j < diags_per_chunk; ++j) {
               for (int s = 0; s < num_segments && c * diags_per_chunk + j < num_diagonals; ++s) {
                  std::vector<int> & vec = *diagonals[c * diags_per_chunk + j][s];
                  for (auto val : vec)
                     all_edges[c]->push_back(val);
               }
            }
            fprintf(stderr, "[%lu]", all_edges[c]->size());
            chunk_sum += all_edges[c]->size();
            max_chunk_size = std::max(max_chunk_size, (int) all_edges[c]->size());
         }
         fprintf(stderr, "\t Chunk sizes:: [%d],", chunk_sum);
         delete edge_wl;
         delete edge_info;
         delete ratings;
         edge_wl = new ArrayType(max_chunk_size);
         edge_info = new ArrayType(2 * max_chunk_size);
         ratings = new FeatureArrayType(max_chunk_size);

         assert(chunk_sum == graph.num_edges());
         //cleanup
         for (int i = 0; i < num_diagonals; ++i) {
            for (int j = 0; j < num_segments; ++j) {
               delete diagonals[i][j];
            }
            delete[] diagonals[i];
         }
         delete[] diagonals;

      }
#else
      const int items_per_chunk = ceil(graph.num_edges() / NUM_CHUNKS);
      int * edge_wl = new int[graph.num_edges()];
      for (size_t i = 0; i < graph.num_edges(); ++i) {
         edge_wl[i] = i;
      }
      std::random_shuffle(edge_wl, &edge_wl[graph.num_edges()]);
      for (int i = 0; i < NUM_CHUNKS; ++i) {
         int * start = edge_wl + i * items_per_chunk;
         int num_items = std::min((int) graph.num_edges() - (i * items_per_chunk), (int) items_per_chunk);
         all_edges[i]->reserve(num_items);
         //fprintf(stderr, "%d->[%d]", i, num_items);
         for (int j = 0; j < num_items; ++j) {
            all_edges[i]->push_back(start[j]);
         }
         //memcpy(all_edges[i]->data(), start, num_items);
      }
      delete edge_wl;
#endif
   }
   /************************************************************************
    *
    ************************************************************************/
   void operator()(int num_steps) {
      SGDCommon::compute_err(graph, features, max_rating);
      for (round = 0; round < num_steps; ++round) {
         //initialize_worklist();
         //this->operator ()();
         this->gpu_operator();
      }
      SGDCommon::compute_err(graph, features, max_rating);
   }
   /************************************************************************
    *
    ************************************************************************/
   void gpu_operator() {
      int i = 0;
      std::vector<int> steps_per_round;
      copy_to_device();
      Galois::OpenCL::Timer timer;
      float kernel_time = 0.0f;
      timer.start();
      for (int c = 0; c < NUM_CHUNKS; ++c) {
         i = 0;
         //fprintf(stderr, "Processing chunk :: %d, size :: %d \n", c, all_edges[c]->size());
         populate_worklist(c);
         copy_chunk_to_device();
         Galois::OpenCL::Timer kernel_timer;
         kernel_timer.start();
         do {
            metadata->host_ptr()[4] = i;
            metadata->copy_to_device();
            sgd_kernel();
            metadata->copy_to_host();
            //fprintf(stderr, "Step[%d], [%d],[%d, %d] \n", i, metadata->host_ptr()[0],metadata->host_ptr()[5],metadata->host_ptr()[6]);
#ifndef EARLY_APPROXIMATE_TERMINATION //If 2% of iterations can be ignored for better performance.
            float ratio_remaining = metadata->host_ptr()[0] / (float) all_edges[c]->size();
            if (ratio_remaining < 0.02f)
               break;
#endif
            sgd_kernel.set_work_size(metadata->host_ptr()[5 + (i + 1) % 2]);
            metadata->host_ptr()[5 + i % 2] = 0;
            ++i;
         } while (metadata->host_ptr()[0] > 0);
         kernel_timer.stop();
         kernel_time += kernel_timer.get_time_seconds();
         steps_per_round.push_back(i);
      }
      timer.stop();
      fprintf(stderr, "Round: [%d], Total time: %6.6gs ,GPU_Work: %6.6g , :: [", round, timer.get_time_seconds(), kernel_time);
      for (auto it : steps_per_round) {
         fprintf(stderr, "%d, ", it);
      }
      fprintf(stderr, "]\n");

   }
   /************************************************************************
    *
    ************************************************************************/
   ~SGDChunked2WLEdgeFunctor() {
      deallocate();
      fprintf(stderr, "Destroying SGDChunked2WLEdgeFunctor object.\n");
   }
};
//###################################################################//
#endif /* SGDCHUNKED2WLEDGE_H_ */
