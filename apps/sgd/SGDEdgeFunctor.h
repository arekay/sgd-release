/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef SGDEDGEFUNCTOR_H_
#define SGDEDGEFUNCTOR_H_

#include <random>
/////////////////////////////////////////////////////////
/************************************************************************
 * Edge functor that does in-place SGD processing. All data should fit in the memory.
 ************************************************************************/
using namespace Galois::OpenCL;
struct SGDEdgeFunctor {
   typedef SGD_LC_LinearArray_Undirected_Graph<Galois::CPUArray, unsigned int, unsigned int> GraphTy;
   typedef Array<int> ArrayType;
   typedef cl_float FeatureType;
   typedef Array<FeatureType> FeatureArrayType;
   typedef CL_Kernel Kernel;
   GraphTy graph;
   std::vector<int> movies;
   ArrayType * edge_wl;
   ArrayType * metadata;
   ArrayType * locks;
   ArrayType * edge_info;
   FeatureArrayType * features;
   FeatureArrayType * ratings;
   Kernel sgd_kernel;
   int num_diagonals;
   float accumulated_error;
   int round;
   unsigned int max_rating;
   /************************************************************************
    *
    ************************************************************************/
   SGDEdgeFunctor(bool road, const char * filename) {
      round = 0;
      fprintf(stderr, "Creating SGDEdgeFunctor- [%d] features.\n", SGD_FEATURE_SIZE);
      std::cout << "Checking sizes: int: " << sizeof(int) << ", cl_int: " << sizeof(cl_int) << ", float: " << sizeof(float) << " ,cl_float: " << sizeof(cl_float) << "\n";
      if (road) {
         SGDCommon::read_as_bipartite(graph, filename);
      } else {
         graph.read(filename);
      }
      allocate();
      initialize_features_random();
      std::cout << "Number of movies found :: " << movies.size() << "\n";
      sgd_kernel.init("sgd_edge_operator.cl", "sgd_edge_functor_op");
      sgd_kernel.set_arg_list(edge_info, locks, edge_wl, metadata, features, ratings);
      if (false) {
         std::ofstream header("graph_header.h");
         header << Galois::OpenCL::str_SGD_LC_LinearArray_Undirected_Graph << "\n";
         header.close();
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   void allocate() {
      features = new FeatureArrayType(graph.num_nodes() * SGD_FEATURE_SIZE);
      fprintf(stderr, "Size of edges :: %6.6g MB\n", (float) (graph.num_edges() * sizeof(unsigned int) / (1024 * 1024)));
      edge_info = new ArrayType(2 * graph.num_edges());
      ratings = new FeatureArrayType(graph.num_edges());
      edge_wl = new ArrayType(graph.num_edges());
      metadata = new ArrayType(16);
      locks = new ArrayType(graph.num_nodes());
   }
   /************************************************************************
    *
    ************************************************************************/
   void deallocate() {
      delete features;
      delete edge_info;
      delete edge_wl;
      delete metadata;
      delete locks;
   }
   /************************************************************************
    *
    ************************************************************************/
   void initialize_worklist() {
      for (size_t i = 0; i < graph.num_edges(); ++i) {
         edge_wl->host_ptr()[i] = i;
         edge_info->host_ptr()[2 * i + 0] = graph.get_edge_src(i);
         edge_info->host_ptr()[2 * i + 1] = graph.out_neighbors()[i];
      }
      for (size_t i = 0; i < graph.num_nodes(); ++i) {
         locks->host_ptr()[i] = -1;
      }
      std::random_shuffle(edge_wl->host_ptr(), &edge_wl->host_ptr()[graph.num_edges()]);
      {
         metadata->host_ptr()[0] = graph.num_edges();
         metadata->host_ptr()[1] = round;
         metadata->host_ptr()[2] = graph.num_edges();
         float t = SGDCommon::SGD_STEP_SIZE(round);
         metadata->host_ptr()[3] = (t);
         fprintf(stderr, "Step-size:: %6.6g\t", t);
         sgd_kernel.set_arg(6, sizeof(cl_float), &t);
      }
      copy_to_device();
      sgd_kernel.set_work_size(graph.num_edges());

   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_device() {
      metadata->copy_to_device();
      features->copy_to_device();
      edge_info->copy_to_device();
      edge_wl->copy_to_device();
      locks->copy_to_device();
      ratings->copy_to_device();
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_host() {
      metadata->copy_to_host();
      features->copy_to_host();
      edge_info->copy_to_host();
      edge_wl->copy_to_host();
      locks->copy_to_host();
      ratings->copy_to_host();
   }
   /************************************************************************
    *
    ************************************************************************/

   void initialize_features_random() {
      Galois::OpenCL::Timer timer;
      timer.start();
      SGDCommon::initialize_features_random(graph, features, locks, movies);

      max_rating = 0;
      for (unsigned int i = 0; i < graph.num_edges(); ++i) {
         max_rating = std::max(max_rating, graph.out_edge_data()[i]);
      }
      for (unsigned int i = 0; i < graph.num_edges(); ++i) {
         ratings->host_ptr()[i] = graph.out_edge_data()[i] / (float) (max_rating);
      }
      std::cout << "] , max_Rating: " << max_rating << "\n";
   }
   /************************************************************************
    *
    ************************************************************************/
   void operator()(int num_steps) {
      SGDCommon::compute_err(graph, features, max_rating);
      for (round = 0; round < num_steps; ++round) {
         initialize_worklist();
         this->gpu_operator();
      }
      SGDCommon::compute_err(graph, features, max_rating);
   }
   /************************************************************************
    *
    ************************************************************************/
   void gpu_operator() {
      Galois::OpenCL::Timer timer;
      int i = 0;
      timer.start();
      do {
         ++i;
         //int processed = 0;
         //float frac = 100 * (1.0f - ((metadata->host_ptr()[0]) / ((float) graph.num_edges())));
         //fprintf(stderr, "Iteration[%d] :: Size:: [%d], Completed :: [%6.6g]\n", i, metadata->host_ptr()[0], frac);
         Galois::OpenCL::OpenCL_Setup::finish();
         sgd_kernel();
         metadata->copy_to_host();
      } while (metadata->host_ptr()[0] > 0);
      timer.stop();
      fprintf(stderr, "Round %d took %d steps took %6.6g seconds\n", round, i, timer.get_time_seconds());
   }
   /************************************************************************
    *
    ************************************************************************/
   ~SGDEdgeFunctor() {
      deallocate();
      fprintf(stderr, "Destroying SGDEdgeFunctor object.\n");
   }
};
//###################################################################//
#endif /* SGDEDGEFUNCTOR_H_ */
