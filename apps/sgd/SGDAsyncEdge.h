/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef SGDASYNCEDGE_H_
#define SGDASYNCEDGE_H_

#include <random>
#include <thread>
#include <atomic>
//#define OPTIMIZE_COPYING_CHUNK 1
#if PROFILE_KERNEL_TIMES
#define PRINT_SCHEDULE_INFO 1
#endif
/////////////////////////////////////////////////////////
/************************************************************************
 * Chunked executor.
 ************************************************************************/
using namespace Galois::OpenCL;
struct SGDAsynEdgeFunctor {
   typedef SGD_LC_LinearArray_Undirected_Graph<Galois::CPUArray, unsigned int, unsigned int> GraphTy;
   typedef Array<cl_int> ArrayType;
   typedef GPUArray<cl_int> GPUArrayType;
   typedef Array<FeatureType> FeatureArrayType;
   typedef GPUArray<FeatureType> GPUFeatureArrayType;
   typedef CL_Kernel Kernel;
   ////////////////////////////////////////////////////////////
   /************************************************
    *
    *************************************************/
   struct AsyncConfig {
      AsyncConfig() :
            NUM_BUFFERS(1), NUM_CHUNKS(1), GPU_WL_SIZE(0), config_type(0) {
      }
      int NUM_BUFFERS;
      int NUM_CHUNKS;
      int GPU_WL_SIZE;
      int config_type;
      template<typename Graph>
      void compute_requirements(Graph & g) {
         using namespace Galois::OpenCL;
         const size_t num_nodes = g.num_nodes();
         const size_t num_edges = g.num_edges();
         const long device_memory = OpenCL_Setup::get_device_memory();
         //Global data is locks + features
         const long global_data = sizeof(int) * num_nodes + num_nodes * (SGD_FEATURE_SIZE + 1) * sizeof(FeatureType);
         const long total_chunk_mem = device_memory - global_data;
         const long edge_data_size = 3 * sizeof(int) * num_edges + 16 * sizeof(int);
         // chunk_data is (src,dst,rating) + metadata
         if (edge_data_size > total_chunk_mem) {
            NUM_BUFFERS = 2; // Use 2-copy engines on the GPU.
            NUM_CHUNKS = ceil((3 * sizeof(int) * num_edges + 16 * sizeof(int)) / (float) (total_chunk_mem / NUM_BUFFERS));
         }
         GPU_WL_SIZE = num_edges / NUM_CHUNKS;
         fprintf(stderr, "Mem. %ld, GlobalData, %ld, ChunkMem, %ld, chunks_required, %d, buffered_chunks, %d \n", device_memory, global_data, total_chunk_mem, NUM_BUFFERS,
               NUM_CHUNKS);
      }
      template<typename Graph>
      void init(Graph & g) {
         compute_requirements(g);
      }
   };
   /************************************************
    *
    *************************************************/
   struct ChunkInfo {
      int chunk_size;
      cl_int * edge_info;
      FeatureType * rating;
      float copy_time;
      float exec_time;
      void allocate(size_t alloc_size, size_t size) { //Note we have two sizes; one for buffer, one for actual data.
         chunk_size = size;
         edge_info = new cl_int[2 * alloc_size];
         rating = new FeatureType[alloc_size];
      }
      void deallocate() {
         delete[] edge_info;
         delete[] rating;
      }
   };
   /************************************************
    *************************************************/
   struct GPUBuffer {
      GPUArrayType * edge_info;
      GPUFeatureArrayType * ratings;
      ArrayType * metadata;
      cl_event copy_events[3];
      Kernel sgd_kernel;
      void allocate(size_t _size) {
         metadata = new ArrayType(16);
         edge_info = new GPUArrayType(2 * _size);
         ratings = new GPUFeatureArrayType(_size);
      }
      void init(const char * filename, const char * kernel_name) {
         sgd_kernel.init(filename, kernel_name);
      }
      void deallocate() {
         delete edge_info;
         delete ratings;
         delete metadata;
      }
      void load_chunk(ChunkInfo * c) {
         edge_info->copy_to_device(c->edge_info, copy_events);
         ratings->copy_to_device(c->rating, copy_events + 1);
         metadata->copy_to_device(copy_events + 2);
      }
      void wait_on_copy() {
         clWaitForEvents(3, copy_events);
      }
   };
   //////////////////////////////
   /************************************************
    *************************************************/

   AsyncConfig config;
   GraphTy graph;
   std::vector<int> movies;
   std::vector<int> user_indices;
   ArrayType * locks;
   FeatureArrayType * features;
   ChunkInfo ** chunk_info;
   GPUBuffer ** buffers;
#if PROFILE_KERNEL_TIMES
   Array<cl_uint> * kernel_times;
   Array<cl_uint> * op_body_times;
   Array<cl_ulong> * time_stamps;
#endif
   float accumulated_error;
   int round;
   unsigned int max_rating;
   char filename[512];
   int high_degree_threshold;
   float shuffle_time;
   std::vector<int> user_edge_count;
   Galois::StatsManager stats;
#ifdef PRINT_SCHEDULE_INFO
      int * step_info;
#endif

   /************************************************************************
    *
    *metadata (16)
    *edge_info, worklist, ratings (2+1+1)*NE
    *locks, features*FEATURE_SIZE, (1+FEATURE_SIZE)NN
    ************************************************************************/
   SGDAsynEdgeFunctor(bool road, const char * p_filename) :
         round(0) {
      high_degree_threshold = 0;
      shuffle_time =0;
      strcpy(filename, p_filename);
      fprintf(stderr, "Creating SGDAsynEdgeFunctor -  features =[%d], Chunks=[%d], Buffers=[%d] .\n", SGD_FEATURE_SIZE, config.NUM_CHUNKS, config.NUM_BUFFERS);
      //      SGDCommon::complete_bipartitie(graph, 192*15,192*15);
      if (road) {
         SGDCommon::read_as_bipartite(graph, filename);
      } else {
         graph.read(p_filename);
      }
      config.init(graph);
      allocate();
      initialize_features_random();
      fprintf(stderr, "Number of movies found :: %ld\n", movies.size());
      if (false) {
         std::ofstream header("graph_header.h");
         header << Galois::OpenCL::str_SGD_LC_LinearArray_Undirected_Graph << "\n";
         header.close();
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   SGDAsynEdgeFunctor(int num_m, int num_u) :
         round(0) {
      high_degree_threshold = 0;
      shuffle_time =0;
      strcpy(filename, "generated-input");
      fprintf(stderr, "Creating SGDAsynEdgeFunctor -  features =[%d], Chunks=[%d], Buffers=[%d] .\n", SGD_FEATURE_SIZE, config.NUM_CHUNKS, config.NUM_BUFFERS);
      SGDCommon::complete_bipartitie(graph, num_m, num_u);
      config.init(graph);
      allocate();
      initialize_features_random();
      fprintf(stderr, "Number of movies found :: %ld\n", movies.size());
   }
   /************************************************************************
    *
    ************************************************************************/
   SGDAsynEdgeFunctor(int num_m) :
         round(0) {
      high_degree_threshold = 0;
      shuffle_time =0;
      strcpy(filename, "gen-diagonal-input");
      fprintf(stderr, "Creating SGDAsynEdgeFunctor -  features =[%d], Chunks=[%d], Buffers=[%d] .\n", SGD_FEATURE_SIZE, config.NUM_CHUNKS, config.NUM_BUFFERS);
      SGDCommon::diagonal_graph(graph, num_m);
      config.init(graph);
      allocate();
      initialize_features_random();
      fprintf(stderr, "Number of movies found :: %ld\n", movies.size());
   }
   /************************************************************************
    *
    ************************************************************************/
   void allocate() {
      features = new FeatureArrayType(graph.num_nodes() * SGD_FEATURE_SIZE);
      locks = new ArrayType(graph.num_nodes());
      buffers = new GPUBuffer*[config.NUM_BUFFERS];
#if PROFILE_KERNEL_TIMES
      kernel_times = new Array<cl_uint>(graph.num_edges());
      op_body_times = new Array<cl_uint>(graph.num_edges());
      time_stamps = new Array<cl_ulong>(graph.num_edges());
      for(size_t i=0; i<graph.num_edges(); ++i){
         kernel_times->host_ptr()[i] = 0;
         op_body_times->host_ptr()[i] = 0;
         time_stamps->host_ptr()[i] = 0;
      }
//      fprintf(stderr, "Allocated timer-memory :: %u \n", graph.num_edges());
      kernel_times->copy_to_device();
      op_body_times->copy_to_device();
      time_stamps->copy_to_device();
      Galois::OpenCL::OpenCL_Setup::append_build_args(" -DPROFILE_KERNEL_TIMES=1 ");
#endif
      for (int i = 0; i < config.NUM_BUFFERS; ++i) {
         buffers[i] = new GPUBuffer();
         buffers[i]->allocate(config.GPU_WL_SIZE);
         buffers[i]->init("./apps/sgd/sgd_edge_operator.cl", "sgd_edge_operator");
         //locks, __global FeatureTypeSIMD * features, __global int * metadata,  __global int * edge_info, __global FeatureType * ratings, FeatureType step_size) {
         buffers[i]->sgd_kernel.set_arg_list(locks, features, buffers[i]->metadata, /*buffers[i]->edge_wl,*/buffers[i]->edge_info, buffers[i]->ratings);
#if PROFILE_KERNEL_TIMES
         buffers[i]->sgd_kernel.set_arg(6, kernel_times);
         buffers[i]->sgd_kernel.set_arg(7, op_body_times);
         buffers[i]->sgd_kernel.set_arg(8, time_stamps);
#endif
      }
      chunk_info = new ChunkInfo *[config.NUM_CHUNKS];
   }
   /************************************************************************
    *
    ************************************************************************/
   void deallocate() {
      for (int i = 0; i < config.NUM_CHUNKS; ++i) {
         chunk_info[i]->deallocate();
         delete chunk_info[i];
      }
      for (int i = 0; i < config.NUM_BUFFERS; ++i) {
         buffers[i]->deallocate();
         delete buffers[i];
      }
      delete[] buffers;
      delete features;
      delete locks;
      delete[] chunk_info;
#ifdef PRINT_SCHEDULE_INFO
      delete []step_info;

#endif
#if PROFILE_KERNEL_TIMES
      delete kernel_times;
      delete op_body_times;
      delete time_stamps;
#endif
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_device() {
      features->copy_to_device();
      locks->copy_to_device();
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_host() {
      features->copy_to_host();
      locks->copy_to_host();
   }
   /************************************************************************
    *
    ************************************************************************/
   void populate_worklist(int buffer_id, int chunk_id) {
      buffers[buffer_id]->metadata->host_ptr()[0] = chunk_info[chunk_id]->chunk_size;
      buffers[buffer_id]->metadata->host_ptr()[1] = round;
      buffers[buffer_id]->metadata->host_ptr()[2] = graph.num_edges();
      float t = SGDCommon::SGD_STEP_SIZE(round);
      buffers[buffer_id]->metadata->host_ptr()[3] = (t);
      buffers[buffer_id]->metadata->host_ptr()[4] = chunk_info[chunk_id]->chunk_size;
      buffers[buffer_id]->sgd_kernel.set_arg(5, sizeof(cl_float), &t);
      buffers[buffer_id]->sgd_kernel.set_work_size(chunk_info[chunk_id]->chunk_size);
      buffers[buffer_id]->load_chunk(chunk_info[chunk_id]);
   }
   /************************************************************************
    *
    ************************************************************************/
   void initialize_features_random() {
      std::vector<int> **all_edges;
      all_edges = new std::vector<int>*[config.NUM_CHUNKS];
      for (int i = 0; i < config.NUM_CHUNKS; ++i) {
         all_edges[i] = new std::vector<int>();
      }

      Galois::OpenCL::Timer timer;
      timer.start();
      SGDCommon::initialize_features_random(graph, features, locks, movies);
      movies.clear();
      unsigned int max_degree = 0, max_degree_id = 0;
      for (unsigned int i = 0; i < graph.num_nodes(); ++i) {
         if (graph.num_neighbors(i) > max_degree) {
            max_degree = graph.num_neighbors(i);
            max_degree_id = i;
         }
         if (graph.num_neighbors(i) > 0) {
            movies.push_back(i);
         } else {
            user_indices.push_back(i);
         }
      }
      graph._max_degree_node = max_degree_id;
      graph._max_degree = max_degree;
      max_rating = 0;
      std::vector<int> user_degrees(graph.num_nodes());
      int max_user_degree =0;
      int max_user_id = 0;
      for (unsigned int i = 0; i < graph.num_edges(); ++i) {
         max_rating = std::max(max_rating, graph.out_edge_data()[i]);
         user_degrees[graph.out_neighbors()[i]]++;
         if(max_user_degree < user_degrees[graph.out_neighbors()[i]]){
            max_user_degree  = user_degrees[graph.out_neighbors()[i]];
            max_user_id= graph.out_neighbors()[i];
         }
      }
      if(max_user_degree > (int)max_degree){
         graph._max_degree = max_user_degree;
         graph._max_degree_node = max_user_id;
      }
      if(false){

         std::vector<int> *degree_dist = new std::vector<int>(std::max(max_user_degree, (int)max_degree));
         for(size_t i=0; i < graph.num_nodes(); ++i){
            if(graph.num_neighbors(i)>0)
               (*degree_dist)[graph.num_neighbors(i)]++;
         }
         for(size_t i=0; i<user_degrees.size(); ++i){
            if(user_degrees[i]>0)
               (*degree_dist)[user_degrees[i]]++;
         }
         std::string log_file(filename);
         log_file.append(".dist.csv");
         {
            std::ofstream out_file(log_file.c_str());
            out_file<<"Degree,Count\n";
       //     long count=0;
            for(size_t i=0; i<degree_dist->size(); ++i){
   //            count+=degree_dist[i];
               out_file<<i<<", "<<(*degree_dist)[i]<<"\n";
     //          out_file<<i<<", "<<count<<"\n";

            }
            out_file.flush();
            out_file.close();
         }
         //TODO RK Bug, should delete upon completion, but get double-delete error!
         // compiler check?
//         delete degree_dist;
      }
      //const = std::max(user_degrees.begin(), user_degrees.end())l
      const int items_per_chunk = ceil(graph.num_edges() / config.NUM_CHUNKS);
      fprintf(stderr, "] , max_Rating: %d, Chunk-size: %d, nodes:%ld, edges: %ld, movies: %ld, users: %ld, max_user_degree:: %d, Max degree:: %d for node: %d\n",
                        max_rating, items_per_chunk, graph.num_nodes() , graph.num_edges(),movies.size(), user_indices.size() ,max_user_degree, max_degree, max_degree_id);
      timer.stop();
      SGDCommon::print_distributions(graph);
      fprintf(stderr, "Initialization time [%6.6f s], ", timer.get_time_seconds());
      timer.clear();
      timer.start();
      distribute_chunks(all_edges);
      timer.stop();
      fprintf(stderr, "Distribution time [%6.6f s], ", timer.get_time_seconds());
      timer.clear();
      timer.start();
      cache_chunks(all_edges);
      timer.stop();
      fprintf(stderr, "Caching time [%6.6f s] ", timer.get_time_seconds());
#ifndef PRE_ALLOCATE_BUFFERS
      timer.clear();
      timer.start();
      pre_allocate_buffers();
      timer.stop();
      fprintf(stderr, "Pre-allocation time [%6.6f s] \n", timer.get_time_seconds());
#endif
      for (int i = 0; i < config.NUM_CHUNKS; ++i) {
         delete all_edges[i];
      }
      delete[] all_edges;
   }
   /************************************************************************
    *
    ************************************************************************/
   void pre_allocate_buffers() {
      for (int i = 0; i < config.NUM_BUFFERS; ++i) {
         buffers[i]->load_chunk(chunk_info[0]);
         buffers[i]->wait_on_copy();
      }
      for (int i = 0; i < config.NUM_CHUNKS; ++i) {
         buffers[0]->load_chunk(chunk_info[i]);
         buffers[0]->wait_on_copy();
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   void cache_chunks(std::vector<int> **all_edges) {
      for (int c = 0; c < config.NUM_CHUNKS; ++c) {
         chunk_info[c] = new ChunkInfo();
         chunk_info[c]->allocate(config.GPU_WL_SIZE, all_edges[c]->size()); //TODO: allocate larger than required. Last chunk might have less values, but creates error when copying.
         for (unsigned int i = 0; i < all_edges[c]->size(); ++i) {
            const int id = (*all_edges[c])[i];
            const int src = graph.get_edge_src(id);
            const int dst = graph.out_neighbors()[id];
            const float rating = graph.out_edge_data()[id] / (float) max_rating;
            chunk_info[c]->edge_info[2 * i] = src;
            chunk_info[c]->edge_info[2 * i + 1] = dst;
            chunk_info[c]->rating[i] = rating;
         }
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   void distribute_chunks(std::vector<int> **all_edges) {
      std::vector<int> in_edge_wl(graph.num_edges());
      std::vector<int> edge_wl;
      for (size_t i = 0; i < graph.num_edges(); ++i) {
         in_edge_wl[i] = i;
      }
      Galois::OpenCL::Timer shuffle_timer;
      shuffle_timer.clear();
      shuffle_timer.start();
      SGDCommon::edge_filter(SGDCommon::EdgeFilterType::RandomShuffle, graph, in_edge_wl, edge_wl);
      shuffle_timer.stop();
      shuffle_time = shuffle_timer.get_time_seconds();
//      SGDCommon::edge_filter(SGDCommon::EdgeFilterType::AverageRatingsHighDegreeMovie, graph, in_edge_wl, edge_wl);
//      SGDCommon::edge_filter(SGDCommon::EdgeFilterType::HighRatingsHighDegreeMovie, graph, in_edge_wl, edge_wl);
      //SGDCommon::edge_filter(SGDCommon::EdgeFilterType::LowRatings, graph, in_edge_wl, edge_wl);
//      SGDCommon::edge_filter(SGDCommon::EdgeFilterType::LowDegreeMovie, graph, in_edge_wl, edge_wl);
      //std::random_shuffle(edge_wl.begin(), edge_wl.end());
      /*      edge_wl.resize(in_edge_wl.size());
       std::copy(in_edge_wl.begin(), in_edge_wl.end(), edge_wl.begin());*/
      size_t num_edges_to_process = edge_wl.size();
      for (int i = 0; i < config.NUM_CHUNKS; ++i) {
         int * start = edge_wl.data() + i * config.GPU_WL_SIZE;
         int num_items = std::min((int) (num_edges_to_process - (i * config.GPU_WL_SIZE)), (int) config.GPU_WL_SIZE);
         all_edges[i]->resize(num_items);
         memcpy(all_edges[i]->data(), start, num_items * sizeof(int));
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   void operator()(int num_steps) {
      Galois::OpenCL::Timer copy_timer;
      copy_timer.start();
      copy_to_device();
      copy_timer.stop();
      fprintf(stderr, "Copy data in time :: %6.6g\n", copy_timer.get_time_seconds());
      SGDCommon::compute_err(graph, features, max_rating);
      for (round = 0; round < num_steps; ++round) {
         this->gpu_operator();
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   std::pair<int, float> process_buffer(int buffer_id, int chunk_id) {
      Galois::OpenCL::Timer kernel_timer, call_timer;
      call_timer.clear();
      kernel_timer.start();
      populate_worklist(buffer_id, chunk_id);
      buffers[buffer_id]->wait_on_copy();
      kernel_timer.stop();
      chunk_info[chunk_id]->copy_time = kernel_timer.get_time_seconds();
      kernel_timer.clear();
      kernel_timer.start();
#ifdef PRINT_SCHEDULE_INFO
      step_info = new int [graph.num_edges()];
      int * edge_wl = new int [2*graph.num_edges()];
      for(size_t i=0; i<graph.num_edges(); ++i)step_info[i]=-1;
#endif
      std::vector<std::pair<int, float> > step_stats;
      step_stats.reserve(1000);      //Sufficient for most inputs we have used.
      /*
       Galois::OpenCL::Timer step_timer;*/
//      std::vector<std::pair<int, float> >
      int curr_step = 0;
      float kernel_time = 0;
      do {
         ++curr_step;
         /*step_timer.clear();
          step_timer.start();*/
         call_timer.start();
         buffers[buffer_id]->sgd_kernel(3, buffers[buffer_id]->copy_events);
         buffers[buffer_id]->sgd_kernel.wait();
         call_timer.stop();
         float curr_kernel_time = call_timer.get_time_seconds();//buffers[buffer_id]->sgd_kernel.last_exec_time();
         kernel_time += curr_kernel_time;
         /*step_timer.stop();*/
         buffers[buffer_id]->metadata->copy_to_host();
#ifdef EARLY_APPROXIMATE_TERMINATION //If 2% of iterations can be ignored for better performance.
         float ratio_remaining = buffers[buffer_id]->metadata->host_ptr()[0]/ (float)graph.num_edges();
         fprintf(stderr, "Step[%d], [%d], %6.6g \n", curr_step, buffers[buffer_id]->metadata->host_ptr()[0], ratio_remaining);
         //     if(ratio_remaining < 0.02f) break;
#endif
         assert(buffers[buffer_id]->metadata->host_ptr()[0] >= 0 && "Items processed overflows!");
#ifdef PRINT_SCHEDULE_INFO
         buffers[buffer_id]->edge_info->copy_to_host(edge_wl);
         for(size_t i=0; i<graph.num_edges(); ++i) {
            if(edge_wl[2*i]<0 && step_info[i]==-1)
            step_info[i]=curr_step;
         }
#endif
         /*step_stats.push_back(std::pair<int, float>(buffers[buffer_id]->metadata->host_ptr()[0], step_timer.get_time_seconds()));*/
         //fprintf(stderr, "%d, ", buffers[buffer_id]->metadata->host_ptr()[0]);
         step_stats.push_back(std::pair<int, float>(buffers[buffer_id]->metadata->host_ptr()[0], curr_kernel_time));
      } while (buffers[buffer_id]->metadata->host_ptr()[0] != 0);
      kernel_timer.stop();
      chunk_info[chunk_id]->exec_time = kernel_timer.get_time_seconds();
      static bool isfirst = true;
      if(isfirst)
      {
         fprintf(stderr, "Printing stats....\n");
         isfirst=false;
         std::string log_file_name((filename));
         log_file_name.append(".edge_locked_stats.csv");
         std::ofstream out_file(log_file_name);
         for (auto i : step_stats) {
            out_file << i.first << "," << i.second << "\n";
         }
         out_file.close();
      }
#ifdef PRINT_SCHEDULE_INFO
      {
         std::ofstream out_file("dynamic_schedule.csv");
         for(size_t i=0; i<graph.num_edges(); ++i) {
            out_file<<i<<", "<<chunk_info[chunk_id]->edge_info[2*i]<<", "<<chunk_info[chunk_id]->edge_info[2*i+1]<<", "<<step_info[i]<<"\n";
         }
         out_file.close();
      }
//      delete [] step_info;
      delete [] edge_wl;
#endif
      return std::pair<int, float>(curr_step, kernel_timer.get_time_seconds());
   }
   /************************************************************************
    *
    ************************************************************************/
   void gpu_operator() {
      float kernel_time = 0;
      Galois::OpenCL::Timer timer;
      timer.start();
      std::vector<std::vector<int>*> rounds_completed;
      for (int i = 0; i < config.NUM_BUFFERS; ++i) {
         rounds_completed.push_back(new std::vector<int>());
      }
      {
         std::vector<std::thread> threads;
         std::vector<float> kernel_timer(config.NUM_BUFFERS);
         std::vector<int> chunks_processed(config.NUM_BUFFERS);
#ifdef _WIN32
#else
         std::atomic<int> work_index = ATOMIC_VAR_INIT(0);
         std::atomic<int> cnt = ATOMIC_VAR_INIT(0);
         for (int i = 0; i < config.NUM_BUFFERS; ++i) {
//            threads.push_back(std::thread([&]()
                  {
               int my_id = std::atomic_fetch_add(&cnt,1);
               int chunk_id = config.NUM_CHUNKS;
               std::pair<int, float> result;
                  while((chunk_id = std::atomic_fetch_add(&work_index,1))<config.NUM_CHUNKS) {
                     result =process_buffer(my_id,chunk_id);
                     kernel_timer[my_id]+=result.second;
                     rounds_completed[my_id]->push_back(result.first);
                     chunks_processed[my_id]++;
                  }
               }
                  //));
         }
//         for (auto& thread : threads) {
//            thread.join();
//         }
         timer.stop();
         ////STATS:
         for (auto t : kernel_timer)
            kernel_time += t;
#endif
#if PROFILE_KERNEL_TIMES
      kernel_times->copy_to_host();
      op_body_times->copy_to_host();
      time_stamps->copy_to_host();
      std::ofstream out_file ("el_profile_edges.log");
//      out_file << " LOG FILE " << filename << " created \n";
//      fprintf(stderr, "\n");
      for(size_t i=0; i<graph.num_edges(); ++i){
         int src = this->chunk_info[0]->edge_info[2*i];
         int dst = this->chunk_info[0]->edge_info[2*i+1];
//         fprintf(stderr,  " %u, ", kernel_times->host_ptr()[i]);
         if( (src==(int)(graph._max_degree_node)) || (dst==(int)(graph._max_degree_node)) ) {
            out_file << src << " , " << dst << " , " << kernel_times->host_ptr()[i]<<", " << op_body_times->host_ptr()[i] << ", ";
#ifdef PRINT_SCHEDULE_INFO
            out_file<< step_info[i] <<", ";
#endif
            out_file<< time_stamps->host_ptr()[i]<<"\n";
         }
      }
//      fprintf(stderr, "\n");
#endif


         /*for (int i = 0; i < config.NUM_BUFFERS; ++i) {
          fprintf(stderr, "%d (%d) : [", i, chunks_processed[i]);
          for (auto it : *rounds_completed[i]) {
          fprintf(stderr, "%d, ", it);
          }
          fprintf(stderr, "]\n");
          }*/
//         float total_chunk_time = 0;
         /*fprintf(stderr, "Chunks:====================\n");
          for (int i = 0; i < config.NUM_CHUNKS; ++i) {
          fprintf(stderr, "(%d, %6.6g, %6.6g)\n", i, chunk_info[i]->copy_time, chunk_info[i]->exec_time);
          total_chunk_time += chunk_info[i]->copy_time + chunk_info[i]->exec_time;
          }
          fprintf(stderr, "===========================\n");*/
         stats.add_stats("Round",round);
         stats.add_stats("LSize",buffers[0]->sgd_kernel.local);
         stats.add_stats("Steps",(*rounds_completed[0])[0]);
         stats.add_stats("TotalTime",timer.get_time_seconds());
         stats.add_stats("GPUWork",kernel_time);
         stats.add_stats("ShuffleTime",shuffle_time);
         stats.end_round();
//         fprintf(stderr, "Round, %d, Steps, %d , Total-time(s), %6.6g ,GPU_Work(s), %6.6g, CHUNK_TIME(s), %6.6g \t",
//               round, (*rounds_completed[0])[0], timer.get_time_seconds(),kernel_time, total_chunk_time);
         copy_to_host();
         SGDCommon::compute_err(graph, features, max_rating);
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   ~SGDAsynEdgeFunctor() {
      deallocate();
      stats.print();
      Galois::OpenCL::OpenCL_Setup::get_default_device()->print_stats();
      fprintf(stderr, "Destroying SGDAsynEdgeFunctor object.\n");
   }
};
//###################################################################//
#endif /* SGDASYNCEDGE_H_ */
