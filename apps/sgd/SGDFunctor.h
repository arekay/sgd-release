/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#ifndef SGDFUNCTOR_H_
#define SGDFUNCTOR_H_

#include "../../ds/GraphUtils.h"
#include <random>
#include "../../apps/sgd/SGDCommon.h"

/************************************************************************
 *
 ************************************************************************/
#include "../../apps/sgd/SGDEdgeFunctor.h"
#include "../../apps/sgd/SGDChunkedEdge.h"
#include "../../apps/sgd/SGDChunked2WLEdge.h"
#include "../../apps/sgd/SGDAsyncEdge.h"
#include "../../apps/sgd/SGDLockHybrid.h"
#include "../../apps/sgd/SGDNodeLocked.h"
#include "../../apps/sgd/SGDEdgeFunctorUtil.h"
#include "../../apps/sgd/SGDStaticSchedule.h"
#include "../../apps/sgd/SGDAllGraphMatchingSchedule.h"
#include "../../apps/sgd/SGDSubGraphMatching.h"

#include "../../apps/sgd/SGDAllGraphMatchingEdgeCoalesced.h"

//###################################################################//

//###################################################################//
void test_sgd(SGD_INPUT_TYPE runType, const char * filename) {
   //With locks
#ifdef SGD_EDGE_LOCKED
   typedef SGDAsynEdgeFunctor SGDFunctorTy; // SGD_EDGE_LOCKED_FN;//SGDFunctorTy;
#elif defined SGD_NODE_LOCKED
   typedef SGDNodeLocked SGDFunctorTy; // SGD_NODE_LOCKED_FN;//SGDFunctorTy;
#elif defined SGD_HYBRID_LOCKED
   typedef SGDLockHybrid SGDFunctorTy; //SGD_HYBRID_LOCKED;//SGDFunctorTy;
#elif defined SGD_ALL_GRAPH_EDGE
   //Without locks, pre-compute schedule
   typedef SGDAllGraphMatchingSchedule<true,true> SGDFunctorTy;//SGD_EDGE_LOCKFREE_FN;//SGDFunctorTy;
#elif defined SGD_ALL_GRAPH_NODE
   //Without locks, pre-compute schedule
   typedef SGDAllGraphMatchingSchedule<false,true> SGDFunctorTy;//SGD_EDGE_LOCKFREE_FN;//SGDFunctorTy;
#elif defined SGD_SUBGRAPH_NODE_LOCKFREE
//   typedef SGDStaticScheduleFlat<true> SGDFunctorTy;
   typedef SGDSubGraphMatching<false,true,1> SGDFunctorTy;//SGD_NODE_LOCKFREE_FN;//SGDFunctorTy;
#elif defined SGD_HYBRID_LOCKFREE
   typedef SGDSubGraphMatching<true,true,1> SGDFunctorTy; //SGD_HYBRID_LOCKFREE_FN;//SGDFunctorTy;
#endif
//   enum SGD_INPUT_TYPE={BIPARTITE=0, ROAD, DIAGONAL, COMPLETE_BIPARTITE};
   SGDFunctorTy * func;
   switch (runType){
   case BIPARTITE:
      fprintf(stderr, "HEADER:: Starting sgd ::  BIPARTITE %s \n", filename);
      func = new SGDFunctorTy(false, filename);
      break;
   case ROAD:
      fprintf(stderr, "HEADER:: Starting sgd ::  ROAD %s \n",  filename);
      func = new SGDFunctorTy(true, filename);
      break;
   case DIAGONAL:
      fprintf(stderr, "HEADER:: Starting sgd ::  DIAGONAL %s \n", filename);
      func = new SGDFunctorTy(atoi(filename));
      break;
   case COMPLETE_BIPARTITE:
      fprintf(stderr, "HEADER:: Starting sgd ::  COMPLETE_BIPARTITE %s \n" , filename);
      func = new SGDFunctorTy(atoi(filename), atoi(filename));
      break;
   default:
      fprintf(stderr, "Invalid specification for runType!");
      assert(false);
   }
   (*func)(SGD_MAX_ROUNDS);
   delete func;
}
#endif /* SGDFUNCTOR_H_ */
