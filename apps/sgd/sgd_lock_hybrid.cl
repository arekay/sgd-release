/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#include "cl_common.cl"
#include "apps/sgd/sgd_common.cl"
/*************************************************************************
 *
 *************************************************************************/
__kernel void sgd_node_operator(
      __global int * locks, __global FeatureTypeSIMD * features,
      __global int * metadata, __global int * edge_indices,__global int * edge_destinations,
      __global FeatureType * ratings, __global int * movies, FeatureType step_size, __local FeatureTypeSIMD * cache, __global uint * next_wl) {
   const uint my_id = get_global_id(0);
   const uint num_movies = metadata[4];
   const int num_nodes = metadata[0];
   const int num_edges = metadata[2];
   __global int * work_counter = &metadata[8];
   __global int * next_wl_counter = &metadata[9];
   bool work_done = false;
   if(my_id < num_movies) {
      const int movie_id = movies[my_id];
#ifdef _SGD_USE_SHARED_MEM_
      __local FeatureTypeSIMD * src_l = &cache[get_local_id(0)*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
      __global FeatureTypeSIMD * s_ptr = &features[movie_id*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
      for(int i=0; i<SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE;++i) {
         src_l[i] = s_ptr[i];
      }
#endif

      for(int idx = edge_indices[movie_id]; idx < edge_indices[movie_id+1]; ++idx) {
         const int dst = edge_destinations[idx];
         const FeatureType rating = ratings[idx];
         if ((lock_1(locks, dst)==true))
         {
#ifdef _SGD_USE_SHARED_MEM_
            work_done=true;
            sgd_kernel_shared_movie_simd(src_l, dst, rating, step_size, features);

#else
            __global FeatureTypeSIMD * s_ptr = &features[movie_id*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
            __global FeatureTypeSIMD * d_ptr = &features[dst*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
              sgd_kernel_global_simd(s_ptr, d_ptr, rating, step_size);
#endif
            unlock_1(locks, dst);
         } //end if.
         else {
            int work_index = atomic_add(next_wl_counter,1);
            next_wl[work_index] =  idx;
         }
      } //end for
#ifdef _SGD_USE_SHARED_MEM_
      {
         if(work_done)
         for(int i=0; i<SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE;++i) {
            s_ptr[i] = src_l[i];
         }
      }
#endif
   } //if my_id < num_nodes
}
/*************************************************************************
 *
 *************************************************************************/
