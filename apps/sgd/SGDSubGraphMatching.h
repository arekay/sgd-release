/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef SGDSubGraphMatching_H_
#define SGDSubGraphMatching_H_

#include <random>
#include <thread>
#include <atomic>
/////////////////////////////////////////////////////////
/************************************************************************
 * Chunked executor.
 ************************************************************************/
using namespace Galois::OpenCL;
template<bool UseLockEdge, bool PrintMatchingStats = false, int EDGE_CHUNK_NUM = 1>
struct SGDSubGraphMatching {
   typedef SGD_LC_LinearArray_Undirected_Graph<Galois::CPUArray, unsigned int, unsigned int> GraphTy;
   typedef Array<cl_int> ArrayType;
   typedef GPUArray<cl_int> GPUArrayType;
   typedef Array<FeatureType> FeatureArrayType;
   typedef GPUArray<FeatureType> GPUFeatureArrayType;
   typedef CL_Kernel Kernel;
   struct MovieChunk {
      int start_index;
      int end_index;
      int max_degree;
      int max_levels;
      int num_edges;
      int edge_start;
      int edge_end;
      MovieChunk() :
            start_index(0), end_index(0), max_degree(0), num_edges(0) {
         max_levels = edge_start = edge_end = 0;
      }
      MovieChunk(int s, int e, int m, int n) :
            start_index(s), end_index(e), max_degree(m), num_edges(n) {
         max_levels = edge_start = edge_end = 0;
      }
      void print() {
         fprintf(stderr, "[Movie:(%d,%d),Edge:(%d,%d,%d),MaxL(%d),]", start_index, end_index, edge_start, edge_end, num_edges, max_levels);
      }
   };
   ////////////////////////////////////////////////////////////
   /************************************************
    *************************************************/
   GraphTy graph;
   std::vector<int> movies;
   std::vector<int> user_indices;
   ArrayType * locks;
   FeatureArrayType * features;
   ArrayType *metadata;
   ArrayType *edge_step;

   float accumulated_error;
   int round;
   unsigned int max_rating;
   char filename[512];

   int *h_edge_indices;
   int *h_edge_destinations;
   int *h_movies_ordered;
   int *h_edge_info;
   float *h_ratings;

   int * edge_reorder_map;

   int *lock_edge_wl;
   int *e_edge_info;
   float * e_ratings;
   std::string matching_header_str; // should be either CREATE or LOAD
   float matching_time; // the time taken to either create or load the matching.

   Kernel lock_free_node_kernel;
   Kernel lock_edge_kernel;
   int filtered_edges;
   Galois::StatsManager stats;
   std::vector<MovieChunk> movie_chunks;
   MovieChunk edge_chunk;
   struct NodeOperatorData {
      GPUArrayType *edge_indices;
      GPUArrayType *edge_destinations;
      GPUArrayType *movies_ordered;
      GPUFeatureArrayType *ratings;
   };
   NodeOperatorData node_data;
   struct EdgeOperatorData {
      GPUArrayType *edge_info;
      GPUFeatureArrayType * ratings;
   };
   EdgeOperatorData edge_data;
   /************************************************************************
    *
    *metadata (16)
    *edge_info, worklist, ratings (2+1+1)*NE
    *locks, features*64, (1+64)NN
    ************************************************************************/
   SGDSubGraphMatching(bool road, const char * p_filename) :
         round(0) {
      locks = nullptr;
      features = nullptr;
      h_edge_indices = nullptr;
      h_edge_destinations = nullptr;
      h_movies_ordered = nullptr;
      h_edge_info = nullptr;
      h_ratings = nullptr;
      edge_step = nullptr;
      metadata = nullptr;
      lock_edge_wl = nullptr;
      e_edge_info = nullptr;
      e_ratings = nullptr;
      edge_reorder_map = nullptr;
      matching_header_str = "";
      matching_time = 0;

      strcpy(filename, p_filename);
      version_info();
      if (road) {
         SGDCommon::read_as_bipartite(graph, filename);
      } else {
         graph.read(filename);
      }
      allocate();
      initialize_features_random();
      fprintf(stderr, "Number of movies found :: %ld\n", movies.size());
   }
   /************************************************************************
    *
    ************************************************************************/
   SGDSubGraphMatching(int num_movies, int num_users) :
         round(0) {
      matching_header_str = "";
      matching_time = 0;

      sprintf(filename, "gen-input_%d_%d", num_movies, num_users);
      version_info();
      SGDCommon::complete_bipartitie(graph, num_movies, num_users);
      allocate();
      initialize_features_random();
      fprintf(stderr, "Number of movies found :: %ld\n", movies.size());
   }
   /************************************************************************
     *
     ************************************************************************/
   SGDSubGraphMatching(int num_m) :
          round(0) {
      matching_header_str = "";
      matching_time = 0;

      sprintf(filename, "gen-diagonal-input_%d", num_m);
       version_info();
       SGDCommon::diagonal_graph(graph, num_m);
       allocate();
       initialize_features_random();
       fprintf(stderr, "Number of movies found :: %ld\n", movies.size());
    }
   /************************************************************************
       *
       ************************************************************************/
      void version_info(){
         char tmp_edge_str[128];
         sprintf(tmp_edge_str, "LockEdge(%d)", EDGE_CHUNK_NUM);
  #ifdef _SGD_USE_SHARED_MEM_
        char shmem_str []= "Yes";
  #else
        char shmem_str []= "No";
  #endif
        fprintf(stderr, "Creating SGDSubGraphMatching[%s]-features =[%d], Shmem=[%s].\n", UseLockEdge ? tmp_edge_str : "LockFree", SGD_FEATURE_SIZE, shmem_str);

      }
   /************************************************************************
    *
    ************************************************************************/
   void initialize_features_random() {
      Galois::OpenCL::Timer timer;
      timer.start();
      SGDCommon::initialize_features_random(graph, features, locks, movies);
      movies.clear();
      unsigned int max_degree = 0, max_degree_id = 0;
      //Collect movies and users.
      for (unsigned int i = 0; i < graph.num_nodes(); ++i) {
         if (graph.num_neighbors(i) > max_degree) {
            max_degree = graph.num_neighbors(i);
            max_degree_id = i;
         }
         if (graph.num_neighbors(i) > 0) {
            movies.push_back(i);
         } else {
            user_indices.push_back(i);
         }
      }
      //Compute max edge-rating to normalize all edges between 0 and 1.
      max_rating = 0;
      for (unsigned int i = 0; i < graph.num_edges(); ++i) {
         max_rating = std::max(max_rating, graph.out_edge_data()[i]);
      }
      fprintf(stderr, "] , max_Rating: %d, movies: %ld, Max degree:: %d for node: %d\n", max_rating, movies.size(), max_degree, max_degree_id);

      //Sort movies in descending degree order.
      h_movies_ordered = new int[movies.size()];
      std::sort(movies.begin(), movies.end(), [&](const int & lhs, const int & rhs) {return graph.num_neighbors(lhs)>graph.num_neighbors(rhs);});
      std::copy(movies.begin(), movies.end(), h_movies_ordered);
      timer.stop();
      //SGDCommon::print_distributions(graph);
      fprintf(stderr, "Initialization time [%6.6f s], ", timer.get_time_seconds());
      timer.clear();
      timer.start();
      distribute_chunks();
      timer.stop();
      fprintf(stderr, "Distribution time [%6.6f s], ", timer.get_time_seconds());
      timer.clear();
   }
   /************************************************************************
    *
    ************************************************************************/
   void allocate() {
      features = new FeatureArrayType(graph.num_nodes() * SGD_FEATURE_SIZE);
      fprintf(stderr, "Size of edges :: %6.6g MB\n", (float) (graph.num_edges() * sizeof(unsigned int) / (1024 * 1024)));
      locks = new ArrayType(graph.num_nodes());
      metadata = new ArrayType(16);
      h_edge_info = new int[2 * graph.num_edges()];
      h_ratings = new float[graph.num_edges()];
      edge_step = new ArrayType(graph.num_edges());
      h_edge_indices = new int[graph.num_nodes() + 1];
      h_edge_destinations = new int[graph.num_edges()];
      lock_edge_wl = new int  [graph.num_edges()];
      ///////////////////////////////////////////////////////////
      lock_free_node_kernel.init("./apps/sgd/sgd_static_operator.cl", "sgd_lock_free_node_operator");
      lock_edge_kernel.init("./apps/sgd/sgd_static_operator.cl", "lock_edge_operator");
//      lock_edge_kernel.set_arg_list(locks, features, metadata, edge_info, ratings, lock_edge_wl);
   }
   /************************************************************************
    *
    ************************************************************************/
   void deallocate() {
      delete[] h_edge_indices;
      delete[] h_edge_destinations;
      delete[] h_movies_ordered;
      delete h_edge_info;
      delete h_ratings;
      if (edge_step)
         delete edge_step;
      if (metadata)
         delete metadata;
      if (lock_edge_wl)
         delete lock_edge_wl;
      if (e_edge_info)
         delete e_edge_info;
      if (e_ratings)
         delete e_ratings;
      if (edge_reorder_map)
         delete edge_reorder_map;
      if (locks)
         delete locks;
      if (features)
         delete features;
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_device() {
      features->copy_to_device();
      locks->copy_to_device();
      edge_step->copy_to_device();
//      edge_info->copy_to_device();
//      ratings->copy_to_device();
      metadata->copy_to_device();
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_host() {
      features->copy_to_host();
//      locks->copy_to_host(); //We dont need this back on host!
      edge_step->copy_to_host();
//      edge_info->copy_to_host(); //We dont need this back on host!
//      ratings->copy_to_host();
      metadata->copy_to_host();
   }
   /************************************************************************
    *Create blocks of movies sized the same as the number of concurrent threads.
    *Also track the edge indices w.r.t original graph so we can relate 'sorted' edges
    *with edges in the original graph.
    ************************************************************************/
   void distribute_chunks() {
#ifdef _SGD_USE_SHARED_MEM_
      unsigned long l_m_size = Galois::OpenCL::OpenCL_Setup::local_memory_size();
      size_t local_size = std::min((size_t) lock_free_node_kernel.get_default_workgroup_size(), (size_t) (l_m_size / (sizeof(float) * SGD_FEATURE_SIZE)));
      //TODO RK : Fix hack for invalid kernel sizes.
            local_size=(512+768)/2;
            //END HACK
#else
      size_t local_size = lock_free_node_kernel.get_default_workgroup_size();
#endif
      lock_free_node_kernel.set_work_size(local_size * Galois::OpenCL::OpenCL_Setup::get_device_eu(), local_size);
      const size_t num_threads = local_size * Galois::OpenCL::OpenCL_Setup::get_device_eu();
      int edge_counter = 0;
      fprintf(stderr, "Movie chunk size: %lu", num_threads);
      edge_reorder_map = new int[graph.num_edges()];
      for (size_t b = 0; b < movies.size(); b += num_threads) {
         MovieChunk curr_block(b, std::min(b + num_threads, movies.size()), 0, 0);
         curr_block.max_degree = graph.num_neighbors(h_movies_ordered[curr_block.start_index]);
         curr_block.edge_start = edge_counter;
         for (int m_idx = curr_block.start_index; m_idx < curr_block.end_index; ++m_idx) {
            int movie_id = h_movies_ordered[m_idx];
            for (unsigned int e_idx = graph.outgoing_index()[movie_id]; e_idx < graph.outgoing_index()[movie_id + 1]; ++e_idx) {
               curr_block.num_edges++;
               int src = graph.get_edge_src(e_idx);
               int dst = graph.out_neighbors()[e_idx];
               float wt = graph.out_edge_data()[e_idx] / (float) max_rating;
               h_edge_info[2 * edge_counter] = src;
               h_edge_info[2 * edge_counter + 1] = dst;
               h_ratings[edge_counter] = wt;
               edge_reorder_map[e_idx] = edge_counter;
               edge_step->host_ptr()[edge_counter] = -1;
               edge_counter++;
            }
         }
         curr_block.edge_end = edge_counter;
         movie_chunks.push_back(curr_block);
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   void operator()(int num_steps) {
      copy_to_device();
      round = 0;
      this->build_matching();
      copy_to_host();
      update_schedule();
      SGDCommon::compute_err(graph, features, max_rating);
      for (; round < num_steps; ++round) {
         this->lock_free_node_operator();
         stats.end_round();
         SGDCommon::compute_err(graph, features, max_rating);
      }
   }
   /************************************************************************
    * Once the edge_step has all the matching numbers for each chunk,
    * we can proceed to reorder the edges for each movie in increasing matching
    * number.
    ************************************************************************/
   void update_schedule() {
      struct EdgeTuple {
         int dst;
         float wt;
         int step;
         EdgeTuple(int d, float w, int s) :
               dst(d), wt(w), step(s) {
         }
      };
      int * tmp = new int[graph.num_edges()]; //wl_0->host_ptr();
      memcpy(tmp, edge_step->host_ptr(), sizeof(int) * graph.num_edges());
      { //1.1) Sort data according to step.
         for (size_t i = 0; i < movies.size(); ++i) {
            int m = h_movies_ordered[i];
            std::vector<EdgeTuple> tmp_store;
            for (unsigned int e_i = graph.outgoing_index()[m]; e_i < graph.outgoing_index()[m + 1]; ++e_i) {
               int dst = graph.out_neighbors()[e_i];
               float rating = graph.out_edge_data()[e_i] / (float) (max_rating);
               int step = tmp[edge_reorder_map[e_i]];
               tmp_store.push_back(EdgeTuple(dst, rating, step));
            }
            std::sort(tmp_store.begin(), tmp_store.end(), [](const EdgeTuple & lhs, const EdgeTuple & rhs) {return lhs.step<rhs.step;});
            //Now copy back!
            for (unsigned int e_i = graph.outgoing_index()[m], j = 0; e_i < graph.outgoing_index()[m + 1]; ++e_i, ++j) {
               graph.out_neighbors()[e_i] = tmp_store[j].dst;
               h_ratings[e_i] = tmp_store[j].wt; /// (float) (max_rating);
               edge_step->host_ptr()[e_i] = tmp_store[j].step;
            }
         }

      }
      { //2) Copy data

         memcpy(h_edge_indices, graph.outgoing_index(), sizeof(unsigned int) * graph.num_nodes());
         memcpy(h_edge_destinations, graph.out_neighbors(), sizeof(unsigned int) * graph.num_edges());
//         ratings->copy_to_device();
         edge_step->copy_to_device();
         features->copy_to_device();
//         movies_ordered->copy_to_device();
      }
//      if(false)
      if (UseLockEdge && EDGE_CHUNK_NUM > 0) {
#if 0
         e_edge_info = new ArrayType(graph.num_edges() * 2);
         e_ratings = new FeatureArrayType(graph.num_edges());
         int edge_counter = 0;
         for (int m_idx = movie_chunks[0].start_index; m_idx < movie_chunks[0].end_index; ++m_idx) {
            int movie = movies_ordered->host_ptr()[m_idx];
            for (unsigned int e_idx = graph.outgoing_index()[movie]; e_idx < graph.outgoing_index()[movie + 1]; ++e_idx) {
               int src = graph.get_edge_src()[e_idx];
               int dst = graph.out_neighbors()[e_idx];
               int rating = graph.out_edge_data()[e_idx];
               e_edge_info->host_ptr()[edge_counter * 2] = src;
               e_edge_info->host_ptr()[edge_counter * 2 + 1] = dst;
               e_ratings->host_ptr()[edge_counter] = rating / (float) (max_rating);
               edge_counter++;
            }
         }
         e_edge_info->copy_to_device();
         e_ratings->copy_to_device();

         int num_items = movie_chunks[0].edge_end - movie_chunks[0].edge_start;
         for (int i = 0; i < num_items; ++i)
         lock_edge_wl->host_ptr()[i] = movie_chunks[0].edge_start + i;
         std::random_shuffle(lock_edge_wl->host_ptr(), lock_edge_wl->host_ptr() + num_items);
         lock_edge_wl->copy_to_device();
         lock_edge_kernel.set_arg_list(locks, features, metadata, e_edge_info, e_ratings, lock_edge_wl);
//         fprintf(stderr, "Done - setup edge+lock kernel!\n");
#else
//         int chunk_num = 0;
         int num_items = 0;
         int edge_counter = 0;
         e_edge_info = new int[(graph.num_edges() * 2)];
         e_ratings = new float[(graph.num_edges())];
         for (int chunk_num = 0; chunk_num < EDGE_CHUNK_NUM; ++chunk_num) {
            num_items += movie_chunks[chunk_num].edge_end - movie_chunks[chunk_num].edge_start;
            for (int m_idx = movie_chunks[chunk_num].start_index; m_idx < movie_chunks[chunk_num].end_index; ++m_idx) {
               int movie = h_movies_ordered[m_idx];
               for (unsigned int e_idx = graph.outgoing_index()[movie]; e_idx < graph.outgoing_index()[movie + 1]; ++e_idx) {
                  lock_edge_wl[edge_counter] = e_idx;
                  edge_counter++;
               }
            }

         }
         edge_chunk.edge_start = 0;
         edge_chunk.edge_end = edge_counter;
         std::random_shuffle(lock_edge_wl, lock_edge_wl + num_items);
         for (int i = 0; i < num_items; ++i) {
            int e_idx = lock_edge_wl[i];
            int src = graph.get_edge_src()[e_idx];
            int dst = graph.out_neighbors()[e_idx];
            int rating = graph.out_edge_data()[e_idx];
            e_edge_info[i * 2] = src;
            e_edge_info[i * 2 + 1] = dst;
            e_ratings[i] = rating / (float) (max_rating);
         }

//         e_edge_info->copy_to_device();
//         e_ratings->copy_to_device();
//         lock_edge_wl->copy_to_device();
//         lock_edge_kernel.set_arg_list(locks, features, metadata, e_edge_info, e_ratings, lock_edge_wl);
         //         fprintf(stderr, "Done - setup edge+lock kernel!\n");
         fprintf(stderr, "Edge-lock-free setup: [%d,%d]\n", edge_chunk.edge_start, edge_chunk.edge_end);
#endif
      }
   }
   /************************************************************************
    *
    ************************************************************************/
   float lock_edge_operator(MovieChunk & m) {
      Galois::OpenCL::Timer timer;
      timer.start();
      //Copy metadata to num_items
//      e_edge_info->copy_to_device();
      edge_data.ratings = new GPUFeatureArrayType(graph.num_edges());
      edge_data.edge_info = new GPUArrayType(graph.num_edges() * 2);
      edge_data.ratings->copy_to_device(e_ratings);
      edge_data.edge_info->copy_to_device(e_edge_info);
      features->copy_to_device();
//      lock_edge_wl->copy_to_device();
      lock_edge_kernel.set_arg_list(locks, features, metadata, edge_data.edge_info, edge_data.ratings);
      const int num_items = m.edge_end - m.edge_start;
      {
         metadata->host_ptr()[0] = num_items;
         metadata->host_ptr()[1] = round;
         metadata->host_ptr()[2] = num_items;
         float t = SGDCommon::SGD_STEP_SIZE(round);
         metadata->host_ptr()[3] = (t);
         metadata->host_ptr()[4] = num_items;
         metadata->host_ptr()[5] = m.edge_start;
         metadata->host_ptr()[6] = m.edge_end;
         lock_edge_kernel.set_arg(5, sizeof(cl_float), &t);
         lock_edge_kernel.set_work_size(num_items);
      }
      int curr_step = 0;
      metadata->copy_to_device();
      timer.stop();
      float copy_time = timer.get_time_seconds();
      timer.stop();
      Galois::OpenCL::OpenCL_Setup::finish();
      timer.start();
      float kernel_time = 0;
      do {
         ++curr_step;
         lock_edge_kernel();
         Galois::OpenCL::OpenCL_Setup::finish();
         kernel_time += lock_edge_kernel.last_exec_time();
         metadata->copy_to_host();
         /*float ratio_remaining = metadata->host_ptr()[0] / (float) num_items;
          fprintf(stderr, "Step[%d], [%d], %6.6g \n", curr_step, metadata->host_ptr()[0], ratio_remaining);*/
         assert(metadata->host_ptr()[0] >= 0 && "Items processed overflows!");
      } while (metadata->host_ptr()[0] != 0);
      timer.stop();
      delete edge_data.edge_info;
      delete edge_data.ratings;
      features->copy_to_host();
//      kernel_time = timer.get_time_seconds();
      stats.add_stats("Round",round);
      stats.add_stats("Steps",curr_step);
      stats.add_stats("Edges",num_items);
      stats.add_stats("TotalTime",timer.get_time_seconds());
      stats.add_stats("CopyTime",copy_time);
      stats.add_stats("KernelTime",kernel_time);
      stats.add_stats(matching_header_str, matching_time);
//      stats.end_round();
//      fprintf(stderr, "Round, %d, Steps, %d , Edges, %d, Total-time(s), %6.6g ,Copy_Time(s), %6.6g, Kernel_time(s), %6.6g \n",
//            round, curr_step, num_items, timer.get_time_seconds(), copy_time, kernel_time);
      return kernel_time;
   }
   /************************************************************************
    *
    *************************************************************************/
   void setup_node_data(){
#ifdef _SGD_USE_SHARED_MEM_
      unsigned long l_m_size = Galois::OpenCL::OpenCL_Setup::local_memory_size();
      size_t local_size = std::min((size_t) lock_free_node_kernel.get_default_workgroup_size(), (size_t) (l_m_size / (sizeof(float) * SGD_FEATURE_SIZE)));
      //TODO RK : Fix hack for invalid kernel sizes.
      local_size=(512+768)/2;
      //END HACK
      const int shmem_size = sizeof(float) * SGD_FEATURE_SIZE * local_size;
#else
      size_t local_size = lock_free_node_kernel.get_default_workgroup_size();
      const int shmem_size = 1;
#endif
      lock_free_node_kernel.set_work_size(local_size * Galois::OpenCL::OpenCL_Setup::get_device_eu(), local_size);
      node_data.edge_destinations = new GPUArrayType(graph.num_edges());
      node_data.ratings = new GPUFeatureArrayType(graph.num_edges());
      node_data.edge_indices = new GPUArrayType(graph.num_nodes() + 1);
      node_data.movies_ordered = new GPUArrayType(graph.num_nodes());
      node_data.edge_destinations->copy_to_device(h_edge_destinations);
      node_data.edge_indices->copy_to_device(h_edge_indices);
      node_data.movies_ordered->copy_to_device(h_movies_ordered, movies.size());
      node_data.ratings->copy_to_device(h_ratings);
      edge_step->copy_to_device();
      lock_free_node_kernel.set_arg_list(node_data.movies_ordered, features, metadata, node_data.edge_indices, node_data.edge_destinations, node_data.ratings, edge_step);
      float f = SGDCommon::SGD_STEP_SIZE(round);
      lock_free_node_kernel.set_arg(7, sizeof(float), &f);
      lock_free_node_kernel.set_arg(8, shmem_size, NULL);
   }
   /************************************************************************
       *
       *************************************************************************/
   void lock_free_node_operator() {
      Galois::OpenCL::Timer timer;
      float kernel_time = 0;
      float setup_time = 0, copy_time = 0;
      timer.clear();
      timer.start();
      //3) Execute kernel
      unsigned long l_m_size = Galois::OpenCL::OpenCL_Setup::local_memory_size();
      size_t local_size = std::min((size_t) lock_free_node_kernel.get_default_workgroup_size(), (size_t) (l_m_size / (sizeof(float) * SGD_FEATURE_SIZE)));
      //TODO RK : Fix hack for invalid kernel sizes.
      local_size=(512+768)/2;
      //END HACK
      int current_chunk = 0;
      timer.stop();
      timer.clear();
      timer.start();
      std::vector<std::pair<int, float>> range_stats;
      long total_edges = 0;
      if (UseLockEdge && EDGE_CHUNK_NUM > 0) {
         /*for (current_chunk = 0; current_chunk < EDGE_CHUNK_NUM; ++current_chunk) {
          kernel_time += lock_edge_operator(movie_chunks[current_chunk]);
          total_edges += movie_chunks[current_chunk].edge_end - movie_chunks[current_chunk].edge_start;
          range_stats.push_back(std::pair<int, float>(movie_chunks[current_chunk].edge_end - movie_chunks[current_chunk].edge_start, kernel_time));
          }*/
         kernel_time = lock_edge_operator(edge_chunk);
         total_edges += edge_chunk.edge_end - edge_chunk.edge_start;
         range_stats.push_back(std::pair<int, float>(movie_chunks[current_chunk].edge_end - movie_chunks[current_chunk].edge_start, kernel_time));
         current_chunk = EDGE_CHUNK_NUM;
      }
      setup_node_data();
      metadata->host_ptr()[8] = 0; //work-counter, should be zero.
      for (unsigned int i = current_chunk; i < movie_chunks.size(); ++i) {
         MovieChunk & range = movie_chunks[i];
         const int num_items = range.end_index - range.start_index;
         metadata->host_ptr()[10] = 0; //barrier_start
         metadata->host_ptr()[11] = 0; //barrier_start
         metadata->host_ptr()[12] = range.start_index; //movies_start
         metadata->host_ptr()[13] = range.end_index; //movies_end
         metadata->host_ptr()[6] = range.max_levels; //range.max_degree;//
         metadata->copy_to_device();
         fprintf(stderr, "Launch- Local %lu, Eu %d, NumItems, %lu", local_size , Galois::OpenCL::OpenCL_Setup::get_device_eu(),  (size_t) (num_items));
         lock_free_node_kernel.set_work_size(std::min(local_size * Galois::OpenCL::OpenCL_Setup::get_device_eu(), (size_t) (num_items)), local_size);
         fprintf(stderr, "Local %lu, Global %lu, EdgesProcessed. %d\n", lock_free_node_kernel.local, lock_free_node_kernel.global, metadata->host_ptr()[8]);
         Galois::OpenCL::OpenCL_Setup::finish();
         lock_free_node_kernel();
         Galois::OpenCL::OpenCL_Setup::finish();
         float f = lock_free_node_kernel.last_exec_time();
         metadata->copy_to_host();
         kernel_time += f;
         range_stats.push_back(std::pair<int, float>(metadata->host_ptr()[8], f));
      }
      fprintf(stderr, "Final count EdgesProcessed:: %d\n", metadata->host_ptr()[8]);
      {
         delete node_data.edge_destinations;
         delete node_data.ratings;
         delete node_data.edge_indices;
         delete node_data.movies_ordered;
      }
      metadata->copy_to_host();
      total_edges += metadata->host_ptr()[8]; //Work counter should be equal to number of edges.
      timer.stop();
      float exec_time = timer.get_time_seconds();
      fprintf(stderr, "Per-chunk-time, ");
      for (auto i : range_stats) {
         fprintf(stderr, "%6.6g,", i.second);
      }
      fprintf(stderr, "\n");
      stats.add_stats("Round", round);
      stats.add_stats("Edges", total_edges);
      stats.add_stats("Setup", setup_time);
      stats.add_stats("TotalTime", exec_time+copy_time);
      stats.add_stats("GPUTime", exec_time);
      stats.add_stats("Kernel", kernel_time);
      stats.add_stats(matching_header_str, matching_time);
//      stats.end_round();
//      fprintf(stderr, "E, %ld, Round, %d, Setup(s), %6.6g, Total-time(s), %6.6g ,GPU_Work(s), %6.6g, Kernel(s), %6.6g \t", total_edges, round, setup_time,
//            exec_time + copy_time,exec_time, kernel_time);
      { //4) Copy results
         features->copy_to_host();
      }
   }
   /************************************************************************
    * Use the movie_chunks to go over each chunk and build a matching for those edges only.
    * This is the "right" balance for the execution model.
    ************************************************************************/
   void build_matching() {
      std::string log_file_name(basename(filename));
      log_file_name.append(UseLockEdge ? "SubGraphEdge" : "SubGraphNode");
      Galois::OpenCL::Timer matching_timer;

      std::string matching_file_name(log_file_name);
      matching_file_name.append(".matching");
      ifstream matching_file(matching_file_name);
      if (matching_file.good()) {
         fprintf(stderr, "Matching info found... loading: %s \n", matching_file_name.c_str());
         matching_timer.clear();
         matching_timer.start();
         SGDCommon::BuildMatchingCompact::from_file(matching_file, graph.num_edges(), h_edge_info, edge_step->host_ptr());
         matching_timer.stop();
         matching_time = matching_timer.get_time_seconds();
         matching_header_str="MatchLoad";


         edge_step->copy_to_device();
         int bCounter=0;
         for (size_t curr_chunk = 0; curr_chunk < movie_chunks.size(); ++curr_chunk,++bCounter) {
            MovieChunk & block = movie_chunks[curr_chunk];
            block.max_levels = 0;
            for (int i = block.edge_start; i < block.edge_end; ++i) {
               block.max_levels = std::max(block.max_levels, edge_step->host_ptr()[i]);
            }
            fprintf(stderr, "Matching,%d, Edges,%d, MaxDegree, %d, MaxLevels, %d\n", bCounter, block.num_edges, block.max_levels,block.max_levels);
         }
      } else {
         fprintf(stderr, "Matching info NOT found... building: %s \n", matching_file_name.c_str());
         copy_to_device();
         SGDCommon::BuildMatchingCompact matching(graph.num_nodes(), graph.num_edges(), h_edge_info, edge_step, log_file_name);
         size_t curr_chunk = 0;
         for (; curr_chunk < movie_chunks.size(); ++curr_chunk) {
            MovieChunk & block = movie_chunks[curr_chunk];
            matching_timer.clear();
            matching_timer.start();
            int curr_step = matching.operator()<PrintMatchingStats>(block.edge_start, block.edge_end);
            matching_timer.stop();
            matching_time += matching_timer.get_time_seconds();
            fprintf(stderr, "\n");
            block.max_levels = curr_step;
            edge_step->copy_to_host();
         }
         matching_header_str="MatchCreate";
         matching.to_file(matching_file_name);
      }
      fprintf(stderr, "Matching info. Num chunks=%d[", (int) movie_chunks.size());
      for (auto m : movie_chunks) {
         fprintf(stderr, "%d, ", m.max_levels);
      }
      fprintf(stderr, "]\n");
   }
   /************************************************************************
    *
    ************************************************************************/
   ~SGDSubGraphMatching() {
      deallocate();
      stats.print();
      fprintf(stderr, "Destroying SGDSubGraphMatching[%s]-features =[%d].\n", UseLockEdge ? "LockEdge(1)" : "LockFree", SGD_FEATURE_SIZE);
   }
};
//###################################################################//
#endif /* SGDSubGraphMatching_H_ */
