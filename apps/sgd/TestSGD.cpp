/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#define _USE_CL 1

//#define _GOPT_DEBUG 1
#include <algorithm>
#include <cstring>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <math.h>
#include <set>
#include <string>
#include <vector>
#include <libgen.h>
#include <random>

#include "../../ocl/ocl_header.h"
#ifndef _USE_CL
#include "cuda_header.h"
#endif

enum SGD_INPUT_TYPE{BIPARTITE=0, ROAD, DIAGONAL, COMPLETE_BIPARTITE};

#include "../../apps/sgd/SGDFunctor.h"
#include "../../apps/matching/GPUMatching.h"
#include "../../ds/GraphUtils.h"

////////////##############################################################///////////
////////////##############################################################///////////
int main(int argc, char ** args) {
    setenv("CUDA_CACHE_DISABLE", "1", 1);
	Galois::OpenCL::OpenCL_Setup::initialize();
	srand(0);
	if (argc != 3){
	   std::cout<<"Usage " << args[0] << " <type> <arguments>.\n";
       std::cout<<"\t b <filename>  -  bipartite graph from file.\n";
       std::cout<<"\t r <filename>  - road network from file.\n";
       std::cout<<"\t d <num_nodes> - diagonal graph (synthetic) for given number of nodes.\n";
       std::cout<<"\t c <num_nodes> - complete bipartite graph (synthetic) for given number of nodes.\n";
       exit(-1);
	}
	const char * fname = args[2];
	SGD_INPUT_TYPE runType = BIPARTITE;
	switch(args[1][0]){
	case 'r':
	   runType = ROAD;
	   break;
	case 'd':
	   runType = DIAGONAL;
	   break;
	case 'c':
	   runType=COMPLETE_BIPARTITE;
	   break;
    case 'b': default:
       runType = BIPARTITE;
       break;
	}
	test_sgd(runType, fname);
	std::cout << "Completed successfully!!!!!!!!!!!\n";
	Galois::OpenCL::OpenCL_Setup::cleanup();
	return 0;
}
////////////##############################################################///////////
////////////##############################################################///////////
