/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
typedef struct _GraphType { 
uint _num_nodes;
uint _num_edges;
 uint node_data_size;
 uint edge_data_size;
 __global NodeData *node_data;
 __global uint *out_index;
 __global uint *out_neighbors;
 __global EdgeData *out_edge_data;
 __global uint * edge_src; }GraphType;
uint in_neighbors_begin(__local GraphType * graph, uint node){ 
 return 0;
}
uint in_neighbors_end(__local GraphType * graph, uint node){ 
 return graph->out_index[node+1]-graph->out_index[node];
}
uint in_neighbors_next(__local GraphType * graph, uint node){ 
 return 1;
}
uint in_neighbors(__local GraphType * graph, uint node, uint nbr){ 
 return graph->out_neighbors[graph->out_index[node]+nbr];
}
__global EdgeData * in_edge_data(__local GraphType * graph, uint node, uint nbr){ 
 return &graph->out_edge_data[graph->out_index[node]+nbr];
}
uint out_neighbors_begin(__local GraphType * graph, uint node){ 
 return 0;
}
uint out_neighbors_end(__local GraphType * graph, uint node){ 
 return graph->out_index[node+1]-graph->out_index[node];
}
uint out_neighbors_next(__local GraphType * graph, uint node){ 
 return 1;
}
uint out_neighbors(__local GraphType * graph,uint node,  uint nbr){ 
 return graph->out_neighbors[graph->out_index[node]+nbr];
}
__global EdgeData * out_edge_data(__local GraphType * graph,uint node,  uint nbr){ 
 return &graph->out_edge_data[graph->out_index[node]+nbr];
}
__global NodeData * node_data(__local GraphType * graph, uint node){ 
 return &graph->node_data[node];
}
void initialize(__local GraphType * graph, __global uint *mem_pool){
uint offset =4;
 graph->_num_nodes=mem_pool[0];
graph->_num_edges=mem_pool[1];
 graph->node_data_size =mem_pool[2];
 graph->edge_data_size=mem_pool[3];
graph->node_data= (__global NodeData *)&mem_pool[offset];
offset +=graph->_num_nodes* graph->node_data_size;
graph->out_index=&mem_pool[offset];
 offset +=graph->_num_nodes + 1;
 graph->out_neighbors=&mem_pool[offset];
offset +=graph->_num_edges;
 graph->out_edge_data=(__global EdgeData*)&mem_pool[offset];
 offset +=graph->_num_edges*graph->edge_data_size;
graph->edge_src = &mem_pool[offset]; offset+=graph->_num_edges;}

