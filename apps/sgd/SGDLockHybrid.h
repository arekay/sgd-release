/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef SGDNODEFUNCTOR_H_
#define SGDNODEFUNCTOR_H_
#include <random>
#include <thread>
#include <atomic>
#include "../../apps/sgd/SGDEdgeFunctorUtil.h"
using namespace Galois::OpenCL;
struct SGDLockHybrid {
   typedef SGD_LC_LinearArray_Undirected_Graph<Galois::CPUArray, unsigned int, unsigned int> GraphTy;
   typedef Array<cl_int> ArrayType;
   typedef GPUArray<cl_int> GPUArrayType;
   typedef Array<FeatureType> FeatureArrayType;
   typedef GPUArray<FeatureType> GPUFeatureArrayType;
   typedef CL_Kernel Kernel;
   ////////////////////////////////////////////////////////////
   /************************************************
    *
    *************************************************/
   struct AsyncConfig {
      AsyncConfig(int buff, int ch, int wl_size) :
            NUM_BUFFERS(buff), NUM_CHUNKS(ch), GPU_WL_SIZE(wl_size) {
      }
      int NUM_BUFFERS;
      int NUM_CHUNKS;
      int GPU_WL_SIZE;
      template<typename Graph>
      void init(Graph & g) {
         using namespace Galois::OpenCL;
         const size_t num_edges = g.num_edges();
         GPU_WL_SIZE = num_edges / NUM_CHUNKS;
      }
   };
   AsyncConfig config;
   GraphTy graph;
   std::vector<int> movies;

   ArrayType * locks;
   FeatureArrayType * features;
   ArrayType * metadata;
   ArrayType * edge_metadata;

   GPUArrayType * edge_indices;
   GPUArrayType * edge_destinations;
   FeatureArrayType * ratings;
   GPUArrayType * movie_index;

   int * h_next_edge_wl;
   int * edge_sources;

   GPUArrayType * next_edge_wl;
   ArrayType * edge_wl;
   SGDEdgeUtil edge_kernel;
   float accumulated_error;
   int round;
   unsigned int max_rating;
   char filename[512];
   Kernel sgd_node_kernel;
   Galois::StatsManager stats;
   /************************************************************************
    *
    *metadata (16)
    *edge_info, worklist, ratings (2+1+1)*NE
    *locks, features*64, (1+64)NN
    ************************************************************************/
   SGDLockHybrid(bool road, const char * p_filename) :
         config(1, 1, 0), round(0) {
      strcpy(filename, p_filename);
      fprintf(stderr, "Creating SGDLockHybrid  -  features =[%d], Chunks=[%d], Buffers=[%d], UseSharedMem=", SGD_FEATURE_SIZE, config.NUM_CHUNKS, config.NUM_BUFFERS);
#ifdef _SGD_USE_SHARED_MEM_
      fprintf(stderr, "[YES]");
#else
      fprintf(stderr, "[NO]");
#endif

      if (road) {
         SGDCommon::read_as_bipartite(graph, filename);
      } else {
         graph.read(p_filename);
      }
      config.init(graph);
      allocate();
      initialize_features_random();
      prepare_metadata();
      fprintf(stderr, "NumMovies[%ld]\n", movies.size());
      if (false) {
         std::ofstream header("graph_header.h");
         header << Galois::OpenCL::str_SGD_LC_LinearArray_Undirected_Graph << "\n";
         header.close();
      }
   }
   /************************************************************************
     *
     ************************************************************************/
   SGDLockHybrid(int num_m) :
      config(1, 1, 0), round(0) {
       strcpy(filename, "gen-diagonal-input");
       fprintf(stderr, "Creating SGDLockHybrid -  features =[%d], Chunks=[%d], Buffers=[%d] .\n", SGD_FEATURE_SIZE, config.NUM_CHUNKS, config.NUM_BUFFERS);
       SGDCommon::diagonal_graph(graph, num_m);
       config.init(graph);
       allocate();
       initialize_features_random();
       fprintf(stderr, "Number of movies found :: %ld\n", movies.size());
    }
   /************************************************************************
    *
    ************************************************************************/

   SGDLockHybrid(int num_m, int num_u) :
         config(1, 1, 0), round(0) {
      strcpy(filename, "generated-input");
      fprintf(stderr, "Creating SGDLockHybrid  -  features =[%d], Chunks=[%d], Buffers=[%d], UseSharedMem=", SGD_FEATURE_SIZE, config.NUM_CHUNKS, config.NUM_BUFFERS);
#ifdef _SGD_USE_SHARED_MEM_
      fprintf(stderr, "[YES]");
#else
      fprintf(stderr, "[NO]");
#endif

      SGDCommon::complete_bipartitie(graph, num_m, num_u);
      config.init(graph);
      allocate();
      initialize_features_random();
      prepare_metadata();
      fprintf(stderr, "NumMovies[%ld]\n", movies.size());
   }
   /************************************************************************
    *
    ************************************************************************/
   void allocate() {
      features = new FeatureArrayType(graph.num_nodes() * SGD_FEATURE_SIZE);
      locks = new ArrayType(graph.num_nodes());
      metadata = new ArrayType(16);
      edge_metadata = new ArrayType(16);
      ratings = new FeatureArrayType(graph.num_edges());
      h_next_edge_wl = new int[graph.num_edges()];
      edge_sources = new int[graph.num_edges()];
      sgd_node_kernel.init("./apps/sgd/sgd_lock_hybrid.cl", "sgd_node_operator");

      unsigned long l_m_size = Galois::OpenCL::OpenCL_Setup::local_memory_size();
#ifdef _SGD_USE_SHARED_MEM_
      size_t local_size = std::min((size_t)sgd_node_kernel.get_default_workgroup_size(), (size_t)(l_m_size/(sizeof(float)*SGD_FEATURE_SIZE)));
      //TODO RK : Fix hack for invalid kernel sizes.
      local_size=(512+768)/2;
      sgd_node_kernel.set_arg(8, sizeof(float) * SGD_FEATURE_SIZE * local_size, NULL);
#else
      size_t local_size = sgd_node_kernel.get_default_workgroup_size();
      sgd_node_kernel.set_arg(8, sizeof(float) * SGD_FEATURE_SIZE * 1, NULL);
#endif
      fprintf(stderr, "SharedMem=[%6.6g],LocalSize [%d]\n", SGDCommon::toMB(l_m_size), (int) local_size);
//      fprintf(stderr, "memory used :: %6.6g \n", Galois::OpenCL::OpenCL_Setup::allocated_bytes / (float) (1024 * 1024));
      OpenCL_Setup::stats.print();
      sgd_node_kernel.set_work_size(movies.size(), local_size);
   }
   /************************************************************************
    *
    ************************************************************************/
   void deallocate() {
      delete features;
      delete locks;
      delete metadata;
      delete ratings;

      delete[] h_next_edge_wl;
      delete[] edge_sources;
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_device() {
      features->copy_to_device();
      locks->copy_to_device();
      metadata->copy_to_device();
      ratings->copy_to_device();
   }
   /************************************************************************
    *
    ************************************************************************/
   void copy_to_host() {
      features->copy_to_host();
      metadata->copy_to_host();
   }
   /************************************************************************
    *
    ************************************************************************/
   void prepare_metadata() {
      metadata->host_ptr()[0] = graph.num_nodes();
      //metadata->host_ptr()[1] = round;
      metadata->host_ptr()[2] = graph.num_edges();
      //metadata->host_ptr()[3] step_size;
      metadata->host_ptr()[4] = movies.size();
      metadata->host_ptr()[6] = 1; //Start phase
      metadata->host_ptr()[7] = graph.num_edges(); //wl_0 size!
      metadata->host_ptr()[8] = 0; //work_counter!
      metadata->host_ptr()[9] = 0; //next_wl_counter
//      sgd_node_kernel.set_arg(6, movie_index);
      if (false) {
         unsigned int counter = 0;
         for (int i = 0; i < metadata->host_ptr()[4]; ++i) {
            unsigned int movie = movies[i];
            for (unsigned int idx = graph.outgoing_index()[movie]; idx < graph.outgoing_index()[movie + 1]; ++idx) {
               //int dst = graph.out_neighbors()[idx];
               counter++;
            }
         }
         std::cout << "Should touch :: " << counter << " edges\n";
      }
//      sgd_node_kernel.set_work_size(movies.size(), sgd_node_kernel.local);
   }
   /************************************************************************
    *
    ************************************************************************/
   void initialize_features_random() {
      Galois::OpenCL::Timer timer;
      timer.start();
      SGDCommon::initialize_features_random(graph, features, locks, movies);
      movies.clear();
      //For each node, initialize features to random, and lock to be unlocked.
      unsigned int max_degree = 0, max_degree_id = 0;
      for (unsigned int i = 0; i < graph.num_nodes(); ++i) {
         for (unsigned int j = graph.outgoing_index(i); j < graph.outgoing_index(i + 1); ++j) {
            edge_sources[j] = i;
         }
         if (graph.num_neighbors(i) > max_degree) {
            max_degree = graph.num_neighbors(i);
            max_degree_id = i;
         }
         if (graph.num_neighbors(i) > 0) {
            movies.push_back(i);
         } else {

         }
      }
      max_rating = 0;
      for (unsigned int i = 0; i < graph.num_edges(); ++i) {
         max_rating = std::max(max_rating, graph.out_edge_data()[i]);
      }
      for (unsigned int i = 0; i < graph.num_edges(); ++i) {
         ratings->host_ptr()[i] = graph.out_edge_data()[i] / (float) max_rating;
      }
//      SGDCommon::reorder(graph);
      /*{ // Uncomment for max_degree time
       movies.clear();
       movies.push_back(max_degree_id);
       }*/
      /*{
       //Uncomment! // Sort movies by degree - reduce thread divergence?
       std::vector<std::pair<int, int> > m_degrees;
       for(auto m:movies){
       m_degrees.push_back(std::pair<int, int>(m,graph.num_neighbors(m)));
       }
       movies.clear();
       std::sort(m_degrees.begin(), m_degrees.end(), [](const std::pair<int, int>& lhs, const std::pair<int, int>& rhs) {
       return lhs.second > rhs.second;});
       for(auto m : m_degrees){
       movies.push_back(m.first);
       }

       }
       */
      /*std::sort(debug_data.user_degrees.begin(), debug_data.user_degrees.end(), [](const std::pair<int, int>& lhs, const std::pair<int, int>& rhs) {
       return lhs.second > rhs.second;});
       std::sort(debug_data.movie_degrees.begin(), debug_data.movie_degrees.end(), [](const std::pair<int, int>& lhs, const std::pair<int, int>& rhs) {
       return lhs.second > rhs.second;});
       */
      /*{
       //Code to rearrange/sort movies for threads to process in certain order.
       movies.clear();

       for (auto m : movie_degrees) {
       if (m.second > 0)
       movies.push_back(m.first);
       }
       }*/

      //TODO: RK - shuffle if necessary. Usually a bad idea (for bgg) since locality is lost in features.
//      std::random_shuffle(movies.begin(), movies.end());
      const int items_per_chunk = ceil(graph.num_edges() / config.NUM_CHUNKS);
      fprintf(stderr, "] , max_Rating: %d, Chunk-size: %d, movies: %ld, Max degree:: %d for node: %d\n", max_rating, items_per_chunk, movies.size(), max_degree, max_degree_id);

//      std::cout << "] , max_Rating: " << max_rating << ", Chunk-size: " << items_per_chunk << ", movies: " << movies.size() << "\n";
//      std::cout << "Max degree:: " << max_degree << " for node: " << max_degree_id << "\n";
//      movie_index = new ArrayType(movies.size());
//      memcpy(h_movie_index, movies.data(), sizeof(int) * movies.size());
      timer.stop();
      fprintf(stderr, "InitTime %6.6f (s), ", timer.get_time_seconds());
      timer.clear();
      timer.start();
      timer.stop();
      fprintf(stderr, "DistTime %6.6f (s) \n", timer.get_time_seconds());
   }
   /************************************************************************
    *
    ************************************************************************/
   void operator()(int num_steps) {
      copy_to_device();
      SGDCommon::compute_err(graph, features, max_rating);
      for (round = 0; round < num_steps; ++round) {
         float t = SGDCommon::SGD_STEP_SIZE(round);
         metadata->host_ptr()[1] = round;
         metadata->host_ptr()[3] = (t);
         metadata->host_ptr()[8] = 0; //work_counter!
         metadata->host_ptr()[9] = 0; //work_counter!
         metadata->copy_to_device();
         sgd_node_kernel.set_arg(7, sizeof(cl_float), &t);
         this->gpu_operator();
         copy_to_host();
         SGDCommon::compute_err(graph, features, max_rating);
         if (false) // Collect stats about unprocessed edges.
         {
            ratings->copy_to_host();
            int movie_degree = 0, max_degree = 0, min_degree = std::numeric_limits<int>::max();
            int counter = 0;
            for (size_t i = 0; i < graph.num_edges(); ++i) {
               if (ratings->host_ptr()[i] != -1) {
                  unsigned int src = graph.get_edge_src(i);
//                  unsigned int dst = graph.out_neighbors()[i];
                  movie_degree += graph.num_neighbors(src);
                  max_degree = std::max(max_degree, (int) graph.num_neighbors(src));
                  min_degree = std::min(min_degree, (int) graph.num_neighbors(src));
                  counter++;
               }
            }
            fprintf(stderr, "Unprocessed_edges,%d, max_degree,%d, min_degree, %d, average_degree,%6.6g \n", counter, max_degree, min_degree, (movie_degree / (float) counter));
//            std::cout << "Unprocessed_edges, " << counter << " max_degree," << max_degree << " min_degree, " << min_degree << ", average_degree" << (movie_degree / (float) counter) << "\n";
         }
      }
   }
   void node_operator_copy_to_device() {
      edge_indices = new GPUArrayType(graph.num_nodes() + 1);
      edge_destinations = new GPUArrayType(graph.num_edges());
      movie_index = new GPUArrayType(movies.size());
      next_edge_wl = new GPUArrayType(graph.num_edges());

      locks->copy_to_device();
      features->copy_to_device();
      metadata->copy_to_device();
      ratings->copy_to_device();
      memset(h_next_edge_wl, 0, sizeof(int) * graph.num_edges());
      next_edge_wl->copy_to_device(h_next_edge_wl);
      edge_indices->copy_to_device(graph.outgoing_index());
      edge_destinations->copy_to_device(graph.out_neighbors());
      movie_index->copy_to_device(movies.data());

      sgd_node_kernel.set_arg_list(locks, features, metadata, edge_indices, edge_destinations, ratings, movie_index);
      {
//         std::cout << "Local memory size is " << l_m_size << " bytes, " << SGDCommon::toMB(l_m_size) << "\n";
#ifdef _SGD_USE_SHARED_MEM_
         unsigned long l_m_size = Galois::OpenCL::OpenCL_Setup::local_memory_size();
         size_t local_size = std::min((size_t)sgd_node_kernel.get_default_workgroup_size(), (size_t)(l_m_size/(sizeof(float)*SGD_FEATURE_SIZE)));
         //TODO RK : Fix hack for invalid kernel sizes.
         local_size=(512+768)/2;
         sgd_node_kernel.set_arg(8, sizeof(float) * SGD_FEATURE_SIZE * local_size, NULL);
#else
         size_t local_size = sgd_node_kernel.get_default_workgroup_size();
         sgd_node_kernel.set_arg(8, sizeof(float) * 1, NULL);
#endif
         sgd_node_kernel.set_work_size(movies.size(),local_size);
      }

      sgd_node_kernel.set_arg(9, next_edge_wl);
//      fprintf(stderr, "WSize %d , %d, %d \n", (int) (movies.size()), sgd_node_kernel.global, sgd_node_kernel.local);
   }
   /*
    __global int * locks,
    __global FeatureTypeSIMD * features,
    __global int * metadata,
    __global int * edge_indices,
    __global int * edge_destinations,
    __global FeatureType * ratings,
    __global int * movies,
    FeatureType step_size,
    __local FeatureTypeSIMD * cache,
    __global uint * next_wl
    * */
   void node_operator_copy_to_host() {
//      ratings->copy_to_host();
      features->copy_to_host();
      next_edge_wl->copy_to_host(h_next_edge_wl);
      delete edge_indices;
      delete edge_destinations;
      delete movie_index;
      delete next_edge_wl;
//      locks->copy_to_device();
      edge_wl = new ArrayType(graph.num_edges() * 3);
      metadata->copy_to_host();
      std::random_shuffle(h_next_edge_wl, h_next_edge_wl + metadata->host_ptr()[9]);
      const int num_edges = metadata->host_ptr()[9];
      for (int i = 0; i < num_edges; ++i) {
         int edge_idx = h_next_edge_wl[i];
         edge_wl->host_ptr()[3 * i + 0] = edge_sources[edge_idx];
         edge_wl->host_ptr()[3 * i + 1] = graph.out_neighbors()[edge_idx];
         edge_wl->host_ptr()[3 * i + 2] = ((int *) (ratings->host_ptr()))[edge_idx];
      }
      edge_wl->copy_to_device();
      edge_kernel.init(std::string(filename),locks, features, edge_metadata, edge_wl);
   }
   /************************************************************************
    *
    ************************************************************************/
   void gpu_operator() {
      {
         Galois::OpenCL::Timer timer, kernel_timer;
         kernel_timer.clear();
         timer.clear();
         timer.start();
         node_operator_copy_to_device();
         timer.stop();
         const float node_setup = timer.get_time_seconds();
         kernel_timer.start();
         sgd_node_kernel();
         sgd_node_kernel.wait();
         Galois::OpenCL::OpenCL_Setup::finish();
         kernel_timer.stop();
         metadata->copy_to_host();
         const float node_kernel_time = kernel_timer.get_time_seconds();
         timer.clear();
         timer.start();
         node_operator_copy_to_host();
         timer.stop();
         const float edge_setup = timer.get_time_seconds();
         timer.clear();
         int * mptr = metadata->host_ptr();
         std::pair<int, float> edge_res = edge_kernel((int) (mptr[9]), round, node_kernel_time);
         stats.add_stats("Round",round);
         stats.add_stats("NLocal",sgd_node_kernel.local);
         stats.add_stats("NodeSetup",node_setup);
         stats.add_stats("NodeTime",node_kernel_time);
         stats.add_stats("Conflicts",mptr[9]);
         stats.add_stats("Conflicts(%)",100 * mptr[9] / (float) (graph.num_edges()));
         stats.add_stats("EdgeSetup",edge_setup);
         stats.add_stats("ELocal",edge_kernel.sgd_edge_kernel.local);
         stats.add_stats("EdgeTime",edge_res.second);
         stats.add_stats("ESteps",edge_res.first);
         stats.add_stats("TotalTime",node_kernel_time + edge_res.second);
//         fprintf(stderr, "Round, %d , NodeSetup, %6.6g, Node-time(s), %6.6g ,Conflicts, %d, %6.6f%% ", round, node_setup, node_kernel_time, mptr[9],
//               100 * mptr[9] / (float) (graph.num_edges()));
//         fprintf(stderr, "EdgeSetup, %6.6g, Edge-time(s), %6.6g, Total-time(s), %6.6g ,Steps, %d, ", edge_setup, edge_res.second, node_kernel_time + edge_res.second,
//               edge_res.first);
         stats.end_round();
         delete edge_wl;
      }

      /*Galois::OpenCL::Timer timer;
       timer.start();
       sgd_node_kernel();
       metadata->copy_to_host();
       timer.stop();
       int * mptr = metadata->host_ptr();
       //      fprintf(stderr, "NN,NE = [%d,%d], Movies [%d] Completed [%d] = [%6.6f%%] ", mptr[0], mptr[2], mptr[4], mptr[9] / 3, 100 * mptr[9] / (float) (graph.num_edges() * 3));
       std::pair<int, float> edge_res = edge_kernel((int) (mptr[9] / 3), round);
       fprintf(stderr, "Round, %d , Node-time(s), %6.6g ,Conflicts, %d, %6.6f%% ", round, timer.get_time_seconds(), mptr[9] / 3, 100 * mptr[9] / (float) (graph.num_edges() * 3));
       fprintf(stderr, "Edge-time(s), %6.6g, Total-time(s), %6.6g ,Steps, %d, ", edge_res.second, timer.get_time_seconds() + edge_res.second, edge_res.first);*/
      //sgd_edge_operator( locks, features, metadata, edge_info, ratings, step_size)
   }
   /************************************************************************
    *
    ************************************************************************/
   ~SGDLockHybrid() {
      deallocate();
      stats.print();
      fprintf(stderr, "Destroying SGDLockHybrid object.\n");
   }
};
#endif /* SGDNODEFUNCTOR_H_ */
