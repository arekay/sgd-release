/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef SGDSTATICSCHEDULE_H_
#define SGDSTATICSCHEDULE_H_

namespace SGDCommon {
struct NodeInfo {
   NodeInfo(int _id) :
         id(_id), degree(0) {
   }
   int id;
   int degree;
};
struct MovieInfo {
   typedef std::pair<int, int> EdgeType;
   MovieInfo(int _id, int _degree) :
         id(_id), degree(_degree) {
   }
   int id;
   int degree;
   std::vector<EdgeType> edges;
   void sort_edges(std::map<int, NodeInfo *> & node_info) {
      std::sort(edges.begin(), edges.end(), [&](const std::pair<int, int>& lhs, const std::pair<int, int>& rhs) {
         return node_info[lhs.first]->degree> node_info[rhs.first]->degree;});
   }
   void print_edges(std::map<int, NodeInfo *> & node_info) {
      std::cout << id << "[" << degree << "]-Edges:[";
      for (auto e : edges) {
         std::cout << "(" << e.first << "/" << node_info[e.first]->degree << "),";
      }
      std::cout << "]\n";
   }
};
/*********************************************************************************************************
 *
 **********************************************************************************************************/
template<typename GraphType>
void reorder(GraphType & graph) {
   std::vector<int> users;
   std::map<int, NodeInfo *> user_degrees;
   std::vector<MovieInfo*> movie_info;
   int max_degree = 0;
   for (unsigned int i = 0; i < graph.num_nodes(); ++i) {
      if (graph.num_neighbors(i) > 0) {
         MovieInfo *m = new MovieInfo(i, graph.num_neighbors(i));
         for (unsigned int j = graph.outgoing_index()[i]; j < graph.outgoing_index()[i + 1]; ++j) {
            m->edges.push_back(std::pair<int, int>(graph.out_neighbors()[j], graph.out_edge_data()[j]));
         }
         movie_info.push_back(m);
         max_degree = std::max(max_degree, (int) (graph.num_neighbors(i)));
      } else {
         users.push_back(i);
         user_degrees[i] = new NodeInfo(i);
      }
   }
   std::sort(movie_info.begin(), movie_info.end(), [&](const MovieInfo * lhs, const MovieInfo * rhs) {
      return lhs->degree> rhs->degree;});

   std::cout << "Reorder-1, movies," << movie_info.size() << ", users," << users.size() << "\n";
   for (unsigned int i = 0; i < graph.num_edges(); ++i) {
      unsigned int dst = graph.out_neighbors()[i];
      user_degrees[dst]->degree++;
   }
   std::cout << "Reorder-2, Max_degree," << max_degree << "\n";
   std::map<int, int> user_order;
   MovieInfo * max_movie = nullptr;
   /////////////////////Find max_degree movie, build order on users!
   { //Code to create a schedule
      const int num_movies_per_block = 192; //movie_info.size();
      const int max_user_per_block = max_degree; //user_degrees.size();
      for (auto m : movie_info) {
         if (m->degree == max_degree) {
            //m->sort_edges(user_degrees);
            for (int e = 0; e < std::min(max_user_per_block, (int) m->edges.size()); ++e) {
               user_order[m->edges[e].first] = e; //map indices to e.
            }
            max_movie = m;
            //            break;
         }
         m->sort_edges(user_degrees);
      }
      /*{//If all users to be included.
       //std::sort(user_degrees.begin(), user_degrees.end(), [](const NodeInfo *& lhs, const NodeInfo *&rhs){return lhs->degree > rhs->degree;});
       std::vector<int> user_sorted;
       for(auto n: user_degrees)
       user_sorted.push_back(n.second->id);
       std::sort(user_sorted.begin(), user_sorted.end(), [&](const int & lhs, const int & rhs){return user_degrees[lhs]->degree > user_degrees[rhs]->degree;});
       user_order.clear();
       for(int i=0; i<user_sorted.size(); ++i){
       user_order[user_sorted[i]]=i;
       }
       }*/

      int ** schedule = new int *[num_movies_per_block];
      for (int i = 0; i < num_movies_per_block; ++i) {
         schedule[i] = new int[max_user_per_block];
         memset(&schedule[i][0], -1, sizeof(int) * max_user_per_block);
      }
      for (int i = 0; i < std::min(max_user_per_block, (int) max_movie->edges.size()); ++i) {
         //         std::cout << i << ", " << max_movie->edges[i].first << "\n";
         schedule[0][i] = max_movie->edges[i].first;
      }
      long missed_edges = 0;
      long scheduled_edges = 0;
      for (int i = 1; i < num_movies_per_block; ++i) {
         MovieInfo * m = movie_info[i];
         std::vector<int> pending_dst;
         for (unsigned int e = 0; e < std::min((size_t) max_user_per_block, m->edges.size()); ++e) {
            if (user_order.find(m->edges[e].first) == user_order.end()) {
               pending_dst.push_back(m->edges[e].first);
               missed_edges++;
            } else {
               schedule[i][user_order[m->edges[e].first]] = m->edges[e].first;
               scheduled_edges++;
            }
         }
         //
      }
      std::cout << "Total-edges, " << graph.num_edges() << ", Missed-edges, " << missed_edges << ", Scheduled, " << scheduled_edges << ", TotalSpace, "
            << (num_movies_per_block * max_user_per_block) << ", scheduledPct, " << (scheduled_edges * 100) / (float(num_movies_per_block * max_user_per_block)) << ", missed, "
            << (missed_edges * 100) / (float(num_movies_per_block * max_user_per_block)) << "\n";
      if (false) {
         std::ofstream file("schedule_bgg.csv");
         for (int i = 0; i < max_user_per_block; ++i) {
            file << user_degrees[schedule[0][i]]->degree << ",";
         }
         file << "\n";
         for (int j = 0; j < max_user_per_block; ++j) {
            for (int i = 0; i < num_movies_per_block; ++i) {
               file << schedule[i][j] << ",";
            }
            file << "\n";
         }
         file.close();
      }

      for (int i = 0; i < num_movies_per_block; ++i)
         delete[] schedule[i];
      delete[] schedule;
   } //End code to create a schedule.
   for (unsigned int i = 0; i < movie_info.size(); ++i)
      delete movie_info[i];
   for (unsigned int i = 0; i < user_degrees.size(); ++i)
      delete user_degrees[i];
}

const char * unlock_kernel_str =
      "__kernel void unlock_kernel( __global int * locks, __global int * metadata) {\n\
   const uint my_id = get_global_id(0);\n\
   const int num_nodes= metadata[6];\n\
   const int num_edges = metadata[2];\n\
   const int chunk_size = metadata[4];\n\
   const int curr_step = metadata[6];\n\
   if(my_id < num_nodes) {\n\
      atomic_xchg(&locks[my_id], -1);\n\
//      locks[my_id]=-1;\n\
   }\n\
}";
const char * matching_kernel =
      "\
      bool lock(__global int * locks, int s, int d) {\n\
   volatile __global int * src = &locks[s]; \n\
   volatile __global int * dst = &locks[d];\n\
   int my_id = get_global_id(0);\n\
   if(atomic_cmpxchg(src, -1, my_id)==-1) {\n\
      if(atomic_cmpxchg(dst, -1, my_id)==-1) {\n\
         return true;\n\
      }\n\
      atomic_cmpxchg(src, my_id, -1);\n\
      return false;\n\
   }\n\
   return false;\n\
}\n\
   __kernel void matching_operator( __global int * locks, __global int * metadata, __global int * edge_info, __global int * edge_step) {\
   const uint my_id = get_global_id(0);\n\
   const int edge_start = metadata[1];\n\
   const int edge_end = metadata[2];\n\
   const int chunk_size = metadata[4];\n\
   const int curr_step = metadata[5];\n\
   __global int * work_counter = &metadata[0];\n\
   if(my_id < chunk_size) {\n\
      const int edge_id = edge_start + my_id;\n\
      const int src = edge_info[2*edge_id];\n\
      const int dst = edge_info[2*edge_id+1];\n\
      if(src != -1)\n\
      {\n\
         if(lock(locks, src, dst)==true)\n\
         {\n\
            edge_step[edge_id]=curr_step;\n\
            atomic_sub(work_counter,1);\n\
            edge_info[2*edge_id]=-1;\n\
         }\n\
      }\n\
   }\n\
}";
/*********************************************************************************************************
 * Build matching.
 Given a list of edges, and a start-end range, determines the matching-sets for the edges
 in the array matching s.t. edges[i] belongs to matching[i].
 **********************************************************************************************************/
template<typename ArrayType>
struct BuildMatching {
   typedef Galois::OpenCL::CL_Kernel Kernel;
   Kernel matching_kernel;
   Kernel unlock_kernel;
   const int num_nodes;
   ArrayType * metadata, *locks;
   std::string log_name;
   BuildMatching(int _num_nodes, ArrayType * _metadata, ArrayType * _locks, std::string _log_name = "input_") :
         num_nodes(_num_nodes), metadata(_metadata), locks(_locks), log_name(_log_name) {
      unlock_kernel.init("./apps/sgd/sgd_static_operator.cl", "unlock_kernel");
      matching_kernel.init("./apps/sgd/sgd_static_operator.cl", "matching_operator");
      unlock_kernel.set_arg_list(locks, metadata);
      unlock_kernel.set_work_size(num_nodes);
   }

   /*
    const int edge_start = metadata[1];
    const int edge_end = metadata[2];
    const int chunk_size = metadata[4];
    const int curr_step = metadata[5];
    __global int * work_counter = &metadata[0];
    */
   template<bool PrintStats = false>
   int operator()(ArrayType *_edge_info, ArrayType *_edge_step, int start_index, int end_index) {
      matching_kernel.set_arg_list(locks, metadata, _edge_info, _edge_step);
      locks->copy_to_device();
      metadata->copy_to_device();
      _edge_info->copy_to_device();
      _edge_step->copy_to_device();
      Galois::OpenCL::Timer timer;
      timer.start();
      unlock_kernel();
      timer.stop();
      float copy_time = timer.get_time_seconds();
      timer.clear();
      timer.start();
      int curr_step = 0;
      float kernel_time = 0;
      float unlock_kernel_time = 0;
      int work_counter = end_index - start_index;
      matching_kernel.set_work_size(work_counter);
      metadata->host_ptr()[0] = work_counter;
      metadata->host_ptr()[1] = start_index;
      metadata->host_ptr()[2] = end_index;
      metadata->host_ptr()[4] = work_counter;
      metadata->host_ptr()[5] = 0;
      metadata->host_ptr()[6] = num_nodes;
      metadata->copy_to_device();
      static int _call_count = 0;
      char curr_log_name[256];
      sprintf(curr_log_name, "%s_matching_%d.csv", log_name.c_str(), _call_count++);
      std::ofstream out_file(curr_log_name);
      do {
         ++curr_step;
         if (PrintStats) {
            out_file << metadata->host_ptr()[0] << ",";
         }
         matching_kernel();
         if (PrintStats) {
            out_file << matching_kernel.last_exec_time() << "\n";
         }

         kernel_time += matching_kernel.last_exec_time();
         metadata->copy_to_host();
         assert(metadata->host_ptr()[0] >= 0 && "Items processed overflows!");
         //fprintf(stderr, "[%d = %6.6g]\n", metadata->host_ptr()[0], metadata->host_ptr()[0]/(float)(work_counter));
         metadata->host_ptr()[5] = curr_step;
         metadata->copy_to_device();
         unlock_kernel();
         unlock_kernel_time += unlock_kernel.last_exec_time();
      } while (metadata->host_ptr()[0] != 0);
      out_file.close();
      timer.stop();
      float exec_time = timer.get_time_seconds();
      fprintf(stderr, "Matching,%d , Edge, %d, Steps, %d , Total-time(s), %6.6g ,GPU_Work(s), %6.6g, Kernel(s), %6.6g, Unlock(s), %6.6g, ", (_call_count - 1),
            end_index - start_index, curr_step, exec_time + copy_time, exec_time, kernel_time, unlock_kernel_time);
      return curr_step;
   }
};
/*********************************************************************************************************
 * Build matching.
 Given a list of edges, and a start-end range, determines the matching-sets for the edges
 in the array matching s.t. edges[i] belongs to matching[i].
 **********************************************************************************************************/
struct BuildMatchingCompact {
   typedef Galois::OpenCL::CL_Kernel Kernel;
   typedef Galois::OpenCL::GPUArray<int> GPUArrayType;
   typedef Galois::OpenCL::Array<int> ArrayType;
   Kernel matching_kernel;
   Kernel unlock_kernel;
   const int num_nodes;
   const int num_edges;
   ArrayType * metadata, *locks, *edge_step;
   GPUArrayType * edge_info;
   int * host_edge_info;
   std::string log_name;
   BuildMatchingCompact(int _num_nodes, int _num_edges, int * _edge_info, ArrayType* _edge_step, std::string _log_name = "input_") :
         num_nodes(_num_nodes), num_edges(_num_edges), edge_step(_edge_step), host_edge_info(_edge_info), log_name(_log_name) {
      locks = new ArrayType(num_nodes);
      metadata = new ArrayType(16);
      edge_info = new GPUArrayType(2 * num_edges);
      edge_info->copy_to_device(_edge_info);
      //TODO initialize locks, edge_step
      unlock_kernel.init("./apps/sgd/sgd_static_operator.cl", "unlock_kernel");
      matching_kernel.init("./apps/sgd/sgd_static_operator.cl", "matching_operator");
      unlock_kernel.set_arg_list(locks, metadata);
      unlock_kernel.set_work_size(num_nodes);
   }
   ~BuildMatchingCompact() {
      delete metadata;
      delete locks;
      delete edge_info;
   }
   /*
    const int edge_start = metadata[1];
    const int edge_end = metadata[2];
    const int chunk_size = metadata[4];
    const int curr_step = metadata[5];
    __global int * work_counter = &metadata[0];
    */
   template<bool PrintStats = false>
   int operator()(int start_index, int end_index) {
      matching_kernel.set_arg_list(locks, metadata, edge_info, edge_step);

      metadata->host_ptr()[6] = num_nodes;
      locks->copy_to_device();
      metadata->copy_to_device();
      Galois::OpenCL::Timer timer;
      timer.start();
      unlock_kernel();
//      fprintf(stderr, " UNLOCK KERNEL LAUNCHED :: %d , %d \n", unlock_kernel.local, unlock_kernel.global);
      timer.stop();
      float copy_time = timer.get_time_seconds();
      timer.clear();
      timer.start();
      int curr_step = 0;
      float kernel_time = 0;
      float unlock_kernel_time = 0;
      int work_counter = end_index - start_index;
      matching_kernel.set_work_size(work_counter);
      metadata->host_ptr()[0] = work_counter;
      metadata->host_ptr()[1] = start_index;
      metadata->host_ptr()[2] = end_index;
      metadata->host_ptr()[4] = work_counter;
      metadata->host_ptr()[5] = 0;
      metadata->host_ptr()[6] = num_nodes;
      metadata->copy_to_device();
      static int _call_count = 0;
      char curr_log_name[256];
      sprintf(curr_log_name, "%s_matching_%d.csv", log_name.c_str(), _call_count++);
      fprintf(stderr,  " Matching :: [start =%d, end=%d], num_nodes=%d, num_edges=%d, chunk_size=%d\n ", start_index, end_index, num_nodes, num_edges, work_counter);
      std::ofstream out_file(curr_log_name);
      do {
         ++curr_step;
         if (PrintStats)
         {
            out_file << metadata->host_ptr()[0] << ",";
         }
         Galois::OpenCL::OpenCL_Setup::finish();
         matching_kernel();
         Galois::OpenCL::OpenCL_Setup::finish();
         if (PrintStats)
         {
            out_file << matching_kernel.last_exec_time() << "\n";
         }
         //Debug conflict-free schedules showing conflicts in graph
         if(false && PrintStats){
            int  * edge_info_host = new int [ 2*num_edges];
            edge_info->copy_to_host(edge_info_host);
            edge_step->copy_to_host();
            locks->copy_to_host();
            for(auto i=0; i < num_nodes; ++i){
               if(locks->host_ptr()[i]!=-1){
                  fprintf(stderr, "[%d] Unlocked:: src=, val=%d\n", i, locks->host_ptr()[i]);
               }
            }

            for(auto i=0; i < num_edges; ++i){
               if(edge_info_host[2*i]!=-1){
                  fprintf(stderr, "[%d] Unmatched :: src=%d, dst=%d, step=%d\n", curr_step,  edge_info_host[2*i], edge_info_host[2*i+1], edge_step->host_ptr()[i]);
               }
            }
            delete []edge_info_host ;

         }
         kernel_time += matching_kernel.last_exec_time();
         metadata->copy_to_host();
         assert(metadata->host_ptr()[0] >= 0 && "Items processed overflows!");
         //fprintf(stderr, "[%d = %6.6g]\n", metadata->host_ptr()[0], metadata->host_ptr()[0]/(float)(work_counter));
         metadata->host_ptr()[5] = curr_step;
         metadata->copy_to_device();
         unlock_kernel();
         unlock_kernel_time += unlock_kernel.last_exec_time();
      } while (metadata->host_ptr()[0] != 0);
      out_file.close();
      timer.stop();
      float exec_time = timer.get_time_seconds();
      fprintf(stderr, "BMC-Matching,%d , Edge, %d, Steps, %d , Total-time(s), %6.6g ,GPU_Work(s), %6.6g, Kernel(s), %6.6g, Unlock(s), %6.6g, ", (_call_count - 1),
            end_index - start_index, curr_step, exec_time + copy_time, exec_time, kernel_time, unlock_kernel_time);
      edge_step->copy_to_host();
      return curr_step;
   }
   void to_file(std::string base_name) {
      std::ofstream file(base_name, ios::binary);
      file.write((char*)host_edge_info, sizeof(int)*2*num_edges);
      file.write((char*)edge_step->host_ptr(), sizeof(int)*num_edges);
//      for (int i = 0; i < num_edges; ++i) {
//         file << host_edge_info[2 * i] << " " << host_edge_info[2 * i + 1] << " " << edge_step->host_ptr()[i] << "\n";
//      }
      file.close();
   }
   static int from_file(std::ifstream & file, int num_edges, int * edge_info, int *edge_step) {
      int max_step = 0;
      file.read((char*)edge_info, sizeof(int)*2*num_edges);
      file.read((char*)edge_step, sizeof(int)*num_edges);
      for (int i = 0; i < num_edges; ++i) {
         if(edge_step[i]==-1){
            fprintf(stderr, "Err at %d, %d, %d, %d\n",i, edge_info[2*i], edge_info[2*i+1], edge_step[i] );
         }
         assert(edge_step[i]!=-1);
         max_step = std::max(edge_step[i], max_step);
      }
      file.close();
      return max_step + 1;
   }
};

/*********************************************************************************************************
 * Build matching 2WL kernel source.
 **********************************************************************************************************/
const char * matching2_kernel =
      "\
      bool lock(__global int * locks, int s, int d) {\n\
   volatile __global int * src = &locks[s]; \n\
   volatile __global int * dst = &locks[d];\n\
   int my_id = get_global_id(0);\n\
   if(atomic_cmpxchg(src, -1, my_id)==-1) {\n\
      if(atomic_cmpxchg(dst, -1, my_id)==-1) {\n\
         return true;\n\
      }\n\
      atomic_cmpxchg(src, my_id, -1);\n\
      return false;\n\
   }\n\
   return false;\n\
}\n\
   __kernel void matching_operator2( __global int * locks, __global int * metadata, __global int * edge_info) {\
   const uint my_id = get_global_id(0);\n\
   const int edge_start = metadata[1];\n\
   const int edge_end = metadata[2];\n\
   const int chunk_size = metadata[4];\n\
   const int curr_step = metadata[5];\n\
   __global int * work_counter = &metadata[0];\n\
   if(my_id < chunk_size) {\n\
      const int edge_id = edge_start + my_id;\n\
      const int src = edge_info[3*edge_id];\n\
      const int dst = edge_info[3*edge_id+1];\n\
      if(src != -1)\n\
      {\n\
         if(lock(locks, src, dst)==true)\n\
         {\n\
            edge_info[3*edge_id+2]=curr_step;\n\
            atomic_sub(work_counter,1);\n\
            edge_info[3*edge_id]=-1;\n\
         }\n\
      }\n\
   }\n\
}";
/*********************************************************************************************************
 * Build matching.
 Given a list of edges, and a start-end range, determines the matching-sets for the edges
 in the array matching s.t. edges[i] belongs to matching[i].
 **********************************************************************************************************/
template<typename ArrayType>
struct BuildMatching2WL {
   typedef Galois::OpenCL::CL_Kernel Kernel;
   Kernel matching_kernel;
   Kernel unlock_kernel;
   const int num_nodes;
   ArrayType * metadata, *locks;
   BuildMatching2WL(int _num_nodes, ArrayType * _metadata, ArrayType * _locks) :
         num_nodes(_num_nodes), metadata(_metadata), locks(_locks) {
      unlock_kernel.init_string(unlock_kernel_str, "unlock_kernel");
      matching_kernel.init_string(matching2_kernel, "matching_operator2");
      unlock_kernel.set_arg_list(locks, metadata);
      unlock_kernel.set_work_size(num_nodes);
   }

   /*
    const int edge_start = metadata[1];
    const int edge_end = metadata[2];
    const int chunk_size = metadata[4];
    const int curr_step = metadata[5];
    __global int * work_counter = &metadata[0];
    */
   template<bool PRINT_STATS = false>
   int operator()(ArrayType *_edge_info, int start_index, int end_index) {
      matching_kernel.set_arg_list(locks, metadata, _edge_info);
      locks->copy_to_device();
      metadata->copy_to_device();
      _edge_info->copy_to_device();
      Galois::OpenCL::Timer timer;
      timer.start();
      unlock_kernel();
      timer.stop();
      float copy_time = timer.get_time_seconds();
      timer.clear();
      timer.start();
      int curr_step = 0;
      float kernel_time = 0;
      float unlock_kernel_time = 0;
      int work_counter = end_index - start_index;
      matching_kernel.set_work_size(work_counter);
      metadata->host_ptr()[0] = work_counter;
      metadata->host_ptr()[1] = start_index;
      metadata->host_ptr()[2] = end_index;
      metadata->host_ptr()[4] = work_counter;
      metadata->host_ptr()[5] = 0;
      metadata->host_ptr()[6] = num_nodes;
      metadata->copy_to_device();
      if (PRINT_STATS) {
         fprintf(stderr, "Starting matching: %d ", work_counter);
      }
      do {
         ++curr_step;
         matching_kernel();
         kernel_time += matching_kernel.last_exec_time();
         metadata->copy_to_host();
         assert(metadata->host_ptr()[0] >= 0 && "Items processed overflows!");
         //fprintf(stderr, "[%d = %6.6g]\n", metadata->host_ptr()[0], metadata->host_ptr()[0]/(float)(work_counter));
         metadata->host_ptr()[5] = curr_step;
         metadata->copy_to_device();
         unlock_kernel();
         unlock_kernel_time += unlock_kernel.last_exec_time();
      } while (metadata->host_ptr()[0] != 0);
      timer.stop();
      float exec_time = timer.get_time_seconds();
      if (PRINT_STATS) {
         fprintf(stderr, "Matching, Steps, %d , Total-time(s), %6.6g ,GPU_Work(s), %6.6g, Kernel(s), %6.6g, Unlock(s), %6.6g,\n", curr_step, exec_time + copy_time, exec_time,
               kernel_time, unlock_kernel_time);
      }
      return curr_step;
   }
};

} //namespace SGDCommon
#endif /* SGDSTATICSCHEDULE_H_ */
