/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#include "cl_common.cl"
#include "apps/sgd/sgd_common.cl"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void sgd_diagonal_operator(__global FeatureTypeSIMD * features, __global int * metadata, __global float * ratings, __global int * cols, float step_size, int iter){
   const uint my_id  = get_global_id(0);
   int num_movies= metadata[2];
   int num_users= metadata[0];
   __global int * counter = &metadata[4];
   if (my_id < num_movies){
      const int src = my_id;
//      const int dst = (my_id + num_movies)%num_users + num_movies;
      const int dst = (my_id + iter)%num_users + num_movies;
      const EdgeData rating = ratings[my_id];
      if(rating != -1){
         atomic_add(counter,1);
      __global FeatureTypeSIMD * s_ptr = & features[src*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
      __global FeatureTypeSIMD * d_ptr = & features[dst*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
#ifdef USE_LOCAL_CACHE //Use local caches by default. Slightly faster.
            sgd_kernel_local_simd(s_ptr,d_ptr,rating, step_size);
#else
//            sgd_kernel_global_simd(s_ptr,d_ptr,rating, step_size);
#endif
      }
   }
}
__kernel void sgd_edge_operator( __global int * locks, __global FeatureTypeSIMD * features, __global int * metadata, __global int * edge_info, __global FeatureType * ratings, FeatureType step_size) {
   const uint my_id = get_global_id(0);
   const int num_edges = metadata[2];
   const int chunk_size = metadata[4];
   __global int * work_counter = &metadata[0];
//   __global NodeData * ndata = (__global NodeData*)features;
   if(my_id < chunk_size) {
      const int src = edge_info[2*my_id];
      const int dst = edge_info[2*my_id+1];
      if(src != -1)
      {
         if(lock(locks, src, dst)==true)
         {
            __global FeatureTypeSIMD * s_ptr = &features[src*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
            __global FeatureTypeSIMD * d_ptr = &features[dst*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
            const EdgeData rating = ratings[my_id];

#ifdef USE_LOCAL_CACHE //Use local caches by default. Slightly faster.
            sgd_kernel_local_simd(s_ptr,d_ptr,rating, step_size);
//            sgd_kernel_local_nosimd(s_ptr,d_ptr,rating, step_size);
#else
            sgd_kernel_global_simd(s_ptr,d_ptr,rating, step_size);
//            sgd_kernel_global_simd(&ndata[src],&ndata[dst],rating, step_size);
//            sgd_kernel_global_nosimd(s_ptr,d_ptr,rating, step_size); //WORKS
#endif
            unlock(locks, src, dst); //can release lock here as subsequent writes by thread are to non-shared data or via atomics
            atomic_sub(work_counter,1);
            atomic_xchg(&edge_info[2*my_id],-1);
         }
      }
   }
}

//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void sgd_edge_operator_small( __global int * locks, __global FeatureTypeSIMD * features, __global int * metadata, __global int * edge_info, FeatureType step_size) {
   const uint my_id = get_global_id(0);
   const int num_edges = metadata[2];
   const int chunk_size = metadata[4];
   __global int * work_counter = &metadata[0];
   if(my_id < chunk_size) {
      const int src = edge_info[3*my_id];
      const int dst = edge_info[3*my_id+1];
      const FeatureType rating = as_float(edge_info[3*my_id+2]);
      if(src != -1)
      {
         if(lock(locks, src, dst)==true)
         {
            __global FeatureTypeSIMD * s_ptr = &features[src*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
            __global FeatureTypeSIMD * d_ptr = &features[dst*SGD_FEATURE_SIZE/FLOAT_SIMD_SIZE];
#ifdef USE_LOCAL_CACHE //Use local caches by default. Slightly faster.
            sgd_kernel_local_simd(s_ptr,d_ptr,rating, step_size);
//            sgd_kernel_local_nosimd(s_ptr,d_ptr,rating, step_size);
#else
            sgd_kernel_global_simd(s_ptr,d_ptr,rating, step_size);
//            sgd_kernel_global_nosimd(s_ptr,d_ptr,rating, step_size);
#endif
            unlock(locks, src, dst); //can release lock here as subsequent writes by thread are to non-shared data or via atomics
            atomic_sub(work_counter,1);
            atomic_xchg(&edge_info[3*my_id],-1);
         }
      }
   }
}
