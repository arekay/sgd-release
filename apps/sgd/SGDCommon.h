/**
 Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU.
 GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
 Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

 @author Rashid Kaleem <rashid.kaleem@gmail.com>

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 */
#ifndef SGDCOMMON_H_
#define SGDCOMMON_H_

#define _SGD_USE_SHARED_MEM_ 1
#define  SGD_FEATURE_SIZE 16

#ifdef __OPENCL_VERSION__
#else

#ifdef _WIN32
template<typename T>
bool isnormal(const T & ) {return true;}
#else
using namespace std;
//template<typename T>
//bool isnormal(const T & v) {
//   return std::isnormal(v);
//}
#endif

const float SGD_LAMBDA = 0.05f;
const float SGD_LEARNING_RATE = 0.012f;
const float SGD_DECAY_RATE = 0.015f;
const int SGD_MAX_ROUNDS = 1;
typedef cl_float FeatureType;

#include "../../apps/sgd/SGDStaticSchedule.h"
namespace SGDCommon {
/************************************************
 *
 *************************************************/
float SGD_STEP_SIZE(int X) {
   return SGD_LEARNING_RATE * 1.5f / (1.0f + SGD_DECAY_RATE * pow(X + 1, 1.5f));
} //Purdue.
//#define SGD_STEP_SIZE(X) (0.001f *1.5f/(1.0+0.9* pow(X+1,1.5))) //Intel.
/************************************************
 *
 *************************************************/
struct SGDNodeData {
   cl_float features[SGD_FEATURE_SIZE];
   float dot_product(const SGDNodeData & d) const {
      float res = 0.0f;
      for (int i = 0; i < SGD_FEATURE_SIZE; ++i)
         res += features[i] * d.features[i];
      return res;
   }
};
/************************************************
 *
 *************************************************/
float sum_vector(const float *a) {
   float res = 0.0f;
   for (int i = 0; i < SGD_FEATURE_SIZE; ++i)
      res += a[i];
   return res;
}
/************************************************
 *
 *************************************************/
template<typename T>
T dot_product(const T *a, const T * b) {
   T res = 0.0f;
   for (int i = 0; i < SGD_FEATURE_SIZE; ++i) {
      assert((a[i] == 0 || isnormal(a[i]) == true));
      assert((b[i] == 0 || isnormal(b[i]) == true));
      res += a[i] * b[i];
   }
   return res;
}
/************************************************
 *
 *************************************************/
float toMB(long val) {
   return val / (float) (1024 * 1024);
}
/************************************************
 *
 *************************************************/
struct DebugData {
   struct NodeStats {
      int max_rating;
      int min_rating;
      int sum_rating;
      int count_rating;
      int my_degree;
      bool is_movie;
      NodeStats() {
         max_rating = sum_rating = count_rating = 0;
         min_rating = std::numeric_limits<int>::max();
         my_degree = 0;
         is_movie = false;
      }
      void stat(int val) {
         max_rating = std::max(max_rating, val);
         min_rating = std::min(min_rating, val);
         sum_rating += (val);
         count_rating++;
      }
   };
   std::vector<std::pair<int, int> > user_degrees;
   std::vector<std::pair<int, int> > movie_degrees;
   std::map<int, int> user_map;
   std::map<int, int> movie_map;
};
/************************************************
 *
 *************************************************/

template<typename GraphTy>
static void write_stats_to_file(GraphTy & graph) {
   //0 Write graph as csv to file:
   {
      std::ofstream out_file("/workspace/rashid/bgg.csv");
      out_file << "Src,Dst,Wt\n";
      for (size_t i = 0; i < graph.num_edges(); ++i) {
         out_file << graph.get_edge_src(i) << "," << graph.out_neighbors()[i] << "," << graph.out_edge_data()[i] << "\n";
      }
      out_file.close();
   }
   //      return;
   //3 Write average-user degree per-movie
   {
      int max_user_degree = 0;
      int max_movie_degree = 0;
      std::vector<DebugData::NodeStats> all_nodes_stats(graph.num_nodes());
      std::vector<int> movie_indices;
      std::vector<int> user_indices;
      {
         for (size_t i = 0; i < graph.num_edges(); ++i) {
            int src = graph.get_edge_src(i);
            int dst = graph.out_neighbors()[i];
            int rating = graph.out_edge_data()[i];
            all_nodes_stats[src].my_degree++;
            all_nodes_stats[dst].my_degree++;
            all_nodes_stats[src].is_movie = true;
            all_nodes_stats[src].stat(rating);
            all_nodes_stats[dst].stat(rating);
            movie_indices.push_back(src);
            user_indices.push_back(dst);
            max_movie_degree = std::max(max_movie_degree, all_nodes_stats[src].my_degree);
            max_user_degree = std::max(max_user_degree, all_nodes_stats[dst].my_degree);
         }
      }
      {
         /*std::sort(debug_data.user_degrees.begin(), debug_data.user_degrees.end(), [](const std::pair<int, int>& lhs, const std::pair<int, int>& rhs) {
          return lhs.second > rhs.second;});
          std::sort(debug_data.movie_degrees.begin(), debug_data.movie_degrees.end(), [](const std::pair<int, int>& lhs, const std::pair<int, int>& rhs) {
          return lhs.second > rhs.second;});
          */
      }
      std::vector<DebugData::NodeStats> user_per_degree_stats(max_user_degree + 1);
      std::vector<DebugData::NodeStats> movie_per_degree_stats(max_movie_degree + 1);
      long sum_ratings = 0;
      for (size_t i = 0; i < graph.num_edges(); ++i) {
         int m = graph.get_edge_src(i);
         int u = graph.out_neighbors()[i];
         int rating = graph.out_edge_data()[i];
         int m_d = all_nodes_stats[m].my_degree;
         int u_d = all_nodes_stats[u].my_degree;
         assert(all_nodes_stats[m].is_movie == true && all_nodes_stats[u].is_movie == false);
         user_per_degree_stats.at(u_d).stat(rating);
         movie_per_degree_stats.at(m_d).stat(rating);
         sum_ratings += rating;
      }
      std::cout << "Sizes:: " << movie_indices.size() << ", " << user_indices.size() << "\n";
      std::cout << "Max-degree:: " << max_movie_degree << ", " << max_user_degree << "\n";
      std::cout << "Average rating:: " << sum_ratings / (float) (graph.num_edges()) << "\n";
      {
         std::ofstream out_file_u("/workspace/rashid/user_stats.csv");
         out_file_u << "Id,Degree,NumNodes,Min,Max,Sum\n";
         for (size_t i = 0; i < user_indices.size(); ++i) {
            int index = user_indices[i];
            assert(all_nodes_stats[index].is_movie == false);
            out_file_u << i << "," << all_nodes_stats[index].count_rating << "," << all_nodes_stats[index].min_rating << "," << all_nodes_stats[index].max_rating << ","
                  << all_nodes_stats[index].sum_rating << "\n";
         }
         out_file_u.close();
      }
      {
         std::ofstream out_file_u("/workspace/rashid/movie_stats.csv");
         out_file_u << "Id,Degree,NumNodes,Min,Max,Sum\n";
         for (size_t i = 0; i < movie_indices.size(); ++i) {
            int index = movie_indices[i];
            assert(all_nodes_stats[index].is_movie == true);
            out_file_u << i << "," << all_nodes_stats[index].count_rating << "," << all_nodes_stats[index].min_rating << "," << all_nodes_stats[index].max_rating << ","
                  << all_nodes_stats[index].sum_rating << "\n";
         }
         out_file_u.close();
      }
      {
         std::ofstream out_file_u("/workspace/rashid/bgg_user_average_degree.csv");
         out_file_u << "Degree,NumNodes,Min,Max,Sum\n";
         for (int i = 0; i < max_user_degree; ++i) {
            if (user_per_degree_stats[i].count_rating > 0)
               out_file_u << i << "," << user_per_degree_stats[i].count_rating << "," << user_per_degree_stats[i].min_rating << "," << user_per_degree_stats[i].max_rating << ","
                     << user_per_degree_stats[i].sum_rating << "\n";
         }
         out_file_u.close();
      }
      {
         std::ofstream out_file_m("/workspace/rashid/bgg_movie_average_degree.csv");
         out_file_m << "Degree,NumNodes,Min,Max,Sum\n";
         for (int i = 0; i < max_movie_degree; ++i) {
            if (movie_per_degree_stats[i].count_rating > 0)
               out_file_m << i << "," << movie_per_degree_stats[i].count_rating << "," << movie_per_degree_stats[i].min_rating << "," << movie_per_degree_stats[i].max_rating << ","
                     << movie_per_degree_stats[i].sum_rating << "\n";
         }
         out_file_m.close();
      }
   }
   //4 Write average-movie degree per-user
   std::cout << "Done writing debug info...\n";
   exit(-1);
}
/************************************************
 *
 ************************************************/
template<typename Graph>
void print_distributions(Graph & graph) {
   std::vector<int> all_ratings;
   for (size_t i = 0; i < graph.num_edges(); ++i) {
      all_ratings.push_back(graph.out_edge_data()[i]);
   }
   std::sort(all_ratings.begin(), all_ratings.end());
   std::vector<int> rating_cutoff;
   for (float i = 0.0f; i < 1.0f; i += 0.1f) {
      rating_cutoff.push_back(all_ratings[i * graph.num_edges()]);
   }
   rating_cutoff.push_back(all_ratings[graph.num_edges() - 1]);
   std::cout << "Ratings: [";
   for (auto i : rating_cutoff) {
      std::cout << i << ", ";
   }
   std::cout << "]\n";
}
/************************************************
 *
 ************************************************/
enum EdgeFilterType {
   IdentityFilter,
   RandomShuffle,
   HighDegreeMovie,
   LowDegreeMovie,
   HighDegreeUser,
   LowDegreeUser,
   HighRatings,
   LowRatings,
   HighRatingsHighDegreeMovie,
   LowRatingsHighDegreeMovie,
   AverageRatingsHighDegreeMovie
};
template<typename GraphTy>
void edge_filter(SGDCommon::EdgeFilterType filter_type, GraphTy & graph, std::vector<int> & in_indices, std::vector<int> & out_indices) {
   //BGG-movies(degrees) ; 43331, 14832, 8682, 5685, 3777, 2403, 1439, 792, 380, 123, 1,
   const unsigned int high_rating_threshold = 1;
   const unsigned int low_rating_threshold = 100;
   const unsigned int high_degree_threshold = 43331;
   const unsigned int low_degree_threshold = 14832;
   float rating_base = 75.0f;
   const float abs_range = 15.0f;

   std::vector<int> ratings;
   long ratings_sum = 0;
   switch (filter_type) {
   case IdentityFilter:
      out_indices.resize(in_indices.size());
      std::copy(in_indices.begin(), in_indices.end(), out_indices.begin());
      fprintf(stderr, "RandomShuffle,");
      break;
   case RandomShuffle:
      out_indices.resize(in_indices.size());
      std::copy(in_indices.begin(), in_indices.end(), out_indices.begin());
      fprintf(stderr, "RandomShuffle,");
      std::random_shuffle(out_indices.begin(), out_indices.end());
      break;
   case HighRatings:
      fprintf(stderr, "HighRatings,%d,", high_rating_threshold);
      for (size_t i = 0; i < in_indices.size(); ++i) {
         if (graph.out_edge_data()[in_indices[i]] <= high_rating_threshold) {
            out_indices.push_back(in_indices[i]);
         }
      }
      break;
   case LowRatings:
      fprintf(stderr, "LowRatings,%d,", low_rating_threshold);
      for (size_t i = 0; i < in_indices.size(); ++i) {
         if (graph.out_edge_data()[in_indices[i]] >= low_rating_threshold) {
            out_indices.push_back(in_indices[i]);
         }
      }
      break;
   case HighDegreeMovie:
      fprintf(stderr, "HighDegreeMovie,%d,", high_degree_threshold);
      for (size_t i = 0; i < in_indices.size(); ++i) {
         if (graph.num_neighbors(graph.get_edge_src(in_indices[i])) < high_degree_threshold) {
            out_indices.push_back(in_indices[i]);
         }
      }
      break;
   case LowDegreeMovie:
      fprintf(stderr, "LowDegreeMovie,%d,", low_degree_threshold);
      for (size_t i = 0; i < in_indices.size(); ++i) {
         if (graph.num_neighbors(graph.get_edge_src(in_indices[i])) > low_degree_threshold) {
            out_indices.push_back(in_indices[i]);
         }
      }
      break;
   case HighRatingsHighDegreeMovie:
      fprintf(stderr, "HighRatingsHighDegreeMovie,%d,%d,", high_degree_threshold, high_rating_threshold);
      for (size_t i = 0; i < in_indices.size(); ++i) {
         if (graph.num_neighbors(graph.get_edge_src(in_indices[i])) < high_degree_threshold || graph.out_edge_data()[in_indices[i]] < high_rating_threshold) {
            out_indices.push_back(in_indices[i]);
         }
      }
      break;
   case LowRatingsHighDegreeMovie:
      fprintf(stderr, "LowRatingsHighDegreeMovie,%d,%d,", high_degree_threshold, high_rating_threshold);
      for (size_t i = 0; i < in_indices.size(); ++i) {
         if (graph.num_neighbors(graph.get_edge_src(in_indices[i])) < high_degree_threshold && graph.out_edge_data()[in_indices[i]] > low_rating_threshold) {
            out_indices.push_back(in_indices[i]);
         }   //End if
      }   //End for
      break;
   case AverageRatingsHighDegreeMovie:
//      fprintf(stderr, "AverageRatingsHighDegreeMovie,%d,%d,", high_degree_threshold, high_rating_threshold);
      for (size_t i = 0; i < in_indices.size(); ++i) {
         if (graph.num_neighbors(graph.get_edge_src(in_indices[i])) >= high_degree_threshold) {
            ratings.push_back(graph.out_edge_data()[in_indices[i]]);
            ratings_sum += graph.out_edge_data()[in_indices[i]];
         }
      }
      rating_base = ratings_sum / (float) ratings.size();
      fprintf(stderr, "AverageRatingsHighDegreeMovie,%d,%6.6g, %6.6g,", high_degree_threshold, rating_base - abs_range, rating_base + rating_base);
      for (size_t i = 0; i < in_indices.size(); ++i) {
         /*if (graph.num_neighbors(graph.get_edge_src(in_indices[i])) > high_degree_threshold) {
          ratings.push_back(graph.out_edge_data()[in_indices[i]]);
          ratings_sum += graph.out_edge_data()[in_indices[i]];
          }*/
         if (graph.num_neighbors(graph.get_edge_src(in_indices[i])) >= high_degree_threshold && graph.out_edge_data()[in_indices[i]] >= (rating_base - abs_range)
               && graph.out_edge_data()[in_indices[i]] <= (rating_base + abs_range)) {

         } else {
            out_indices.push_back(in_indices[i]);
         }   //End if
      }   //End for

      break;
   default:
      fprintf(stderr, "NoFilter,");
      break;
   };
//   std::random_shuffle(out_indices.begin(), out_indices.end());
   fprintf(stderr, "Filtered,In,%d,Out,%d,Diff,%6.6g \n", (int) in_indices.size(), (int) out_indices.size(), out_indices.size() * 100 / (float) (in_indices.size()));
#if 0 //To print the degrees to a file
   {
      fprintf(stderr, "High-degree ratings (%d), average. %6.6g [", (int) ratings.size(), ratings_sum / (float) ratings.size());
      std::sort(ratings.begin(), ratings.end());
      for (unsigned int i = 0; i < ratings.size(); i += ratings.size() / 10) {
         fprintf(stderr, "%d, ", ratings[i]);
      }
      fprintf(stderr, "]\n");
      std::ofstream out_file("/workspace/rashid/high_degree_rating_dist.csv");
      for (auto i : ratings) {
         out_file << i << ", ";
      }
      out_file.close();
   }
#endif
   return;
}
/************************************************************************
 *
 ************************************************************************/
template<typename GraphType, typename FeatureArrayType>
void compute_err(GraphType & graph, FeatureArrayType * features, int max_rating) {
   int fail_count = 0;
   float sum = 0;
   for (unsigned int i = 0; i < features->size(); ++i) {
      float f = features->host_ptr()[i];
      sum += f;
      if ((f != 0 && isnormal(f) == false)) {
         fail_count++;
      }
   }
   fprintf(stderr, "Failed:: %6.6g,Sum, %6.6g ", fail_count / (float) (features->size()), sum);
   float accumulated_error = 0.0f;
   float max_err = 0.0f;
   for (unsigned int i = 0; i < graph.num_edges(); ++i) {
      unsigned int src = graph.get_edge_src(i);
      unsigned int dst = graph.out_neighbors()[i];
      float rating = graph.out_edge_data()[i] / (float) max_rating;
      float computed_rating = SGDCommon::dot_product(&features->host_ptr()[src * SGD_FEATURE_SIZE], &features->host_ptr()[dst * SGD_FEATURE_SIZE]);
      float err = (computed_rating - rating);
      max_err = std::max((double) max_err, (double) fabs(err));
      accumulated_error += err * err;
   }
   accumulated_error /= (float) graph.num_edges();
   float rms = std::sqrt((float) accumulated_error);
   fprintf(stderr, "Average_error, %6.6f , max_error, %6.6f, RMS, %6.6f \n", accumulated_error, max_err, rms);
}
/************************************************************************
 *
 ************************************************************************/
template<typename GraphType, typename FeatureArrayType, typename LockType>
void initialize_features_random(GraphType & graph, FeatureArrayType * features, LockType * locks, std::vector<int> & movies) {
   using namespace std;
   Galois::OpenCL::Timer timer;
   timer.start();
   FeatureType top = 1.0 / sqrt(SGD_FEATURE_SIZE);
   uniform_real_distribution<FeatureType> dist(0, top);
   mt19937 gen;
   /*      std::uniform_real_distribution<FeatureType> dist(-1.0f, 1.0f);*/
   FeatureType feature_sum = 0.0f, min_feature = top, max_feature = -top;
   //For each node, initialize features to random, and lock to be unlocked.
   for (unsigned int i = 0; i < graph.num_nodes(); ++i) {
      locks->host_ptr()[i] = -1;
      FeatureType * features_l = &(features->host_ptr()[i * SGD_FEATURE_SIZE]); //graph.node_data()[i].features;
      for (int j = 0; j < SGD_FEATURE_SIZE; ++j) {
         feature_sum += (features_l[j] = dist(gen));
         max_feature = std::max(features_l[j], max_feature);
         min_feature = std::min(features_l[j], min_feature);
         assert(isnormal(features_l[j]) || features_l[j] == 0);
      }
      if (graph.num_neighbors(i) > 0)
         movies.push_back(i);
   }
   timer.stop();
   std::cout << "initial features:: " << feature_sum << " , [" << min_feature << " , " << max_feature;
}
/************************************************************************
 *
 ************************************************************************/
template<typename GraphType, typename FeatureArrayType>
void initialize_features_random(GraphType & graph, FeatureArrayType * features, std::vector<int> & movies) {
   using namespace std;
   Galois::OpenCL::Timer timer;
   timer.start();
   FeatureType top = 1.0 / sqrt(SGD_FEATURE_SIZE);
   uniform_real_distribution<FeatureType> dist(0, top);
   mt19937 gen;
   /*      std::uniform_real_distribution<FeatureType> dist(-1.0f, 1.0f);*/
   FeatureType feature_sum = 0.0f, min_feature = top, max_feature = -top;
   //For each node, initialize features to random, and lock to be unlocked.
   for (unsigned int i = 0; i < graph.num_nodes(); ++i) {
      FeatureType * features_l = &(features->host_ptr()[i * SGD_FEATURE_SIZE]); //graph.node_data()[i].features;
      for (int j = 0; j < SGD_FEATURE_SIZE; ++j) {
         feature_sum += (features_l[j] = dist(gen));
         max_feature = std::max(features_l[j], max_feature);
         min_feature = std::min(features_l[j], min_feature);
         assert(isnormal(features_l[j]) || features_l[j] == 0);
      }
      if (graph.num_neighbors(i) > 0)
         movies.push_back(i);
   }
   timer.stop();
   std::cout << "initial features:: " << feature_sum << " , [" << min_feature << " , " << max_feature;
}

template<typename GraphType>
void complete_bipartitie(GraphType & g, int num_movies, int num_users) {
   g.init(num_movies + num_users, num_users * num_movies);
   int index = 0;
   for (int i = 0; i < num_movies; ++i) {
      g.outgoing_index()[i] = index;
      for (int j = 0; j < num_users; ++j) {
         g.get_edge_src()[index + j] = i;
//         g.out_neighbors()[index + j] = num_movies + ((j + i) % num_users);
         g.out_neighbors()[index + j] = num_movies + j;
         g.out_edge_data()[index + j] = 3;
      }
      index += num_users;
   }
   for (int i = num_movies; i < num_movies + num_users; ++i) {
      g.outgoing_index()[i] = index;
   }
   g.outgoing_index()[num_movies + num_users] = index;

   //if (false)
   {
      std::ofstream out_file("gen_graph.csv");
      for (int i = 0; i < num_movies; ++i) {
         for (size_t nbr_idx = g.outgoing_index()[i]; nbr_idx < g.outgoing_index()[i + 1]; ++nbr_idx) {
            out_file << g.out_neighbors()[nbr_idx] << ",";
         }
         out_file << "\n";
      }
      out_file.close();
   }
}   //End complete_bipartitie
/************************************************
 *
 *************************************************/
template<typename GraphType>
void diagonal_graph(GraphType & g, int num_nodes) {
   g.init(2 * num_nodes, num_nodes);
   for (int i = 0; i < num_nodes; ++i) {
      g.outgoing_index()[i] = i;
      g.get_edge_src()[i] = i;
      g.out_neighbors()[i] = i + num_nodes;
      g.out_edge_data()[i] = 3;
   }
   for (int i = num_nodes; i < 2 * num_nodes; ++i) {
      g.outgoing_index()[i] = num_nodes;
   }
   g.outgoing_index()[2 * num_nodes] = num_nodes;

   //if(false)
   {
      std::ofstream out_file("gen_graph.csv");
      for (int i = 0; i < num_nodes; ++i) {
         out_file << i << " :: " ;
         for (size_t nbr_idx = g.outgoing_index()[i]; nbr_idx < g.outgoing_index()[i + 1]; ++nbr_idx) {
            out_file <<g.out_neighbors()[nbr_idx] << ",";
         }
         out_file << "\n";
      }
      out_file.close();
   }

}   //End complete_bipartitie
/************************************************
 *
 *************************************************/
template<typename GraphType>
void read_as_bipartite(GraphType & g, const char * filename) {
   typedef Galois::OpenCL::LC_LinearArray_Undirected_Graph<Galois::CPUArray, int, int> LCGraph;
   LCGraph tmp_graph;
   tmp_graph.read(filename);
   g.init(2 * tmp_graph.num_nodes(), tmp_graph.num_edges());
   for (unsigned int i = 0; i < tmp_graph.num_nodes(); ++i) {
      g.outgoing_index()[i] = tmp_graph.outgoing_index()[i];
      for (unsigned int j = tmp_graph.outgoing_index()[i]; j < tmp_graph.outgoing_index()[i + 1]; ++j) {
         g.get_edge_src()[j] = i;
//         g.out_neighbors()[index + j] = num_movies + ((j + i) % num_users);
         g.out_neighbors()[j] = tmp_graph.out_neighbors()[j] + tmp_graph.num_nodes();
         g.out_edge_data()[j] = tmp_graph.out_edge_data()[j];
      }
   }
   g.outgoing_index()[tmp_graph.num_nodes()] = tmp_graph.outgoing_index()[tmp_graph.num_nodes()];
   for (unsigned int i = tmp_graph.num_nodes(); i < g.num_nodes(); ++i) {
      g.outgoing_index()[i + 1] = g.outgoing_index()[tmp_graph.num_nodes()];
   }
}   //End complete_bipartitie
/************************************************
 *
 *************************************************/
/************************************************************************
 *
 ************************************************************************/
/************************************************************************
 *
 ************************************************************************/
template<typename GraphType>
void compute_err(GraphType & graph) {
   int fail_count = 0;
   float sum = 0;
   float sum_ratings = 0;
   for (unsigned int i = 0; i < graph.num_nodes(); ++i) {
      for (int idx = 0; idx < SGD_FEATURE_SIZE; ++idx) {
         float f = graph.node_data()[i].features[idx];
         sum += f;
         if ((f != 0 && isnormal(f) == false)) {
            fail_count++;
         }

      }
   }
   fprintf(stderr, "Failed:: %6.6g,Sum, %6.6g ", fail_count / (float) (graph.num_nodes() * SGD_FEATURE_SIZE), sum);
   float accumulated_error = 0.0f;
   float max_err = 0.0f;
   typedef typename GraphType::NodeDataType NodeDataType;
   NodeDataType * features = graph.node_data();
   for (unsigned int i = 0; i < graph.num_edges(); ++i) {
      unsigned int src = graph.out_edge_src()[i];
      unsigned int dst = graph.out_neighbors()[i];
      float rating = graph.out_edge_data()[i];
      sum_ratings += rating;
      float computed_rating = features[src].dot_product(features[dst]);
      float err = (computed_rating - rating);
      max_err = std::max((double) max_err, (double) fabs(err));
      accumulated_error += err * err;
   }
   accumulated_error /= (float) graph.num_edges();
   float rms = std::sqrt((float) accumulated_error);
   fprintf(stderr, "Average_error, %6.6f , max_error, %6.6f, RMS, %6.6f , RatingsSum, %6.6g\n", accumulated_error, max_err, rms, sum_ratings);
}
template<typename GraphType>
float initialize_sgd_lv(GraphType & graph, std::vector<int> & movies) {
//   typedef typename GraphType::NodeDataType NodeDataType;
   typedef typename GraphType::EdgeDataType EdgeDataType;
   using namespace std;
   Galois::OpenCL::Timer timer;
   timer.start();
   FeatureType top = 1.0 / sqrt(SGD_FEATURE_SIZE);
   uniform_real_distribution<FeatureType> dist(0, top);
   mt19937 gen;
   FeatureType feature_sum = 0.0f, min_feature = top, max_feature = -top;
   EdgeDataType max_edge_weight = 0;
   //For each node, initialize features to random, and lock to be unlocked.
   for (unsigned int i = 0; i < graph.num_nodes(); ++i) {
      FeatureType * features_l = &(graph.node_data()[i].features[0]); //graph.node_data()[i].features;
      for (int j = 0; j < SGD_FEATURE_SIZE; ++j) {
         feature_sum += (features_l[j] = dist(gen));
         max_feature = std::max(features_l[j], max_feature);
         min_feature = std::min(features_l[j], min_feature);
         assert(isnormal(features_l[j]) || features_l[j] == 0);
      }
      for (auto idx = graph.outgoing_index()[i]; idx < graph.outgoing_index()[i + 1]; ++idx) {
         max_edge_weight = std::max(max_edge_weight, graph.out_edge_data()[idx]);
      }
      if (graph.num_neighbors(i) > 0)
         movies.push_back(i);
   }
   for (unsigned int i = 0; i < graph.num_nodes(); ++i) {
      for (auto idx = graph.outgoing_index()[i]; idx < graph.outgoing_index()[i + 1]; ++idx) {
         graph.out_edge_data()[idx] /= max_edge_weight;
      }
   }
   timer.stop();
   std::cout << "initial features:: " << feature_sum << " , [" << min_feature << " , " << max_feature;
   return max_edge_weight;
}
} //End namespace SGDCommon
/************************************************
 *
 *************************************************/

#endif // OpenCL.
#endif /* SGDCOMMON_H_ */
