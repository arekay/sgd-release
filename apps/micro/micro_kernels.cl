/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU.
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics:enable
#pragma OPENCL EXTENSION cl_khr_global_int32_extended_atomics:enable
#pragma OPENCL EXTENSION cl_khr_local_int32_base_atomics	:enable
#pragma OPENCL EXTENSION cl_khr_local_int32_extended_atomics:enable
#pragma OPENCL EXTENSION cl_khr_int64_base_atomics:enable
#pragma OPENCL EXTENSION cl_khr_int64_extended_atomics:enable



__kernel void num_groups_test(int num_threads, __global int * work_size, __global int * result){
   int id=  get_global_id(0);
   if(id < num_threads){
      int tmp=0;
      if(id < 1024)
      for(int i=0; i < work_size[id]; ++i){
         for(int j=0; j < work_size[id]; ++j){
            tmp+=i*j;
         }
      }
      result[id] = get_num_groups(0);
   }
}

/* Please Write the OpenCL Kernel(s) code here*/
__kernel
void pt_barrier_kernel( __global int * mem_pool){
//1) Deconstruct the data structures.
volatile __global int * phase =mem_pool +0; //1
volatile __global int * counter =mem_pool +1; //10
volatile __global int * barrier_counter =mem_pool +2; //4096
volatile __global int * barrier_sense =mem_pool +3; //0
volatile __global int * thread_counter = mem_pool+4;
volatile __global int * num_groups= mem_pool+5;
int local_sense = 0;
///////////////////////
mem_fence(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
if(get_global_id(0)<*thread_counter)
{
   do{
      if(get_global_id(0)==0)
         atomic_add(counter, 1);
      mem_fence(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
         if(atomic_sub(barrier_counter,1)==1){
            atomic_xor(barrier_sense, 1);
         };
      mem_fence(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
      /////################Barrier##############//////////
      while(*barrier_sense!=1){
         atomic_add(phase,1);
        };
      /////################Barrier##############//////////
      }while(0);//end global counter!=0
   atomic_xchg(phase, get_num_groups(0));
}
mem_fence(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
}//End wl_operator

///////############################################/////////////////////
void start_global_barrier(int fold, volatile __global volatile int* count){
  barrier(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
    if(get_local_id(0) == 0){
      atomic_add(count, 1);
      barrier(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
      int count_val = atomic_or(count,0);
      while(count_val < fold* get_num_groups(0) ){
          count_val = atomic_or(count,0);
       }
    }
    barrier(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
}

__kernel
void wg_barrier_kernel( __global int * mem_pool){
//1) Deconstruct the data structures.
volatile __global int * phase =mem_pool +0; //1
volatile __global volatile int * counter =mem_pool +1; //10
volatile __global int * barrier_counter =mem_pool +2; //4096
volatile __global int * barrier_sense =mem_pool +3; //0
volatile __global int * thread_counter = mem_pool+4;
volatile __global int * num_groups= mem_pool+5;
int local_sense = 0;
///////////////////////
mem_fence(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
if(get_global_id(0)<*thread_counter)
{
   do{
      if(get_global_id(0)==0)
         atomic_add(counter, 1);
      mem_fence(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
      /////################Barrier##############//////////
      start_global_barrier(atomic_or(counter,0), barrier_sense);
/////################Barrier##############//////////
      mem_fence(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
      }while(atomic_or(counter,0)<10);//end global counter!=0
   atomic_xchg(phase, get_num_groups(0));
}
}//End wl_operator

///////############################################/////////////////////


__kernel void micro_kernel(__global float * arr){
__local float arr1[10]; __private float arr2[1024];
float local_sum =0;
const uint gid = get_global_id(0);
for(float i=0 ;i<(1<<20);i+=1) {
    local_sum+=i;
}
for(int i=0;i<10; ++i)arr1[i]=(uint)local_sum;
for(int i=0;i<1024; ++i)arr2[i]=local_sum;
      for(int i=0;i<1024; ++i)arr2[0]+=arr2[i];
arr[gid] = arr2[0];
arr[gid] = local_sum;
return;
}
///////############################################/////////////////////
__kernel  void atomic_kernel( uint num_items , __global volatile uint * a, __global volatile uint * shared_loc){
   uint my_id = get_global_id(0);
//   for(int i=0; i<num_items; ++i)
           {
           int pos = atomic_add(shared_loc, 1);
           a[pos] = my_id;
   }
}//End atomic_kernel


///////############################################/////////////////////
__kernel  void empty_kernel( ){
   uint my_id = get_global_id(0);
}//End atomic_kernel

