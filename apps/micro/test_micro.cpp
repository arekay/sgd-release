/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU.
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <sys/time.h>
#include <string>
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
extern "C" {
#include "CL/cl.h"
}
;
#endif
#define DEFAULT_PLATFORM 0
#define DEFAULT_DEVICE 1
struct Timer {
   double _start;
   double _end;
   Timer() :
         _start(0), _end(0) {
   }
   void clear() {
      _start = _end = 0;
   }
   void start() {
      _start = rtclock();
   }
   void stop() {
      _end = rtclock();
   }
   double get_time_microseconds(void) {
      return (_end - _start);
   }

   static double rtclock() {
      struct timezone Tzp;
      struct timeval Tp;
      int stat;
      stat = gettimeofday(&Tp, &Tzp);
      if (stat != 0)
         printf("Error return from gettimeofday: %d", stat);
      return (Tp.tv_sec + Tp.tv_usec * 1.0e-6);
   }
};
double get_time_milli(void) {
   struct timeval tv;
   gettimeofday(&tv, NULL);
   double time_in_mill = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000;
   return time_in_mill;
}

#define MAX_GPU_SIZE (1<<28)

////////////////////////////////////////////////////////////////////////////////

// Simple compute kernel which computes the square of an input array
//
////////////////////////////////////////////////////////////////////////////////
struct CLEnv {
   size_t global;                      // global domain size for our calculation
   size_t local;                       // local domain size for our calculation
   cl_device_id device_id;             // compute device id
   cl_context context;                 // compute context
   cl_command_queue commands;          // compute command queue
   cl_program program;                 // compute program
   cl_kernel kernel;                   // compute kernel
   cl_platform_id platform;
};
/*********************************************************************
 *
 **********************************************************************/
static inline char *load_program_source(const char *filename) {
   FILE *fh;
   struct stat statbuf;
   char *source;
   if (!(fh = fopen(filename, "rb")))
      return NULL;
   stat(filename, &statbuf);
   source = (char *) malloc(statbuf.st_size + 1);
   fread(source, statbuf.st_size, 1, fh);
   source[statbuf.st_size] = 0;
   return source;
}
/*********************************************************************
 *
 **********************************************************************/
int setup_env(CLEnv & env, const char * kernel_name) {
   int err;
   cl_platform_id platforms[3];
   err = clGetPlatformIDs(3, &platforms[0], NULL);
   env.platform = platforms[DEFAULT_PLATFORM];
   if (err != CL_SUCCESS) {
      printf("Error: Failed to create a platform! :: %d \n", err);
      return EXIT_FAILURE;
   }
   cl_device_id devices[5];
   err = clGetDeviceIDs(env.platform, CL_DEVICE_TYPE_ALL, 5, &devices[0], NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to create a device group! :: %d \n", err);
      return EXIT_FAILURE;
   }
   env.device_id = devices[DEFAULT_DEVICE];
   // Create a compute context
   //
   env.context = clCreateContext(0, 1, &env.device_id, NULL, NULL, &err);
   if (!env.context) {
      printf("Error: Failed to create a compute context!\n");
      return EXIT_FAILURE;
   }
   // Create a command commands
   env.commands = clCreateCommandQueue(env.context, env.device_id, 0, &err);
   if (!env.commands) {
      printf("Error: Failed to create a command commands!\n");
      return EXIT_FAILURE;
   }
   // Create the compute program from the source buffer
   const char * source = load_program_source("apps/micro/micro_kernels.cl");
   env.program = clCreateProgramWithSource(env.context, 1, (const char **) &source, NULL, &err);
   if (!env.program) {
      printf("Error: Failed to create compute program!\n");
      return EXIT_FAILURE;
   }
   // Build the program executable
   err = clBuildProgram(env.program, 0, NULL, NULL, NULL, NULL);
   if (err != CL_SUCCESS) {
      size_t len;
      char buffer[2048];
      printf("Error: Failed to build program executable!\n");
      clGetProgramBuildInfo(env.program, env.device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
      printf("%s\n", buffer);
      exit(1);
   }

   // Create the compute kernel in the program we wish to run
   env.kernel = clCreateKernel(env.program, kernel_name, &err);
   if (!env.kernel || err != CL_SUCCESS) {
      printf("Error: Failed to create compute kernel!\n");
      exit(1);
   }
   err = clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(env.local), &env.local, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to retrieve kernel work group info! %d\n", err);
      exit(1);
   }
   printf("Local size detected :: %lu\n", env.local);

   return 0;
}
/*********************************************************************
 *
 **********************************************************************/
double apply_kernel(CLEnv & env, unsigned int size) {
   int err;
   float * data = new float[size];
   memset(data, 0, sizeof(float) * size);
   cl_mem input = clCreateBuffer(env.context, CL_MEM_READ_WRITE, sizeof(float) * size, NULL, NULL);
   if (!input) {
      printf("Error: Failed to allocate device memory!\n");
      exit(1);
   }
   Timer timer;
   timer.start();
   err = clEnqueueWriteBuffer(env.commands, input, CL_TRUE, 0, sizeof(float) * size, data, 0, NULL, NULL);
   if (err != CL_SUCCESS) {
      printf("Error(%d): Failed to write to source array!\n", err);
      exit(1);
   }
   // Set the arguments to our compute kernel
   err = clSetKernelArg(env.kernel, 0, sizeof(cl_mem), &input);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to set kernel arguments! %d\n", err);
      exit(1);
   }
   // Get the maximum work group size for executing the kernel on the device
   err = clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(env.local), &env.local, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to retrieve kernel work group info! %d\n", err);
      exit(1);
   }
   unsigned long local_mem_used;
   unsigned long private_mem_used;
   err |= clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_LOCAL_MEM_SIZE, sizeof(local_mem_used), &local_mem_used, NULL);
   err |= clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_PRIVATE_MEM_SIZE, sizeof(private_mem_used), &private_mem_used, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to retrieve kernel work group info! %d\n", err);
      exit(1);
   }
   // Execute the kernel over the entire range of our 1d input data set
   // using the maximum number of work group items for this device
   env.global = size;
   env.local = env.local < size ? env.local : size;
   printf("LocalMem : %ld Private : %ld Local size %ld Global size %ld \n", local_mem_used, private_mem_used, env.local, env.global);
   err = clEnqueueNDRangeKernel(env.commands, env.kernel, 1, NULL, &env.global, &env.local, 0, NULL, NULL);
   if (err) {
      printf("Error: Failed to execute kernel[%d] err:%d!\n", size, err);
      exit(-1);
   }
   // Wait for the command commands to get serviced before reading back results
   clFinish(env.commands);
   // Read back the results from the device to verify the output
   err = clEnqueueReadBuffer(env.commands, input, CL_TRUE, 0, sizeof(float) * size, data, 0, NULL, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to read output array! %d\n", err);
      exit(1);
   }
   timer.stop();
   // Validate our results
   unsigned int correct = 0;
   const float seed = (1 << 20);
//    fprintf(stderr, "Seed: %f Result: %f\n",seed, data[0]);
   float tmp_res = 0;
   for (int i = 0; i < seed; ++i) {
      tmp_res += i;
   }

   //  fprintf(stderr, "Temp :%f \n", tmp_res);
   for (unsigned int i = 0; i < size; i++) {
      if (data[i] == tmp_res) {
         correct++;
      }
   }
   // Print a brief summary detailing the results
   //
   if (correct != size)
      printf("ERR!!! Computed '%ud/%ud' correct values!\n", correct, size);
   delete[] data;
   clReleaseMemObject(input);
//    return end_time_main-start_time_main;
   return timer.get_time_microseconds();
}
/*********************************************************************
 *
 **********************************************************************/

double apply_barrier_kernel(CLEnv & env, int num_threads) {
   //const int num_threads = 4096;
   int err;
   int * data = new int[6];
   //////////////
   data[0] = 1; //phase
   data[1] = 0; //counter
   data[2] = num_threads; //barrier_counter
   data[3] = 0; //barrier_sense;
   data[4] = num_threads; //barrier_sense;

   /////////////
   cl_mem input = clCreateBuffer(env.context, CL_MEM_READ_WRITE, sizeof(int) * 6, NULL, NULL);
   if (!input) {
      printf("Error: Failed to allocate device memory!\n");
      exit(1);
   }
   Timer timer;
   timer.start();
   err = clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(env.local), &env.local, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to retrieve kernel work group info! %d\n", err);
      exit(1);
   }
   // Execute the kernel over the entire range of our 1d input data set
   // using the maximum number of work group items for this device
   env.global = ceil(num_threads / (double) env.local) * env.local;
   data[5] = env.global / env.local;

   err = clEnqueueWriteBuffer(env.commands, input, CL_TRUE, 0, sizeof(int) * 6, data, 0, NULL, NULL);
   if (err != CL_SUCCESS) {
      printf("Error(%d): Failed to write to source array!\n", err);
      exit(1);
   }
   // Set the arguments to our compute kernel
   err = clSetKernelArg(env.kernel, 0, sizeof(cl_mem), &input);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to set kernel arguments! %d\n", err);
      exit(1);
   }
   // Get the maximum work group size for executing the kernel on the device
   printf(" Start::Size = [ %lu, %lu, %lu] :: ", env.global, env.local, env.global / env.local);
   fflush(stdout);
   err = clEnqueueNDRangeKernel(env.commands, env.kernel, 1, NULL, &env.global, &env.local, 0, NULL, NULL);
   if (err) {
      printf("Error: Failed to execute kernel err:%d!\n", err);
      exit(-1);
   }
   // Wait for the command commands to get serviced before reading back results
   clFinish(env.commands);
   // Read back the results from the device to verify the output
   err = clEnqueueReadBuffer(env.commands, input, CL_TRUE, 0, sizeof(int) * 6, data, 0, NULL, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to read output array! %d\n", err);
      exit(1);
   }
   timer.stop();

   //  fprintf(stderr, "Temp :%f \n", tmp_res);
   printf("Results [ ");
   for (unsigned int i = 0; i < 6; i++) {
      printf("%d, ", data[i]);
   }
   printf("]\n");
   delete[] data;
   clReleaseMemObject(input);
//    return end_time_main-start_time_main;
   return timer.get_time_microseconds();
}
/*********************************************************************
 *
 **********************************************************************/

double apply_atomic_kernel(CLEnv & env, unsigned int size) {
   int err;
   const int array_size = size;
   unsigned int * data = new unsigned int[array_size];
   memset(data, 0, sizeof(unsigned int) * array_size);
   cl_mem input = clCreateBuffer(env.context, CL_MEM_READ_WRITE, sizeof(unsigned int) * array_size, NULL, NULL);
   cl_mem atomic_loc = clCreateBuffer(env.context, CL_MEM_READ_WRITE, sizeof(unsigned int) * 1, NULL, NULL);
   if (!input) {
      printf("Error: Failed to allocate device memory!\n");
      exit(1);
   }
   Timer timer;

   err = clEnqueueWriteBuffer(env.commands, input, CL_TRUE, 0, sizeof(unsigned int) * array_size, data, 0, NULL, NULL);
   err |= clEnqueueWriteBuffer(env.commands, atomic_loc, CL_TRUE, 0, sizeof(unsigned int) , data, 0, NULL, NULL);
   if (err != CL_SUCCESS) {
      printf("Error(%d): Failed to write to source array!\n", err);
      exit(1);
   }
   // Set the arguments to our compute kernel
   err = clSetKernelArg(env.kernel, 0, sizeof(cl_uint), &size);
   err |= clSetKernelArg(env.kernel, 1, sizeof(cl_mem), &input);
   err |= clSetKernelArg(env.kernel, 2, sizeof(cl_mem), &atomic_loc);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to set kernel arguments! %d\n", err);
      exit(1);
   }
   // Get the maximum work group size for executing the kernel on the device
   err = clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(env.local), &env.local, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to retrieve kernel work group info! %d\n", err);
      exit(1);
   }
   unsigned long local_mem_used;
   unsigned long private_mem_used;
   err |= clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_LOCAL_MEM_SIZE, sizeof(local_mem_used), &local_mem_used, NULL);
   err |= clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_PRIVATE_MEM_SIZE, sizeof(private_mem_used), &private_mem_used, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to retrieve kernel work group info! %d\n", err);
      exit(1);
   }
   // Execute the kernel over the entire range of our 1d input data set
   // using the maximum number of work group items for this device
   env.global = size;
   env.local = env.local < size ? env.local : size;
   printf("LocalMem : %ld Private : %ld Local size %ld Global size %ld \t", local_mem_used, private_mem_used, env.local, env.global);
   timer.clear();
   timer.start();
   err = clEnqueueNDRangeKernel(env.commands, env.kernel, 1, NULL, &env.global, NULL, 0, NULL, NULL);
   if (err) {
      printf("Error: Failed to execute kernel[%d] err:%d!\n", size, err);
      exit(-1);
   }
   // Wait for the command commands to get serviced before reading back results
   clFinish(env.commands);
   timer.stop();
   // Read back the results from the device to verify the output
   err = clEnqueueReadBuffer(env.commands, input, CL_TRUE, 0, sizeof(unsigned int) * array_size, data, 0, NULL, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to read output array! %d\n", err);
      exit(1);
   }

   // Validate our results
   unsigned int correct = 0;
   double sum = 0;
   for (unsigned int i = 0; i < size; ++i) {
      sum += 1;
   }
   for (int i = 0; i < array_size; ++i) {
      if (data[i] == sum)
         correct++;
   }
   printf("Correct results (%g) for %d of %d items, index-0::[%d] \n", sum, correct, size, data[0]);
   //
   delete[] data;
   clReleaseMemObject(input);
//    return end_time_main-start_time_main;
   return timer.get_time_microseconds();
}
/*********************************************************************
 *
 **********************************************************************/

void test_atomic() {
   CLEnv env;
   setup_env(env, "atomic_kernel");
   double time_average;
   for (int size = 1; size <= MAX_GPU_SIZE; size *= 2) {
      time_average = 0;
      for (int run = 0; run < 5; ++run)
         time_average += apply_atomic_kernel(env, size);
      time_average /= 5.0f;
      printf("Size %d, Time %f\n", size, time_average);
   }
   clReleaseProgram(env.program);
   clReleaseKernel(env.kernel);
   clReleaseCommandQueue(env.commands);
   clReleaseContext(env.context);
   return;
}
/*********************************************************************
 *
 **********************************************************************/
double call_empty_kernel(CLEnv & env, unsigned int size) {
   int err;
   const int array_size = size;
//   unsigned int * data = new unsigned int[array_size];
//   memset(data, 0, sizeof(unsigned int) * array_size);
//   cl_mem input = clCreateBuffer(env.context, CL_MEM_READ_WRITE, sizeof(unsigned int) * array_size, NULL, NULL);
//   if (!input) {
//      printf("Error: Failed to allocate device memory!\n");
//      exit(1);
//   }
   Timer timer;

//   err = clEnqueueWriteBuffer(env.commands, input, CL_TRUE, 0, sizeof(unsigned int) * array_size, data, 0, NULL, NULL);
//   if (err != CL_SUCCESS) {
//      printf("Error(%d): Failed to write to source array!\n", err);
//      exit(1);
//   }
//   // Set the arguments to our compute kernel
//   err |= clSetKernelArg(env.kernel, 0, sizeof(cl_mem), &input);
//   if (err != CL_SUCCESS) {
//      printf("Error: Failed to set kernel arguments! %d\n", err);
//      exit(1);
//   }
   // Get the maximum work group size for executing the kernel on the device
   err = clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(env.local), &env.local, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to retrieve kernel work group info! %d\n", err);
      exit(1);
   }
   unsigned long local_mem_used;
   unsigned long private_mem_used;
   err |= clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_LOCAL_MEM_SIZE, sizeof(local_mem_used), &local_mem_used, NULL);
   err |= clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_PRIVATE_MEM_SIZE, sizeof(private_mem_used), &private_mem_used, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to retrieve kernel work group info! %d\n", err);
      exit(1);
   }
   // Execute the kernel over the entire range of our 1d input data set
   // using the maximum number of work group items for this device
   env.global = size;
   env.local = env.local < size ? env.local : size;
   printf("LocalMem : %ld Private : %ld Local size %ld Global size %ld \n", local_mem_used, private_mem_used, env.local, env.global);
   timer.clear();
   timer.start();
   err = clEnqueueNDRangeKernel(env.commands, env.kernel, 1, NULL, &env.global, NULL, 0, NULL, NULL);
   if (err) {
      printf("Error: Failed to execute kernel[%d] err:%d!\n", size, err);
      exit(-1);
   }
   // Wait for the command commands to get serviced before reading back results
   clFinish(env.commands);
   timer.stop();
   // Read back the results from the device to verify the output
//   err = clEnqueueReadBuffer(env.commands, input, CL_TRUE, 0, sizeof(unsigned int) * array_size, data, 0, NULL, NULL);
//   if (err != CL_SUCCESS) {
//      printf("Error: Failed to read output array! %d\n", err);
//      exit(1);
//   }
//
//   delete[] data;
//   clReleaseMemObject(input);
//    return end_time_main-start_time_main;
   return timer.get_time_microseconds();
}
/*********************************************************************
 *
 **********************************************************************/

void test_call_overhead(){
   CLEnv env;
   setup_env(env, "empty_kernel");
   double time_average;
   for (int size = 1; size <= MAX_GPU_SIZE; size *= 2) {
      time_average = 0;
      for (int run = 0; run < 5; ++run)
         time_average += call_empty_kernel(env, size);
      time_average /= 5.0f;
      printf("Size %d, Time %f\n", size, time_average);
   }
   clReleaseProgram(env.program);
   clReleaseKernel(env.kernel);
   clReleaseCommandQueue(env.commands);
   clReleaseContext(env.context);
   return;

}
/*********************************************************************
 *
 **********************************************************************/

void test_micro() {
   CLEnv env;
   setup_env(env, "micro_kernel");
   printf("Max size %d \n", MAX_GPU_SIZE);
   double time_average;
   for (int size = 1; size < MAX_GPU_SIZE; size *= 2) {
      time_average = 0;
      for (int run = 0; run < 5; ++run)
         time_average += apply_kernel(env, size);
      time_average /= 5.0f;
      printf("Size %d, Time %f\n", size, time_average);
   }
   clReleaseProgram(env.program);
   clReleaseKernel(env.kernel);
   clReleaseCommandQueue(env.commands);
   clReleaseContext(env.context);
   return;
}
/*********************************************************************
 *
 **********************************************************************/

void test_copy() {
   CLEnv env;
   setup_env(env, "micro_kernel");
   printf("Max size %d \n", MAX_GPU_SIZE);
   double time_average;
   const int num_runs = 20;
   for (int size = 1; size < MAX_GPU_SIZE; size *= 2)
   {
      time_average = 0;
      for (int run = 0; run < num_runs; ++run)
      {
         int err;
            int * data = new int[size];
            memset(data, 0, sizeof(int) * size);
            cl_mem input = clCreateBuffer(env.context, CL_MEM_READ_WRITE, sizeof(int) * size, NULL, NULL);
            if (!input) {
               printf("Error: Failed to allocate device memory!\n");
               exit(1);
            }
            Timer timer;
            timer.start();
            err = clEnqueueWriteBuffer(env.commands, input, CL_TRUE, 0, sizeof(int) * size, data, 0, NULL, NULL);
            if (err != CL_SUCCESS) {
               printf("Error(%d): Failed to write to source array!\n", err);
               exit(1);
            }
            // Wait for the command commands to get serviced before reading back results
            clFinish(env.commands);
            // Read back the results from the device to verify the output
            err = clEnqueueReadBuffer(env.commands, input, CL_TRUE, 0, sizeof(int) * size, data, 0, NULL, NULL);
            if (err != CL_SUCCESS) {
               printf("Error: Failed to read output array! %d\n", err);
               exit(1);
            }
            timer.stop();
            // Validate our results
            delete[] data;
            clReleaseMemObject(input);
         //    return end_time_main-start_time_main;
            time_average+= timer.get_time_microseconds();
      }
         time_average /= (float)(num_runs);
      printf("Size %d, Time %f\n", size, time_average);
   }
   clReleaseCommandQueue(env.commands);
   clReleaseContext(env.context);
   return;
}
/*********************************************************************
 *
 **********************************************************************/
void test_barrier(bool wg_barr) {
   CLEnv env;
   if (wg_barr)
      setup_env(env, "wg_barrier_kernel");
   else
      setup_env(env, "pt_barrier_kernel");
   printf("Max size %d \n", MAX_GPU_SIZE);
   for (int i = 8; i < 16384; i += 8) {
      printf("[%d] :: ", i);
      apply_barrier_kernel(env, i);
   }
   clReleaseProgram(env.program);
   clReleaseKernel(env.kernel);
   clReleaseCommandQueue(env.commands);
   clReleaseContext(env.context);
   return;
}
/*********************************************************************
 *
 **********************************************************************/
double apply_numgroups_kernel(CLEnv & env, int num_threads) {
   //const int num_threads = 4096;
   int err;
   int * work_sizes = new int[num_threads];
   int * results = new int[num_threads];
   for (int i = 0; i < num_threads; ++i) {
      work_sizes[i] = num_threads-i;
      results[i] = 0;
   }
   /////////////
   cl_mem g_wsize = clCreateBuffer(env.context, CL_MEM_READ_WRITE, sizeof(int) * num_threads, NULL, NULL);
   if (!g_wsize) {
      printf("Error: Failed to allocate device memory!\n");
      exit(1);
   }
   cl_mem g_result = clCreateBuffer(env.context, CL_MEM_READ_WRITE, sizeof(int) * num_threads, NULL, NULL);
   if (!g_result) {
      printf("Error: Failed to allocate device memory!\n");
      exit(1);
   }
   Timer timer;
   timer.start();

   err = clEnqueueWriteBuffer(env.commands, g_wsize, CL_TRUE, 0, sizeof(int) * num_threads, work_sizes, 0, NULL, NULL);
   if (err != CL_SUCCESS) {
      printf("Error(%d): Failed to write to source array!\n", err);
      exit(1);
   }
   err = clEnqueueWriteBuffer(env.commands, g_result, CL_TRUE, 0, sizeof(int) * num_threads, results, 0, NULL, NULL);
   if (err != CL_SUCCESS) {
      printf("Error(%d): Failed to write to source array!\n", err);
      exit(1);
   }
   // Set the arguments to our compute kernel
   err = clSetKernelArg(env.kernel, 0, sizeof(cl_int), &num_threads);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to set kernel arguments! %d\n", err);
      exit(1);
   }
   err = clSetKernelArg(env.kernel, 1, sizeof(cl_mem), &g_wsize);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to set kernel arguments! %d\n", err);
      exit(1);
   }
   err = clSetKernelArg(env.kernel, 2, sizeof(cl_mem), &g_result);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to set kernel arguments! %d\n", err);
      exit(1);
   }
   // Get the maximum work group size for executing the kernel on the device
   env.global = env.local * ceil(num_threads/(float)env.local);
   env.local = env.local < num_threads ? env.local : num_threads;
   printf(" Start::Size = [ %lu, %lu, %lu] :: ", env.global, env.local, env.global / env.local);
   fflush(stdout);
   err = clEnqueueNDRangeKernel(env.commands, env.kernel, 1, NULL, &env.global, &env.local, 0, NULL, NULL);
   if (err) {
      printf("Error: Failed to execute kernel err:%d!\n", err);
      exit(-1);
   }
   // Wait for the command commands to get serviced before reading back results
   clFinish(env.commands);
   // Read back the results from the device to verify the output
   err = clEnqueueReadBuffer(env.commands, g_result, CL_TRUE, 0, sizeof(int) * num_threads, results, 0, NULL, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to read output array! %d\n", err);
      exit(1);
   }
   timer.stop();

   //  fprintf(stderr, "Temp :%f \n", tmp_res);
   fprintf(stderr, "Results [ ");
   int min_res = num_threads, max_res = 0;
   for (unsigned int i = 0; i < num_threads; i++) {
      min_res = std::min(min_res, results[i]);
      max_res = std::max(max_res, results[i]);
//      printf("%d, ", results[i]);
   }
   fprintf(stderr, "%d, %d, %d ", num_threads, min_res, max_res);
   fprintf(stderr, "]\n");
   delete[] results;
   delete[] work_sizes;
   clReleaseMemObject(g_wsize);
   clReleaseMemObject(g_result);
//    return end_time_main-start_time_main;
   return timer.get_time_microseconds();
}
/*********************************************************************
 *
 **********************************************************************/
void test_num_groups() {
   CLEnv env;
   setup_env(env, "num_groups_test");
   printf("Max size %d \n", MAX_GPU_SIZE);
   for (int i = 1585; i < 16384; i += 512) {
      printf("[%d] :: ", i);
      apply_numgroups_kernel(env, i);
   }
   clReleaseProgram(env.program);
   clReleaseKernel(env.kernel);
   clReleaseCommandQueue(env.commands);
   clReleaseContext(env.context);
   return;
}
/*********************************************************************
 *
 **********************************************************************/
enum TestType {
   ATOMIC, MICRO, THREAD_BARRIER, GROUP_BARRIER, NUMGROUPTEST, MEMCOPY, CALL_OVERHEAD
};
/*********************************************************************
 *
 **********************************************************************/
int main(int argc, char ** argv) {
   TestType test_type = ATOMIC;
   if (argc > 1) {
      if (strcmp(argv[1], "atomic") == 0) {
         test_type = ATOMIC;
      } else if (strcmp(argv[1], "micro") == 0) {
         test_type = MICRO;
      } else if (strcmp(argv[1], "thread_barrier") == 0) {
         test_type = THREAD_BARRIER;
      } else if (strcmp(argv[1], "numgrouptest") == 0) {
         test_type = NUMGROUPTEST;
      }else if (strcmp(argv[1],"memcopy") == 0){
         test_type = MEMCOPY;
      }
      else if (strcmp(argv[1], "call")==0){
         test_type=CALL_OVERHEAD;
      }
   }    //end if
   else {
      printf("Options: atomic - Test atomics\n");
      printf("       : micro - Memory accesses\n");
      printf("       : thread_barrier - Test per-thread barrier\n");
      printf("       : group_barrier - Test group-barriers(default)\n");
      printf("       : numgrouptest  - Test group-divergence behavior\n");
      printf("       : memcopy  - Test memory copy\n");
   }
   switch (test_type) {
   case ATOMIC:
      test_atomic();
      break;
   case MICRO:
      test_micro();
      break;
   case THREAD_BARRIER:
      test_barrier(false);
      break;
   case GROUP_BARRIER:
      test_barrier(true);
      break;
   case MEMCOPY:
      test_copy();
      break;
   case CALL_OVERHEAD:
      test_call_overhead();
      break;
   case NUMGROUPTEST:
      test_num_groups();
      break;
   default:
      break;
   }
   printf("Terminating....\n");
   return 0;
}
/*********************************************************************
 *
 **********************************************************************/
