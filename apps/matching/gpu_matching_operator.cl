/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_int64_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_int64_extended_atomics : enable
#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics :enable
#pragma OPENCL EXTENSION cl_khr_global_int32_extended_atomics : enable
#pragma OPENCL EXTENSION cl_khr_local_int32_base_atomics :enable
#pragma OPENCL EXTENSION cl_khr_local_int32_extended_atomics : enable
#pragma OPENCL EXTENSION cl_khr_fp64: enable
#pragma OPENCL EXTENSION cl_khr_fp16: enable

bool lock(__global int * locks, int s, int d) {
#ifdef _SGD_NO_LOCKS_
   return true;
#else
   volatile __global int * src = &locks[s]; //&(node_data(graph, s)->lock);
   volatile __global int * dst = &locks[d];//&(node_data(graph, d)->lock);
   int my_id = get_global_id(0);
   if(atomic_cmpxchg(src, -1, my_id)==-1) {
      if(atomic_cmpxchg(dst, -1, my_id)==-1) {
         return true;
      }
      atomic_cmpxchg(src, my_id, -1);
      return false;
   }
   return false;
#endif
}
///
void unlock( __global int * locks, int s, int d) {
#ifdef _SGD_NO_LOCKS_
#else
   volatile __global int * src = &locks[s]; //&(node_data(graph, s)->lock);
   volatile __global int * dst = &locks[d];//&(node_data(graph, d)->lock);
   int my_id = get_global_id(0);
   atomic_cmpxchg(dst, my_id, -1);
   atomic_cmpxchg(src, my_id, -1);
#endif
}

////////////////////////////////////////////

////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void sgd_edge_matching( __global int * locks,  __global int * metadata,  __global int * edge_info, __global int *phase, __global int *wlist) {
   const uint my_id = get_global_id(0);
   const int num_edges = metadata[2];
   const int chunk_size = metadata[4];
   const int curr_phase = metadata[6];
   const int curr_step= metadata[9];
   __global int * curr_wl, *next_wl;
   __global int * next_wl_counter, *curr_size;
   if ((curr_step%2)==0){
      curr_wl = wlist;
      next_wl = wlist+num_edges;
      next_wl_counter = metadata +8;
      curr_size = metadata +7;
   }else{
      next_wl = wlist;
      curr_wl = wlist+num_edges;
      next_wl_counter = metadata +7;
      curr_size = metadata +8;
   }

   if(my_id < *curr_size)
   {
         const int work_id = curr_wl[my_id];
         const int src = edge_info[2*work_id];
         const int dst = edge_info[2*work_id+1];
         if(phase[work_id]==0)
         {
            if(lock(locks, src, dst)==true)
            {
               //Update phase[my_id] to the current phase.
               phase[work_id] = curr_phase;
               //Do not release locks, this would be done when a new phase is found.
            }//End if lock!
            else{
               int counter = atomic_add(next_wl_counter, 1);
               next_wl[counter]=work_id;
            }
         }
   }
}
