/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef GPUMATCHING_H_
#define GPUMATCHING_H_
namespace Galois {
namespace GPUMatching {
template<typename Graph>
struct GPUMatching {
   typedef typename Galois::OpenCL::Array<cl_int> ArrayType;
   typedef typename Galois::OpenCL::GPUArray<cl_int> GPUArrayType;
   typedef typename Galois::OpenCL::Array<FeatureType> FeatureArrayType;
   typedef typename Galois::OpenCL::GPUArray<FeatureType> GPUFeatureArrayType;
   typedef typename Galois::OpenCL::CL_Kernel Kernel;
   ArrayType * locks;
   ArrayType *phase;
   ArrayType *m_worklist;
   ArrayType *metadata;
   ArrayType *edge_info;
   Graph & graph;
   Kernel matching_kernel;
   GPUMatching(Graph & g) :
         graph(g) {
      matching_kernel.init("./apps/matching/gpu_matching_operator.cl", "sgd_edge_matching");
      phase = new ArrayType(graph.num_edges());
      m_worklist = new ArrayType(2 * graph.num_edges());
      locks = new ArrayType(graph.num_nodes());
      metadata = new ArrayType(16);
      edge_info = new ArrayType(2 * graph.num_edges());
      for (size_t i = 0; i < graph.num_edges(); ++i) {
         phase->host_data[i] = 0;
         m_worklist->host_data[i] = i;
         m_worklist->host_data[graph.num_edges() + i] = 0;
         edge_info->host_data[2 * i] = graph.get_edge_src()[i];
         edge_info->host_data[2 * i + 1] = graph.out_neighbors()[i];
      }
      for (size_t i = 0; i < graph.num_nodes(); ++i) {
         locks->host_data[i] = -1;
      }

   }
   ~GPUMatching() {
      delete phase;
      delete m_worklist;
      delete locks;
      delete metadata;
      delete edge_info;
   }

   void do_matching() {
      //__kernel void sgd_edge_matching( __global int * locks,  __global int * metadata,  __global int * edge_info, __global int *phase) {
      /*memset(phase->host_data, 0,sizeof(graph.num_edges())*sizeof(int));
       memset(locks->host_data, 0,sizeof(graph.num_nodes())*sizeof(int));*/
      phase->copy_to_device();
      locks->copy_to_device();
      {
         metadata->host_ptr()[0] = graph.num_nodes();
         //metadata->host_ptr()[1] = round;
         metadata->host_ptr()[2] = graph.num_edges();
         metadata->host_ptr()[4] = graph.num_nodes();
         metadata->host_ptr()[6] = 1; //Start phase
         metadata->host_ptr()[7] = graph.num_edges(); //wl_0 size!
         metadata->host_ptr()[8] = 0; //wl_1 size!
         metadata->host_ptr()[9] = 0; //step
         matching_kernel.set_work_size(graph.num_nodes());
         fprintf(stderr, "Matching pre- [%d,%d,%d]\n", metadata->host_ptr()[2], metadata->host_ptr()[4], metadata->host_ptr()[6]);
      }
      matching_kernel.set_arg_list(locks, metadata, edge_info, phase, m_worklist);
//      buffers[0]->wait_on_copy();
      int * m_ptr = metadata->host_ptr();
      {
         int max_degree = 0;
         for (size_t i = 0; i < graph.num_nodes(); ++i)
            max_degree = std::max(max_degree, (int) (graph.outgoing_index()[i + 1] - graph.outgoing_index()[i]));
         fprintf(stderr, "max degree = %d\n", max_degree);
      }
      unsigned int total_work = 0;
      do {
         const int curr_index = ((m_ptr[9]) % 2 == 0) ? 0 : 1;
         const int next_index = (curr_index == 1) ? 0 : 1;
         matching_kernel.set_work_size(m_ptr[7 + curr_index]);
         matching_kernel();
         metadata->copy_to_host();
         int work_done = m_ptr[7 + curr_index] - m_ptr[7 + next_index];
         //fprintf(stderr, "Match(Phase, WL_0,, W:_1, Step, Prev, (curr,prev)):[%d,%d,%d,%d,%d,(%d,%d), %d += %d]\n", m_ptr[6],m_ptr[7], m_ptr[8], m_ptr[9], old_counter, curr_index, next_index, total_work, work_done);
         if (work_done == 0) {
            //Nothing new added.
            for (size_t i = 0; i < graph.num_nodes(); ++i)
               locks->host_data[i] = -1;
            locks->copy_to_device();
            m_ptr[6] += 1;
         } else {

         }
         total_work += work_done;
         //Now update step and counter for next round.
         m_ptr[9] += 1;
         m_ptr[7 + curr_index] = 0;
         /*phase->copy_to_host();
          match_counter = 0;
          for(size_t i= 0; i < graph.num_edges(); ++i){
          match_counter+=(phase->host_ptr()[i]!=0);
          }
          fprintf(stderr, "Matched :: %d\n", match_counter);
          buffers[0]->metadata->host_ptr()[6] +=1;
          for(size_t i=0;i<graph.num_nodes(); ++i)locks->host_data[i]=-1;
          locks->copy_to_device();*/
         metadata->copy_to_device();
      } while (total_work != graph.num_edges());
      fprintf(stderr, "Done matching after [%d] rounds, [%d] steps!\n", metadata->host_ptr()[6], metadata->host_ptr()[9]);
      phase->copy_to_host();
      //Write edges and phases;
      {
         std::ofstream out_file("independent_set.csv");
         for (size_t i = 0; i < graph.num_edges(); ++i) {
            out_file << i << ", " << edge_info[2 * i] << ", " << edge_info[2 * i + 1] << ", " << phase->host_ptr()[i] << "\n";
         }
         out_file.close();
      }
      delete phase;
      delete m_worklist;
   }

};

/************************************************************************
 *
 *
 *************************************************************************/

}//End namespace GPUMatching
}//End namespace Galois

#endif /* GPUMATCHING_H_ */
