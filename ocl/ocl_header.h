/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
extern "C" {
#include "CL/cl.h"
}
;
#endif

#ifdef _ALTERA_FPGA_USE_
   # define _ALTERA_EMULATOR_USE_ 1
#else
   #define _GALOIS_BUILD_INITIALIZER_KERNEL_ 1
#endif

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sys/stat.h>
#include <assert.h>
#include "../gopt/common_header.h"
#include "CLErrors.h"
#include "CLDeviceManager.h"
#include "../ocl/util/StatsManager.h"
#include "../ds/Arrays/Arrays.h"
//////////////////////////////////////////
#ifndef GOPT_OCL_HEADER_H_
#define GOPT_OCL_HEADER_H_


#endif /* GOPT_OCL_HEADER_H_ */
