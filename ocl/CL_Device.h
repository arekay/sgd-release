/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef GALOISGPU_OCL_CL_DEVICE_H_
#define GALOISGPU_OCL_CL_DEVICE_H_

namespace Galois{
namespace OpenCL{

struct CL_Device {
protected:
   friend struct Galois::OpenCL::CL_Platform;
   cl_device_id _id;
   cl_command_queue _queue;
   cl_context _context;
   const CL_Platform * platform;
#if _GALOIS_BUILD_INITIALIZER_KERNEL_
   cl_kernel init_kernel;
   const char * init_kernel_str; //= ;
#endif
   DeviceStats _stats;
   explicit CL_Device(CL_Platform * p, cl_device_id p_id) :
         _id(0),platform(p)
#if _GALOIS_BUILD_INITIALIZER_KERNEL_
   , init_kernel_str("__kernel void init(__global int * arr, int size, int val){const int id = get_global_id(0); if(id < size){arr[id]=val;}}")
#endif
   {
      initialize(p_id);
      cl_int err;
      // Create the compute program from the source buffer                   [6]
#if _GALOIS_BUILD_INITIALIZER_KERNEL_
      cl_program init_program = clCreateProgramWithSource(_context, 1, &init_kernel_str, NULL, &err);
      CHECK_CL_ERROR(err, "clCreateProgramWithSource failed.");
      CHECK_ERROR_NULL(init_program, "clCreateProgramWithSource");
      // Build the program executable                                        [7]
      err = clBuildProgram(init_program, 0, NULL, "", NULL, NULL);
      if (err != CL_SUCCESS) {
         size_t len;
         char buffer[10 * 2048];
         clGetProgramBuildInfo(init_program, _id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
         std::cout << "\n====Kernel build log====\n" << buffer << "\n=====END LOG=====\n";
      }
      init_kernel = clCreateKernel(init_program, "init", &err);
#endif
   }
public:
   void finish(){
      clFinish(command_queue());
   }
   cl_device_id id(){
      return _id;
   }
   cl_command_queue command_queue(){
      return _queue;
   }
   cl_context context(){
      return _context;
   }
   void print_stats() {
      fprintf(stderr, "Device: %s, ", name().c_str());
      _stats.print();
      fprintf(stderr, "\n");
   }
   void print() const {
      CL_Device::print_info(_id);
   }
   DeviceStats & stats(){
      return _stats;
   }
   void build_init_kernel();
   template<typename T>
   void init_on_device(cl_mem arr, size_t sz, const T & val);
   static float toMB(long v) {
      return v / (float) (1024 * 1024);
   }
   ~CL_Device() {
      fprintf(stderr, "Released device :: %s \n", name().c_str());
   }
   const CL_Platform* get_platrform() {
//      fprintf(stderr, "Getting platform for %s\n", name().c_str());
      return platform;
   }
   cl_ulong get_device_shared_memory() {
      cl_ulong ret;
      CHECK_CL_ERROR(clGetDeviceInfo(_id, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(cl_ulong), &ret, NULL), "Local mem size");
      return ret;
   }
   cl_ulong get_max_allocation_size() {
      cl_ulong ret;
      CHECK_CL_ERROR(clGetDeviceInfo(_id, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(cl_ulong), &ret, NULL), "Global mem size");
      return ret;
   }
   cl_uint get_device_threads() {
      cl_uint num_eus;
      CHECK_CL_ERROR(clGetDeviceInfo(_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &num_eus, NULL), "clGetDeviceInfo");
      size_t max_wg_size;
      CHECK_CL_ERROR(clGetDeviceInfo(_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &max_wg_size, NULL), "clGetDeviceInfo");
      return num_eus * max_wg_size;
   }
   void initialize(cl_device_id p_id) {
      _id = p_id;
//      check_device(_id);
      cl_int err;
      _context = clCreateContext(0, 1, &_id, NULL, NULL, &err);
      CHECK_ERROR_NULL(&_context, "clCreateContext");
      //CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE
#ifdef _GOPT_CL_ENABLE_PROFILING_
      _queue = clCreateCommandQueue(_context, _id, CL_QUEUE_PROFILING_ENABLE, &err);
#else
      _queue = clCreateCommandQueue(_context, _id, 0 /*CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE*/, &err);
#endif
      CHECK_ERROR_NULL(&_queue, "clCreateCommandQueue ");
   DEBUG_CODE( check_command_queue(_queue);)
//      print();
}

static void check_device(cl_device_id p_id) {
   cl_ulong mem_cache_size;
   cl_device_mem_cache_type mem_type;
   cl_uint line_size;
   CHECK_CL_ERROR(clGetDeviceInfo(p_id, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), &mem_cache_size, NULL), "Global mem size");
   std::cout << "Global memory " << mem_cache_size << "\n";

   CHECK_CL_ERROR(clGetDeviceInfo(p_id, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, sizeof(cl_ulong), &mem_cache_size, NULL), "Global mem cache size.");
   CHECK_CL_ERROR(clGetDeviceInfo(p_id, CL_DEVICE_GLOBAL_MEM_CACHE_TYPE, sizeof(cl_device_mem_cache_type), &mem_type, NULL), "Global mem cache type");
   CHECK_CL_ERROR(clGetDeviceInfo(p_id, CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, sizeof(cl_ulong), &line_size, NULL), "Global mem cache line size");
   std::cout << "Global mem cache " << mem_cache_size << " , Line size " << line_size << " , ";
   if (mem_type & CL_READ_WRITE_CACHE)
      std::cout << "CL_READ_WRITE_CACHE ";
   if (mem_type & CL_READ_ONLY_CACHE)
      std::cout << "CL_READ_ONLY_CACHE";
   if (mem_type & CL_NONE)
      std::cout << "CL_NONE";
   std::cout << "\n";

   CHECK_CL_ERROR(clGetDeviceInfo(p_id, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(cl_ulong), &mem_cache_size, NULL), "Local mem size");
   CHECK_CL_ERROR(clGetDeviceInfo(p_id, CL_DEVICE_LOCAL_MEM_TYPE, sizeof(cl_device_mem_cache_type), &mem_type, NULL), "Local mem type");
   std::cout << "Local mem cache " << mem_cache_size << " , ";
   if (mem_type & CL_READ_WRITE_CACHE)
      std::cout << "CL_READ_WRITE_CACHE ";
   if (mem_type & CL_READ_ONLY_CACHE)
      std::cout << "CL_READ_ONLY_CACHE";
   if (mem_type & CL_NONE)
      std::cout << "CL_NONE";
   std::cout << "\n";

   cl_ulong constant_size;
   CHECK_CL_ERROR(clGetDeviceInfo(p_id, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, sizeof(cl_ulong), &constant_size, NULL), "Consant size");
   std::cout << "Constant buffer size " << (constant_size / (1024)) << " KB\n";
   CHECK_CL_ERROR(clGetDeviceInfo(p_id, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(cl_uint), &line_size, NULL), "Max frequency");
   std::cout << "Max frequencey " << line_size << "\n";
   CHECK_CL_ERROR(clGetDeviceInfo(p_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &line_size, NULL), "Max compute units");
   std::cout << "Max Compute units " << line_size << "\n";
   CHECK_CL_ERROR(clGetDeviceInfo(p_id, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(cl_ulong), &mem_cache_size, NULL), "Max mem allocation size");
   std::cout << "Max mem allocation size " << mem_cache_size << "\n";
   size_t temp;
   CHECK_CL_ERROR(clGetDeviceInfo(p_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &temp, NULL), "Max work-group size");
   std::cout << "Max work-group size " << temp << "\n";
   CHECK_CL_ERROR(clGetDeviceInfo(p_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint), &line_size, NULL), "Max work-item dimensions");
   std::cout << "Max work-item dimension " << line_size << "\n";
   size_t arr[10];
   CHECK_CL_ERROR(clGetDeviceInfo(p_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(arr), arr, NULL), "Max work-item size");
   std::cout << "Max work item sizes ";
   for (int i = 0; i < 10; ++i)
      std::cout << " " << arr[i] << ", ";
   std::cout << "\n";

}
std::string name() const {
//   check_device(id);
   char string_holder[4 * 256];
   CHECK_CL_ERROR(clGetDeviceInfo(_id, CL_DEVICE_NAME, 1024, string_holder, NULL), "clGetDeviceInfo-1");
   return std::string(string_holder);
}
static void print_info(const cl_device_id _device_id) {
   char string_holder[4 * 256];
   CHECK_CL_ERROR(clGetDeviceInfo(_device_id, CL_DEVICE_NAME, 1024, string_holder, NULL), "clGetDeviceInfo-1");
   char cl_version[256];
   CHECK_CL_ERROR(clGetDeviceInfo(_device_id, CL_DEVICE_VERSION, 256, cl_version, NULL), "clGetDeviceInfo-2");
   char cl_c_version[256];
   CHECK_CL_ERROR(clGetDeviceInfo(_device_id, CL_DEVICE_OPENCL_C_VERSION, 256, cl_c_version, NULL), "clGetDeviceInfo-3");
   cl_bool use_unified_mem;
   CHECK_CL_ERROR(clGetDeviceInfo(_device_id, CL_DEVICE_HOST_UNIFIED_MEMORY, sizeof(cl_bool), &use_unified_mem, NULL), "clGetDeviceInfo-4");
   cl_uint freq;
   CHECK_CL_ERROR(clGetDeviceInfo(_device_id, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(cl_uint), &freq, NULL), "Max frequency");
   size_t max_wg_size;
   CHECK_CL_ERROR(clGetDeviceInfo(_device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &max_wg_size, NULL), "clGetDeviceInfo");
   cl_uint num_eus;
   CHECK_CL_ERROR(clGetDeviceInfo(_device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &num_eus, NULL), "clGetDeviceInfo");
   cl_ulong global_mem_size;
   CHECK_CL_ERROR(clGetDeviceInfo(_device_id, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), &global_mem_size, NULL), "Global mem size");
   cl_ulong max_alloc_size;
   CHECK_CL_ERROR(clGetDeviceInfo(_device_id, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(cl_ulong), &max_alloc_size, NULL), "Global mem size");
   cl_uint mem_align_size;
   CHECK_CL_ERROR(clGetDeviceInfo(_device_id, CL_DEVICE_MEM_BASE_ADDR_ALIGN, sizeof(cl_uint), &mem_align_size, NULL), "Mem alignment(bits)");
   cl_uint mem_address_bits;
   CHECK_CL_ERROR(clGetDeviceInfo(_device_id, CL_DEVICE_ADDRESS_BITS, sizeof(cl_uint), &mem_address_bits, NULL), "Mem address(bits)");
   std::cerr << "" << string_holder << " @" << freq << " Hz, [" << num_eus << " EUs, (max-workgroup=" << max_wg_size << ")" << toMB(global_mem_size) << "MB (Max-"
         << toMB(max_alloc_size) << " MB " << mem_align_size << "-bit aligned) Address:" << mem_address_bits << "-bits] (CL::" << cl_version << "CL_CC:: " << cl_c_version << ")";
#if 0//ifdef CL_VERSION_1_2 //Device affinity only supported in opencl 1.2
   cl_device_affinity_domain affinity;
   CHECK_CL_ERROR(clGetDeviceInfo(_device_id, CL_DEVICE_PARTITION_AFFINITY_DOMAIN, sizeof(cl_device_affinity_domain), &affinity, NULL), "Global mem size");
   std::cout<<" :: Affinity ::"<< affinity;
#endif
   std::cerr << ", Unified Mem:" << (use_unified_mem ? "Y" : "N") << "\n";
}
static cl_uint get_device_eu(cl_device_id dev) {
   cl_uint num_eus;
   CHECK_CL_ERROR(clGetDeviceInfo(dev, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &num_eus, NULL), "clGetDeviceInfo");
   return num_eus;
}
static cl_uint get_device_threads(cl_device_id dev) {
   cl_uint num_eus;
   CHECK_CL_ERROR(clGetDeviceInfo(dev, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &num_eus, NULL), "clGetDeviceInfo");
   size_t max_wg_size;
   CHECK_CL_ERROR(clGetDeviceInfo(dev, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &max_wg_size, NULL), "clGetDeviceInfo");
   return num_eus * max_wg_size;
}
static cl_ulong get_device_memory(cl_device_id dev) {
   cl_ulong ret;
   CHECK_CL_ERROR(clGetDeviceInfo(dev, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), &ret, NULL), "Global mem size");
   return ret;
}
static cl_ulong get_device_shared_memory(cl_device_id dev) {
   cl_ulong ret;
   CHECK_CL_ERROR(clGetDeviceInfo(dev, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(cl_ulong), &ret, NULL), "Local mem size");
   return ret;
}

static cl_ulong get_max_allocation_size(cl_device_id dev) {
   cl_ulong ret;
   CHECK_CL_ERROR(clGetDeviceInfo(dev, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(cl_ulong), &ret, NULL), "Global mem size");
   return ret;
}

};
}
}



#endif /* GALOISGPU_OCL_CL_DEVICE_H_ */
