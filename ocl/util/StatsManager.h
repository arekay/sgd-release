/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
/*
 * StatsManager.h
 *
 *  Created on: Sep 8, 2014
 *      Author: rashid
 */

#include <vector>
#ifndef STATSMANAGER_H_
#define STATSMANAGER_H_
namespace Galois {
struct StatsManager {
   typedef std::pair<std::string, float> StatType;
   typedef std::vector<StatType> RoundStatList;
   typedef std::vector<RoundStatList*> AllStats;
   AllStats stats_list;
   int active_round;
   StatsManager() {
      stats_list.push_back(new RoundStatList());
      active_round = 0;
   }
   void add_stats(std::string header, float val) {
      stats_list[active_round]->push_back(StatType(header, val));
   }
   void end_round() {
      stats_list.push_back(new RoundStatList());
      active_round++;
   }
   void print() {
      float  * summary = new float[stats_list[0]->size()];
      fprintf(stderr, "===============STATS=================\n");
      int idx = 0;
      for (auto v : *stats_list[0]) {
         fprintf(stderr, "%s, ", v.first.c_str());
         summary[idx++]=0;
      }
      fprintf(stderr, "\n");
      for (int i = 0; i < active_round; ++i) {
         idx=0;
         for (auto v : *stats_list[i]) {
            fprintf(stderr, "%6.6g, ", v.second);
            summary[idx++]+=v.second;
         }
         fprintf(stderr, "\n");
      }
      fprintf(stderr, "AVG, ");
      for(size_t i=1; i<stats_list[0]->size(); ++i ){
         fprintf(stderr, "%6.6g, ",summary[i]/active_round);
      }
      fprintf(stderr, "\n");
      fprintf(stderr, "===============END-STATS=================\n");
   }
   void clear(){
      for (int i =0; i<active_round; ++i){
         delete stats_list[i];
      }
      stats_list.clear();
   }
   ~StatsManager(){
      clear();
   }
};
}
;
//namespace Galois
#endif /* STATSMANAGER_H_ */
