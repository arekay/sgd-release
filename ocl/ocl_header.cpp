/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#include "../ocl/ocl_header.h"
//#include "../exp/Util.h"
namespace Galois {
    namespace OpenCL {
        /*Last event. Should remove.*/
        cl_event OpenCL_Setup::event=nullptr;
        /*Global context*/
        cl_context OpenCL_Setup::context=nullptr;               // context
        /*Single command queue*/
        cl_command_queue OpenCL_Setup::queue=nullptr;           // command queue
        /*program to be compiled/executed*/
        cl_program OpenCL_Setup::program=nullptr;
        /*Has the runtime already been initialized*/
        bool OpenCL_Setup::initialized=false;
        /*Track memory usage */
        DeviceStats OpenCL_Setup::stats;
        /*The default device/platform is changed here.*/
        int  OpenCL_Setup::GOPT_CL_DEFAULT_PLATFORM_ID=1;
        int  OpenCL_Setup::GOPT_CL_DEFAULT_DEVICE_ID=0;
        std::vector<CL_Platform*>  OpenCL_Setup::platforms;
        std::string OpenCL_Setup::build_args;
    }
    
}
