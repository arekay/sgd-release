/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#include <boost/iterator/counting_iterator.hpp>
#include <boost/iterator/transform_iterator.hpp>
#include <boost/type_traits/is_pod.hpp>
#include <boost/utility.hpp>

#ifndef FILEGRAPH_H_
#define FILEGRAPH_H_

/////////////////////////////
//! Graph serialized to a file
class FileGraph{
public:
  typedef uint32_t GraphNode;

protected:
  void* volatile masterMapping;
  size_t masterLength;
  uint64_t sizeEdgeTy;
  int masterFD;

  uint64_t* outIdx;
  uint32_t* outs;

  char* edgeData;

  uint64_t numEdges;
  uint64_t numNodes;

  uint64_t getEdgeIdx(GraphNode src, GraphNode dst) const;
  uint32_t* raw_neighbor_begin(GraphNode N) const;
  uint32_t* raw_neighbor_end(GraphNode N) const;

  struct Convert32: public std::unary_function<uint32_t, uint32_t> {
    uint32_t operator()(uint32_t x) const {
      return le32toh(x);
    }
  };

  struct Convert64: public std::unary_function<uint64_t,uint64_t> {
    uint64_t operator()(uint64_t x) const {
      return le64toh(x);
    }
  };

  //! Initialize graph from block of memory
  void parse(void* m);

  //! Read graph connectivity information from memory
  void structureFromMem(void* mem, size_t len, bool clone);

  void* structureFromArrays(uint64_t* outIdxs, uint64_t numNodes,
      uint32_t* outs, uint64_t numEdges, size_t sizeofEdgeData);

  void* structureFromGraph(FileGraph& g, size_t sizeofEdgeData);


public:
  // Node Handling

  //! Check if a node is in the graph (already added)
  bool containsNode(const GraphNode n) const;

  // Edge Handling
  template<typename EdgeTy>
  EdgeTy& getEdgeData(GraphNode src, GraphNode dst) {
    assert(sizeEdgeTy == sizeof(EdgeTy));
    return reinterpret_cast<EdgeTy*>(edgeData)[getEdgeIdx(src, dst)];
  }

  // Iterators
  typedef boost::counting_iterator<uint64_t> edge_iterator;
  edge_iterator edge_begin(GraphNode N) const;
  edge_iterator edge_end(GraphNode N) const;

  EdgesWithNoFlagIterator<FileGraph> out_edges(GraphNode N) {
    return EdgesWithNoFlagIterator<FileGraph>(*this, N);
  }

//  /**
//   * Sorts outgoing edges of a node. Comparison function is over EdgeTy.
//   */
//  template<typename EdgeTy, typename CompTy>
//  void sortEdgesByEdgeData(GraphNode N, const CompTy& comp = std::less<EdgeTy>()) {
//    typedef LargeArrayWrapper<GraphNode> EdgeDst;
//    typedef LargeArrayWrapper<EdgeTy> EdgeData;
//    typedef EdgeSortIterator<GraphNode,uint64_t,EdgeDst,EdgeData> edge_sort_iterator;
//
//    EdgeDst edgeDst(outs, numEdges);
//    EdgeData ed(edgeData, numEdges);
//
//    edge_sort_iterator begin(std::distance(outs, raw_neighbor_begin(N)), &edgeDst, &ed);
//    edge_sort_iterator end(std::distance(outs, raw_neighbor_end(N)), &edgeDst, &ed);
//
//    std::sort(begin, end, EdgeSortCompWrapper<EdgeSortValue<GraphNode,EdgeTy>,CompTy>(comp));
//  }
//
//  /**
//   * Sorts outgoing edges of a node. Comparison function is over <code>EdgeSortValue<EdgeTy></code>.
//   */
//  template<typename EdgeTy, typename CompTy>
//  void sortEdges(GraphNode N, const CompTy& comp) {
//    typedef LargeArrayWrapper<GraphNode> EdgeDst;
//    typedef LargeArrayWrapper<EdgeTy> EdgeData;
//    typedef EdgeSortIterator<GraphNode,uint64_t,EdgeDst,EdgeData> edge_sort_iterator;
//
//    EdgeDst edgeDst(outs, numEdges);
//    EdgeData ed(edgeData, numEdges);
//
//    edge_sort_iterator begin(std::distance(outs, raw_neighbor_begin(N)), &edgeDst, &ed);
//    edge_sort_iterator end(std::distance(outs, raw_neighbor_end(N)), &edgeDst, &ed);
//
//    std::sort(begin, end, comp);
//  }

  template<typename EdgeTy>
  EdgeTy& getEdgeData(edge_iterator it) const {
    return reinterpret_cast<EdgeTy*>(edgeData)[*it];
  }

  GraphNode getEdgeDst(edge_iterator it) const;

  typedef boost::transform_iterator<Convert32, uint32_t*> neighbor_iterator;
  typedef boost::transform_iterator<Convert32, uint32_t*> node_id_iterator;
  typedef boost::transform_iterator<Convert64, uint64_t*> edge_id_iterator;

  neighbor_iterator neighbor_begin(GraphNode N) const {
    return boost::make_transform_iterator(raw_neighbor_begin(N), Convert32());
  }

  neighbor_iterator neighbor_end(GraphNode N) const {
    return boost::make_transform_iterator(raw_neighbor_end(N), Convert32());
  }

  node_id_iterator node_id_begin() const;
  node_id_iterator node_id_end() const;
  edge_id_iterator edge_id_begin() const;
  edge_id_iterator edge_id_end() const;

  template<typename EdgeTy>
  EdgeTy& getEdgeData(neighbor_iterator it) {
    return reinterpret_cast<EdgeTy*>(edgeData)[std::distance(outs, it.base())];
  }

  template<typename EdgeTy>
  EdgeTy* edge_data_begin() const {
    return reinterpret_cast<EdgeTy*>(edgeData);
  }

  template<typename EdgeTy>
  EdgeTy* edge_data_end() const {
    assert(sizeof(EdgeTy) == sizeEdgeTy);
    EdgeTy* r = reinterpret_cast<EdgeTy*>(edgeData);
    return &r[numEdges];
  }

  bool hasNeighbor(GraphNode N1, GraphNode N2) const;

  typedef boost::counting_iterator<uint64_t> iterator;

  //! Iterate over nodes in graph (not thread safe)
  iterator begin() const;

  iterator end() const;

  //! The number of nodes in the graph
  unsigned int size() const;

  //! The number of edges in the graph
  unsigned int sizeEdges() const;

  //! sizeof an edge
  size_t sizeEdge() const { return sizeEdgeTy; }

  FileGraph();
  ~FileGraph();

  //! Read graph connectivity information from file
  void structureFromFile(const std::string& filename);

  //! Read graph connectivity information from arrays.
  //! Return a pointer to array to populate with edge data.
  template<typename T>
  T* structureFromArrays(uint64_t* outIdxs, uint64_t numNodes,
      uint32_t* outs, uint64_t numEdges) {
    return reinterpret_cast<T*>(structureFromArrays(outIdx, numNodes, outs, numEdges, sizeof(T)));
  }

  //! Read graph connectivity information from arrays.
  //! Return a pointer to array to populate with edge data.
  template<typename T>
  T* structureFromGraph(FileGraph& g) {
    return reinterpret_cast<T*>(structureFromGraph(g, sizeof(T)));
  }

  //! Write graph connectivity information to file
  void structureToFile(const std::string& file);

  void swap(FileGraph& other);
  void cloneFrom(FileGraph& other);
};



FileGraph::FileGraph()
  : masterMapping(0), masterLength(0), masterFD(0),
    outIdx(0), outs(0), edgeData(0),
    numEdges(0), numNodes(0)
{
}

FileGraph::~FileGraph() {
  if (masterMapping)
    munmap(masterMapping, masterLength);
  if (masterFD)
    close(masterFD);
}

//FIXME: perform le -> host on data here too
void FileGraph::parse(void* m) {
  //parse file
  uint64_t* fptr = (uint64_t*)m;
  __attribute__((unused)) uint64_t version = le64toh(*fptr++);
  assert(version == 1);
  sizeEdgeTy = le64toh(*fptr++);
  numNodes = le64toh(*fptr++);
  numEdges = le64toh(*fptr++);
  outIdx = fptr;
  fptr += numNodes;
  uint32_t* fptr32 = (uint32_t*)fptr;
  outs = fptr32;
  fptr32 += numEdges;
  if (numEdges % 2)
    fptr32 += 1;
  edgeData = (char*)fptr32;
}

void FileGraph::structureFromMem(void* mem, size_t len, bool clone) {
  masterLength = len;

  if (clone) {
    int _MAP_BASE = MAP_ANONYMOUS | MAP_PRIVATE;
#ifdef MAP_POPULATE
    _MAP_BASE |= MAP_POPULATE;
#endif

    void* m = mmap(0, masterLength, PROT_READ | PROT_WRITE, _MAP_BASE, -1, 0);
    if (m == MAP_FAILED) {
      fprintf(stderr, "failed copying graph");
    }
    memcpy(m, mem, len);
    parse(m);
    masterMapping = m;
  } else {
    parse(mem);
    masterMapping = mem;
  }
}

void* FileGraph::structureFromGraph(FileGraph& g, size_t sizeof_edge_data) {
  // Allocate
  size_t common = g.masterLength - (g.sizeEdgeTy * g.numEdges);
  size_t len = common + (sizeof_edge_data * g.numEdges);
  int _MAP_BASE = MAP_ANONYMOUS | MAP_PRIVATE;
#ifdef MAP_POPULATE
  _MAP_BASE |= MAP_POPULATE;
#endif
  void* m = mmap(0, len, PROT_READ | PROT_WRITE, _MAP_BASE, -1, 0);
  if (m == MAP_FAILED) {
     fprintf(stderr,  "failed copying graph");
  }
  memcpy(m, g.masterMapping, common);
  uint64_t* fptr = (uint64_t*)m;
  fptr[1] = sizeof_edge_data; // Update sizeof(edgeData)
  parse(m);
  structureFromMem(m, len, false);

  return edgeData;
}

void* FileGraph::structureFromArrays(uint64_t* out_idx, uint64_t num_nodes,
      uint32_t* outs, uint64_t num_edges, size_t sizeof_edge_data) {
  //version
  uint64_t version = 1;
  uint64_t nBytes = sizeof(uint64_t) * 4; // version, sizeof_edge_data, numNodes, numEdges

  nBytes += sizeof(uint64_t) * num_nodes;
  nBytes += sizeof(uint32_t) * num_edges;
  if (num_edges % 2) {
    nBytes += sizeof(uint32_t); // padding
  }
  nBytes += sizeof_edge_data * num_edges;

  int _MAP_BASE = MAP_ANONYMOUS | MAP_PRIVATE;
#ifdef MAP_POPULATE
  _MAP_BASE |= MAP_POPULATE;
#endif

  char* t = (char*) mmap(0, nBytes, PROT_READ | PROT_WRITE, _MAP_BASE, -1, 0);
  if (t == MAP_FAILED) {
    t = 0;
    fprintf(stderr,  "failed allocating graph");
  }
  char* base = t;
  memcpy(t, &version, sizeof(version));
  t += sizeof(version);
  memcpy(t, &sizeof_edge_data, sizeof(sizeof_edge_data));
  t += sizeof(sizeof_edge_data);
  memcpy(t, &num_nodes, sizeof(num_nodes));
  t += sizeof(num_nodes);
  memcpy(t, &num_edges, sizeof(num_edges));
  t += sizeof(num_edges);
  memcpy(t, out_idx, sizeof(*out_idx) * num_nodes);
  t += sizeof(*out_idx) * num_nodes;
  memcpy(t, outs, sizeof(*outs) * num_edges);
  if (num_edges % 2) {
    t += sizeof(uint32_t); // padding
  }

  structureFromMem(base, nBytes, false);
  return edgeData;
}

void FileGraph::structureFromFile(const std::string& filename) {
  masterFD = open(filename.c_str(), O_RDONLY);
  if (masterFD == -1) {
     fprintf(stderr, "failed opening %s", filename.c_str());
  }

  struct stat buf;
  int f = fstat(masterFD, &buf);
  if (f == -1) {
     fprintf(stderr, "failed reading %s", filename.c_str());
  }
  masterLength = buf.st_size;

  int _MAP_BASE = MAP_PRIVATE;
#ifdef MAP_POPULATE
  _MAP_BASE |= MAP_POPULATE;
#endif

  void* m = mmap(0, masterLength, PROT_READ, _MAP_BASE, masterFD, 0);
  if (m == MAP_FAILED) {
    m = 0;
    fprintf(stderr, "failed reading %s", filename.c_str());
  }
  parse(m);
  masterMapping = m;
}

//FIXME: perform host -> le on data
void FileGraph::structureToFile(const std::string& file) {
  ssize_t retval;
  //ASSUME LE machine
  mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
  int fd = open(file.c_str(), O_WRONLY | O_CREAT | O_TRUNC, mode);
  size_t total = masterLength;
  char* ptr = (char*) masterMapping;
  while (total) {
    retval = write(fd, ptr, total);
    if (retval == -1) {
      fprintf(stderr,  "failed writing to %s", file.c_str());
    } else if (retval == 0) {
      fprintf(stderr,  "ran out of space writing to %s", file.c_str());
    }
    total -= retval;
    ptr += retval;
  }
  close(fd);
}

void FileGraph::swap(FileGraph& other) {
  std::swap(masterMapping, other.masterMapping);
  std::swap(masterLength, other.masterLength);
  std::swap(sizeEdgeTy, other.sizeEdgeTy);
  std::swap(masterFD, other.masterFD);
  std::swap(outIdx, other.outIdx);
  std::swap(outs, other.outs);
  std::swap(edgeData, other.edgeData);
  std::swap(numEdges, other.numEdges);
  std::swap(numNodes, other.numNodes);
}

void FileGraph::cloneFrom(FileGraph& other) {
  structureFromMem(other.masterMapping, other.masterLength, true);
}

uint64_t FileGraph::getEdgeIdx(GraphNode src, GraphNode dst) const {
  for (uint32_t* ii = raw_neighbor_begin(src),
     *ee = raw_neighbor_end(src); ii != ee; ++ii)
    if (le32toh(*ii) == dst)
      return std::distance(outs, ii);
  return ~static_cast<uint64_t>(0);
}

uint32_t* FileGraph::raw_neighbor_begin(GraphNode N) const {
  return (N == 0) ? &outs[0] : &outs[le64toh(outIdx[N-1])];
}

uint32_t* FileGraph::raw_neighbor_end(GraphNode N) const {
  return &outs[le64toh(outIdx[N])];
}

FileGraph::edge_iterator FileGraph::edge_begin(GraphNode N) const {
  return edge_iterator(N == 0 ? 0 : le64toh(outIdx[N-1]));
}
FileGraph::edge_iterator FileGraph::edge_end(GraphNode N) const {
  return edge_iterator(le64toh(outIdx[N]));
}

FileGraph::GraphNode FileGraph::getEdgeDst(edge_iterator it) const {
  return le32toh(outs[*it]);
}

FileGraph::node_id_iterator FileGraph::node_id_begin() const {
  return boost::make_transform_iterator(&outs[0], Convert32());
}

FileGraph::node_id_iterator FileGraph::node_id_end() const {
  return boost::make_transform_iterator(&outs[numEdges], Convert32());
}

FileGraph::edge_id_iterator FileGraph::edge_id_begin() const {
  return boost::make_transform_iterator(&outIdx[0], Convert64());
}

FileGraph::edge_id_iterator FileGraph::edge_id_end() const {
  return boost::make_transform_iterator(&outIdx[numNodes], Convert64());
}

bool FileGraph::hasNeighbor(GraphNode N1, GraphNode N2) const {
  return getEdgeIdx(N1,N2) != ~static_cast<uint64_t>(0);
}

FileGraph::iterator FileGraph::begin() const {
  return iterator(0);
}

FileGraph::iterator FileGraph::end() const {
  return iterator(numNodes);
}

unsigned int FileGraph::size() const {
  return numNodes;
}

unsigned int FileGraph::sizeEdges() const {
  return numEdges;
}

bool FileGraph::containsNode(const GraphNode n) const {
  return n < numNodes;
}


#endif /* FILEGRAPH_H_ */
