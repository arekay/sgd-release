/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#include <iostream>
#include <fstream>
#include <cstring>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include <limits>
#include "common_header.h"

#ifndef LC_GRAPH2_H_
#define LC_GRAPH2_H_

namespace GOpt {
template<template<typename > class GPUWrapper, typename NodeDataTy, typename EdgeDataTy>
struct LC_Graph2 {
   template<typename T> using ArrayType = GPUWrapper<T>;
//   typedef GPUWrapper<unsigned int> ArrayType;
   typedef GPUWrapper<unsigned int> GPUType;
   typedef typename GPUWrapper<unsigned int>::HostPtrType HostPtrType;
   typedef typename GPUWrapper<unsigned int>::DevicePtrType DevicePtrType;
   typedef NodeDataTy NodeDataType;
   typedef EdgeDataTy EdgeDataType;
   size_t num_nodes;
   size_t num_edges;
   const size_t SizeEdgeData;
   const size_t SizeNodeData;
   GPUType * gpu_graph;
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   LC_Graph2():SizeEdgeData(sizeof(EdgeDataType)/sizeof(unsigned int)),SizeNodeData(sizeof(NodeDataType)/sizeof(unsigned int)){
      num_nodes = num_edges = 0;
      gpu_graph =0;
   }
   NodeDataType * node_data() {
      return (NodeDataType*)gpu_graph->host_data + 4;
   }
   unsigned int * outgoing_index() {
      return gpu_graph->host_data + 4 + num_nodes*SizeNodeData;
   }
   unsigned int * neighbors() {
      return gpu_graph->host_data + 4 + num_nodes*SizeNodeData + num_nodes + 1;
   }
   EdgeDataType * edge_data() {
      return (EdgeDataType *)(gpu_graph->host_data + 4 + num_nodes*SizeNodeData + num_nodes + 1 + num_edges);
   }
   unsigned int * last() {
      return gpu_graph->host_data + 4 + num_nodes*SizeNodeData + num_nodes + 1 + num_edges + num_edges*SizeEdgeData;
   }
   GPUType * get_array_ptr(void){
      return gpu_graph;
   }
   void init(size_t n_n, size_t n_e) {
      num_nodes = n_n;
      num_edges = n_e;
      std::cout << "Allocating NN: " << num_nodes << " , NE :" << num_edges << "\n";
      //Num_nodes, num_edges, [node_data] , [outgoing_index], [neighbors], [edge_data]
      gpu_graph = new GPUType(4 + num_nodes*SizeNodeData + num_nodes + 1 + num_edges + num_edges*SizeEdgeData);
      (*gpu_graph)[0] = num_nodes;
      (*gpu_graph)[1] = num_edges;
      (*gpu_graph)[2] = SizeNodeData;
      (*gpu_graph)[3] = SizeEdgeData;
      //allocate_on_gpu();
   }
   /////////////////////////////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////////////////////
   void copy_to_device(void) {
      gpu_graph->copy_to_device();
   }
   /////////////////////////////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////////////////////
   void copy_to_host(void) {
      gpu_graph->copy_to_host();
   }
   DevicePtrType & device_ptr() {
      return gpu_graph->device_ptr();
   }
   HostPtrType & host_ptr(void) {
       return gpu_graph->host_ptr();
   }
   void print_header(void){
      std::cout << "Header :: [";
      for(unsigned int i=0; i<6; ++i){
         std::cout<<gpu_graph->operator[](i) << ",";
      }
      std::cout<<"\n";
      return;
   }
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void print_node(unsigned int idx, const char * post = "") {
      if (idx < num_nodes) {
         std::cout << "N-" << idx << "(" << (node_data())[idx] << ")" << " :: [";
         for (size_t i = (outgoing_index())[idx]; i < (outgoing_index())[idx + 1]; ++i) {
            std::cout << " " << (neighbors())[i] << "(" << (edge_data())[i] << "), ";
         }
         std::cout << "]"<<post;
      }
      return;
   }
   /////////////////////////////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////////////////////

   void allocate_on_gpu() {
      return;
   }

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void print_graph(void) {
      std::cout << "\n====Printing graph (" << num_nodes << " , " << num_edges << ")=====\n";
      for (size_t i = 0; i < num_nodes; ++i) {
         print_node(i);
         std::cout << "\n";
      }
      return;
   }
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void print_compact(void) {
      std::cout << "\nOut-index [";
      for (size_t i = 0; i < num_nodes + 1; ++i) {
         std::cout << " " << outgoing_index()[i] << ",";
      }
      std::cout << "]\nNeigh[";
      for (size_t i = 0; i < num_edges; ++i) {
         std::cout << " " << neighbors()[i] << ",";
      }
      std::cout << "]\nEData [";
      for (size_t i = 0; i < num_edges; ++i) {
         std::cout << " " << edge_data()[i] << ",";
      }
      std::cout << "]";
   }
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void read_graph(const char * filename) {
      std::cout<<"LC_Graph2 :: Reading .gr file " << filename << "\n";
      std::ifstream in_file;
      in_file.open(filename);
      std::string buffer;
      std::pair<int, int> e_buffer;
      getline(in_file, buffer);
      while (DIMACS_GR_Challenge9_Format<unsigned int, unsigned int>::is_comment(buffer)) {
         getline(in_file, buffer);
      }
      e_buffer = DIMACS_GR_Challenge9_Format<unsigned int, unsigned int>::parse_header(buffer);
      //Read in number of nodes and vertices.
      std::cout << "Read header line " << buffer << " \n";
      //Undirected edges = twice directed edges!
      // Now the tricky part. We need to accumulate the edge-count for each
      // node, and then use those in a sorted manner to palce edges in the
      // neighbors and edge_data array.
      typedef std::map<unsigned int, unsigned int> EdgeListBuffer;
      typedef std::map<unsigned int, EdgeListBuffer *> GraphBuffer;
      GraphBuffer all_edges;
      size_t num_nodes_l = e_buffer.first + 1, num_edges_l = 0;
      std::cout << "Number of nodes " << num_nodes_l << "\n";
      for (size_t i = 0; i < num_nodes_l; ++i) {
         all_edges[i] = new EdgeListBuffer();
      }
      std::pair<unsigned int, std::pair<unsigned int, unsigned int> > e_vec_buffer;
      do {
         getline(in_file, buffer);
         if (buffer.size() > 0 && DIMACS_GR_Challenge9_Format<unsigned int, unsigned int>::is_comment(buffer) == false) {
            e_vec_buffer = DIMACS_GR_Challenge9_Format<unsigned int, unsigned int>::parse_edge_pair(buffer);
            (*all_edges[e_vec_buffer.first])[e_vec_buffer.second.first] = e_vec_buffer.second.second;
            (*all_edges[e_vec_buffer.second.first])[e_vec_buffer.first] = e_vec_buffer.second.second;
            num_edges_l += 2;
         }
      } while (in_file.good());
      num_edges_l = 0;
      for (GraphBuffer::iterator it = all_edges.begin(); it != all_edges.end(); ++it) {
         num_edges_l += it->second->size();
      }
      ////Now we are going to populate the edges.
      std::cout << "Number of edges" << num_edges_l << "\n";
      init(num_nodes_l, num_edges_l);
      size_t curr_edge_count = 0;
      size_t l_max_degree = 0;
      for (size_t i = 0; i < num_nodes; ++i) {
         outgoing_index()[(i)] = curr_edge_count;
         node_data()[(i)] = (std::numeric_limits<unsigned int>::max()) / 2;
         if (all_edges[i]->size() > l_max_degree)
            l_max_degree = all_edges[i]->size();
         for (EdgeListBuffer::iterator it = all_edges[i]->begin(); it != all_edges[i]->end(); ++it) {
            neighbors()[curr_edge_count] = it->first;
            edge_data()[curr_edge_count] = it->second;
            ++curr_edge_count;
         }
      }
      std::cout << "Max degree enountered is " << l_max_degree << "\n";
      outgoing_index()[num_nodes] = num_edges;
      //assert(num_edges == curr_edge_count);
      ///Clean-up : VERY VERY SLOW!!!!
      for (size_t i = 0; i < num_nodes; ++i) {
         delete all_edges[i];
      }
   }
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void deallocate(void) {
      delete gpu_graph;
   }
};
//End LC_Graph
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
}//End namespace GOpt
#endif /* LC_GRAPH2_H_ */
