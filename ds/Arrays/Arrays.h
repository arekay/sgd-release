/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef ARRAY_WRAPPER_H_
#define ARRAY_WRAPPER_H_
namespace Galois {
namespace OpenCL {

/////////////////////////////////////////////////////////////////
/*******************************************************************************
 *
 ********************************************************************************/
enum DeviceMemoryType {
   DISCRETE, HOST_CACHED, PINNED, CONSTANT
};
static inline void ReportCopyToDevice(CL_Device * dev, size_t sz, cl_int err = CL_SUCCESS) {
   Galois::OpenCL::OpenCL_Setup::stats.copied_to_device += sz;
   dev->stats().copied_to_device += sz;
   (void) err;
DEBUG_CODE(
      if(err!=CL_SUCCESS)fprintf(stderr, "Failed copy to device [ %d bytes ]!\n", sz);
      else fprintf(stderr, "Did copy to device[ %d bytes ] !\n", sz);)
}
/////////////////////////////////
static inline void ReportCopyToHost(CL_Device * dev, size_t sz, cl_int err = CL_SUCCESS) {
dev->stats().copied_to_host += sz;
Galois::OpenCL::OpenCL_Setup::stats.copied_to_host += sz;
(void) err;
DEBUG_CODE(
   if(err!=CL_SUCCESS)fprintf(stderr, "Failed copy to host [ %d bytes ]!\n", sz);
   else fprintf(stderr, "Did copy to device[ %d host ] !\n", sz);)
}
/////////////////////////////////////
static inline void ReportDataAllocation(CL_Device * dev, size_t sz, cl_int err = CL_SUCCESS) {
dev->stats().allocated += sz;
dev->stats().max_allocated = std::max(dev->stats().max_allocated, dev->stats().allocated);
Galois::OpenCL::OpenCL_Setup::stats.allocated += sz;
Galois::OpenCL::OpenCL_Setup::stats.max_allocated = std::max(Galois::OpenCL::OpenCL_Setup::stats.max_allocated, Galois::OpenCL::OpenCL_Setup::stats.allocated);
Galois::OpenCL::CHECK_CL_ERROR(err, "Data allocation/deallocation failed.");
DEBUG_CODE(
   fprintf(stderr, "Allocating array %6.6g MB on device-%d (%s), total(system) %6.6g\n", (sz / (float) (1024 * 1024)), dev->id(), dev->name().c_str(),(Galois::OpenCL::OpenCL_Setup::stats.allocated / (float) (1024 * 1024)));
   dev->stats().print_long();
)
}

}      //namespace OpenCL

/*******************************************************************************
 *
 ********************************************************************************/
}      //namespace Galois
#include "ArrayImpl.h"
#include "MultiDeviceArray.h"
#include "CPUArray.h"
#include "GPUArray.h"
#include "OnDemandArray.h"
#endif /* ARRAY_WRAPPER_H_ */
