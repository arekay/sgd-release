/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef GALOISGPU_OCL_CPUARRAY_H_
#define GALOISGPU_OCL_CPUARRAY_H_

namespace Galois{
/*******************************************************************************
 *
 ********************************************************************************/
template<typename T>
struct CPUArray {
   typedef cl_mem DevicePtrType;
   typedef T * HostPtrType;
   explicit CPUArray(size_t sz, Galois::OpenCL::CL_Device * d=nullptr) :
            num_elements(sz) {
      (void)d;
         host_data = new T[num_elements];
      }
      ////////////////////////////////////////////////
   void copy_to_device() {
   }
   ////////////////////////////////////////////////
   void copy_to_host() {
   }
   ////////////////////////////////////////////////
   void init_on_device(const T & val) {
      (void) val;
   }
   ////////////////////////////////////////////////
   size_t size() {
      return num_elements;
   }
   ////////////////////////////////////////////////
   operator T*() {
      return host_data;
   }
   T & operator [](size_t idx) {
      return host_data[idx];
   }
   DevicePtrType device_ptr(void) {
      return (DevicePtrType) 0;
   }
   HostPtrType & host_ptr(void) {
      return host_data;
   }
   CPUArray<T> * get_array_ptr(void) {
      return this;
   }
   ~CPUArray<T>() {
      if (host_data)
         delete[] host_data;
   }
   HostPtrType host_data;
   size_t num_elements;
protected:
};
}//end namespace Galois



#endif /* GALOISGPU_OCL_CPUARRAY_H_ */
