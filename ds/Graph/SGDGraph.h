/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
/*
 * LC_LinearArray_Undirected_Graph.h
 *
 *  Created on: Oct 24, 2013
 *  Single array representation, has outgoing edges.
 *      Author: rashid
 */

#include <iostream>
#include <fstream>
#include <cstring>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include <limits>
#include <math.h>

#ifndef _SGD_LC_LinearArray_Undirected_Graph_H_
#define _SGD_LC_LinearArray_Undirected_Graph_H_

namespace Galois {
namespace OpenCL {
static const char * str_SGD_LC_LinearArray_Undirected_Graph = "typedef struct _GraphType { \n"
      "uint _num_nodes;\n"
      "uint _num_edges;\n "
      "uint node_data_size;\n "
      "uint edge_data_size;\n "
      "__global NodeData *node_data;\n "
      "__global uint *out_index;\n "
      "__global uint *out_neighbors;\n "
      "__global EdgeData *out_edge_data;\n "
      "__global uint * edge_src; }GraphType;\n"
      "uint in_neighbors_begin(__local GraphType * graph, uint node){ \n return 0;\n}\n"
      "uint in_neighbors_end(__local GraphType * graph, uint node){ \n return graph->out_index[node+1]-graph->out_index[node];\n}\n"
      "uint in_neighbors_next(__local GraphType * graph, uint node){ \n return 1;\n}\n"
      "uint in_neighbors(__local GraphType * graph, uint node, uint nbr){ \n return graph->out_neighbors[graph->out_index[node]+nbr];\n}\n"
      "__global EdgeData * in_edge_data(__local GraphType * graph, uint node, uint nbr){ \n return &graph->out_edge_data[graph->out_index[node]+nbr];\n}\n"
      "uint out_neighbors_begin(__local GraphType * graph, uint node){ \n return 0;\n}\n"
      "uint out_neighbors_end(__local GraphType * graph, uint node){ \n return graph->out_index[node+1]-graph->out_index[node];\n}\n"
      "uint out_neighbors_next(__local GraphType * graph, uint node){ \n return 1;\n}\n"
      "uint out_neighbors(__local GraphType * graph,uint node,  uint nbr){ \n return graph->out_neighbors[graph->out_index[node]+nbr];\n}\n"
      "__global EdgeData * out_edge_data(__local GraphType * graph,uint node,  uint nbr){ \n return &graph->out_edge_data[graph->out_index[node]+nbr];\n}\n"
      "__global NodeData * node_data(__local GraphType * graph, uint node){ \n return &graph->node_data[node];\n}\n"
      ///////////////////////////////////////////////////
      "void initialize(__local GraphType * graph, __global uint *mem_pool){\nuint offset =4;\n graph->_num_nodes=mem_pool[0];\n"
      "graph->_num_edges=mem_pool[1];\n graph->node_data_size =mem_pool[2];\n graph->edge_data_size=mem_pool[3];\n"
      "graph->node_data= (__global NodeData *)&mem_pool[offset];\noffset +=graph->_num_nodes* graph->node_data_size;\n"
      "graph->out_index=&mem_pool[offset];\n offset +=graph->_num_nodes + 1;\n graph->out_neighbors=&mem_pool[offset];\n"
      "offset +=graph->_num_edges;\n graph->out_edge_data=(__global EdgeData*)&mem_pool[offset];\n offset +=graph->_num_edges*graph->edge_data_size;\n"
      "graph->edge_src = &mem_pool[offset]; offset+=graph->_num_edges;}\n";

template<template<typename > class GPUWrapper, typename NodeDataTy, typename EdgeDataTy>
struct SGD_LC_LinearArray_Undirected_Graph {
   //Are you using gcc/4.7+ Error on line below for earlier versions.
#ifdef _WIN32
   typedef GPUWrapper<unsigned int> GPUType;
   typedef typename GPUWrapper<unsigned int>::HostPtrType HostPtrType;
   typedef typename GPUWrapper<unsigned int>::DevicePtrType DevicePtrType;
#else
   template<typename T> using ArrayType = GPUWrapper<T>;
   //   typedef GPUWrapper<unsigned int> ArrayType;
   typedef GPUWrapper<unsigned int> GPUType;
   typedef typename GPUWrapper<unsigned int>::HostPtrType HostPtrType;
   typedef typename GPUWrapper<unsigned int>::DevicePtrType DevicePtrType;
#endif   
   typedef NodeDataTy NodeDataType;
   typedef EdgeDataTy EdgeDataType;
   typedef unsigned int NodeIDType;
   typedef unsigned int EdgeIDType;
   size_t _num_nodes;
   size_t _num_edges;
   unsigned int _max_degree;
   unsigned int _max_degree_node;
   const size_t SizeEdgeData;
   const size_t SizeNodeData;
   GPUType * gpu_graph;
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   SGD_LC_LinearArray_Undirected_Graph() :
         SizeEdgeData(sizeof(EdgeDataType) / sizeof(unsigned int)), SizeNodeData(sizeof(NodeDataType) / sizeof(unsigned int)) {
      DEBUG_CODE(std::cout << "Created LC_LinearArray_Graph with " << SizeNodeData << " node " << SizeEdgeData << " edge data. ";);
      _max_degree = _max_degree_node = _num_nodes = _num_edges = 0;
      gpu_graph = 0;
   }
   void read(const char * filename) {
      readFromGR(*this, filename);
      for (unsigned int i = 0; i < num_nodes(); ++i) {
         for (unsigned int e = outgoing_index()[i]; e < outgoing_index()[i + 1]; ++e) {
            get_edge_src()[e] = i;
         }
      }
   }
   NodeDataType * node_data() {
      return (NodeDataType*) gpu_graph->host_data + 4;
   }
   const NodeDataType * node_data() const {
      return (NodeDataType*) gpu_graph->host_data + 4;
   }
   unsigned int * outgoing_index() {
      return (unsigned int*) (node_data()) + _num_nodes * SizeNodeData;
   }
   const unsigned int * outgoing_index() const {
      return (unsigned int*) (node_data()) + _num_nodes * SizeNodeData;
   }

   unsigned int outgoing_index(const int idx) const {
      return ((unsigned int*) (gpu_graph->host_data + 4) + _num_nodes * SizeNodeData)[idx];
   }
   unsigned int * out_neighbors() {
      return (unsigned int *) outgoing_index() + _num_nodes + 1;
   }
   const unsigned int * out_neighbors() const {
      return (unsigned int *) outgoing_index() + _num_nodes + 1;
   }

   EdgeDataType * out_edge_data() {
      return (EdgeDataType *) (unsigned int *) (out_neighbors()) + _num_edges;
   }
   const EdgeDataType * out_edge_data() const {
      return (EdgeDataType *) (unsigned int *) (out_neighbors()) + _num_edges;
   }

   EdgeDataType & out_edge_data(unsigned int node_id, unsigned int nbr_id) {
      return ((EdgeDataType*) out_edge_data())[outgoing_index()[node_id] + nbr_id];
   }
   unsigned int & out_neighbors(unsigned int node_id, unsigned int nbr_id) {
      return ((unsigned int *) out_neighbors())[outgoing_index()[node_id] + nbr_id];
   }
   unsigned int * incoming_index() {
      return outgoing_index();
   }
   const unsigned int * incoming_index() const {
      return outgoing_index();
   }
   unsigned int * in_neighbors() {
      return outgoing_index();
   }
   const unsigned int * in_neighbors() const {
      return outgoing_index();
   }
   EdgeDataType * in_edge_data() {
      return out_edge_data();
   }
   const EdgeDataType * in_edge_data() const {
      return out_edge_data();
   }
   unsigned int * get_edge_src() {
      return out_edge_data() + _num_edges;
   }
   const unsigned int * get_edge_src() const {
      return out_edge_data() + _num_edges;
   }
   unsigned int get_edge_src(int edge_index) {
      return get_edge_src()[edge_index];
   }
   unsigned int * last() {
      return (unsigned int *) in_edge_data() + _num_edges * SizeEdgeData;
   }
   GPUType * get_array_ptr(void) {
      return gpu_graph;
   }
   size_t num_nodes() {
      return _num_nodes;
   }
   size_t num_edges() {
      return _num_edges;
   }
   size_t num_neighbors(const unsigned int node_id) const {
      return outgoing_index(node_id + 1) - outgoing_index(node_id);
   }
   size_t max_degree() {
      return _max_degree;
   }
   void init(size_t n_n, size_t n_e) {
      _num_nodes = n_n;
      _num_edges = n_e;
      const int arr_size = (4 + (_num_nodes * SizeNodeData) + (_num_nodes + 1) + (_num_edges) + (_num_edges * SizeEdgeData) + (_num_edges));
      std::cout << "Allocating NN: " << _num_nodes << "(" << SizeNodeData << ") , NE :" << _num_edges << ", TOTAL:: " << arr_size << "\n";
      //Num_nodes, num_edges, [node_data] , [outgoing_index], [out_neighbors], [edge_data] , [src indices]
      fprintf(stderr, "GraphSize :: %6.6g MB\n", arr_size / (float(1024 * 1024)));
      gpu_graph = new GPUType(4 + (_num_nodes * SizeNodeData) + (_num_nodes + 1) + (_num_edges) + (_num_edges * SizeEdgeData) + (_num_edges));
      (*gpu_graph)[0] = (int) _num_nodes;
      (*gpu_graph)[1] = (int) _num_edges;
      (*gpu_graph)[2] = (int) SizeNodeData;
      (*gpu_graph)[3] = (int) SizeEdgeData;
      //allocate_on_gpu();
   }
   /////////////////////////////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////////////////////
   void copy_to_device(void) {
      gpu_graph->copy_to_device();
   }
   /////////////////////////////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////////////////////
   void copy_to_host(void) {
      gpu_graph->copy_to_host();
   }
   DevicePtrType & device_ptr() {
      return gpu_graph->device_ptr();
   }
   HostPtrType & host_ptr(void) {
      return gpu_graph->host_ptr();
   }
   void print_header(void) {
      std::cout << "Header :: [";
      for (unsigned int i = 0; i < 6; ++i) {
         std::cout << gpu_graph->operator[](i) << ",";
      }
      std::cout << "\n";
      return;
   }
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void print_node(unsigned int idx, const char * post = "") {
      if (idx < _num_nodes) {
         std::cout << "N-" << idx << "(" << (node_data())[idx] << ")" << " :: [";
         for (size_t i = (outgoing_index())[idx]; i < (outgoing_index())[idx + 1]; ++i) {
            std::cout << " " << (out_neighbors())[i] << "(" << (out_edge_data())[i] << "<" << node_data()[out_neighbors()[i]] << ">" << "), ";
         }
         std::cout << "]" << post;
      }
      return;
   }

   static const char * get_graph_decl() {
      return str_SGD_LC_LinearArray_Undirected_Graph;
   }
   /////////////////////////////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////////////////////
   void allocate_on_gpu() {
      return;
   }
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void print_graph(void) {
      std::cout << "\n====Printing graph (" << _num_nodes << " , " << _num_edges << ")=====\n";
      for (size_t i = 0; i < _num_nodes; ++i) {
         print_node(i);
         std::cout << "\n";
      }
      return;
   }

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void update_in_neighbors(void) {
   }
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void print_compact(void) {
      std::cout << "Summary:: [" << _num_nodes << ", " << _num_edges << ", " << outgoing_index()[_num_nodes] << "]";
      std::cout << "\nOut-index [";
      for (size_t i = 0; i < _num_nodes + 1; ++i) {
         if (i < _num_nodes && outgoing_index()[i] > outgoing_index()[i + 1])
            std::cout << "**ERR**";
         std::cout << " " << outgoing_index()[i] << ",";
      }
      std::cout << "]\nNeigh[";
      for (size_t i = 0; i < _num_edges; ++i) {
         if (out_neighbors()[i] > _num_nodes)
            std::cout << "**ERR**";
         std::cout << " " << out_neighbors()[i] << ",";
      }
      std::cout << "]\nEData [";
      for (size_t i = 0; i < _num_edges; ++i) {
         std::cout << " " << out_edge_data()[i] << ",";
      }
      std::cout << "]";
   }
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   unsigned int verify() {
      unsigned int * t_node_data = node_data();
      unsigned int * t_outgoing_index = outgoing_index();
      unsigned int * t_neighbors = out_neighbors();
      unsigned int * t_out_edge_data = out_edge_data();
      unsigned int err_count = 0;
      for (unsigned int node_id = 0; node_id < _num_nodes; ++node_id) {
         unsigned int curr_distance = t_node_data[node_id];
         //Go over all the neighbors.
         for (unsigned int idx = t_outgoing_index[node_id]; idx < t_outgoing_index[node_id + 1]; ++idx) {
            unsigned int temp = t_node_data[t_neighbors[idx]];
            if (curr_distance + t_out_edge_data[idx] < temp) {
               if (err_count < 10) {
                  std::cout << "Error :: ";
                  print_node(node_id);
                  std::cout << " With :: ";
                  print_node(t_neighbors[idx]);
                  std::cout << "\n";
               }
               err_count++;
            }
         }
      } //End for
      return err_count;
   }
   ////////////##############################################################///////////
   ////////////##############################################################///////////
   unsigned int verify_in() {
      return 0;
   }
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void deallocate(void) {
      delete gpu_graph;
   }

};
//End LC_Graph
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
}
}   //End namespaces
#endif /* _SGD_LC_LinearArray_Undirected_Graph_H_ */
