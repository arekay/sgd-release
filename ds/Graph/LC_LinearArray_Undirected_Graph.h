/**
 Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU.
 GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
 Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

 @author Rashid Kaleem <rashid.kaleem@gmail.com>

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 */
/*
 * LC_LinearArray_Undirected_Graph.h
 *
 *  Created on: Oct 24, 2013
 *  Single array representation, has outgoing edges.
 *      Author: rashid
 */

#include <iostream>
#include <fstream>
#include <cstring>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include <limits>

#ifndef LC_LinearArray_Undirected_Graph_H_
#define LC_LinearArray_Undirected_Graph_H_

namespace Galois {
namespace OpenCL {
static const char * str_LC_LinearArray_Undirected_Graph = "typedef struct _GraphType { \n"
      "uint _num_nodes;\n"
      "uint _num_edges;\n "
      "uint _node_data_size;\n "
      "uint _edge_data_size;\n "
      "__global NodeData *_node_data;\n "
      "__global uint *_out_index;\n "
      "__global uint *_out_neighbors;\n "
      "__global EdgeData *_out_edge_data;\n }GraphType;\n"
      "uint in_neighbors_begin(__local GraphType * graph, uint node){ \n return 0;\n}\n"
      "uint in_neighbors_end(__local GraphType * graph, uint node){ \n return graph->_out_index[node+1]-graph->_out_index[node];\n}\n"
      "uint in_neighbors_next(__local GraphType * graph, uint node){ \n return 1;\n}\n"
      "uint in_neighbors(__local GraphType * graph, uint node, uint nbr){ \n return graph->_out_neighbors[graph->_out_index[node]+nbr];\n}\n"
      "__global EdgeData * in_edge_data(__local GraphType * graph, uint node, uint nbr){ \n return &graph->_out_edge_data[graph->_out_index[node]+nbr];\n}\n"
      "uint out_neighbors_begin(__local GraphType * graph, uint node){ \n return 0;\n}\n"
      "uint out_neighbors_end(__local GraphType * graph, uint node){ \n return graph->_out_index[node+1]-graph->_out_index[node];\n}\n"
      "uint out_neighbors_next(__local GraphType * graph, uint node){ \n return 1;\n}\n"
      "uint out_neighbors(__local GraphType * graph,uint node,  uint nbr){ \n return graph->_out_neighbors[graph->_out_index[node]+nbr];\n}\n"
      "__global EdgeData * out_edge_data(__local GraphType * graph,uint node,  uint nbr){ \n return &graph->_out_edge_data[graph->_out_index[node]+nbr];\n}\n"
      "__global NodeData * node_data(__local GraphType * graph, uint node){ \n return &graph->_node_data[node];\n}\n";

template<template<typename > class GPUWrapper, typename NodeDataTy, typename EdgeDataTy>
struct LC_LinearArray_Undirected_Graph {
   //Are you using gcc/4.7+ Error on line below for earlier versions.
#ifdef _WIN32
   typedef GPUWrapper<unsigned int> GPUType;
   typedef typename GPUWrapper<unsigned int>::HostPtrType HostPtrType;
   typedef typename GPUWrapper<unsigned int>::DevicePtrType DevicePtrType;
#else
   template<typename T> using ArrayType = GPUWrapper<T>;
   //   typedef GPUWrapper<unsigned int> ArrayType;
   typedef GPUWrapper<unsigned int> GPUType;
   typedef typename GPUWrapper<unsigned int>::HostPtrType HostPtrType;
   typedef typename GPUWrapper<unsigned int>::DevicePtrType DevicePtrType;
#endif
   typedef NodeDataTy NodeDataType;
   typedef EdgeDataTy EdgeDataType;
   typedef unsigned int NodeIDType;
   typedef unsigned int EdgeIDType;
   size_t _num_nodes;
   size_t _num_edges;
   unsigned int _max_degree;
   const size_t SizeEdgeData;
   const size_t SizeNodeData;
   GPUType * gpu_graph;
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   LC_LinearArray_Undirected_Graph() :
         SizeEdgeData(sizeof(EdgeDataType) / sizeof(unsigned int)), SizeNodeData(sizeof(NodeDataType) / sizeof(unsigned int)) {
      DEBUG_CODE(std::cout << "Created LC_LinearArray_Graph with " << SizeNodeData << " node " << SizeEdgeData << " edge data. ";);
      _max_degree = _num_nodes = _num_edges = 0;
      gpu_graph = 0;
   }

   void read(const char * filename) {
      readFromGR(*this, filename);
   }
   NodeDataType * node_data() {
      return (NodeDataType*) gpu_graph->host_data + 4;
   }
   unsigned int * outgoing_index() {
      return (unsigned int*) (node_data()) + _num_nodes * SizeNodeData;
   }
   unsigned int * out_neighbors() {
      return (unsigned int *) outgoing_index() + _num_nodes + 1;
   }
   EdgeDataType * out_edge_data() {
      return (EdgeDataType *) (unsigned int *) (out_neighbors()) + _num_edges;
   }
   EdgeDataType & out_edge_data(unsigned int node_id, unsigned int nbr_id) {
      return ((EdgeDataType*) out_edge_data())[outgoing_index()[node_id] + nbr_id];
   }
   unsigned int & out_neighbors(unsigned int node_id, unsigned int nbr_id) {
      return ((unsigned int *) out_neighbors())[outgoing_index()[node_id] + nbr_id];
   }
   unsigned int * incoming_index() {
      return outgoing_index();
   }
   unsigned int * in_neighbors() {
      return outgoing_index();
   }
   EdgeDataType * in_edge_data() {
      return out_edge_data();
   }
   unsigned int * last() {
      return (unsigned int *) in_edge_data() + _num_edges * SizeEdgeData;
   }
   GPUType * get_array_ptr(void) {
      return gpu_graph;
   }
   size_t num_nodes() {
      return _num_nodes;
   }
   size_t num_edges() {
      return _num_edges;
   }
   size_t size() {
      return gpu_graph->size();
   }
   size_t num_neighbors(unsigned int node_id) {
      return outgoing_index()[node_id + 1] - outgoing_index()[node_id];
   }
   size_t max_degree() {
      return _max_degree;
   }
   void init(size_t n_n, size_t n_e) {
      _num_nodes = n_n;
      _num_edges = n_e;
      //std::cout << "Allocating NN: " << _num_nodes << " , NE :" << _num_edges << ". ";
      //Num_nodes, num_edges, [node_data] , [outgoing_index], [out_neighbors], [edge_data]
      gpu_graph = new GPUType(4 + (_num_nodes * SizeNodeData) + (_num_nodes + 1) + (_num_edges) + (_num_edges * SizeEdgeData));
      (*gpu_graph)[0] = (int) _num_nodes;
      (*gpu_graph)[1] = (int) _num_edges;
      (*gpu_graph)[2] = (int) SizeNodeData;
      (*gpu_graph)[3] = (int) SizeEdgeData;
      //allocate_on_gpu();
   }
   /////////////////////////////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////////////////////
   void copy_to_device(void) {
      gpu_graph->copy_to_device();
   }
   /////////////////////////////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////////////////////
   void copy_to_host(void) {
      gpu_graph->copy_to_host();
   }
   DevicePtrType & device_ptr() {
      return gpu_graph->device_ptr();
   }
   DevicePtrType & device_ptr(CL_Device * device) {
      return gpu_graph->device_ptr(device);
   }
   HostPtrType & host_ptr(void) {
      return gpu_graph->host_ptr();
   }
   void print_header(void) {
      std::cout << "Header :: [";
      for (unsigned int i = 0; i < 6; ++i) {
         std::cout << gpu_graph->operator[](i) << ",";
      }
      std::cout << "\n";
      return;
   }
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void print_node(unsigned int idx, const char * post = "") {
      if (idx < _num_nodes) {
         std::cout << "N-" << idx << "(" << (node_data())[idx] << ")" << " :: [";
         for (size_t i = (outgoing_index())[idx]; i < (outgoing_index())[idx + 1]; ++i) {
            std::cout << " " << (out_neighbors())[i] << "(" << (out_edge_data())[i] << "<" << node_data()[out_neighbors()[i]] << ">" << "), ";
         }
         std::cout << "]" << post;
      }
      return;
   }

   static const char * get_graph_decl(std::string & res) {
//      std::string to_return("");
      /*to_return.append("typedef struct _GraphType { \n"
       "uint _num_nodes;\n"
       "uint _num_edges;\n "
       "uint node_data_size;\n "
       "uint edge_data_size;\n "
       "volatile __global NodeData volatile *node_data;\n "
       "__global uint *out_index;\n "
       "__global uint *out_neighbors;\n "
       "__global EdgeData *out_edge_data;\n }GraphType;\n");

       to_return.append("uint in_neighbors_begin(__local GraphType * graph, uint node){ \n return 0;\n}\n");
       to_return.append("uint in_neighbors_end(__local GraphType * graph, uint node){ \n return graph->out_index[node+1]-graph->out_index[node];\n}\n");
       to_return.append("uint in_neighbors_next(__local GraphType * graph, uint node){ \n return 1;\n}\n");
       to_return.append("uint in_neighbors(__local GraphType * graph, uint node, uint nbr){ \n return graph->out_neighbors[graph->out_index[node]+nbr];\n}\n");
       to_return.append("__global EdgeData * in_edge_data(__local GraphType * graph, uint node, uint nbr){ \n return &graph->out_edge_data[graph->out_index[node]+nbr];\n}\n");

       to_return.append("uint out_neighbors_begin(__local GraphType * graph, uint node){ \n return 0;\n}\n");
       to_return.append("uint out_neighbors_end(__local GraphType * graph, uint node){ \n return graph->out_index[node+1]-graph->out_index[node];\n}\n");
       to_return.append("uint out_neighbors_next(__local GraphType * graph, uint node){ \n return 1;\n}\n");
       to_return.append("uint out_neighbors(__local GraphType * graph,uint node,  uint nbr){ \n return graph->out_neighbors[graph->out_index[node]+nbr];\n}\n");
       to_return.append("__global EdgeData * out_edge_data(__local GraphType * graph,uint node,  uint nbr){ \n return &graph->out_edge_data[graph->out_index[node]+nbr];\n}\n");
       to_return.append("__global NodeData * node_data(__local GraphType * graph, uint node){ \n return &graph->node_data[node];\n}\n");*/
      /////////////Initialization code
      std::string str;
      str = std::string("void initialize(__local GraphType * graph, __global uint *mem_pool){\nuint offset =4;\n") + "graph->_num_nodes=mem_pool[0];\n"
            + "graph->_num_edges=mem_pool[1];\n" + "graph->_node_data_size=mem_pool[2];\n" + "graph->_edge_data_size=mem_pool[3];\n"
            + "graph->_node_data= (__global NodeData *)&mem_pool[offset];\n" + "offset +=graph->_num_nodes* graph->_node_data_size;\n";
      str += std::string("") + "graph->_out_index=&mem_pool[offset];\n" + "offset +=graph->_num_nodes + 1;\n" + "graph->_out_neighbors=&mem_pool[offset];\n"
            + "offset +=graph->_num_edges;\n" + "graph->_out_edge_data=(__global EdgeData*)&mem_pool[offset];\n" + "offset +=graph->_num_edges*graph->_edge_data_size;\n}\n";
      /////////////End initialization code.
      res.append(str_LC_LinearArray_Undirected_Graph);
      res.append(str);
//      fprintf(stderr, "%s", res.c_str());
      return res.c_str();
   }
   /////////////////////////////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////////////////////
   void allocate_on_gpu() {
      return;
   }
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void print_graph(void) {
      std::cout << "\n====Printing graph (" << _num_nodes << " , " << _num_edges << ")=====\n";
      for (size_t i = 0; i < _num_nodes; ++i) {
         print_node(i);
         std::cout << "\n";
      }
      return;
   }

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void update_in_neighbors(void) {
   }
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void print_compact(void) {
      std::cout << "Summary:: [" << _num_nodes << ", " << _num_edges << ", " << outgoing_index()[_num_nodes] << "]";
      std::cout << "\nOut-index [";
      for (size_t i = 0; i < _num_nodes + 1; ++i) {
         if (i < _num_nodes && outgoing_index()[i] > outgoing_index()[i + 1])
            std::cout << "**ERR**";
         std::cout << " " << outgoing_index()[i] << ",";
      }
      std::cout << "]\nNeigh[";
      for (size_t i = 0; i < _num_edges; ++i) {
         if (out_neighbors()[i] > _num_nodes)
            std::cout << "**ERR**";
         std::cout << " " << out_neighbors()[i] << ",";
      }
      std::cout << "]\nEData [";
      for (size_t i = 0; i < _num_edges; ++i) {
         std::cout << " " << out_edge_data()[i] << ",";
      }
      std::cout << "]";
   }
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   unsigned int verify() {
      unsigned int * t_node_data = node_data();
      unsigned int * t_outgoing_index = outgoing_index();
      unsigned int * t_neighbors = out_neighbors();
      unsigned int * t_out_edge_data = out_edge_data();
      unsigned int err_count = 0;
      for (unsigned int node_id = 0; node_id < _num_nodes; ++node_id) {
         unsigned int curr_distance = t_node_data[node_id];
         //Go over all the neighbors.
         for (unsigned int idx = t_outgoing_index[node_id]; idx < t_outgoing_index[node_id + 1]; ++idx) {
            unsigned int temp = t_node_data[t_neighbors[idx]];
            if (curr_distance + t_out_edge_data[idx] < temp) {
//               if (err_count < 10)
               {
                  std::cout << "Error :: ";
                  print_node(node_id);
                  std::cout << " With :: ";
                  print_node(t_neighbors[idx]);
                  std::cout << "\n";
               }
               err_count++;
            }
         }
      } //End for
      return err_count;
   }
   ////////////##############################################################///////////
   ////////////##############################################################///////////
   unsigned int verify_in() {
      return 0;
   }
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
   void deallocate(void) {
      delete gpu_graph;
   }

};
//End LC_Graph
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
}
}   //End namespaces
#endif /* LC_LinearArray_Undirected_Graph_H_ */
