/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
extern "C" {
#include "CL/cl.h"
}
;
#endif

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include <limits>

#ifdef _WIN32
#define NOMINMAX
#include <windows.h>
#include <process.h>
#include <time.h>
#include <Psapi.h>
#else
#include <sys/time.h>
#endif

#ifndef COMMON_HEADER_H_
#define COMMON_HEADER_H_

#define _GALOIS_HETERO_DISABLE_TIMER 0

#include "timer.h"
namespace Galois {
namespace OpenCL {

#ifdef _GOPT_DEBUG
#define DEBUG_CODE(X) {X}
#define INFO_CODE(X) {X}
#else
#define DEBUG_CODE(X) {}
#define INFO_CODE(X) {}
#endif

//long long inline get_time_in_microseconds() {
//   struct timeval tv;
//   gettimeofday(&tv, NULL);
//   return tv.tv_sec * 1000000 + tv.tv_usec;
//}
///////////////////////////////////////////////////////////////////////////////////
/*
 * A simple timer class, use this to make measurements so they are easily comparable.
 * */


///////////////////////////////////////////////////////////////////////////////////
template<typename EType, typename NType>
class DIMACS_GR_Challenge9_Format {
public:
   static bool is_comment(std::string & str) {
      return str.c_str()[0] == 'c';
   }
   static std::pair<size_t, size_t> parse_header(std::string & s) {
      if (s.c_str()[0] == 'p') {
         char buff[256];
         strcpy(buff, s.substr(1, s.size()).c_str());
         char * tok = strtok(buff, " ");
         tok = strtok(NULL, " "); // Ignore problem name for challenge9 formats.
         size_t num_nodes = atoi(tok) - 1; // edges start from zero.
         tok = strtok(NULL, " ");
         size_t num_edges = atoi(tok);
         return std::pair<size_t, size_t>(num_nodes, num_edges);
      }
      return std::pair<size_t, size_t>((size_t) -1, (size_t) -1);
   }
   static std::pair<NType, std::pair<NType, EType> > parse_edge_pair(std::string & s) {
      if (s.c_str()[0] == 'a') {
         char buff[256];
         strcpy(buff, s.substr(1, s.size()).c_str());
         char * tok = strtok(buff, " ");
         NType src_node = atoi(tok) - 1;
         tok = strtok(NULL, " ");
         NType dst_node = atoi(tok) - 1;
         tok = strtok(NULL, " ");
         EType edge_wt = atoi(tok);
         return std::pair<NType, std::pair<NType, EType> >(src_node, std::pair<NType, EType>(dst_node, edge_wt));
      }
      return std::pair<NType, std::pair<NType, EType> >(-1, std::pair<NType, EType>(-1, -1));
   }
};
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
////// Generate a random float between 'low' and 'high'
/////////////////////////////////////////////////////////////////////////////////////
inline float rrange(float low, float high) {
   return (int) rand() * (high - low) / (float) RAND_MAX + low;
}
#ifdef _WIN32
inline double time_event_seconds(cl_event & event ) {
#else
inline double time_event_seconds(cl_event & event __attribute__((unused))) {
#endif

#ifdef _CL_PROFILE_EVENTS_
   cl_ulong start_time, end_time;
   clWaitForEvents(1, &event);
   clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(start_time), &start_time, NULL);
   clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(end_time), &end_time, NULL);
   double total_time = ((end_time - start_time)/((double)(1000*1000*1000)));
   return total_time;
#else
   return 0;
#endif
}
/////////////////////////////
inline int next_power_2(int x) {
   /*int i=0;
    do{++i; val>>=1; std::cout<<"{"<<val<<"}";}while(val>0);
    //for(; val>0; ++i, val>>=1)std::cout<<"{"<<val<<"}";
    return 1<<(i);*/
   if (x < 0)
      return 0;
   --x;
   x |= x >> 1;
   x |= x >> 2;
   x |= x >> 4;
   x |= x >> 8;
   x |= x >> 16;
   return x + 1;
}
/////////////////////////////
}
}  //End namespaces

#endif /* COMMON_HEADER_H_ */
