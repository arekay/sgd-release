/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifndef GALOISGPU_GOPT_TIMER_H_
#define GALOISGPU_GOPT_TIMER_H_

namespace Galois{
namespace OpenCL{


#if _GALOIS_HETERO_DISABLE_TIMER
/////////Disable timers.
struct Timer {
   Timer() {
   }
   void clear() {
   }
   void start() {
   }
   void stop() {
   }
   double get_time_seconds(void) {
      return 0;
   }
};//End struct timer;
#else

#ifdef _WIN32
/////////Windows timers
struct Timer {
   double _start;
   double _end;
   Timer() :
         _start(0), _end(0) {
   }
   void clear() {
      _start = _end = 0;
   }
   void start() {
      _start = rtclock();
   }
   void stop() {
      _end = rtclock();
   }
   double get_time_seconds(void) {
      return (_end - _start);
   }
   static double rtclock() {
      LARGE_INTEGER tickPerSecond, tick;
      QueryPerformanceFrequency(&tickPerSecond);
      QueryPerformanceCounter(&tick);
      return (tick.QuadPart*1000000/tickPerSecond.QuadPart)*1.0e-6;
      //return (Tp.tv_sec + Tp.tv_usec * 1.0e-6);
   }
};//End struct timer;
#else
#ifdef  HAVE_CXX11_CHRONO
//#include <ctime>
//#include <ratio>
#include <chrono>
   struct Timer {
   using namespace std::chrono;
   double _start, _end;
      Timer():_start(0), _end(0) {
      }
      void clear() {
         _start = _end = 0;
      }
      void start() {
         _start = rtclock();
      }
      void stop() {
         _end = rtclock();
      }
      double get_time_seconds(void) {
         using namespace std::chrono;
         return (_end - _start)/system_clock::period::den;
      }
      static  double rtclock() {
         using namespace std::chrono;
         return  std::chrono::high_resolution_clock::now().time_since_epoch().count();
      }
};
#else
   struct Timer {
      double _start;
      double _end;
      Timer() :
            _start(0), _end(0) {
      }
      void clear() {
         _start = _end = 0;
      }
      void start() {
         _start = rtclock();
      }
      void stop() {
         _end = rtclock();
      }
      double get_time_seconds(void) {
         return (_end - _start);
      }
   #include <time.h>
      static double rtclock() {
         struct timespec t;
         clock_gettime(CLOCK_MONOTONIC_RAW, &t);
         return (t.tv_sec + t.tv_nsec * 1.0e-9);
      }
   };
#endif //End if 0

#endif //End if WINDOWS

#endif //End if disable timers.
}
}

#endif /* GALOISGPU_GOPT_TIMER_H_ */
