#Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU. 
#GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
#Copyright (C) 2015, The University of Texas at Austin. All rights reserved.
#
#@author Rashid Kaleem <rashid.kaleem@gmail.com>
#
#This library is free software; you can redistribute it and/or
#modify it under the terms of the GNU Lesser General Public
#License as published by the Free Software Foundation; either
#version 2.1 of the License.
#
#This library is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#Lesser General Public License for more details.
#
#You should have received a copy of the GNU Lesser General Public
#License along with this library; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

OS_NAME_STRING=$(shell uname)
CFLAGS= -m64 -Wall -Wextra -lrt -std=c++0x -O3 #-gdwarf-3 #-g -DVTUNE_PROFILE#-g -DVTUNE_PROFILE 
ifeq ($(OS_NAME_STRING),Linux)
	CL_COMPILE_OPTIONS+=-I${CUDATK_PATH}/include -L${CUDATK_PATH}/lib64  -L/usr/lib64/nvidia -lOpenCL
endif
ifeq ($(OS_NAME_STRING),Darwin)
	CL_COMPILE_OPTIONS=-I/${CUDATK_PATH}/include/ -framework OpenCL -framework CUDA
endif
CL_COMPILE_OPTIONS+=-I./CL_HEADER
all: sgd all_cuda micro

clean: clean_sgd clean_cuda clean_micro

###################################################################################################################################################### 
sgd: sgd_edge_lock sgd_node_lock sgd_lock_hyb sgd_all_graph_edge_lock_free sgd_all_graph_node_lock_free sgd_sub_graph_node_lock_free sgd_hyb_lock_free

clean_sgd:
	rm sgd_edge_lock sgd_node_lock sgd_lock_hyb sgd_all_graph_edge_lock_free sgd_all_graph_node_lock_free sgd_sub_graph_node_lock_free sgd_hyb_lock_free

sgd_edge_lock: ocl/ocl_header.cpp apps/sgd/TestSGD.cpp gopt/*.h   apps/sgd/*.h 
	${CXX} ${CFLAGS} -D_USE_CL -DSGD_EDGE_LOCKED ocl/ocl_header.cpp apps/sgd/TestSGD.cpp -o sgd_edge_lock $(CL_COMPILE_OPTIONS) 

el_profile: ocl/ocl_header.cpp apps/sgd/TestSGD.cpp gopt/*.h   apps/sgd/*.h 
	${CXX} ${CFLAGS} -D_USE_CL -DSGD_EDGE_LOCKED -DPROFILE_KERNEL_TIMES=1  ocl/ocl_header.cpp apps/sgd/TestSGD.cpp -o sgd_edge_lock $(CL_COMPILE_OPTIONS) 


sgd_node_lock: ocl/ocl_header.cpp apps/sgd/TestSGD.cpp gopt/*.h   apps/sgd/*.h 
	${CXX} ${CFLAGS} -D_USE_CL -DSGD_NODE_LOCKED ocl/ocl_header.cpp apps/sgd/TestSGD.cpp -o sgd_node_lock $(CL_COMPILE_OPTIONS)

sgd_lock_hyb: ocl/ocl_header.cpp apps/sgd/TestSGD.cpp gopt/*.h   apps/sgd/*.h 
	${CXX} ${CFLAGS} -D_USE_CL -DSGD_HYBRID_LOCKED ocl/ocl_header.cpp apps/sgd/TestSGD.cpp -o sgd_lock_hyb $(CL_COMPILE_OPTIONS)
	
sgd_all_graph_edge_lock_free: ocl/ocl_header.cpp apps/sgd/TestSGD.cpp gopt/*.h   apps/sgd/*.h 
	${CXX} ${CFLAGS} -D_USE_CL -DSGD_ALL_GRAPH_EDGE ocl/ocl_header.cpp apps/sgd/TestSGD.cpp -o sgd_all_graph_edge_lock_free $(CL_COMPILE_OPTIONS)

agme_profile: ocl/ocl_header.cpp apps/sgd/TestSGD.cpp gopt/*.h   apps/sgd/*.h 
	${CXX} ${CFLAGS} -D_USE_CL -DSGD_ALL_GRAPH_EDGE -DPROFILE_KERNEL_TIMES=1 ocl/ocl_header.cpp apps/sgd/TestSGD.cpp -o sgd_all_graph_edge_lock_free $(CL_COMPILE_OPTIONS)

sgd_all_graph_node_lock_free: ocl/ocl_header.cpp apps/sgd/TestSGD.cpp gopt/*.h   apps/sgd/*.h 
	${CXX} ${CFLAGS} -D_USE_CL -DSGD_ALL_GRAPH_NODE ocl/ocl_header.cpp apps/sgd/TestSGD.cpp -o sgd_all_graph_node_lock_free $(CL_COMPILE_OPTIONS)

sgd_sub_graph_node_lock_free: ocl/ocl_header.cpp apps/sgd/TestSGD.cpp gopt/*.h   apps/sgd/*.h 
	${CXX} ${CFLAGS} -D_USE_CL -DSGD_SUBGRAPH_NODE_LOCKFREE ocl/ocl_header.cpp apps/sgd/TestSGD.cpp -o sgd_sub_graph_node_lock_free $(CL_COMPILE_OPTIONS)

sgd_hyb_lock_free: ocl/ocl_header.cpp apps/sgd/TestSGD.cpp gopt/*.h   apps/sgd/*.h 
	${CXX} ${CFLAGS} -D_USE_CL -DSGD_HYBRID_LOCKFREE ocl/ocl_header.cpp apps/sgd/TestSGD.cpp -o sgd_hyb_lock_free $(CL_COMPILE_OPTIONS)
	
######################################################################################################################################################
#CUDA Versions::

CUDA_COMPILE_OPTIONS=-m64 -lineinfo -arch sm_35 -O3
INCLUDE_DIR=-I./cuda/include

all_cuda: diag blk_diag

clean_cuda: 
	rm diag blk_diag

diag: cuda/TestCudaSGD.cu cuda/diagonal/*
		nvcc ${CUDA_COMPILE_OPTIONS} -I./cuda/diagonal $(INCLUDE_DIR) cuda/TestCudaSGD.cu -o diag
blk_diag: cuda/TestCudaSGD.cu cuda/block_diagonal/*
		nvcc ${CUDA_COMPILE_OPTIONS} -DBLOCKSIZE=1 -DR=2 -DC=2 -I./cuda/block_diagonal $(INCLUDE_DIR) cuda/TestCudaSGD.cu -o blk_diag

######################################################################################################################################################
#Microbenchmarks
micro: apps/micro/test_micro.cpp apps/micro/micro_kernels.cl 
	${CXX} ${CFLAGS} apps/micro/test_micro.cpp -o micro $(CL_COMPILE_OPTIONS)
clean_micro:
	rm micro


s_micro: apps/micro/simple_kernels.cpp apps/micro/simple_kernels.cl 
	${CXX} ${CFLAGS} apps/micro/simple_kernels.cpp -o s_micro $(CL_COMPILE_OPTIONS)
s_clean:
	rm s_micro
