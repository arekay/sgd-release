/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU.
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <sys/time.h>
#include <string>
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
extern "C" {
#include "CL/cl.h"
}
;
#endif
#define DEFAULT_PLATFORM 0
#define DEFAULT_DEVICE 0 
struct Timer {
   double _start;
   double _end;
   Timer() :
         _start(0), _end(0) {
   }
   void clear() {
      _start = _end = 0;
   }
   void start() {
      _start = rtclock();
   }
   void stop() {
      _end = rtclock();
   }
   double get_time_microseconds(void) {
      return (_end - _start);
   }

   static double rtclock() {
      struct timezone Tzp;
      struct timeval Tp;
      int stat;
      stat = gettimeofday(&Tp, &Tzp);
      if (stat != 0)
         printf("Error return from gettimeofday: %d", stat);
      return (Tp.tv_sec + Tp.tv_usec * 1.0e-6);
   }
};
double get_time_milli(void) {
   struct timeval tv;
   gettimeofday(&tv, NULL);
   double time_in_mill = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000;
   return time_in_mill;
}

#define MAX_GPU_SIZE (1<<28)

////////////////////////////////////////////////////////////////////////////////

// Simple compute kernel which computes the square of an input array
//
////////////////////////////////////////////////////////////////////////////////
struct CLEnv {
   size_t global;                      // global domain size for our calculation
   size_t local;                       // local domain size for our calculation
   cl_device_id device_id;             // compute device id
   cl_context context;                 // compute context
   cl_command_queue commands;          // compute command queue
   cl_program program;                 // compute program
   cl_kernel kernel;                   // compute kernel
   cl_platform_id platform;
};
/*********************************************************************
 *
 **********************************************************************/
static inline char *load_program_source(const char *filename) {
   FILE *fh;
   struct stat statbuf;
   char *source;
   if (!(fh = fopen(filename, "rb"))){
      fprintf(stderr, "Failed to open file\n");
      return NULL;
   }
   stat(filename, &statbuf);
   source = (char *) malloc(statbuf.st_size + 1);
   fread(source, statbuf.st_size, 1, fh);
   source[statbuf.st_size] = 0;
   return source;
}
/*********************************************************************
 *
 **********************************************************************/
int setup_env(CLEnv & env, const char * kernel_name) {
   int err;
   cl_platform_id platforms[3];
   err = clGetPlatformIDs(3, &platforms[0], NULL);
   env.platform = platforms[DEFAULT_PLATFORM];
   if (err != CL_SUCCESS) {
      printf("Error: Failed to create a platform! :: %d \n", err);
      return EXIT_FAILURE;
   }
   cl_device_id devices[5];
   err = clGetDeviceIDs(env.platform, CL_DEVICE_TYPE_ALL, 5, &devices[0], NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to create a device group! :: %d \n", err);
      return EXIT_FAILURE;
   }
   env.device_id = devices[DEFAULT_DEVICE];
   // Create a compute context
   //
   env.context = clCreateContext(0, 1, &env.device_id, NULL, NULL, &err);
   if (!env.context) {
      printf("Error: Failed to create a compute context!\n");
      return EXIT_FAILURE;
   }
   // Create a command commands
   env.commands = clCreateCommandQueue(env.context, env.device_id, 0, &err);
   if (!env.commands) {
      printf("Error: Failed to create a command commands!\n");
      return EXIT_FAILURE;
   }
   // Create the compute program from the source buffer
   const char * source = load_program_source("micro_kernels.cl");
   env.program = clCreateProgramWithSource(env.context, 1, (const char **) &source, NULL, &err);
   if (!env.program) {
      printf("Error: Failed to create compute program!\n");
      return EXIT_FAILURE;
   }
   // Build the program executable
   err = clBuildProgram(env.program, 0, NULL, NULL, NULL, NULL);
   if (err != CL_SUCCESS) {
      size_t len;
      char buffer[2048];
      printf("Error: Failed to build program executable!\n");
      clGetProgramBuildInfo(env.program, env.device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
      printf("%s\n", buffer);
      exit(1);
   }

   // Create the compute kernel in the program we wish to run
   env.kernel = clCreateKernel(env.program, kernel_name, &err);
   if (!env.kernel || err != CL_SUCCESS) {
      printf("Error: Failed to create compute kernel!\n");
      exit(1);
   }
   err = clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(env.local), &env.local, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to retrieve kernel work group info! %d\n", err);
      exit(1);
   }
   printf("Local size detected :: %lu\n", env.local);

   return 0;
}
/*********************************************************************
 *
 **********************************************************************/
double apply_atomic_kernel(CLEnv & env, unsigned int size) {
   int err;
   const int array_size = size;
   unsigned int * data = new unsigned int[array_size];
   memset(data, 0, sizeof(unsigned int) * array_size);
   cl_mem input = clCreateBuffer(env.context, CL_MEM_READ_WRITE, sizeof(unsigned int) * array_size, NULL, NULL);
   cl_mem atomic_loc = clCreateBuffer(env.context, CL_MEM_READ_WRITE, sizeof(unsigned int) * 1, NULL, NULL);
   if (!input) {
      printf("Error: Failed to allocate device memory!\n");
      exit(1);
   }
   Timer timer;

   err = clEnqueueWriteBuffer(env.commands, input, CL_TRUE, 0, sizeof(unsigned int) * array_size, data, 0, NULL, NULL);
   err |= clEnqueueWriteBuffer(env.commands, atomic_loc, CL_TRUE, 0, sizeof(unsigned int) , data, 0, NULL, NULL);
   if (err != CL_SUCCESS) {
      printf("Error(%d): Failed to write to source array!\n", err);
      exit(1);
   }
   // Set the arguments to our compute kernel
   err = clSetKernelArg(env.kernel, 0, sizeof(cl_uint), &size);
   err |= clSetKernelArg(env.kernel, 1, sizeof(cl_mem), &input);
   err |= clSetKernelArg(env.kernel, 2, sizeof(cl_mem), &atomic_loc);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to set kernel arguments! %d\n", err);
      exit(1);
   }
   // Get the maximum work group size for executing the kernel on the device
   err = clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(env.local), &env.local, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to retrieve kernel work group info! %d\n", err);
      exit(1);
   }
   unsigned long local_mem_used;
   unsigned long private_mem_used;
   err |= clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_LOCAL_MEM_SIZE, sizeof(local_mem_used), &local_mem_used, NULL);
   err |= clGetKernelWorkGroupInfo(env.kernel, env.device_id, CL_KERNEL_PRIVATE_MEM_SIZE, sizeof(private_mem_used), &private_mem_used, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to retrieve kernel work group info! %d\n", err);
      exit(1);
   }
   // Execute the kernel over the entire range of our 1d input data set
   // using the maximum number of work group items for this device
   env.global = size;
   env.local = env.local < size ? env.local : size;
   //printf("LocalMem : %ld Private : %ld Local size %ld Global size %ld \t", local_mem_used, private_mem_used, env.local, env.global);
   timer.clear();
   timer.start();
   err = clEnqueueNDRangeKernel(env.commands, env.kernel, 1, NULL, &env.global, NULL, 0, NULL, NULL);
   if (err) {
      printf("Error: Failed to execute kernel[%d] err:%d!\n", size, err);
      exit(-1);
   }
   // Wait for the command commands to get serviced before reading back results
   clFinish(env.commands);
   timer.stop();
   // Read back the results from the device to verify the output
   err = clEnqueueReadBuffer(env.commands, input, CL_TRUE, 0, sizeof(unsigned int) * array_size, data, 0, NULL, NULL);
   if (err != CL_SUCCESS) {
      printf("Error: Failed to read output array! %d\n", err);
      exit(1);
   }

   // Validate our results
   unsigned int correct = 0;
   double sum = 0;
   for (unsigned int i = 0; i < size; ++i) {
      sum += 1;
   }
   for (int i = 0; i < array_size; ++i) {
      if (data[i] == sum)
         correct++;
   }
   //printf("Correct results (%g) for %d of %d items, index-0::[%d] \n", sum, correct, size, data[0]);
   //
   delete[] data;
   clReleaseMemObject(input);
//    return end_time_main-start_time_main;
   return timer.get_time_microseconds();
}
/*********************************************************************
 *
 **********************************************************************/

void test_atomic() {
   CLEnv env;
   setup_env(env, "atomic_kernel");
   double time_average;
   for (int size = 1; size <= MAX_GPU_SIZE; size *= 2) {
      time_average = 0;
      for (int run = 0; run < 5; ++run)
         time_average += apply_atomic_kernel(env, size);
      time_average /= 5.0f;
      printf("Size %d, Time %f\n", size, time_average);
   }
   clReleaseProgram(env.program);
   clReleaseKernel(env.kernel);
   clReleaseCommandQueue(env.commands);
   clReleaseContext(env.context);
   return;
}
/*********************************************************************
 *
 **********************************************************************/
int main(int argc, char ** argv) {
   test_atomic();
   printf("Terminating....\n");
   return 0;
}
/*********************************************************************
 *
 **********************************************************************/
