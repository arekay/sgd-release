/**
Stochastic Gradient Descent on the GPU - evaluation of different schedules on the GPU.
GPGPU8 (http://dl.acm.org/citation.cfm?id=2716289)
Copyright (C) 2015, The University of Texas at Austin. All rights reserved.

@author Anand Venkat <anandv@cs.utah.edu>
@author Rashid Kaleem <rashid.kaleem@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#ifdef _WIN32

#define NOMINMAX
#include <windows.h>
#include <process.h>
#include <time.h>
#include <Psapi.h>
#else
#include <sys/time.h>
#endif
#include <cuda.h>
#include "SGDCommonCu.h"
#include "SGDGraphCu.h"
#include <algorithm>

#define GRANULARITY 5000
#define BLOCKSIZE 1

#ifndef GALOISGPU_APPS_SGD_CUDA_SGDASYNCEDGECU_H_
#define GALOISGPU_APPS_SGD_CUDA_SGDASYNCEDGECU_H_
#define col_(_t5) col[_t5]

#define index_(i) index[i]
#define index__(i) index[i + 1]
#define _P_DATA1_(i) _P_DATA1[i]
#define _P_DATA1__(i) _P_DATA1[i + 1]
#define __rose_lt(x,y) ((x)<(y)?(x):(y))


#define index_(i) index[i]
#define index__(i) index[i + 1]
///
struct RunStats{
   int round;
   int curr_step;
   float total_time;
   float time_per_diagonal;
   float insp_time;
   RunStats(int r, int s, float t, float t_p_d, float i_t){
      round=r;
      curr_step=s;
      total_time=t;
      time_per_diagonal=t_p_d;
      insp_time=i_t;
   }
   RunStats(){
      round = curr_step=0;
      total_time= time_per_diagonal=insp_time=0.0f;
   }
//   fprintf(stderr, "diag\t%d\t%d\t%6.6g\t%6.6g\t%6.6g\t", round, curr_step,total_time,total_time/(double)(m+ n -1), insp_time);
};
struct StatAccumulator{
   std::vector<RunStats> stats;
   void push_stats(int r, int s, float t, float t_p_d, float i_t){
      RunStats rs(r,s,t,t_p_d, i_t);
      stats.push_back(rs);
//      fprintf(stderr, "diag#\t%d\t%d\t%6.6g\t%6.6g\t%6.6g\t", r, s,t,t_p_d, i_t);
   }
   void print(){
      RunStats sum;
      for(int i=0;i< stats.size(); ++i){
         RunStats &s=stats[i];
         sum.round+=s.round;
         sum.curr_step+=s.curr_step;
         sum.total_time+=s.total_time;
         sum.time_per_diagonal+=s.time_per_diagonal;
         sum.insp_time+=s.insp_time;
      }
      size_t num_items = stats.size();
      fprintf(stderr, "AVG,%6.6g,%6.6g,%6.6g,%6.6g,%6.6g\n", (float)sum.round/num_items,
            (float)sum.curr_step/num_items,
            sum.total_time/num_items,
            sum.time_per_diagonal/num_items,
            sum.insp_time/num_items);
   }
};
///
struct gen_a_list 
{
	int col_;
	float ratings[1];
	struct gen_a_list *next;
}
;

struct gen_mk 
{
	struct gen_a_list *ptr;
}
;

bool out_degree_compare(std::pair<int, int> i, std::pair<int, int> j) {
	return (i.second > j.second);
}


__device__ void segreduce_warp2(float *y,float *val)
{
	int tx = threadIdx.x;
	float left=0;

	if(tx >= 1) { left = val[tx -   1]; val[tx] += left; left = 0;}
	__syncthreads();

	if(tx >= 2) { left = val[tx -  2]; val[tx] += left; left = 0;}
	__syncthreads();
	if(tx >= 4) { left = val[tx -  4];  val[tx] += left; left = 0;}
	__syncthreads();
	if(tx >= 8) { left = val[tx -  8];  val[tx] += left; left = 0;}
	__syncthreads();

	if (tx == SGD_FEATURE_SIZE - 1)
		*y += val[tx];
	__syncthreads();
}


__global__ void sgd_diag_operator(float *fv,int *metadata, float *new_ratings,int *_P_DATA2, int *_P_DATA1, float step_size,int t2)
{
	int bx;
	bx = blockIdx.x;
	int tx;
	tx = threadIdx.x;
	int ty;
	ty = threadIdx.y;
	__device__ __shared__ float _P2[BLOCKSIZE];
	__device__ __shared__ float _P3[BLOCKSIZE * SGD_FEATURE_SIZE];
	int newVariable1;
	int t4;
	int t6;
	int t8;
	if (ty <= _P_DATA1__(t2) - _P_DATA1_(t2) - BLOCKSIZE* bx - 1)
		newVariable1 = _P_DATA2[BLOCKSIZE* bx + _P_DATA1_(t2) + ty];
	if (ty <= _P_DATA1__(t2) - _P_DATA1_(t2) - BLOCKSIZE* bx - 1) {
		_P2[ty] = -new_ratings[(BLOCKSIZE*bx + _P_DATA1_(t2) + ty) * 1];
		_P3[tx + ty * SGD_FEATURE_SIZE] = (fv[newVariable1*SGD_FEATURE_SIZE + tx] * fv[(newVariable1 + (t2 + 1))*SGD_FEATURE_SIZE + tx]);
		segreduce_warp2(&_P2[ty],&_P3[0 + ty * SGD_FEATURE_SIZE]);
		fv[newVariable1*SGD_FEATURE_SIZE + tx] -= (step_size * ((_P2[ty] * fv[(newVariable1 + (t2 + 1))*SGD_FEATURE_SIZE + tx]) + (0.05f * fv[newVariable1*SGD_FEATURE_SIZE + tx])));
		fv[(newVariable1 + (t2 + 1))*SGD_FEATURE_SIZE+tx] -= (step_size * ((_P2[ty] * fv[newVariable1*SGD_FEATURE_SIZE + tx]) + (0.05f * fv[(newVariable1 + (t2 + 1))*SGD_FEATURE_SIZE + tx])));
	}
}




struct Timer {
	double _start;
	double _end;
	Timer() :
		_start(0), _end(0) {
		}
	void clear() {
		_start = _end = 0;
	}
	void start() {
		_start = rtclock();
	}
	void stop() {
		_end = rtclock();
	}
	double get_time_seconds(void) {
		return (_end - _start);
	}

#ifdef _WIN32
	static double rtclock() {
		LARGE_INTEGER tickPerSecond, tick;
		QueryPerformanceFrequency(&tickPerSecond);
		QueryPerformanceCounter(&tick);
		return (tick.QuadPart*1000000/tickPerSecond.QuadPart)*1.0e-6;
		//return (Tp.tv_sec + Tp.tv_usec * 1.0e-6);
	}
#else
	static double rtclock() {
		struct timezone Tzp;
		struct timeval Tp;
		int stat;
		stat = gettimeofday(&Tp, &Tzp);
		if (stat != 0)
			printf("Error return from gettimeofday: %d", stat);
		return (Tp.tv_sec + Tp.tv_usec * 1.0e-6);
	}
#endif
};

/************************************************************************
 ************************************************************************/
//__global__ void
template<typename T>
struct CUDAArray {
	T * device_data;
	T * host_data;
	size_t _size;

	CUDAArray(size_t s) :
		_size(s) {
			host_data = new T[_size];
			device_data = NULL;
			//cudaMalloc(&device_data, sizeof(T) * _size);
		}
	~CUDAArray() {
		delete[] host_data;
		if(device_data != NULL)
			cudaFree(device_data);
	}
	void copy_to_device() {
		cudaMemcpy(device_data, host_data, sizeof(T) * _size, cudaMemcpyHostToDevice);
	}
	void create_on_device() {
		cudaMalloc(&device_data, sizeof(T) * _size);
	}

	void copy_to_host() {
		cudaMemcpy(host_data, device_data, sizeof(T) * _size, cudaMemcpyDeviceToHost);
	}
	T* host_ptr() {
		return host_data;
	}
	T * device_ptr() {
		return device_data;
	}
	size_t size() {
		return _size;
	}
};

/************************************************************************

 ************************************************************************/
struct SGDAsynEdgeCudaFunctor {
	typedef SGD_LC_LinearArray_Undirected_Graph<unsigned int, unsigned int> GraphTy;
	typedef CUDAArray<int> ArrayType;
	typedef CUDAArray<float> FeatureArrayType;
	////////////////////////////////////////////////////////////
	/************************************************
	 *
	 *************************************************/
	GraphTy graph;
	std::vector<int> movies;
	std::map<int, int> old_pos_to_new_pos;
	std::vector<std::pair<int, int> > sorted_nodes; //1st field is the positionof the node, second field is the out_degree

	std::vector<int> user_indices;
	ArrayType * metadata;

	//for CSR-DIA
	ArrayType *index;
	ArrayType *diag_number;
	ArrayType *new_index;
	ArrayType *col;
	ArrayType *new_col;
	FeatureArrayType *new_ratings;
	StatAccumulator stats;

	//to track diagonal arrays
	int count_of_diagonals;
	FeatureArrayType * features;
	//   ArrayType * edge_info;
	FeatureArrayType * ratings;
	float accumulated_error;
	int round;
	unsigned int max_rating;
	char filename[512];
	std::vector<int> user_edge_count;
	/************************************************************************
	 *
	 *metadata (16)
	 *edge_info, worklist, ratings (2+1+1)*NE
	 *locks, features*FEATURE_SIZE, (1+FEATURE_SIZE)NN
	 ************************************************************************/
	SGDAsynEdgeCudaFunctor(bool road, const char * p_filename) :
		round(0) {
			strcpy(filename, p_filename);
			fprintf(stderr, "Creating SGDAsynEdgeCudaFunctor -  features =[%d].\n", SGD_FEATURE_SIZE);
			graph.read(p_filename);
			allocate();
			initialize();
			fprintf(stderr, "Number of movies found :: %ld\n", movies.size());
		}
	/************************************************************************
	 *
	 ************************************************************************/
	SGDAsynEdgeCudaFunctor(int num_m, int num_u) :
		round(0) {
			strcpy(filename, "generated-input");
			fprintf(stderr, "Creating SGDAsynEdgeFunctor -  features =[%d] .\n", SGD_FEATURE_SIZE);
			complete_bipartitie(graph, num_m, num_u);
			allocate();
			initialize();
			fprintf(stderr, "Number of movies found :: %ld\n", movies.size());
		}
	/************************************************************************
	 *
	 ************************************************************************/
	SGDAsynEdgeCudaFunctor(int num_m) :
		round(0) {
			strcpy(filename, "gen-diagonal-input");
			fprintf(stderr, "Creating SGDAsynEdgeFunctor -  features =[%d] .\n", SGD_FEATURE_SIZE);
			diagonal_graph(graph, num_m);
			allocate();
			initialize();
			fprintf(stderr, "Number of movies found :: %ld\n", movies.size());
		}
	/************************************************************************
	 *
	 ************************************************************************/
	void allocate() {
		features = new FeatureArrayType(graph.num_nodes() * SGD_FEATURE_SIZE);
		features->create_on_device();
		ratings = new FeatureArrayType(graph.num_edges());
		new_ratings = new FeatureArrayType(graph.num_edges());
		new_ratings->create_on_device();
		metadata = new ArrayType(16);
		metadata->create_on_device();
		index = new ArrayType(graph.num_nodes() + 1);
		col = new ArrayType(graph.num_edges());
		new_col = new ArrayType(graph.num_edges());
		new_col->create_on_device();
	}
	/************************************************************************
	 *
	 ************************************************************************/
	void deallocate() {
		delete features;
		delete ratings;
		delete metadata;
		delete index;
		delete new_ratings;
		delete new_index;
		delete new_col;
	}
	/************************************************************************
	 *
	 ************************************************************************/
	void copy_to_device() {
		features->copy_to_device();
	}
	/************************************************************************
	 *
	 ************************************************************************/
	void copy_to_host() {
		features->copy_to_host();
	}
	/************************************************************************
	 *
	 ************************************************************************/
	void initialize() {
		{
			int deviceCount;
			cudaGetDeviceCount(&deviceCount);
			int device;
			for (device = 0; device < deviceCount; ++device) {
				cudaDeviceProp deviceProp;
				cudaGetDeviceProperties(&deviceProp, device);
				fprintf(stderr, "Device %s (%d) : CC %d.%d, MaxThreads:%d \n", deviceProp.name, device, deviceProp.major, deviceProp.minor, deviceProp.maxThreadsPerBlock);
			}

		}
		std::vector<int> all_edges;
		initialize_features_random(graph, features, movies);
		movies.clear();
		unsigned int max_degree = 0, max_degree_id = 0;
		for (unsigned int i = 0; i < graph.num_nodes(); ++i) {
			sorted_nodes.push_back(std::pair<int, int>(i, graph.num_neighbors(i)));
			if (graph.num_neighbors(i) > max_degree) {
				max_degree = graph.num_neighbors(i);
				max_degree_id = i;
			}
			if (graph.num_neighbors(i) > 0) {
				movies.push_back(i);
			} else {
				user_indices.push_back(i);
			}
		}
		std::sort(sorted_nodes.begin(), sorted_nodes.end(), out_degree_compare);
		max_rating = 0;
		for (unsigned int i = 0; i < graph.num_edges(); ++i) {
			max_rating = std::max(max_rating, graph.out_edge_data()[i]);
		}
		fprintf(stderr, "] , max_Rating: %d, movies: %ld, Max degree:: %d for node: %d\n", max_rating, movies.size(), max_degree, max_degree_id);
		distribute_chunks(all_edges);
		cache_chunks(all_edges);
	}
	/************************************************************************
	 *
	 ************************************************************************/
	void cache_chunks(std::vector<int> &all_edges) {
		index->host_ptr()[0] = 0;
		int count = 0;
		int user_count = 	movies.size();
		for (int i = 0; i < sorted_nodes.size(); i++) {
			//fprintf(stderr, "node %d\n", i);
			for (int j = 0; j < sorted_nodes[i].second; j++) {
				int old_pos = graph.out_neighbors(sorted_nodes[i].first, j);
				if(old_pos_to_new_pos.find(old_pos) != old_pos_to_new_pos.end())
					col->host_ptr()[count] = old_pos_to_new_pos.find(old_pos)->second;
				else{
					col->host_ptr()[count] = user_count;
					old_pos_to_new_pos.insert(std::pair<int,int>(old_pos,user_count));
					user_count++;
				}
				ratings->host_ptr()[count++] = graph.out_edge_data(sorted_nodes[i].first, j);      //(float)max_rating;
				//old_pos_to_new_pos[old_pos] = i;
			}
			index->host_ptr()[i + 1] = count;
			//fprintf(stderr, "node %d\n", i);
		}
		graph.outgoing_index()[0] = index->host_ptr()[0];
		for (int i = 0; i < sorted_nodes.size(); i++) {
			graph.outgoing_index()[i + 1] = index->host_ptr()[i + 1];
			for (int j = index->host_ptr()[i]; j < index->host_ptr()[i + 1]; j++) {
				//col->host_ptr()[j] = col->host_ptr()[j];
				graph.out_neighbors(i, j - index->host_ptr()[i]) = col->host_ptr()[j];
				graph.out_edge_data(i, j - index->host_ptr()[i]) = ratings->host_ptr()[j];
				ratings->host_ptr()[j] /= (float) max_rating;
			}
		}
	}
	/************************************************************************
	 *
	 ************************************************************************/
	void distribute_chunks(std::vector<int> &all_edges) {
		std::vector<int> in_edge_wl(graph.num_edges());
		for (size_t i = 0; i < graph.num_edges(); ++i) {
			in_edge_wl[i] = i;
		}
		size_t num_edges_to_process = in_edge_wl.size();
		int num_items = graph.num_edges();
		all_edges.resize(num_items);
		memcpy(all_edges.data(), in_edge_wl.data(), num_items * sizeof(int));

	}
	/************************************************************************
	 *
	 ************************************************************************/
	void operator()(int num_steps) {
		copy_to_device();
		compute_err(graph, features, max_rating);
		for (round = 0; round < num_steps; ++round) {
			//         edge_info->copy_to_device();
			this->gpu_operator();
			copy_to_host();
			compute_err(graph, features, max_rating);
		}
		stats.print();
	}
	/************************************************************************
	 *
	 ************************************************************************/
	void gpu_operator() {
		int curr_step = 0;
		metadata->host_ptr()[4] = graph.num_edges();
		double total_time = 0;
		double insp_time=0;
		metadata->host_ptr()[2] = movies.size();
		metadata->host_ptr()[4] = 0;
		metadata->host_ptr()[0] = user_indices.size();
		metadata->copy_to_device();

		const float step_size = SGD_STEP_SIZE(round);
		dim3  block_size = dim3(SGD_FEATURE_SIZE,BLOCKSIZE);      
		int num_blocks = ceil(movies.size() / (float)BLOCKSIZE);
		int n = movies.size();
		int m = user_indices.size();
		cudaError_t err;

		Timer timer, timer2;
		int iter;
		int num_items = graph.num_edges();
		int iter2 = 0;
		timer2.start();


		new_index = new ArrayType(movies.size() + user_indices.size());
		new_index->create_on_device();

		diag_inspector(movies.size(), user_indices.size(), index->host_ptr(), ratings->host_ptr(), col->host_ptr(), iter2, count_of_diagonals);


		timer2.stop();
		insp_time += timer2.get_time_seconds();
		new_col->copy_to_device();
		new_ratings->copy_to_device();
		new_index->copy_to_device();
		//if(user_indices.size() > movies.size()){
			timer.start();
			for(iter=0; iter < m + n - 1; iter++){
				num_blocks = (new_index->host_ptr()[iter+1] - new_index->host_ptr()[iter])%(BLOCKSIZE)== 0 ? \
					     (new_index->host_ptr()[iter+1] - new_index->host_ptr()[iter])/(BLOCKSIZE) :\
					     (new_index->host_ptr()[iter+1] - new_index->host_ptr()[iter])/(BLOCKSIZE) + 1 ;

				const int work_items = new_index->host_ptr()[iter+1] - new_index->host_ptr()[iter];


				if(work_items > 0)

					sgd_diag_operator<<< num_blocks, block_size>>>(
							features->device_ptr(),
							metadata->device_ptr(),
							new_ratings->device_ptr(),
							new_col->device_ptr(),new_index->device_ptr(),step_size, iter);
			}
			cudaDeviceSynchronize();
			timer.stop();
		//}
		total_time += timer.get_time_seconds();
		if ((err = cudaGetLastError()) != cudaSuccess) {
			fprintf(stderr, "aborted %s \n", cudaGetErrorString(err));
			exit(-1);
		}
		metadata->copy_to_host();
		fprintf(stderr, "diag\t%d\t%d\t%6.6g\t%6.6g\t%6.6g\t", round, curr_step,total_time,total_time/(double)(m+ n -1), insp_time);
		stats.push_stats(round, curr_step, total_time, total_time/(double)(m+n-1), insp_time);
	}

	void diag_inspector(int n,int m, int *index,float *ratings,int *col, int iter, int count_of_diagonals){
		int t6;
		int t4;
		int t2;
		int newVariable0;
		int In_1;
		struct gen_a_list *_P_DATA4;
		struct gen_mk *_P_DATA3;
		float *_new_ratings;
		struct gen_a_list **_P1;
		int *_P_DATA2;
		int chill_count_1;
		int *_P_DATA1;
		int chill_count_0;
		int _t11;
		int _t10;
		int _t9;
		int _t8;
		int _t7;
		int _t6;
		int _t4;
		int _t5;
		int l;
		int _t3;
		int _t2;
		int _t1;
		int i;
		int j;
		int k;
		_P_DATA1 = (int *)malloc(sizeof(int ) * (m+n));
		_P1 = ((struct gen_a_list **)(malloc(sizeof(struct gen_a_list *) * (m+n)))) ; 

		chill_count_1 = 0;
		for (_t8 = 0; _t8 <= m+ n - 1; _t8 += 1) {
			_P1[1 * _t8] = NULL;
			_P_DATA1[1 * _t8] = 0;
		}
		for (t2 = 0; t2 <= n-1; t2 += 1) 
			for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
				_t8 = col_(t4) - t2  -1;


				_P_DATA4 = (struct gen_a_list *)malloc(sizeof(struct gen_a_list ) * 1);
				_P_DATA4-> ratings[0] = 0;
				_P_DATA4 -> col_ = t2;   
				_P_DATA4 -> next = _P1[_t8];

				_P1[_t8] = _P_DATA4;

				chill_count_1 += 1;
				_P_DATA1[_t8+1] += 1;
				_P1[_t8] -> ratings[0] = ratings[t4];
			}
		_P_DATA2 = ((int *)(malloc(sizeof(int ) * chill_count_1)));

		_new_ratings = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 1))));

		for (t2 = 0; t2 <= m +n - 2; t2 += 1) {

			for (newVariable0 = 1 - _P_DATA1[1 * t2 + 1]; newVariable0 <= 0; newVariable0 += 1) {

				_P_DATA2[_P_DATA1[1 * t2] - newVariable0] = _P1[t2] -> col_;
				_new_ratings[1 * (_P_DATA1[1 * t2] - newVariable0)] = _P1[t2] -> ratings[0];
				_P_DATA4 = _P1[t2] -> next;
				free(_P1[t2]);
				_P1[t2] = _P_DATA4;
			}
			_P_DATA1[1 * t2 + 1] += _P_DATA1[1 * t2];


		}    

		memcpy(new_ratings->host_ptr(), _new_ratings, sizeof(float ) * (chill_count_1 * 1) );
		memcpy(new_col->host_ptr(), _P_DATA2, sizeof(int ) * (chill_count_1 * 1) );
		memcpy(new_index->host_ptr(), _P_DATA1, sizeof(int ) * (m+ n) );

		free(_P_DATA1);
		free(_P_DATA2);
		free(_new_ratings);
	}

	/************************************************************************
	 *
	 ************************************************************************/
	~SGDAsynEdgeCudaFunctor() {
		deallocate();
		fprintf(stderr, "Destroying SGDAsynEdgeCudaFunctor object.\n");
	}
};
//###################################################################//

#endif /* GALOISGPU_APPS_SGD_CUDA_SGDASYNCEDGECU_H_ */
